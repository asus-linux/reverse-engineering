/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembly of msdm.dat, Sat Nov 11 14:02:56 2023
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000   4]                    Signature : "MSDM"    [Microsoft Data Management Table]
[004h 0004   4]                 Table Length : 00000055
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : BF
[00Ah 0010   6]                       Oem ID : "ALASKA"
[010h 0016   8]                 Oem Table ID : "A M I "
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "ASUS"
[020h 0032   4]        Asl Compiler Revision : 00000001

[024h 0036  49] Software Licensing Structure : 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 034h 0052  16 */                            00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 044h 0068  16 */                            00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 054h 0084   1 */                            00                                              /* . */\

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 BF 41 4C 41 53 4B 41  // MSDMU.....ALASKA
    0010: 41 20 4D 20 49 20 00 00 09 20 07 01 41 53 55 53  // A M I ... ..ASUS
    0020: 01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 59 4E 34 4B 48 2D 37 43  // ........YN4KH-7C
    0040: 4A 37 51 2D 38 33 57 54 37 2D 42 36 52 38 37 2D  // J7Q-83WT7-B6R87-
    0050: 58 37 37 43 4A                                   // X77CJ
