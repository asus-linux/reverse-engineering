/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20230628 (64-bit version)
 * Copyright (c) 2000 - 2023 Intel Corporation
 * 
 * Disassembly of fidt.dat
 *
 * ACPI Data Table [FIDT]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000 004h]                   Signature : "FIDT"    
[004h 0004 004h]                Table Length : 0000009C
[008h 0008 001h]                    Revision : 01
[009h 0009 001h]                    Checksum : 5B
[00Ah 0010 006h]                      Oem ID : "_ASUS_"
[010h 0016 008h]                Oem Table ID : "Notebook"
[018h 0024 004h]                Oem Revision : 01072009
[01Ch 0028 004h]             Asl Compiler ID : "AMI "
[020h 0032 004h]       Asl Compiler Revision : 00010013


**** Unknown ACPI table signature [FIDT]


Raw Table Data: Length 156 (0x9C)

    0000: 46 49 44 54 9C 00 00 00 01 5B 5F 41 53 55 53 5F  // FIDT.....[_ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 4D 49 20  // Notebook. ..AMI 
    0020: 13 00 01 00 24 46 49 44 04 78 00 31 41 5A 49 50  // ....$FID.x.1AZIP
    0030: 33 32 30 00 3D 23 BD 51 B5 EB 58 18 FD F1 5C 8D  // 320.=#.Q..X...\.
    0040: 32 75 BA 5D 30 35 00 32 39 00 30 33 00 32 30 00  // 2u.]05.29.03.20.
    0050: E8 07 06 05 0C 08 0E FF FF 5F 41 53 55 53 5F 4E  // ........._ASUS_N
    0060: 6F 74 65 62 6F 6F 6B 31 00 00 00 FF FF FF FF FF  // otebook1........
    0070: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0080: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0090: FF FF FF FF FF FF FF FF FF FF FF FF              // ............
