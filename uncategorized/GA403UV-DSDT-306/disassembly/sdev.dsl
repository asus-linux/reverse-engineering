/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20230628 (64-bit version)
 * Copyright (c) 2000 - 2023 Intel Corporation
 * 
 * Disassembly of sdev.dat
 *
 * ACPI Data Table [SDEV]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000 004h]                   Signature : "SDEV"    [Secure Devices Table]
[004h 0004 004h]                Table Length : 00000144
[008h 0008 001h]                    Revision : 01
[009h 0009 001h]                    Checksum : E4
[00Ah 0010 006h]                      Oem ID : "AMD"
[010h 0016 008h]                Oem Table ID : "SdevTble"
[018h 0024 004h]                Oem Revision : 00000001
[01Ch 0028 004h]             Asl Compiler ID : "ACPI"
[020h 0032 004h]       Asl Compiler Revision : 00000002


[024h 0036 001h]               Subtable Type : 01 [PCIe Endpoint Device]
[025h 0037 001h]       Flags (decoded below) : 01
                Allow handoff to unsecure OS : 1
            Secure access components present : 0
[026h 0038 002h]                      Length : 00C2

[004h 0004 002h]                     Segment : 0000
[006h 0006 002h]                   Start Bus : 0065
[008h 0008 002h]                 Path Offset : 0010
[00Ah 0010 002h]                 Path Length : 0004
[00Ch 0012 002h]          Vendor Data Offset : 0014
[00Eh 0014 002h]          Vendor Data Length : 00AE
[034h 0052 001h]                      Device : 00
[035h 0053 001h]                    Function : 04
[036h 0054 001h]                      Device : 00
[037h 0055 001h]                    Function : 00
[000h 0000 0AEh]                 Vendor Data : 00 57 00 02 01 01 77 32 51 00 03 00 02 0E 03 00 /* .W....w2Q....... */\
/* 010h 0016  16 */                            34 00 23 00 0D 0E 61 CE 6B 56 8B 7D B0 E2 27 A9 /* 4.#...a.kV.}..'. */\
/* 020h 0032  16 */                            9A 63 89 27 52 CA 41 09 17 FC 09 3C 78 4C 4B C0 /* .c.'R.A....<xLK. */\
/* 030h 0048  16 */                            3F 7C D0 84 5C 5F 53 42 2E 50 43 49 30 2E 47 50 /* ?|..\_SB.PCI0.GP */\
/* 040h 0064  16 */                            31 37 2E 58 48 43 31 2E 52 48 55 42 2E 50 52 54 /* 17.XHC1.RHUB.PRT */\
/* 050h 0080  16 */                            31 2E 57 43 41 4D 00 00 57 00 02 01 01 77 32 51 /* 1.WCAM..W....w2Q */\
/* 060h 0096  16 */                            00 03 00 00 0E 03 00 34 00 23 00 0D 0E 61 CE 6B /* .......4.#...a.k */\
/* 070h 0112  16 */                            56 8B 7D B0 E2 27 A9 9A 63 89 27 52 CA 41 09 17 /* V.}..'..c.'R.A.. */\
/* 080h 0128  16 */                            FC 09 3C 78 4C 4B C0 3F 7C D0 84 5C 5F 53 42 2E /* ..<xLK.?|..\_SB. */\
/* 090h 0144  16 */                            50 43 49 30 2E 47 50 31 37 2E 58 48 43 31 2E 52 /* PCI0.GP17.XHC1.R */\
/* 0A0h 0160  14 */                            48 55 42 2E 50 52 54 31 2E 43 41 4D 32 00       /* HUB.PRT1.CAM2. */\

[0E6h 0230 001h]               Subtable Type : 00 [Namespace Device]
[0E7h 0231 001h]       Flags (decoded below) : 01
                Allow handoff to unsecure OS : 1
            Secure access components present : 0
[0E8h 0232 002h]                      Length : 002F

[004h 0004 002h]            Device ID Offset : 000C
[006h 0006 002h]            Device ID Length : 0023
[008h 0008 002h]          Vendor Data Offset : 000C
[00Ah 0010 002h]          Vendor Data Length : 0000
[00Ch 0012 023h]                    Namepath : "\_SB.PCI0.GP17.XHC1.RHUB.PRT1.WCAM"

[115h 0277 001h]               Subtable Type : 00 [Namespace Device]
[116h 0278 001h]       Flags (decoded below) : 01
                Allow handoff to unsecure OS : 1
            Secure access components present : 0
[117h 0279 002h]                      Length : 002F

[004h 0004 002h]            Device ID Offset : 000C
[006h 0006 002h]            Device ID Length : 0023
[008h 0008 002h]          Vendor Data Offset : 000C
[00Ah 0010 002h]          Vendor Data Length : 0000
[00Ch 0012 023h]                    Namepath : "\_SB.PCI0.GP17.XHC1.RHUB.PRT1.CAM2"

Raw Table Data: Length 324 (0x144)

    0000: 53 44 45 56 44 01 00 00 01 E4 41 4D 44 00 00 00  // SDEVD.....AMD...
    0010: 53 64 65 76 54 62 6C 65 01 00 00 00 41 43 50 49  // SdevTble....ACPI
    0020: 02 00 00 00 01 01 C2 00 00 00 65 00 10 00 04 00  // ..........e.....
    0030: 14 00 AE 00 00 04 00 00 00 57 00 02 01 01 77 32  // .........W....w2
    0040: 51 00 03 00 02 0E 03 00 34 00 23 00 0D 0E 61 CE  // Q.......4.#...a.
    0050: 6B 56 8B 7D B0 E2 27 A9 9A 63 89 27 52 CA 41 09  // kV.}..'..c.'R.A.
    0060: 17 FC 09 3C 78 4C 4B C0 3F 7C D0 84 5C 5F 53 42  // ...<xLK.?|..\_SB
    0070: 2E 50 43 49 30 2E 47 50 31 37 2E 58 48 43 31 2E  // .PCI0.GP17.XHC1.
    0080: 52 48 55 42 2E 50 52 54 31 2E 57 43 41 4D 00 00  // RHUB.PRT1.WCAM..
    0090: 57 00 02 01 01 77 32 51 00 03 00 00 0E 03 00 34  // W....w2Q.......4
    00A0: 00 23 00 0D 0E 61 CE 6B 56 8B 7D B0 E2 27 A9 9A  // .#...a.kV.}..'..
    00B0: 63 89 27 52 CA 41 09 17 FC 09 3C 78 4C 4B C0 3F  // c.'R.A....<xLK.?
    00C0: 7C D0 84 5C 5F 53 42 2E 50 43 49 30 2E 47 50 31  // |..\_SB.PCI0.GP1
    00D0: 37 2E 58 48 43 31 2E 52 48 55 42 2E 50 52 54 31  // 7.XHC1.RHUB.PRT1
    00E0: 2E 43 41 4D 32 00 00 01 2F 00 0C 00 23 00 0C 00  // .CAM2.../...#...
    00F0: 00 00 5C 5F 53 42 2E 50 43 49 30 2E 47 50 31 37  // ..\_SB.PCI0.GP17
    0100: 2E 58 48 43 31 2E 52 48 55 42 2E 50 52 54 31 2E  // .XHC1.RHUB.PRT1.
    0110: 57 43 41 4D 00 00 01 2F 00 0C 00 23 00 0C 00 00  // WCAM.../...#....
    0120: 00 5C 5F 53 42 2E 50 43 49 30 2E 47 50 31 37 2E  // .\_SB.PCI0.GP17.
    0130: 58 48 43 31 2E 52 48 55 42 2E 50 52 54 31 2E 43  // XHC1.RHUB.PRT1.C
    0140: 41 4D 32 00                                      // AM2.
