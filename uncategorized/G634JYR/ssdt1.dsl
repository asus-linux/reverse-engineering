/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt1.dat, Thu Mar  7 17:18:08 2024
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00002EA6 (11942)
 *     Revision         0x02
 *     Checksum         0x3A
 *     OEM ID           "DptfTb"
 *     OEM Table ID     "DptfTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "DptfTb", "DptfTabl", 0x00001000)
{
    External (_SB_.AAC0, FieldUnitObj)
    External (_SB_.ACRT, FieldUnitObj)
    External (_SB_.APSV, FieldUnitObj)
    External (_SB_.CBMI, FieldUnitObj)
    External (_SB_.CFGD, FieldUnitObj)
    External (_SB_.CLVL, FieldUnitObj)
    External (_SB_.CPID, UnknownObj)
    External (_SB_.CPPC, FieldUnitObj)
    External (_SB_.CTC0, FieldUnitObj)
    External (_SB_.CTC1, FieldUnitObj)
    External (_SB_.CTC2, FieldUnitObj)
    External (_SB_.OSCP, IntObj)
    External (_SB_.PAGD, DeviceObj)
    External (_SB_.PAGD._PUR, PkgObj)
    External (_SB_.PAGD._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.LPCB, DeviceObj)
    External (_SB_.PC00.LPCB.BATM, FieldUnitObj)
    External (_SB_.PC00.LPCB.CLOT, FieldUnitObj)
    External (_SB_.PC00.LPCB.CTMP, FieldUnitObj)
    External (_SB_.PC00.LPCB.ECOK, IntObj)
    External (_SB_.PC00.LPCB.M662, FieldUnitObj)
    External (_SB_.PC00.LPCB.MUT0, MutexObj)
    External (_SB_.PC00.LPCB.Q4HI, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4HU, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LO, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LU, FieldUnitObj)
    External (_SB_.PC00.LPCB.SEN2, DeviceObj)
    External (_SB_.PC00.LPCB.SEN3, DeviceObj)
    External (_SB_.PC00.LPCB.VRTT, UnknownObj)
    External (_SB_.PC00.MC__.MHBR, FieldUnitObj)
    External (_SB_.PC00.TCPU, DeviceObj)
    External (_SB_.PL10, FieldUnitObj)
    External (_SB_.PL11, FieldUnitObj)
    External (_SB_.PL12, FieldUnitObj)
    External (_SB_.PL20, FieldUnitObj)
    External (_SB_.PL21, FieldUnitObj)
    External (_SB_.PL22, FieldUnitObj)
    External (_SB_.PLW0, FieldUnitObj)
    External (_SB_.PLW1, FieldUnitObj)
    External (_SB_.PLW2, FieldUnitObj)
    External (_SB_.PR00, ProcessorObj)
    External (_SB_.PR00._PSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TPC, IntObj)
    External (_SB_.PR00._TSD, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00.LPSS, PkgObj)
    External (_SB_.PR00.TPSS, PkgObj)
    External (_SB_.PR00.TSMC, PkgObj)
    External (_SB_.PR00.TSMF, PkgObj)
    External (_SB_.PR01, ProcessorObj)
    External (_SB_.PR02, ProcessorObj)
    External (_SB_.PR03, ProcessorObj)
    External (_SB_.PR04, ProcessorObj)
    External (_SB_.PR05, ProcessorObj)
    External (_SB_.PR06, ProcessorObj)
    External (_SB_.PR07, ProcessorObj)
    External (_SB_.PR08, ProcessorObj)
    External (_SB_.PR09, ProcessorObj)
    External (_SB_.PR10, ProcessorObj)
    External (_SB_.PR11, ProcessorObj)
    External (_SB_.PR12, ProcessorObj)
    External (_SB_.PR13, ProcessorObj)
    External (_SB_.PR14, ProcessorObj)
    External (_SB_.PR15, ProcessorObj)
    External (_SB_.PR16, ProcessorObj)
    External (_SB_.PR17, ProcessorObj)
    External (_SB_.PR18, ProcessorObj)
    External (_SB_.PR19, ProcessorObj)
    External (_SB_.PR20, ProcessorObj)
    External (_SB_.PR21, ProcessorObj)
    External (_SB_.PR22, ProcessorObj)
    External (_SB_.PR23, ProcessorObj)
    External (_SB_.PR24, ProcessorObj)
    External (_SB_.PR25, ProcessorObj)
    External (_SB_.PR26, ProcessorObj)
    External (_SB_.PR27, ProcessorObj)
    External (_SB_.PR28, ProcessorObj)
    External (_SB_.PR29, ProcessorObj)
    External (_SB_.PR30, ProcessorObj)
    External (_SB_.PR31, ProcessorObj)
    External (_SB_.SLPB, DeviceObj)
    External (_SB_.TAR0, FieldUnitObj)
    External (_SB_.TAR1, FieldUnitObj)
    External (_SB_.TAR2, FieldUnitObj)
    External (_TZ_.ETMD, IntObj)
    External (_TZ_.THRM, ThermalZoneObj)
    External (ACTT, IntObj)
    External (ATPC, IntObj)
    External (BATR, IntObj)
    External (CHGE, IntObj)
    External (CRTT, IntObj)
    External (DCFE, IntObj)
    External (DPTF, IntObj)
    External (ECON, IntObj)
    External (FND1, IntObj)
    External (FND2, IntObj)
    External (FND3, IntObj)
    External (HIDW, MethodObj)    // 4 Arguments
    External (HIWC, MethodObj)    // 1 Arguments
    External (IN34, IntObj)
    External (ODV0, IntObj)
    External (ODV1, IntObj)
    External (ODV2, IntObj)
    External (ODV3, IntObj)
    External (ODV4, IntObj)
    External (ODV5, IntObj)
    External (PCHE, FieldUnitObj)
    External (PF00, IntObj)
    External (PLID, IntObj)
    External (PNHM, IntObj)
    External (PPPR, IntObj)
    External (PPSZ, IntObj)
    External (PSVT, IntObj)
    External (PTPC, IntObj)
    External (PWRE, IntObj)
    External (PWRS, IntObj)
    External (S1DE, IntObj)
    External (S2DE, IntObj)
    External (S3DE, IntObj)
    External (S4DE, IntObj)
    External (S5DE, IntObj)
    External (S6DE, IntObj)
    External (S6P2, IntObj)
    External (SADE, IntObj)
    External (SSP1, IntObj)
    External (SSP2, IntObj)
    External (SSP3, IntObj)
    External (SSP4, IntObj)
    External (SSP5, IntObj)
    External (TCNT, IntObj)
    External (TSOD, IntObj)

    Scope (\_SB)
    {
        Device (IETM)
        {
            Method (GCID, 0, Serialized)
            {
                Switch ((\_SB.CPID & 0x0FFF0FF0))
                {
                    Case (0x000B0670)
                    {
                        Return (Zero)
                    }
                    Case (0x000B06A0)
                    {
                        Return (Zero)
                    }
                    Case (0x000B06F0)
                    {
                        Return (Zero)
                    }
                    Default
                    {
                        Return (One)
                    }

                }
            }

            Method (GHID, 1, Serialized)
            {
                Local0 = \_SB.IETM.GCID ()
                If ((Zero == Local0))
                {
                    If ((Arg0 == "IETM"))
                    {
                        Return ("INTC10A0")
                    }

                    If ((Arg0 == "SEN1"))
                    {
                        Return ("INTC10A1")
                    }

                    If ((Arg0 == "SEN2"))
                    {
                        Return ("INTC10A1")
                    }

                    If ((Arg0 == "SEN3"))
                    {
                        Return ("INTC10A1")
                    }

                    If ((Arg0 == "SEN4"))
                    {
                        Return ("INTC10A1")
                    }

                    If ((Arg0 == "SEN5"))
                    {
                        Return ("INTC10A1")
                    }

                    If ((Arg0 == "TPCH"))
                    {
                        Return ("INTC10A3")
                    }

                    If ((Arg0 == "TFN1"))
                    {
                        Return ("INTC10A2")
                    }

                    If ((Arg0 == "TFN2"))
                    {
                        Return ("INTC10A2")
                    }

                    If ((Arg0 == "TFN3"))
                    {
                        Return ("INTC10A2")
                    }

                    If ((Arg0 == "TPWR"))
                    {
                        Return ("INTC10A4")
                    }

                    If ((Arg0 == "BAT1"))
                    {
                        Return ("INTC10A5")
                    }

                    If ((Arg0 == "CHRG"))
                    {
                        Return ("INTC10A1")
                    }

                    Return ("XXXX9999")
                }
                Else
                {
                    If ((Arg0 == "IETM"))
                    {
                        Return ("INTC1041")
                    }

                    If ((Arg0 == "SEN1"))
                    {
                        Return ("INTC1046")
                    }

                    If ((Arg0 == "SEN2"))
                    {
                        Return ("INTC1046")
                    }

                    If ((Arg0 == "SEN3"))
                    {
                        Return ("INTC1046")
                    }

                    If ((Arg0 == "SEN4"))
                    {
                        Return ("INTC1046")
                    }

                    If ((Arg0 == "SEN5"))
                    {
                        Return ("INTC1046")
                    }

                    If ((Arg0 == "TPCH"))
                    {
                        Return ("INTC1049")
                    }

                    If ((Arg0 == "TFN1"))
                    {
                        Return ("INTC1048")
                    }

                    If ((Arg0 == "TFN2"))
                    {
                        Return ("INTC1048")
                    }

                    If ((Arg0 == "TFN3"))
                    {
                        Return ("INTC1048")
                    }

                    If ((Arg0 == "TPWR"))
                    {
                        Return ("INTC1060")
                    }

                    If ((Arg0 == "BAT1"))
                    {
                        Return ("INTC1061")
                    }

                    If ((Arg0 == "CHRG"))
                    {
                        Return ("INTC1046")
                    }

                    Return ("XXXX9999")
                }
            }

            Name (_UID, "IETM")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If (CondRefOf (HIWC))
                {
                    If (HIWC (Arg0))
                    {
                        If (CondRefOf (HIDW))
                        {
                            Return (HIDW (Arg0, Arg1, Arg2, Arg3))
                        }
                    }
                }

                Return (Buffer (One)
                {
                     0x00                                             // .
                })
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If (((\DPTF == One) && (\IN34 == One)))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Name (PTRP, Zero)
            Name (PSEM, Zero)
            Name (ATRP, Zero)
            Name (ASEM, Zero)
            Name (YTRP, Zero)
            Name (YSEM, Zero)
            Method (_OSC, 4, Serialized)  // _OSC: Operating System Capabilities
            {
                CreateDWordField (Arg3, Zero, STS1)
                CreateDWordField (Arg3, 0x04, CAP1)
                If ((Arg1 != One))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x0A
                    Return (Arg3)
                }

                If ((Arg2 != 0x02))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x02
                    Return (Arg3)
                }

                If (CondRefOf (\_SB.APSV))
                {
                    If ((PSEM == Zero))
                    {
                        PSEM = One
                        PTRP = \_SB.APSV /* External reference */
                    }
                }

                If (CondRefOf (\_SB.AAC0))
                {
                    If ((ASEM == Zero))
                    {
                        ASEM = One
                        ATRP = \_SB.AAC0 /* External reference */
                    }
                }

                If (CondRefOf (\_SB.ACRT))
                {
                    If ((YSEM == Zero))
                    {
                        YSEM = One
                        YTRP = \_SB.ACRT /* External reference */
                    }
                }

                If ((Arg0 == ToUUID ("b23ba85d-c8b7-3542-88de-8de2ffcfd698") /* Unknown UUID */))
                {
                    If (~(STS1 & One))
                    {
                        If ((CAP1 & One))
                        {
                            If ((CAP1 & 0x02))
                            {
                                \_SB.AAC0 = 0x6E
                                \_TZ.ETMD = Zero
                            }
                            Else
                            {
                                \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                                \_TZ.ETMD = One
                            }

                            If ((CAP1 & 0x04))
                            {
                                \_SB.APSV = 0x6E
                            }
                            Else
                            {
                                \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            }

                            If ((CAP1 & 0x08))
                            {
                                \_SB.ACRT = 0xD2
                            }
                            Else
                            {
                                \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            }
                        }
                        Else
                        {
                            \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                            \_TZ.ETMD = One
                        }

                        Notify (\_TZ.THRM, 0x81) // Information Change
                    }

                    Return (Arg3)
                }

                Return (Arg3)
            }

            Method (DCFG, 0, NotSerialized)
            {
                Return (\DCFE) /* External reference */
            }

            Name (ODVX, Package (0x06)
            {
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero
            })
            Method (ODVP, 0, Serialized)
            {
                ODVX [Zero] = \ODV0 /* External reference */
                ODVX [One] = \ODV1 /* External reference */
                ODVX [0x02] = \ODV2 /* External reference */
                ODVX [0x03] = \ODV3 /* External reference */
                ODVX [0x04] = \ODV4 /* External reference */
                ODVX [0x05] = \ODV5 /* External reference */
                Return (ODVX) /* \_SB_.IETM.ODVX */
            }

            Scope (\_SB.PC00.TCPU)
            {
                Name (PFLG, Zero)
                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    If ((\SADE == One))
                    {
                        Return (0x0F)
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                OperationRegion (CPWR, SystemMemory, ((\_SB.PC00.MC.MHBR << 0x0F) + 0x5000), 0x1000)
                Field (CPWR, ByteAcc, NoLock, Preserve)
                {
                    Offset (0x930), 
                    PTDP,   15, 
                    Offset (0x932), 
                    PMIN,   15, 
                    Offset (0x934), 
                    PMAX,   15, 
                    Offset (0x936), 
                    TMAX,   7, 
                    Offset (0x938), 
                    PWRU,   4, 
                    Offset (0x939), 
                    EGYU,   5, 
                    Offset (0x93A), 
                    TIMU,   4, 
                    Offset (0x958), 
                    Offset (0x95C), 
                    LPMS,   1, 
                    CTNL,   2, 
                    Offset (0x978), 
                    PCTP,   8, 
                    Offset (0x998), 
                    RP0C,   8, 
                    RP1C,   8, 
                    RPNC,   8, 
                    Offset (0xF3C), 
                    TRAT,   8, 
                    Offset (0xF40), 
                    PTD1,   15, 
                    Offset (0xF42), 
                    TRA1,   8, 
                    Offset (0xF44), 
                    PMX1,   15, 
                    Offset (0xF46), 
                    PMN1,   15, 
                    Offset (0xF48), 
                    PTD2,   15, 
                    Offset (0xF4A), 
                    TRA2,   8, 
                    Offset (0xF4C), 
                    PMX2,   15, 
                    Offset (0xF4E), 
                    PMN2,   15, 
                    Offset (0xF50), 
                    CTCL,   2, 
                        ,   29, 
                    CLCK,   1, 
                    MNTR,   8
                }

                Name (XPCC, Zero)
                Method (PPCC, 0, Serialized)
                {
                    If (((XPCC == Zero) && CondRefOf (\_SB.CBMI)))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                If (((\_SB.CLVL >= One) && (\_SB.CLVL <= 0x03)))
                                {
                                    CPL0 ()
                                    XPCC = One
                                }
                            }
                            Case (One)
                            {
                                If (((\_SB.CLVL == 0x02) || (\_SB.CLVL == 0x03)))
                                {
                                    CPL1 ()
                                    XPCC = One
                                }
                            }
                            Case (0x02)
                            {
                                If ((\_SB.CLVL == 0x03))
                                {
                                    CPL2 ()
                                    XPCC = One
                                }
                            }

                        }
                    }

                    Return (NPCC) /* \_SB_.PC00.TCPU.NPCC */
                }

                Name (NPCC, Package (0x03)
                {
                    0x02, 
                    Package (0x06)
                    {
                        Zero, 
                        0x88B8, 
                        0xAFC8, 
                        0x6D60, 
                        0x7D00, 
                        0x03E8
                    }, 

                    Package (0x06)
                    {
                        One, 
                        0xDBBA, 
                        0xDBBA, 
                        Zero, 
                        Zero, 
                        0x03E8
                    }
                })
                Method (CPNU, 2, Serialized)
                {
                    Name (CNVT, Zero)
                    Name (PPUU, Zero)
                    Name (RMDR, Zero)
                    If ((PWRU == Zero))
                    {
                        PPUU = One
                    }
                    Else
                    {
                        PPUU = (PWRU-- << 0x02)
                    }

                    Divide (Arg0, PPUU, RMDR, CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    If ((Arg1 == Zero))
                    {
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                    Else
                    {
                        CNVT *= 0x03E8
                        RMDR *= 0x03E8
                        RMDR /= PPUU
                        CNVT += RMDR /* \_SB_.PC00.TCPU.CPNU.RMDR */
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                }

                Method (CPL0, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL10, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW0 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW0 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL1, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL11, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW1 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW1 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL2, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL12, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW2 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW2 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Name (LSTM, Zero)
                Name (_PPC, Zero)  // _PPC: Performance Present Capabilities
                Method (SPPC, 1, Serialized)
                {
                    If (CondRefOf (\_SB.CPPC))
                    {
                        \_SB.CPPC = Arg0
                    }

                    If ((ToInteger (\TCNT) > Zero))
                    {
                        Notify (\_SB.PR00, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > One))
                    {
                        Notify (\_SB.PR01, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x02))
                    {
                        Notify (\_SB.PR02, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x03))
                    {
                        Notify (\_SB.PR03, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x04))
                    {
                        Notify (\_SB.PR04, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x05))
                    {
                        Notify (\_SB.PR05, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x06))
                    {
                        Notify (\_SB.PR06, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x07))
                    {
                        Notify (\_SB.PR07, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x08))
                    {
                        Notify (\_SB.PR08, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x09))
                    {
                        Notify (\_SB.PR09, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0A))
                    {
                        Notify (\_SB.PR10, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0B))
                    {
                        Notify (\_SB.PR11, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0C))
                    {
                        Notify (\_SB.PR12, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0D))
                    {
                        Notify (\_SB.PR13, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0E))
                    {
                        Notify (\_SB.PR14, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0F))
                    {
                        Notify (\_SB.PR15, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x10))
                    {
                        Notify (\_SB.PR16, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x11))
                    {
                        Notify (\_SB.PR17, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x12))
                    {
                        Notify (\_SB.PR18, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x13))
                    {
                        Notify (\_SB.PR19, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x14))
                    {
                        Notify (\_SB.PR20, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x15))
                    {
                        Notify (\_SB.PR21, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x16))
                    {
                        Notify (\_SB.PR22, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x17))
                    {
                        Notify (\_SB.PR23, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x18))
                    {
                        Notify (\_SB.PR24, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x19))
                    {
                        Notify (\_SB.PR25, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1A))
                    {
                        Notify (\_SB.PR26, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1B))
                    {
                        Notify (\_SB.PR27, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1C))
                    {
                        Notify (\_SB.PR28, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1D))
                    {
                        Notify (\_SB.PR29, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1E))
                    {
                        Notify (\_SB.PR30, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1F))
                    {
                        Notify (\_SB.PR31, 0x80) // Status Change
                    }
                }

                Method (SPUR, 1, NotSerialized)
                {
                    If ((Arg0 <= \TCNT))
                    {
                        If ((\_SB.PAGD._STA () == 0x0F))
                        {
                            \_SB.PAGD._PUR [One] = Arg0
                            Notify (\_SB.PAGD, 0x80) // Status Change
                        }
                    }
                }

                Method (PCCC, 0, Serialized)
                {
                    PCCX [Zero] = One
                    Switch (ToInteger (CPNU (PTDP, Zero)))
                    {
                        Case (0x39)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xA7F8
                            DerefOf (PCCX [One]) [One] = 0x00017318
                        }
                        Case (0x2F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x9858
                            DerefOf (PCCX [One]) [One] = 0x00014C08
                        }
                        Case (0x25)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x7148
                            DerefOf (PCCX [One]) [One] = 0xD6D8
                        }
                        Case (0x19)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x3E80
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0B)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x61A8
                        }
                        Default
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xFF
                            DerefOf (PCCX [One]) [One] = 0xFF
                        }

                    }

                    Return (PCCX) /* \_SB_.PC00.TCPU.PCCX */
                }

                Name (PCCX, Package (0x02)
                {
                    0x80000000, 
                    Package (0x02)
                    {
                        0x80000000, 
                        0x80000000
                    }
                })
                Name (KEFF, Package (0x1E)
                {
                    Package (0x02)
                    {
                        0x01BC, 
                        Zero
                    }, 

                    Package (0x02)
                    {
                        0x01CF, 
                        0x27
                    }, 

                    Package (0x02)
                    {
                        0x01E1, 
                        0x4B
                    }, 

                    Package (0x02)
                    {
                        0x01F3, 
                        0x6C
                    }, 

                    Package (0x02)
                    {
                        0x0206, 
                        0x8B
                    }, 

                    Package (0x02)
                    {
                        0x0218, 
                        0xA8
                    }, 

                    Package (0x02)
                    {
                        0x022A, 
                        0xC3
                    }, 

                    Package (0x02)
                    {
                        0x023D, 
                        0xDD
                    }, 

                    Package (0x02)
                    {
                        0x024F, 
                        0xF4
                    }, 

                    Package (0x02)
                    {
                        0x0261, 
                        0x010B
                    }, 

                    Package (0x02)
                    {
                        0x0274, 
                        0x011F
                    }, 

                    Package (0x02)
                    {
                        0x032C, 
                        0x01BD
                    }, 

                    Package (0x02)
                    {
                        0x03D7, 
                        0x0227
                    }, 

                    Package (0x02)
                    {
                        0x048B, 
                        0x026D
                    }, 

                    Package (0x02)
                    {
                        0x053E, 
                        0x02A1
                    }, 

                    Package (0x02)
                    {
                        0x05F7, 
                        0x02C6
                    }, 

                    Package (0x02)
                    {
                        0x06A8, 
                        0x02E6
                    }, 

                    Package (0x02)
                    {
                        0x075D, 
                        0x02FF
                    }, 

                    Package (0x02)
                    {
                        0x0818, 
                        0x0311
                    }, 

                    Package (0x02)
                    {
                        0x08CF, 
                        0x0322
                    }, 

                    Package (0x02)
                    {
                        0x179C, 
                        0x0381
                    }, 

                    Package (0x02)
                    {
                        0x2DDC, 
                        0x039C
                    }, 

                    Package (0x02)
                    {
                        0x44A8, 
                        0x039E
                    }, 

                    Package (0x02)
                    {
                        0x5C35, 
                        0x0397
                    }, 

                    Package (0x02)
                    {
                        0x747D, 
                        0x038D
                    }, 

                    Package (0x02)
                    {
                        0x8D7F, 
                        0x0382
                    }, 

                    Package (0x02)
                    {
                        0xA768, 
                        0x0376
                    }, 

                    Package (0x02)
                    {
                        0xC23B, 
                        0x0369
                    }, 

                    Package (0x02)
                    {
                        0xDE26, 
                        0x035A
                    }, 

                    Package (0x02)
                    {
                        0xFB7C, 
                        0x034A
                    }
                })
                Name (CEUP, Package (0x06)
                {
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000
                })
                Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                {
                    LSTM = Arg0
                    Notify (\_SB.PC00.TCPU, 0x91) // Device-Specific
                }

                Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                {
                    Return (0x0ADE)
                }

                Name (PTYP, Zero)
                Method (_PSS, 0, NotSerialized)  // _PSS: Performance Supported States
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        Return (\_SB.PR00._PSS ())
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }, 

                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TSS, 0, NotSerialized)  // _TSS: Throttling Supported States
                {
                    If (CondRefOf (\_SB.PR00._TSS))
                    {
                        Return (\_SB.PR00._TSS ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                One, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TPC, 0, NotSerialized)  // _TPC: Throttling Present Capabilities
                {
                    If (CondRefOf (\_SB.PR00._TPC))
                    {
                        Return (\_SB.PR00._TPC) /* External reference */
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PTC, 0, NotSerialized)  // _PTC: Processor Throttling Control
                {
                    If ((CondRefOf (\PF00) && (\PF00 != 0x80000000)))
                    {
                        If ((\PF00 & 0x04))
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }
                            })
                        }
                        Else
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }
                            })
                        }
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }, 

                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }
                        })
                    }
                }

                Method (_TSD, 0, NotSerialized)  // _TSD: Throttling State Dependencies
                {
                    If (CondRefOf (\_SB.PR00._TSD))
                    {
                        Return (\_SB.PR00._TSD ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                0x05, 
                                Zero, 
                                Zero, 
                                0xFC, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TDL, 0, NotSerialized)  // _TDL: T-State Depth Limit
                {
                    If ((CondRefOf (\_SB.PR00._TSS) && CondRefOf (\_SB.CFGD)))
                    {
                        If ((\_SB.CFGD & 0x2000))
                        {
                            Return ((SizeOf (\_SB.PR00.TSMF) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.TSMC) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PDL, 0, NotSerialized)  // _PDL: P-state Depth Limit
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        If ((\_SB.OSCP & 0x0400))
                        {
                            Return ((SizeOf (\_SB.PR00.TPSS) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.LPSS) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }
            }

            Scope (\_SB.IETM)
            {
                Name (CTSP, Package (0x01)
                {
                    ToUUID ("e145970a-e4c1-4d73-900e-c9c5a69dd067") /* Unknown UUID */
                })
            }

            Scope (\_SB.PC00.TCPU)
            {
                Method (TDPL, 0, Serialized)
                {
                    Name (AAAA, Zero)
                    Name (BBBB, Zero)
                    Name (CCCC, Zero)
                    Local0 = CTNL /* \_SB_.PC00.TCPU.CTNL */
                    If (((Local0 == One) || (Local0 == 0x02)))
                    {
                        Local0 = \_SB.CLVL /* External reference */
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Zero
                        })
                    }

                    If ((CLCK == One))
                    {
                        Local0 = One
                    }

                    AAAA = CPNU (\_SB.PL10, One)
                    BBBB = CPNU (\_SB.PL11, One)
                    CCCC = CPNU (\_SB.PL12, One)
                    Name (TMP1, Package (0x01)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP2, Package (0x02)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP3, Package (0x03)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    If ((Local0 == 0x03))
                    {
                        If ((AAAA > BBBB))
                        {
                            If ((AAAA > CCCC))
                            {
                                If ((BBBB > CCCC))
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local4 = One
                                    LEV1 = One
                                    Local5 = 0x02
                                    LEV2 = 0x02
                                }
                                Else
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local5 = One
                                    LEV1 = 0x02
                                    Local4 = 0x02
                                    LEV2 = One
                                }
                            }
                            Else
                            {
                                Local5 = Zero
                                LEV0 = 0x02
                                Local3 = One
                                LEV1 = Zero
                                Local4 = 0x02
                                LEV2 = One
                            }
                        }
                        ElseIf ((BBBB > CCCC))
                        {
                            If ((AAAA > CCCC))
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local3 = One
                                LEV1 = Zero
                                Local5 = 0x02
                                LEV2 = 0x02
                            }
                            Else
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local5 = One
                                LEV1 = 0x02
                                Local3 = 0x02
                                LEV2 = Zero
                            }
                        }
                        Else
                        {
                            Local5 = Zero
                            LEV0 = 0x02
                            Local4 = One
                            LEV1 = One
                            Local3 = 0x02
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP3 [Local3]) [One] = Local2
                        DerefOf (TMP3 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP3 [Local3]) [0x03] = Local1
                        DerefOf (TMP3 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP3 [Local4]) [One] = Local2
                        DerefOf (TMP3 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP3 [Local4]) [0x03] = Local1
                        DerefOf (TMP3 [Local4]) [0x04] = Zero
                        Local1 = (\_SB.TAR2 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local5]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                        DerefOf (TMP3 [Local5]) [One] = Local2
                        DerefOf (TMP3 [Local5]) [0x02] = \_SB.CTC2 /* External reference */
                        DerefOf (TMP3 [Local5]) [0x03] = Local1
                        DerefOf (TMP3 [Local5]) [0x04] = Zero
                        Return (TMP3) /* \_SB_.PC00.TCPU.TDPL.TMP3 */
                    }

                    If ((Local0 == 0x02))
                    {
                        If ((AAAA > BBBB))
                        {
                            Local3 = Zero
                            Local4 = One
                            LEV0 = Zero
                            LEV1 = One
                            LEV2 = Zero
                        }
                        Else
                        {
                            Local4 = Zero
                            Local3 = One
                            LEV0 = One
                            LEV1 = Zero
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP2 [Local3]) [One] = Local2
                        DerefOf (TMP2 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP2 [Local3]) [0x03] = Local1
                        DerefOf (TMP2 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP2 [Local4]) [One] = Local2
                        DerefOf (TMP2 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP2 [Local4]) [0x03] = Local1
                        DerefOf (TMP2 [Local4]) [0x04] = Zero
                        Return (TMP2) /* \_SB_.PC00.TCPU.TDPL.TMP2 */
                    }

                    If ((Local0 == One))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                Local1 = (\_SB.TAR0 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC0 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = Zero
                                LEV1 = Zero
                                LEV2 = Zero
                            }
                            Case (One)
                            {
                                Local1 = (\_SB.TAR1 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC1 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = One
                                LEV1 = One
                                LEV2 = One
                            }
                            Case (0x02)
                            {
                                Local1 = (\_SB.TAR2 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC2 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = 0x02
                                LEV1 = 0x02
                                LEV2 = 0x02
                            }

                        }

                        Return (TMP1) /* \_SB_.PC00.TCPU.TDPL.TMP1 */
                    }

                    Return (Zero)
                }

                Name (MAXT, Zero)
                Method (TDPC, 0, NotSerialized)
                {
                    Return (MAXT) /* \_SB_.PC00.TCPU.MAXT */
                }

                Name (LEV0, Zero)
                Name (LEV1, Zero)
                Name (LEV2, Zero)
                Method (STDP, 1, Serialized)
                {
                    If ((Arg0 >= \_SB.CLVL))
                    {
                        Return (Zero)
                    }

                    Switch (ToInteger (Arg0))
                    {
                        Case (Zero)
                        {
                            Local0 = LEV0 /* \_SB_.PC00.TCPU.LEV0 */
                        }
                        Case (One)
                        {
                            Local0 = LEV1 /* \_SB_.PC00.TCPU.LEV1 */
                        }
                        Case (0x02)
                        {
                            Local0 = LEV2 /* \_SB_.PC00.TCPU.LEV2 */
                        }

                    }

                    Switch (ToInteger (Local0))
                    {
                        Case (Zero)
                        {
                            CPL0 ()
                        }
                        Case (One)
                        {
                            CPL1 ()
                        }
                        Case (0x02)
                        {
                            CPL2 ()
                        }

                    }

                    Notify (\_SB.PC00.TCPU, 0x83) // Device-Specific Change
                }
            }

            Scope (\_SB.IETM)
            {
                Name (PTTL, 0x14)
                Name (PSVT, Package (0x04)
                {
                    0x02, 
                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.TCPU, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN2, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN3, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }
                })
            }

            Scope (\_SB.PC00.LPCB)
            {
                Device (SEN3)
                {
                    Name (_HID, "INTC1046")  // _HID: Hardware ID
                    Name (_UID, "SEN3")  // _UID: Unique ID
                    Name (_STR, Unicode ("Remote GT Sensor"))  // _STR: Description String
                    Name (PTYP, 0x03)
                    Name (CTYP, Zero)
                    Name (PFLG, Zero)
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        If ((\S3DE == One))
                        {
                            Return (0x0F)
                        }
                        Else
                        {
                            Return (Zero)
                        }
                    }

                    Method (_TMP, 0, Serialized)  // _TMP: Temperature
                    {
                        Return (\_SB.IETM.CTOK (\_SB.PC00.LPCB.M662))
                    }

                    Name (PATC, 0x02)
                    Method (PAT0, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4LO = Local1
                        \_SB.PC00.LPCB.Q4LU = One
                    }

                    Method (PAT1, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4HI = Local1
                        \_SB.PC00.LPCB.Q4HU = One
                    }

                    Name (GTSH, 0x14)
                    Name (LSTM, Zero)
                    Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                    {
                        LSTM = Arg0
                        Notify (\_SB.PC00.LPCB.SEN3, 0x91) // Device-Specific
                    }

                    Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                    {
                        Return (0x0ADE)
                    }

                    Name (S3AC, 0x3C)
                    Name (S3A1, 0x32)
                    Name (S3A2, 0x28)
                    Name (S3PV, 0x41)
                    Name (S3CC, 0x50)
                    Name (S3C3, 0x46)
                    Name (S3HP, 0x4B)
                    Name (SSP3, Zero)
                    Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
                    {
                        Return (SSP3) /* \_SB_.PC00.LPCB.SEN3.SSP3 */
                    }

                    Method (_AC3, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Local1 = \_SB.IETM.CTOK (S3AC)
                        If ((LSTM >= Local1))
                        {
                            Return ((Local1 - 0x14))
                        }
                        Else
                        {
                            Return (Local1)
                        }
                    }

                    Method (_AC4, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A1))
                    }

                    Method (_AC5, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A2))
                    }

                    Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3PV))
                    }

                    Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3CC))
                    }

                    Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3C3))
                    }

                    Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3HP))
                    }
                }
            }

            Method (GDDV, 0, Serialized)
            {
                Return (Package (0x01)
                {
                    Buffer (0x05E5)
                    {
                        /* 0000 */  0xE5, 0x1F, 0x94, 0x00, 0x00, 0x00, 0x00, 0x02,  // ........
                        /* 0008 */  0x00, 0x00, 0x00, 0x40, 0x67, 0x64, 0x64, 0x76,  // ...@gddv
                        /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0020 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0028 */  0x00, 0x00, 0x00, 0x00, 0x4F, 0x45, 0x4D, 0x20,  // ....OEM 
                        /* 0030 */  0x45, 0x78, 0x70, 0x6F, 0x72, 0x74, 0x65, 0x64,  // Exported
                        /* 0038 */  0x20, 0x44, 0x61, 0x74, 0x61, 0x56, 0x61, 0x75,  //  DataVau
                        /* 0040 */  0x6C, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // lt......
                        /* 0048 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0050 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0058 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0060 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0068 */  0x00, 0x00, 0x00, 0x00, 0xE5, 0x00, 0x84, 0xC5,  // ........
                        /* 0070 */  0x22, 0x22, 0x8B, 0x44, 0xF8, 0x85, 0xED, 0x67,  // "".D...g
                        /* 0078 */  0x3E, 0x74, 0x20, 0x00, 0xF6, 0x05, 0xD2, 0xC7,  // >t .....
                        /* 0080 */  0x90, 0xF0, 0xAC, 0xC1, 0xB2, 0x8B, 0x68, 0x10,  // ......h.
                        /* 0088 */  0xA4, 0x94, 0x2A, 0x5B, 0x51, 0x05, 0x00, 0x00,  // ..*[Q...
                        /* 0090 */  0x52, 0x45, 0x50, 0x4F, 0x5D, 0x00, 0x00, 0x00,  // REPO]...
                        /* 0098 */  0x01, 0xA6, 0x4D, 0x00, 0x00, 0x00, 0x00, 0x00,  // ..M.....
                        /* 00A0 */  0x00, 0x00, 0x72, 0x87, 0xCD, 0xFF, 0x6D, 0x24,  // ..r...m$
                        /* 00A8 */  0x47, 0xDB, 0x3D, 0x24, 0x92, 0xB4, 0x16, 0x6F,  // G.=$...o
                        /* 00B0 */  0x45, 0xD8, 0xC3, 0xF5, 0x66, 0x14, 0x9F, 0x22,  // E...f.."
                        /* 00B8 */  0xD7, 0xF7, 0xDE, 0x67, 0x90, 0x9A, 0xA2, 0x0D,  // ...g....
                        /* 00C0 */  0x39, 0x25, 0xAD, 0xC3, 0x1A, 0xAD, 0x52, 0x0B,  // 9%....R.
                        /* 00C8 */  0x75, 0x38, 0xE1, 0xA4, 0x14, 0x41, 0x7B, 0xAA,  // u8...A{.
                        /* 00D0 */  0xA2, 0x50, 0x70, 0x9E, 0x31, 0xC3, 0xD3, 0x49,  // .Pp.1..I
                        /* 00D8 */  0x6F, 0x05, 0x92, 0x41, 0x2F, 0x84, 0xE4, 0xF5,  // o..A/...
                        /* 00E0 */  0xF5, 0x79, 0x87, 0xE5, 0x02, 0x86, 0x81, 0x06,  // .y......
                        /* 00E8 */  0x19, 0x02, 0xA1, 0x0F, 0x2F, 0x9B, 0x87, 0xFA,  // ..../...
                        /* 00F0 */  0x1E, 0xCA, 0x3D, 0x4C, 0x07, 0x1B, 0x11, 0xAF,  // ..=L....
                        /* 00F8 */  0x03, 0x19, 0x2B, 0x3F, 0x03, 0xDF, 0x11, 0xEA,  // ..+?....
                        /* 0100 */  0x80, 0xBC, 0x1D, 0x29, 0x2E, 0x0D, 0x73, 0x49,  // ...)..sI
                        /* 0108 */  0x7F, 0xDC, 0x45, 0xCD, 0x6A, 0xE5, 0x8E, 0xBA,  // ..E.j...
                        /* 0110 */  0x87, 0x9B, 0x57, 0xA0, 0x4A, 0x98, 0x35, 0x54,  // ..W.J.5T
                        /* 0118 */  0x8A, 0x46, 0x7F, 0x27, 0xFE, 0x43, 0xCC, 0x99,  // .F.'.C..
                        /* 0120 */  0x2D, 0x7F, 0xA1, 0xE4, 0x40, 0xF5, 0x16, 0x45,  // -...@..E
                        /* 0128 */  0x8C, 0xA5, 0xC8, 0x15, 0xAD, 0x10, 0x75, 0xA0,  // ......u.
                        /* 0130 */  0x29, 0xE5, 0x5D, 0x8F, 0xA0, 0xF8, 0x0D, 0x5E,  // ).]....^
                        /* 0138 */  0x83, 0xB0, 0x18, 0x74, 0xA3, 0xD9, 0x35, 0x1C,  // ...t..5.
                        /* 0140 */  0x41, 0xC7, 0x9B, 0x14, 0xC9, 0x3D, 0x64, 0x63,  // A....=dc
                        /* 0148 */  0x66, 0x94, 0xD7, 0xA1, 0xD2, 0xC3, 0x05, 0xA7,  // f.......
                        /* 0150 */  0xDF, 0xA9, 0x93, 0xF5, 0x50, 0x35, 0x45, 0xAB,  // ....P5E.
                        /* 0158 */  0x3F, 0x1A, 0x8B, 0xAD, 0x68, 0xFC, 0x27, 0xDA,  // ?...h.'.
                        /* 0160 */  0x6F, 0x44, 0xAB, 0xCE, 0x91, 0xC8, 0x3B, 0x9C,  // oD....;.
                        /* 0168 */  0x98, 0x77, 0x3E, 0xE6, 0x44, 0xB3, 0x80, 0x43,  // .w>.D..C
                        /* 0170 */  0x55, 0xC3, 0x6C, 0x86, 0x11, 0xE4, 0xA8, 0x64,  // U.l....d
                        /* 0178 */  0xEE, 0xFD, 0xF9, 0xFE, 0x7B, 0x89, 0xE1, 0xAD,  // ....{...
                        /* 0180 */  0xE5, 0x28, 0xEB, 0x43, 0x65, 0x07, 0x0D, 0x3C,  // .(.Ce..<
                        /* 0188 */  0x39, 0xBC, 0x1F, 0x4C, 0x37, 0x4C, 0xAE, 0x9F,  // 9..L7L..
                        /* 0190 */  0x38, 0x89, 0x66, 0x8D, 0xAA, 0xD7, 0x5C, 0x36,  // 8.f...\6
                        /* 0198 */  0xE4, 0x5F, 0xD9, 0x68, 0xD7, 0x57, 0xAF, 0xF9,  // ._.h.W..
                        /* 01A0 */  0x98, 0xB6, 0xA6, 0xEE, 0x9B, 0x1F, 0x08, 0x04,  // ........
                        /* 01A8 */  0x98, 0x6E, 0x1E, 0xBD, 0xEC, 0xF4, 0x58, 0x20,  // .n....X 
                        /* 01B0 */  0x80, 0x56, 0xD8, 0x72, 0xDB, 0x4D, 0x72, 0x3B,  // .V.r.Mr;
                        /* 01B8 */  0x8D, 0xE5, 0x39, 0xDF, 0xAB, 0x6D, 0x48, 0x76,  // ..9..mHv
                        /* 01C0 */  0xAF, 0xAA, 0x95, 0x5B, 0x76, 0x12, 0x70, 0x74,  // ...[v.pt
                        /* 01C8 */  0x37, 0x3A, 0x33, 0xC8, 0x31, 0x1D, 0x68, 0x37,  // 7:3.1.h7
                        /* 01D0 */  0xF6, 0x74, 0x7F, 0x89, 0xC7, 0x60, 0xE8, 0xD0,  // .t...`..
                        /* 01D8 */  0x2E, 0xC6, 0xAE, 0xCD, 0xA3, 0x97, 0x5A, 0x0E,  // ......Z.
                        /* 01E0 */  0x1F, 0x4E, 0xA1, 0x61, 0x76, 0x04, 0x30, 0xB6,  // .N.av.0.
                        /* 01E8 */  0x63, 0xF5, 0x9D, 0xE4, 0xFD, 0x2C, 0x00, 0x1B,  // c....,..
                        /* 01F0 */  0xA2, 0xDD, 0x89, 0xEB, 0x41, 0x6D, 0x54, 0xA8,  // ....AmT.
                        /* 01F8 */  0x54, 0x26, 0x2A, 0xEB, 0x07, 0x62, 0x3C, 0xC9,  // T&*..b<.
                        /* 0200 */  0xB0, 0x6F, 0x05, 0x52, 0x0C, 0xDC, 0x55, 0x0D,  // .o.R..U.
                        /* 0208 */  0x69, 0xCA, 0x2F, 0x88, 0xB2, 0x1D, 0x50, 0x3B,  // i./...P;
                        /* 0210 */  0x6B, 0x49, 0xAB, 0xBC, 0xC7, 0x66, 0x75, 0x96,  // kI...fu.
                        /* 0218 */  0xEB, 0xEB, 0xE7, 0x6F, 0x18, 0xFC, 0x70, 0x4F,  // ...o..pO
                        /* 0220 */  0xBA, 0xD9, 0x5B, 0x2F, 0x44, 0x36, 0xA3, 0xFD,  // ..[/D6..
                        /* 0228 */  0x94, 0xE3, 0x54, 0x4F, 0xBF, 0x4E, 0xD0, 0xC1,  // ..TO.N..
                        /* 0230 */  0xFB, 0xAE, 0x04, 0xBD, 0xAC, 0x95, 0x55, 0xCD,  // ......U.
                        /* 0238 */  0x52, 0x6A, 0x9A, 0x46, 0x19, 0x39, 0x0F, 0xC9,  // Rj.F.9..
                        /* 0240 */  0x67, 0x8A, 0xF9, 0x86, 0x9F, 0x9E, 0x38, 0x93,  // g.....8.
                        /* 0248 */  0xEA, 0xDE, 0x74, 0xE8, 0x22, 0xBE, 0x3E, 0x0C,  // ..t.".>.
                        /* 0250 */  0xE7, 0x4E, 0xB2, 0x24, 0xD3, 0xFF, 0xAA, 0x27,  // .N.$...'
                        /* 0258 */  0xAE, 0xE3, 0xDF, 0x19, 0x64, 0xEC, 0x31, 0x4C,  // ....d.1L
                        /* 0260 */  0x43, 0xAC, 0xF2, 0x1C, 0xE9, 0x1C, 0x61, 0x88,  // C.....a.
                        /* 0268 */  0xC5, 0x6C, 0x91, 0xEF, 0x13, 0xF7, 0xD2, 0x7A,  // .l.....z
                        /* 0270 */  0x47, 0x72, 0xDB, 0x1C, 0xBF, 0x29, 0xEF, 0xF1,  // Gr...)..
                        /* 0278 */  0x92, 0x11, 0xBD, 0x88, 0xDF, 0xBC, 0x31, 0x36,  // ......16
                        /* 0280 */  0xC5, 0x84, 0x9C, 0xCE, 0x95, 0x9D, 0xEC, 0xD6,  // ........
                        /* 0288 */  0xBD, 0xAB, 0x68, 0x7D, 0xFC, 0xA1, 0xEB, 0x1E,  // ..h}....
                        /* 0290 */  0xF3, 0x79, 0x6E, 0x66, 0xCF, 0xFF, 0xC2, 0xB5,  // .ynf....
                        /* 0298 */  0x18, 0x92, 0xF8, 0xB9, 0x68, 0x57, 0x27, 0xF8,  // ....hW'.
                        /* 02A0 */  0x39, 0xD5, 0xD0, 0xF1, 0xF6, 0x5E, 0x04, 0x12,  // 9....^..
                        /* 02A8 */  0x88, 0xCB, 0x2E, 0x05, 0x57, 0x27, 0x46, 0x1E,  // ....W'F.
                        /* 02B0 */  0xAC, 0xC4, 0x2E, 0xAE, 0x3B, 0xD8, 0x81, 0x9C,  // ....;...
                        /* 02B8 */  0xDE, 0x77, 0x67, 0x1A, 0x73, 0x46, 0xA8, 0xCA,  // .wg.sF..
                        /* 02C0 */  0x32, 0x9D, 0xCA, 0x91, 0x38, 0x51, 0x97, 0xD2,  // 2...8Q..
                        /* 02C8 */  0x60, 0xC9, 0x72, 0x9B, 0x0E, 0x97, 0x7D, 0x36,  // `.r...}6
                        /* 02D0 */  0x79, 0x69, 0x45, 0x75, 0x2C, 0x0A, 0xDC, 0xBC,  // yiEu,...
                        /* 02D8 */  0xFC, 0xC9, 0x29, 0x23, 0x2A, 0x62, 0x1C, 0x0E,  // ..)#*b..
                        /* 02E0 */  0xFF, 0xEA, 0xDA, 0x9D, 0xB8, 0x92, 0x3D, 0xEB,  // ......=.
                        /* 02E8 */  0x31, 0x00, 0xAA, 0x0C, 0x14, 0x44, 0x0A, 0xA3,  // 1....D..
                        /* 02F0 */  0x0A, 0xE0, 0xF6, 0xE4, 0xFF, 0x78, 0x5F, 0x1F,  // .....x_.
                        /* 02F8 */  0x51, 0x8B, 0x86, 0xD0, 0xA5, 0x16, 0xFA, 0xD1,  // Q.......
                        /* 0300 */  0x3F, 0xF9, 0x4F, 0x61, 0xD5, 0x11, 0xF7, 0x90,  // ?.Oa....
                        /* 0308 */  0xD4, 0x9B, 0xFF, 0x06, 0xA3, 0xA8, 0xAD, 0xF7,  // ........
                        /* 0310 */  0xC5, 0x17, 0x60, 0xD1, 0x79, 0x34, 0x1E, 0x3B,  // ..`.y4.;
                        /* 0318 */  0xD4, 0x26, 0x7E, 0x73, 0xE4, 0xC6, 0xCA, 0xFC,  // .&~s....
                        /* 0320 */  0x17, 0x34, 0x46, 0x03, 0xB0, 0x1B, 0x5D, 0x84,  // .4F...].
                        /* 0328 */  0xEB, 0x9D, 0x86, 0xAB, 0x0C, 0xBF, 0x1A, 0x56,  // .......V
                        /* 0330 */  0x2D, 0xB1, 0x38, 0x86, 0x93, 0x19, 0x73, 0x13,  // -.8...s.
                        /* 0338 */  0x3B, 0x55, 0xF1, 0xF0, 0x2A, 0x1E, 0x53, 0x66,  // ;U..*.Sf
                        /* 0340 */  0xBC, 0x23, 0x99, 0xD3, 0x76, 0x12, 0x31, 0x61,  // .#..v.1a
                        /* 0348 */  0x9F, 0x4D, 0xBC, 0x02, 0xAE, 0x1C, 0xA2, 0x65,  // .M.....e
                        /* 0350 */  0x3C, 0x0E, 0x16, 0xFD, 0x4E, 0x4D, 0x06, 0xD9,  // <...NM..
                        /* 0358 */  0xF9, 0x7F, 0x9F, 0x90, 0xE4, 0x9B, 0xA7, 0xA3,  // ........
                        /* 0360 */  0x13, 0xCA, 0xDC, 0x0B, 0xC6, 0x19, 0xD0, 0xD8,  // ........
                        /* 0368 */  0xE3, 0xDE, 0xFA, 0x88, 0x26, 0xC3, 0x26, 0x61,  // ....&.&a
                        /* 0370 */  0x2C, 0x7E, 0xD8, 0xC9, 0xCB, 0x49, 0x0F, 0x75,  // ,~...I.u
                        /* 0378 */  0x76, 0x22, 0x43, 0x66, 0x40, 0xD7, 0xC6, 0x02,  // v"Cf@...
                        /* 0380 */  0x76, 0x6D, 0xC0, 0xF0, 0x84, 0xD8, 0xC4, 0xD0,  // vm......
                        /* 0388 */  0x4A, 0x22, 0x5A, 0x9B, 0x6F, 0x44, 0x80, 0x47,  // J"Z.oD.G
                        /* 0390 */  0xD5, 0x25, 0xE0, 0x33, 0x6B, 0xC8, 0x08, 0x67,  // .%.3k..g
                        /* 0398 */  0xA0, 0xF4, 0x9F, 0xA0, 0x2C, 0xC6, 0x71, 0x86,  // ....,.q.
                        /* 03A0 */  0x48, 0x04, 0xB6, 0xAF, 0x70, 0xC1, 0xEF, 0x02,  // H...p...
                        /* 03A8 */  0x3C, 0x4B, 0xE7, 0xD7, 0x1F, 0xEF, 0x1B, 0x4F,  // <K.....O
                        /* 03B0 */  0x5A, 0xB6, 0x5C, 0x68, 0xBB, 0x4B, 0xF8, 0x05,  // Z.\h.K..
                        /* 03B8 */  0x18, 0x81, 0xEA, 0x50, 0xB9, 0x38, 0xC0, 0x1B,  // ...P.8..
                        /* 03C0 */  0x5C, 0xE9, 0xA3, 0x3E, 0x4D, 0x18, 0x49, 0xFD,  // \..>M.I.
                        /* 03C8 */  0xCE, 0xF8, 0xCE, 0xD5, 0x87, 0x30, 0x67, 0xE1,  // .....0g.
                        /* 03D0 */  0x3E, 0xC1, 0x3B, 0xA3, 0x46, 0xF7, 0x92, 0xFB,  // >.;.F...
                        /* 03D8 */  0x77, 0x5F, 0xEA, 0x5D, 0x49, 0x5C, 0x3A, 0x7E,  // w_.]I\:~
                        /* 03E0 */  0x18, 0xCB, 0x83, 0xFE, 0xAE, 0x98, 0x09, 0x7F,  // ........
                        /* 03E8 */  0xE1, 0x99, 0x68, 0xC3, 0xA8, 0xBC, 0x69, 0xF5,  // ..h...i.
                        /* 03F0 */  0xBC, 0x74, 0x11, 0xA0, 0xD9, 0x6D, 0x19, 0x72,  // .t...m.r
                        /* 03F8 */  0x1A, 0xE4, 0x60, 0x28, 0x1E, 0x3E, 0x87, 0x69,  // ..`(.>.i
                        /* 0400 */  0xFE, 0x1C, 0x27, 0x66, 0xBC, 0xA3, 0x9E, 0x43,  // ..'f...C
                        /* 0408 */  0x9C, 0xEB, 0xA5, 0x77, 0x82, 0x6A, 0x3D, 0xDB,  // ...w.j=.
                        /* 0410 */  0xAE, 0x0F, 0x5C, 0x75, 0x03, 0x99, 0x89, 0x35,  // ..\u...5
                        /* 0418 */  0x0C, 0xD0, 0x68, 0x1D, 0x19, 0x6E, 0xB1, 0x12,  // ..h..n..
                        /* 0420 */  0x40, 0x50, 0x43, 0x77, 0x11, 0x15, 0x2E, 0xD3,  // @PCw....
                        /* 0428 */  0x55, 0x7E, 0x2B, 0xAD, 0xB8, 0x47, 0xD6, 0x5A,  // U~+..G.Z
                        /* 0430 */  0xF4, 0xF8, 0xBC, 0x69, 0x54, 0x36, 0x80, 0xF3,  // ...iT6..
                        /* 0438 */  0x39, 0x50, 0x0D, 0xCE, 0x35, 0x1B, 0x2D, 0xAC,  // 9P..5.-.
                        /* 0440 */  0xB3, 0xBD, 0xAC, 0x45, 0x09, 0x75, 0xE9, 0xDA,  // ...E.u..
                        /* 0448 */  0x41, 0xC1, 0x90, 0x45, 0xFB, 0x2D, 0x35, 0xAF,  // A..E.-5.
                        /* 0450 */  0xF3, 0x32, 0x67, 0xC3, 0x99, 0x90, 0x30, 0x5F,  // .2g...0_
                        /* 0458 */  0xF9, 0x83, 0x35, 0x2F, 0x4D, 0xF0, 0x3E, 0xC9,  // ..5/M.>.
                        /* 0460 */  0x79, 0xFB, 0xCB, 0x6D, 0x07, 0x69, 0xB8, 0xD7,  // y..m.i..
                        /* 0468 */  0xE6, 0xD7, 0x7D, 0xAE, 0x31, 0xB3, 0x57, 0xB9,  // ..}.1.W.
                        /* 0470 */  0x58, 0xF8, 0x6D, 0xE3, 0x38, 0xE0, 0x41, 0x6F,  // X.m.8.Ao
                        /* 0478 */  0x40, 0x89, 0xBE, 0x6F, 0xFE, 0x1D, 0xC9, 0xCF,  // @..o....
                        /* 0480 */  0xBC, 0xF4, 0xED, 0x2F, 0x75, 0x3A, 0x28, 0x12,  // .../u:(.
                        /* 0488 */  0xE9, 0x84, 0x13, 0x2C, 0x8D, 0x54, 0x5F, 0xD2,  // ...,.T_.
                        /* 0490 */  0x71, 0x94, 0x47, 0x82, 0x14, 0xFE, 0x4E, 0xC4,  // q.G...N.
                        /* 0498 */  0x85, 0xD8, 0x88, 0x2F, 0xC5, 0x0A, 0x8E, 0xEF,  // .../....
                        /* 04A0 */  0x0A, 0xAD, 0x13, 0x8B, 0x4F, 0xCD, 0x58, 0xF6,  // ....O.X.
                        /* 04A8 */  0xC0, 0x28, 0x98, 0x08, 0x42, 0x8E, 0xC1, 0x05,  // .(..B...
                        /* 04B0 */  0xF1, 0xAE, 0x36, 0x0A, 0x25, 0xC7, 0x27, 0x5A,  // ..6.%.'Z
                        /* 04B8 */  0x78, 0x1B, 0xC4, 0xBB, 0x62, 0x0A, 0xCB, 0xC6,  // x...b...
                        /* 04C0 */  0xC0, 0xC4, 0xFA, 0xB0, 0xCB, 0xED, 0x9A, 0xA2,  // ........
                        /* 04C8 */  0x14, 0x4E, 0xCF, 0xD2, 0x59, 0x34, 0x34, 0x0F,  // .N..Y44.
                        /* 04D0 */  0xBC, 0x65, 0xF5, 0x7C, 0x77, 0xA0, 0x90, 0x33,  // .e.|w..3
                        /* 04D8 */  0x64, 0xD2, 0x7F, 0xAA, 0x36, 0xEC, 0xDC, 0x04,  // d...6...
                        /* 04E0 */  0x1D, 0x7D, 0x6A, 0x07, 0xE9, 0xCF, 0x8F, 0x6F,  // .}j....o
                        /* 04E8 */  0xD7, 0x70, 0x87, 0x1F, 0x6A, 0xC5, 0x8B, 0xBD,  // .p..j...
                        /* 04F0 */  0xB8, 0x03, 0x10, 0x48, 0xCE, 0x1E, 0x4D, 0x9A,  // ...H..M.
                        /* 04F8 */  0xB7, 0x54, 0x6A, 0x7B, 0xAF, 0xEC, 0x01, 0x4F,  // .Tj{...O
                        /* 0500 */  0x36, 0x2A, 0x5E, 0xCF, 0x89, 0xB6, 0xC1, 0x99,  // 6*^.....
                        /* 0508 */  0x21, 0x18, 0x5A, 0x77, 0xB1, 0x01, 0xF4, 0x5F,  // !.Zw..._
                        /* 0510 */  0x78, 0x53, 0x5E, 0xDA, 0x06, 0x46, 0x26, 0xA5,  // xS^..F&.
                        /* 0518 */  0x9D, 0xF5, 0xB3, 0xCE, 0x59, 0x5E, 0x3A, 0xAC,  // ....Y^:.
                        /* 0520 */  0xBB, 0xDA, 0x33, 0x8F, 0x09, 0x65, 0xDD, 0x6D,  // ..3..e.m
                        /* 0528 */  0xA0, 0xC5, 0xE7, 0xF8, 0x1B, 0xED, 0xDA, 0xE4,  // ........
                        /* 0530 */  0x61, 0x56, 0xE5, 0xED, 0xDB, 0x49, 0x21, 0x8E,  // aV...I!.
                        /* 0538 */  0xAA, 0x51, 0x31, 0x67, 0x0F, 0xB0, 0x15, 0xDE,  // .Q1g....
                        /* 0540 */  0x20, 0xBF, 0x46, 0xD4, 0xE5, 0x1C, 0x65, 0x90,  //  .F...e.
                        /* 0548 */  0x77, 0xC3, 0xAD, 0x52, 0x0D, 0xAB, 0x84, 0xD7,  // w..R....
                        /* 0550 */  0x2A, 0xAF, 0xEA, 0x91, 0x24, 0x03, 0xF6, 0x73,  // *...$..s
                        /* 0558 */  0x09, 0x90, 0x2A, 0x91, 0xB6, 0x2F, 0x7E, 0xCB,  // ..*../~.
                        /* 0560 */  0x88, 0xDD, 0x64, 0x40, 0xF3, 0x1F, 0xFC, 0x76,  // ..d@...v
                        /* 0568 */  0x77, 0xE5, 0x95, 0x80, 0x4C, 0xBE, 0x4D, 0x68,  // w...L.Mh
                        /* 0570 */  0xD8, 0x08, 0x8B, 0xF0, 0x56, 0xE0, 0x56, 0x97,  // ....V.V.
                        /* 0578 */  0x54, 0x5B, 0x7A, 0xB5, 0x90, 0xA8, 0x7C, 0x49,  // T[z...|I
                        /* 0580 */  0x3E, 0x49, 0xD8, 0x05, 0xFA, 0xE2, 0x48, 0xCD,  // >I....H.
                        /* 0588 */  0xFF, 0xF0, 0x55, 0x87, 0xEF, 0x81, 0xE9, 0x94,  // ..U.....
                        /* 0590 */  0x12, 0x8A, 0x8F, 0x63, 0xB6, 0x14, 0x29, 0xDD,  // ...c..).
                        /* 0598 */  0x63, 0x70, 0x53, 0x7A, 0xDA, 0x37, 0x7D, 0xB4,  // cpSz.7}.
                        /* 05A0 */  0xCB, 0xD4, 0x6B, 0xDB, 0x73, 0xB0, 0x5A, 0xBA,  // ..k.s.Z.
                        /* 05A8 */  0xFB, 0x40, 0x48, 0x54, 0xE8, 0xD5, 0xF2, 0xB4,  // .@HT....
                        /* 05B0 */  0xA4, 0x57, 0x0B, 0x2E, 0x30, 0x34, 0xA6, 0xD5,  // .W..04..
                        /* 05B8 */  0x09, 0x42, 0xF8, 0x6F, 0x1E, 0x64, 0xA6, 0xCA,  // .B.o.d..
                        /* 05C0 */  0x31, 0xF1, 0xA5, 0x8B, 0x27, 0x3C, 0x6B, 0xFC,  // 1...'<k.
                        /* 05C8 */  0x34, 0x20, 0x32, 0x75, 0x73, 0x16, 0xD6, 0x26,  // 4 2us..&
                        /* 05D0 */  0x57, 0xC4, 0xD3, 0xDE, 0xDB, 0x0B, 0xD7, 0x07,  // W.......
                        /* 05D8 */  0x79, 0xD7, 0x74, 0xFF, 0x5E, 0xEA, 0x38, 0x97,  // y.t.^.8.
                        /* 05E0 */  0x6D, 0x3C, 0x6B, 0x10, 0x49                     // m<k.I
                    }
                })
            }

            Method (IMOK, 1, NotSerialized)
            {
                Return (Arg0)
            }
        }
    }

    Scope (\_SB.IETM)
    {
        Method (KTOC, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return (((Arg0 - 0x0AAC) / 0x0A))
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (CTOK, 1, Serialized)
        {
            Return (((Arg0 * 0x0A) + 0x0AAC))
        }

        Method (C10K, 1, Serialized)
        {
            Name (TMP1, Buffer (0x10)
            {
                 0x00                                             // .
            })
            CreateByteField (TMP1, Zero, TMPL)
            CreateByteField (TMP1, One, TMPH)
            Local0 = (Arg0 + 0x0AAC)
            TMPL = (Local0 & 0xFF)
            TMPH = ((Local0 & 0xFF00) >> 0x08)
            ToInteger (TMP1, Local1)
            Return (Local1)
        }

        Method (K10C, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return ((Arg0 - 0x0AAC))
            }
            Else
            {
                Return (Zero)
            }
        }
    }
}

