/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt18.dat, Thu Mar  7 17:18:08 2024
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000394 (916)
 *     Revision         0x02
 *     Checksum         0x19
 *     OEM ID           "PmRef"
 *     OEM Table ID     "Cpu0Cst"
 *     OEM Revision     0x00003001 (12289)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "PmRef", "Cpu0Cst", 0x00003001)
{
    External (_SB_.GGIV, MethodObj)    // 1 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.GFX0, DeviceObj)
    External (_SB_.PC00.LPCB.ACIN, FieldUnitObj)
    External (_SB_.PC00.LPCB.DBCP, FieldUnitObj)
    External (_SB_.PC00.LPCB.ECA4, FieldUnitObj)
    External (_SB_.PC00.LPCB.MBF4, IntObj)
    External (_SB_.PC00.LPCB.MBF5, IntObj)
    External (_SB_.PC00.LPCB.NEDP, IntObj)
    External (_SB_.PC00.LPCB.RCPU, IntObj)
    External (_SB_.PC00.LPCB.RDNT, IntObj)
    External (_SB_.PC00.LPCB.RVGA, IntObj)
    External (_SB_.PC00.PEG1, DeviceObj)
    External (_SB_.PC00.PEG1.CEDR, UnknownObj)
    External (_SB_.PC00.PEG1.DGCX, IntObj)
    External (_SB_.PC00.PEG1.DL23, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.L23D, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.LREN, UnknownObj)
    External (_SB_.PC00.PEG1.PEGP, DeviceObj)
    External (_SB_.PC00.PEG1.PEGP._ADR, DeviceObj)
    External (_SB_.PC00.PEG1.PEGP.GC6I, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.PEGP.VEID, FieldUnitObj)
    External (_SB_.PC00.PEG1.POFF, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.PXP_._OFF, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.PXP_._ON_, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.TDGC, IntObj)
    External (_SB_.PC00.PEG1.TGPC, IntObj)
    External (_SB_.PR00, DeviceObj)
    External (_SB_.PR01, ProcessorObj)
    External (_SB_.PR02, ProcessorObj)
    External (_SB_.PR03, ProcessorObj)
    External (_SB_.PR04, ProcessorObj)
    External (_SB_.PR05, ProcessorObj)
    External (_SB_.PR06, ProcessorObj)
    External (_SB_.PR07, ProcessorObj)
    External (_SB_.PR08, ProcessorObj)
    External (_SB_.PR09, ProcessorObj)
    External (_SB_.PR10, ProcessorObj)
    External (_SB_.PR11, ProcessorObj)
    External (_SB_.PR12, ProcessorObj)
    External (_SB_.PR13, ProcessorObj)
    External (_SB_.PR14, ProcessorObj)
    External (_SB_.PR15, ProcessorObj)
    External (_SB_.PR16, ProcessorObj)
    External (_SB_.PR17, ProcessorObj)
    External (_SB_.PR18, ProcessorObj)
    External (_SB_.PR19, ProcessorObj)
    External (_SB_.PWOK, IntObj)
    External (_SB_.SGOV, MethodObj)    // 2 Arguments
    External (C6LT, UnknownObj)
    External (C6MW, UnknownObj)
    External (C7LT, UnknownObj)
    External (C7MW, UnknownObj)
    External (CDLT, UnknownObj)
    External (CDLV, UnknownObj)
    External (CDMW, UnknownObj)
    External (CDPW, UnknownObj)
    External (CFGD, UnknownObj)
    External (CHPV, UnknownObj)
    External (CUMA, IntObj)
    External (DID1, UnknownObj)
    External (DID2, UnknownObj)
    External (DID3, UnknownObj)
    External (DID4, UnknownObj)
    External (DID5, UnknownObj)
    External (DID6, UnknownObj)
    External (DID7, UnknownObj)
    External (DID8, UnknownObj)
    External (DR00, UnknownObj)
    External (DR01, UnknownObj)
    External (DR02, UnknownObj)
    External (DR03, UnknownObj)
    External (DR04, UnknownObj)
    External (DR05, UnknownObj)
    External (DR06, UnknownObj)
    External (DR07, UnknownObj)
    External (DR08, UnknownObj)
    External (DR09, UnknownObj)
    External (DR10, UnknownObj)
    External (EBAS, UnknownObj)
    External (FEMD, UnknownObj)
    External (FMBL, UnknownObj)
    External (HGFL, UnknownObj)
    External (HGMD, UnknownObj)
    External (HRA0, UnknownObj)
    External (HRE0, UnknownObj)
    External (HRG0, UnknownObj)
    External (HYSS, UnknownObj)
    External (IOBS, UnknownObj)
    External (MDEL, UnknownObj)
    External (NVAF, UnknownObj)
    External (NVGA, UnknownObj)
    External (NVHA, UnknownObj)
    External (NXD1, UnknownObj)
    External (NXD2, UnknownObj)
    External (NXD3, UnknownObj)
    External (NXD4, UnknownObj)
    External (NXD5, UnknownObj)
    External (NXD6, UnknownObj)
    External (NXD7, UnknownObj)
    External (NXD8, UnknownObj)
    External (OSYS, UnknownObj)
    External (P1GP, UnknownObj)
    External (P1RE, UnknownObj)
    External (P1RP, UnknownObj)
    External (PANL, UnknownObj)
    External (PF00, UnknownObj)
    External (PFLV, UnknownObj)
    External (PWA0, UnknownObj)
    External (PWE0, UnknownObj)
    External (PWG0, UnknownObj)
    External (PWRS, UnknownObj)
    External (SGGP, UnknownObj)
    External (SSMP, UnknownObj)
    External (TCNT, FieldUnitObj)
    External (VK01, UnknownObj)
    External (VK02, UnknownObj)
    External (VK03, UnknownObj)
    External (VK04, UnknownObj)
    External (VK05, UnknownObj)
    External (VK06, UnknownObj)
    External (VK07, UnknownObj)
    External (VK08, UnknownObj)
    External (VK09, UnknownObj)
    External (VK10, UnknownObj)
    External (VK11, UnknownObj)
    External (XBAS, UnknownObj)

    Scope (\_SB.PR00)
    {
        Name (C1TM, Package (0x04)
        {
            ResourceTemplate ()
            {
                Register (FFixedHW, 
                    0x00,               // Bit Width
                    0x00,               // Bit Offset
                    0x0000000000000000, // Address
                    ,)
            }, 

            One, 
            One, 
            0x03E8
        })
        Name (C6TM, Package (0x04)
        {
            ResourceTemplate ()
            {
                Register (SystemIO, 
                    0x08,               // Bit Width
                    0x00,               // Bit Offset
                    0x0000000000001815, // Address
                    ,)
            }, 

            0x02, 
            Zero, 
            0x015E
        })
        Name (C7TM, Package (0x04)
        {
            ResourceTemplate ()
            {
                Register (SystemIO, 
                    0x08,               // Bit Width
                    0x00,               // Bit Offset
                    0x0000000000001816, // Address
                    ,)
            }, 

            0x02, 
            Zero, 
            0xC8
        })
        Name (CDTM, Package (0x04)
        {
            ResourceTemplate ()
            {
                Register (SystemIO, 
                    0x08,               // Bit Width
                    0x00,               // Bit Offset
                    0x0000000000001816, // Address
                    ,)
            }, 

            0x03, 
            Zero, 
            Zero
        })
        Name (MWES, ResourceTemplate ()
        {
            Register (FFixedHW, 
                0x01,               // Bit Width
                0x02,               // Bit Offset
                0x0000000000000000, // Address
                0x01,               // Access Size
                )
        })
        Name (AC2V, Zero)
        Name (AC3V, Zero)
        Name (C3ST, Package (0x04)
        {
            0x03, 
            Package (0x00) {}, 
            Package (0x00) {}, 
            Package (0x00) {}
        })
        Name (C2ST, Package (0x03)
        {
            0x02, 
            Package (0x00) {}, 
            Package (0x00) {}
        })
        Name (C1ST, Package (0x02)
        {
            One, 
            Package (0x00) {}
        })
        Name (CSTF, Zero)
        Method (_CST, 0, Serialized)  // _CST: C-States
        {
            If (!CSTF)
            {
                C6TM [0x02] = C6LT /* External reference */
                C7TM [0x02] = C7LT /* External reference */
                CDTM [0x02] = CDLT /* External reference */
                CDTM [0x03] = CDPW /* External reference */
                DerefOf (CDTM [Zero]) [0x07] = CDLV /* External reference */
                If (((CFGD & 0x0800) && (PF00 & 0x0200)))
                {
                    C1TM [Zero] = MWES /* \_SB_.PR00.MWES */
                    C6TM [Zero] = MWES /* \_SB_.PR00.MWES */
                    C7TM [Zero] = MWES /* \_SB_.PR00.MWES */
                    CDTM [Zero] = MWES /* \_SB_.PR00.MWES */
                    DerefOf (C6TM [Zero]) [0x07] = C6MW /* External reference */
                    DerefOf (C7TM [Zero]) [0x07] = C7MW /* External reference */
                    DerefOf (CDTM [Zero]) [0x07] = CDMW /* External reference */
                }
                ElseIf (((CFGD & 0x0800) && (PF00 & 0x0100)))
                {
                    C1TM [Zero] = MWES /* \_SB_.PR00.MWES */
                }

                CSTF = Ones
            }

            AC2V = Zero
            AC3V = Zero
            C3ST [One] = C1TM /* \_SB_.PR00.C1TM */
            If ((CFGD & 0x20))
            {
                C3ST [0x02] = C7TM /* \_SB_.PR00.C7TM */
                AC2V = Ones
            }
            ElseIf ((CFGD & 0x10))
            {
                C3ST [0x02] = C6TM /* \_SB_.PR00.C6TM */
                AC2V = Ones
            }

            If ((CFGD & 0x4000))
            {
                C3ST [0x03] = CDTM /* \_SB_.PR00.CDTM */
                AC3V = Ones
            }

            If ((AC2V && AC3V))
            {
                Return (C3ST) /* \_SB_.PR00.C3ST */
            }
            ElseIf (AC2V)
            {
                C2ST [One] = DerefOf (C3ST [One])
                C2ST [0x02] = DerefOf (C3ST [0x02])
                Return (C2ST) /* \_SB_.PR00.C2ST */
            }
            ElseIf (AC3V)
            {
                C2ST [One] = DerefOf (C3ST [One])
                C2ST [0x02] = DerefOf (C3ST [0x03])
                DerefOf (C2ST [0x02]) [One] = 0x02
                Return (C2ST) /* \_SB_.PR00.C2ST */
            }
            Else
            {
                C1ST [One] = DerefOf (C3ST [One])
                Return (C1ST) /* \_SB_.PR00.C1ST */
            }
        }
    }
}

