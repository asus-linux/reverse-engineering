/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt4.dat, Thu Nov 16 10:41:40 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00004E78 (20088)
 *     Revision         0x01
 *     Checksum         0xFE
 *     OEM ID           "OptRf2"
 *     OEM Table ID     "Opt2Tabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 1, "OptRf2", "Opt2Tabl", 0x00001000)
{
    External (_SB_.ALIB, MethodObj)    // 2 Arguments
    External (_SB_.PCI0.GPP0, DeviceObj)
    External (_SB_.PCI0.GPP0._ADR, IntObj)
    External (_SB_.PCI0.SBRG.EC0_.BAY1, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.BAY2, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.CTMP, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.DMUS, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.ECOK, IntObj)
    External (_SB_.PCI0.SBRG.EC0_.EDAD, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.FTBL, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.G40C, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.GBLD, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.LRPL, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.LRPS, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.MOLR, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.MUT0, MutexObj)
    External (_SB_.PCI0.SBRG.EC0_.MUT1, MutexObj)
    External (_SB_.PCI0.SBRG.EC0_.NDF9, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.NVPS, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.PWMD, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.S4UM, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.SBLD, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.SDNT, FieldUnitObj)
    External (_SB_.PCI0.SBRG.EC0_.VGAT, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.VIUF, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.VRTT, UnknownObj)
    External (_SB_.PLTF.C000, DeviceObj)
    External (_SB_.PLTF.C001, DeviceObj)
    External (_SB_.PLTF.C002, DeviceObj)
    External (_SB_.PLTF.C003, DeviceObj)
    External (_SB_.PLTF.C004, DeviceObj)
    External (_SB_.PLTF.C005, DeviceObj)
    External (_SB_.PLTF.C006, DeviceObj)
    External (_SB_.PLTF.C007, DeviceObj)
    External (_SB_.PLTF.C008, DeviceObj)
    External (_SB_.PLTF.C009, DeviceObj)
    External (_SB_.PLTF.C00A, DeviceObj)
    External (_SB_.PLTF.C00B, DeviceObj)
    External (_SB_.PLTF.C00C, DeviceObj)
    External (_SB_.PLTF.C00D, DeviceObj)
    External (_SB_.PLTF.C00E, DeviceObj)
    External (_SB_.PLTF.C00F, DeviceObj)
    External (_SB_.PLTF.C010, DeviceObj)
    External (_SB_.PLTF.C011, DeviceObj)
    External (_SB_.PLTF.C012, DeviceObj)
    External (_SB_.PLTF.C013, DeviceObj)
    External (_SB_.PLTF.C014, DeviceObj)
    External (_SB_.PLTF.C015, DeviceObj)
    External (_SB_.PLTF.C016, DeviceObj)
    External (_SB_.PLTF.C017, DeviceObj)
    External (_SB_.PLTF.C018, DeviceObj)
    External (_SB_.PLTF.C019, DeviceObj)
    External (_SB_.PLTF.C01A, DeviceObj)
    External (_SB_.PLTF.C01B, DeviceObj)
    External (_SB_.PLTF.C01C, DeviceObj)
    External (_SB_.PLTF.C01D, DeviceObj)
    External (_SB_.PLTF.C01E, DeviceObj)
    External (_SB_.PLTF.C01F, DeviceObj)
    External (CUMA, IntObj)
    External (DPMF, UnknownObj)
    External (EID1, UnknownObj)
    External (EID2, UnknownObj)
    External (GPCE, IntObj)
    External (GTPM, IntObj)
    External (M009, MethodObj)    // 1 Arguments
    External (M010, MethodObj)    // 2 Arguments
    External (M017, MethodObj)    // 6 Arguments
    External (M018, MethodObj)    // 7 Arguments
    External (M019, MethodObj)    // 4 Arguments
    External (M020, MethodObj)    // 5 Arguments
    External (M023, MethodObj)    // 3 Arguments
    External (M027, MethodObj)    // 3 Arguments
    External (M028, MethodObj)    // 4 Arguments
    External (M037, DeviceObj)
    External (M046, IntObj)
    External (M047, IntObj)
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M23A, FieldUnitObj)
    External (M249, MethodObj)    // 4 Arguments
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M29A, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M31C, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M402, MethodObj)    // 3 Arguments
    External (M403, MethodObj)    // 4 Arguments
    External (M404, BuffObj)
    External (M408, MutexObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)
    External (M453, FieldUnitObj)
    External (M454, FieldUnitObj)
    External (M455, FieldUnitObj)
    External (M456, FieldUnitObj)
    External (M457, FieldUnitObj)
    External (M4C0, FieldUnitObj)
    External (M4F0, FieldUnitObj)
    External (M610, FieldUnitObj)
    External (M620, FieldUnitObj)
    External (M631, FieldUnitObj)
    External (M652, FieldUnitObj)
    External (NBFM, IntObj)
    External (PEBS, BuffFieldObj)
    External (PWEN, UnknownObj)
    External (PWOK, UnknownObj)
    External (REST, UnknownObj)
    External (SDMF, UnknownObj)
    External (UMFG, IntObj)
    External (WOSR, IntObj)

    Name (TOPT, 0x02)
    OperationRegion (TPNV, SystemMemory, 0x924DA000, 0x004D)
    Field (TPNV, AnyAcc, Lock, Preserve)
    {
        MXD1,   32, 
        MXD2,   32, 
        MXD3,   32, 
        MXD4,   32, 
        MXD5,   32, 
        MXD6,   32, 
        MXD7,   32, 
        MXD8,   32, 
        EBAS,   32, 
        DGVS,   32, 
        DGVB,   32, 
        HYSS,   32, 
        NVAF,   8, 
        DEID,   16, 
        DPMF,   8, 
        SDMF,   8, 
        SGFL,   8, 
        NVGA,   32, 
        NVHA,   32, 
        TCNT,   8, 
        CMPL,   8, 
        CNPL,   8, 
        CNLL,   8, 
        CLPL,   8, 
        KFAX,   8, 
        PTOF,   8, 
        TM00,   8, 
        TM01,   8, 
        TM02,   8, 
        TM03,   8, 
        TM04,   32
    }

    Method (TPV0, 0, Serialized)
    {
        M402 (Zero, One, One)
    }

    Method (TPV1, 0, Serialized)
    {
        M403 (Zero, One, One, One)
    }

    Method (TPV2, 0, Serialized)
    {
        M403 (Zero, One, One, Zero)
    }

    Scope (\_SB.PCI0.GPP0)
    {
        Method (BPGN, 0, Serialized)
        {
        }

        Method (APGN, 0, Serialized)
        {
        }

        Method (BPGF, 0, Serialized)
        {
        }

        Method (APGF, 0, Serialized)
        {
        }
    }

    Scope (\_SB.PCI0.GPP0)
    {
        Device (PEGP)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Method (_STA, 0, Serialized)  // _STA: Status
            {
                Local2 = M017 (Zero, One, One, 0x68, 0x04, One)
                If (((Local2 == Zero) && (\CUMA == Zero)))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (_EJ0, 1, NotSerialized)  // _EJx: Eject Device, x=0-9
            {
                If ((GPCE == One))
                {
                    \_SB.PCI0.GPP0.PG00._OFF ()
                    UMFG = One
                    WOSR = Zero
                }
            }

            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }

            OperationRegion (QMIP, SystemIO, 0xB2, One)
            Field (QMIP, ByteAcc, NoLock, Preserve)
            {
                QSMI,   8
            }

            Name (MMID, Package (0x02)
            {
                Package (0x03)
                {
                    Zero, 
                    "PI3DPX8", 
                    0x00030003
                }, 

                Package (0x03)
                {
                    One, 
                    "NON-MUX or Error", 
                    Zero
                }
            })
            Method (_DOD, 0, NotSerialized)  // _DOD: Display Output Devices
            {
                Return (Package (0x01)
                {
                    0x8000A450
                })
            }

            Device (LCD0)
            {
                Method (_ADR, 0, Serialized)  // _ADR: Address
                {
                    Return (0x8000A450)
                }

                Method (_DDC, 1, Serialized)  // _DDC: Display Data Current
                {
                    If (((Arg0 == One) || (Arg0 == 0x02)))
                    {
                        OperationRegion (EDMB, SystemMemory, \_SB.PCI0.SBRG.EC0.EDAD, 0x0100)
                        Field (EDMB, AnyAcc, NoLock, Preserve)
                        {
                            EDMD,   2048
                        }

                        Name (BUFF, Buffer (0x0100)
                        {
                             0x00                                             // .
                        })
                        BUFF = EDMD /* \_SB_.PCI0.GPP0.PEGP.LCD0._DDC.EDMD */
                        Return (BUFF) /* \_SB_.PCI0.GPP0.PEGP.LCD0._DDC.BUFF */
                    }

                    Return (Zero)
                }

                Method (MXDS, 1, NotSerialized)
                {
                    Local0 = Arg0
                    Local1 = (Local0 & 0x0F)
                    Local2 = (Local0 & 0x10)
                    If ((Local1 == Zero))
                    {
                        If ((\_SB.PCI0.SBRG.EC0.MOLR == Zero))
                        {
                            Return (One)
                        }
                        Else
                        {
                            Return (0x02)
                        }
                    }
                    ElseIf ((Local1 == One))
                    {
                        If ((Local2 == 0x10))
                        {
                            \_SB.PCI0.SBRG.EC0.DMUS = One
                        }
                        Else
                        {
                            \_SB.PCI0.SBRG.EC0.DMUS = Zero
                        }

                        Return (One)
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (MXDM, 1, NotSerialized)
                {
                    Local0 = Arg0
                    Local1 = (Local0 & 0x07)
                    If ((Local1 == Zero))
                    {
                        Local2 = DPMF /* \DPMF */
                        Return (Local2)
                    }
                    ElseIf ((Local1 < 0x05))
                    {
                        SDMF = Local1
                        QSMI = 0xB8
                    }
                    Else
                    {
                        Return (Zero)
                    }

                    Return (One)
                }

                Method (MXID, 1, NotSerialized)
                {
                    If ((Arg0 == Zero))
                    {
                        Local0 = DerefOf (DerefOf (MMID [Zero]) [0x02])
                        Return (Local0)
                    }
                }

                Method (LRST, 1, NotSerialized)
                {
                    Local0 = Arg0
                    Local1 = (Local0 & 0x07)
                    If ((Local1 == Zero))
                    {
                        If ((\_SB.PCI0.SBRG.EC0.LRPL == Zero))
                        {
                            Return (One)
                        }
                        Else
                        {
                            Return (0x02)
                        }
                    }
                    ElseIf ((Local1 == One))
                    {
                        \_SB.PCI0.SBRG.EC0.LRPS = Zero
                    }
                    ElseIf ((Local1 == 0x02))
                    {
                        \_SB.PCI0.SBRG.EC0.LRPS = One
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }
            }
        }

        Device (NHDA)
        {
            Name (_ADR, One)  // _ADR: Address
            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((M097 != Zero))
                {
                    Return (Zero)
                }
                Else
                {
                    Return (0x0F)
                }
            }
        }

        Method (_DSD, 0, Serialized)  // _DSD: Device-Specific Data
        {
            Return (Package (0x06)
            {
                ToUUID ("6b4ad420-8fd3-4364-acf8-eb94876fd9eb") /* Unknown UUID */, 
                Package (0x00) {}, 
                ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                Package (0x01)
                {
                    Package (0x02)
                    {
                        "HotPlugSupportInD3", 
                        One
                    }
                }, 

                ToUUID ("fdf06fad-f744-4451-bb64-ecd792215b10") /* Unknown UUID */, 
                Package (0x01)
                {
                    Package (0x02)
                    {
                        "FundamentalDeviceResetTriggeredOnD3ToD0", 
                        One
                    }
                }
            })
        }

        Method (PON, 0, NotSerialized)
        {
            M010 (REST, Zero)
            Sleep (One)
            M010 (PWEN, One)
            Sleep (0x78)
            M010 (REST, One)
            Sleep (0xC8)
        }

        Method (L23D, 1, Serialized)
        {
            Local1 = TM02 /* \TM02 */
            If ((Local1 == Zero))
            {
                Local1 = 0x0A
            }

            Sleep (Local1)
            If (((\UMFG == One) || (GPCE == 0x02)))
            {
                \_SB.ALIB (0x13, 0x09)
                Local0 = Zero
                While (((M017 (Zero, One, One, 0x6A, Zero, 0x04) < 0x04) || 
                    (M017 (Zero, One, One, 0x6A, 0x0D, One) != One)))
                {
                    If ((Local0 > 0x13BB))
                    {
                        Break
                    }

                    Stall (0x63)
                    Local0++
                }

                Local0 = Zero
                Local1 = (M249 (Zero, Zero, Zero, 0x11140294) & 0x3F)
                While ((Local1 != 0x10))
                {
                    If ((Local0 > 0x13BB))
                    {
                        Break
                    }

                    Stall (0x63)
                    Local1 = (M249 (Zero, Zero, Zero, 0x11140294) & 0x3F)
                    Local0++
                }
            }
            Else
            {
                \_SB.ALIB (0x13, 0x09)
                Local0 = Zero
                While (((M017 (Zero, One, One, 0x6A, Zero, 0x04) < 0x04) || 
                    (M017 (Zero, One, One, 0x6A, 0x0D, One) != One)))
                {
                    If ((Local0 > 0x13BB))
                    {
                        Break
                    }

                    Stall (0x63)
                    Local0++
                }

                Local0 = Zero
                Local1 = (M249 (Zero, Zero, Zero, 0x11140294) & 0x3F)
                While ((Local1 != 0x10))
                {
                    If ((Local0 > 0x13BB))
                    {
                        Break
                    }

                    Stall (0x63)
                    Local1 = (M249 (Zero, Zero, Zero, 0x11140294) & 0x3F)
                    Local0++
                }

                TPV2 ()
                Local1 = TM03 /* \TM03 */
                If ((Local1 == Zero))
                {
                    Local1 = 0x64
                }

                Sleep (Local1)
            }
        }

        Method (POFF, 0, NotSerialized)
        {
            M010 (REST, Zero)
            Sleep (One)
            M010 (PWEN, Zero)
            Sleep (0x64)
        }

        Method (DL23, 1, Serialized)
        {
            If ((Arg0 == One))
            {
                If (((\UMFG == One) || (GPCE == One)))
                {
                    LNKD = One
                }
                Else
                {
                    TPV0 ()
                    Local2 = M017 (Zero, One, One, 0x70, Zero, 0x10)
                    M018 (Zero, One, One, 0x70, Zero, 0x10, (Local2 & 0xEFD7))
                    \_SB.ALIB (0x12, 0x09)
                    Sleep (0x1E)
                }
            }

            Local1 = TM04 /* \TM04 */
            If ((Local1 == Zero))
            {
                Local1 = 0x0A
            }

            Sleep (Local1)
        }

        OperationRegion (RPCX, SystemMemory, ((\PEBS + (((\_SB.PCI0.GPP0._ADR & 0x00FF0000) >> 0x10) << 
            0x0F)) + ((\_SB.PCI0.GPP0._ADR & 0xFF) << 0x0C)), 0x1000)
        Field (RPCX, DWordAcc, NoLock, Preserve)
        {
            RVID,   32, 
            CMDR,   8, 
            Offset (0x19), 
            PRBN,   8, 
            Offset (0x54), 
            D0ST,   2, 
            Offset (0x62), 
            CEDR,   1, 
            Offset (0x68), 
            ASPM,   2, 
                ,   2, 
            LNKD,   1, 
            Offset (0x6A), 
            CULS,   4, 
            Offset (0x80), 
            Offset (0x81), 
                ,   2, 
            LREN,   1
        }

        Method (GSTA, 0, NotSerialized)
        {
            If ((M009 (REST) == Zero))
            {
                Return (Zero)
            }
            Else
            {
                Return (One)
            }
        }

        Name (TDGC, Zero)
        Name (DGCX, Zero)
        Name (TGPC, Buffer (0x04)
        {
             0x00                                             // .
        })
        PowerResource (PG00, 0x00, 0x0000)
        {
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((RVID == Ones))
                {
                    Return (Zero)
                }

                Return (GSTA ())
            }

            Method (_ON, 0, Serialized)  // _ON_: Power On
            {
                M023 (Zero, One, One)
                Acquire (\_SB.PCI0.SBRG.EC0.MUT0, 0x2000)
                \_SB.PCI0.SBRG.EC0.VGAT = One
                Release (\_SB.PCI0.SBRG.EC0.MUT0)
                Acquire (\_SB.PCI0.SBRG.EC0.MUT1, 0x2000)
                If ((\UMFG != One))
                {
                    If ((GSTA () != Zero))
                    {
                        Return (Zero)
                    }

                    Local4 = M453 /* External reference */
                    M453 = Zero
                    If ((\_SB.PCI0.GPP0.TDGC == One))
                    {
                        If ((\_SB.PCI0.GPP0.DGCX == 0x03))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6O ()
                        }
                        ElseIf ((\_SB.PCI0.GPP0.DGCX == 0x04))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6O ()
                        }

                        \_SB.PCI0.GPP0.TDGC = Zero
                        \_SB.PCI0.GPP0.DGCX = Zero
                    }
                    Else
                    {
                        \_SB.PCI0.GPP0.BPGN ()
                        \_SB.PCI0.GPP0.PON ()
                        \_SB.PCI0.GPP0.L23D (PTOF)
                        \_SB.PCI0.GPP0.LREN = \_SB.PCI0.GPP0.PEGP.LTRE
                        \_SB.PCI0.GPP0.CEDR = One
                        \_SB.PCI0.GPP0.PEGP.SSSV = HYSS /* \HYSS */
                        \_SB.PCI0.GPP0.APGN ()
                    }

                    M453 = Local4
                }

                If ((\_SB.PCI0.SBRG.EC0.S4UM == One))
                {
                    Sleep (0x03E8)
                    \_SB.PCI0.GPP0.PG00._OFF ()
                    UMFG = One
                    WOSR = Zero
                    \_SB.PCI0.SBRG.EC0.S4UM = Zero
                }

                Release (\_SB.PCI0.SBRG.EC0.MUT1)
            }

            Method (_OFF, 0, Serialized)  // _OFF: Power Off
            {
                Acquire (\_SB.PCI0.SBRG.EC0.MUT0, 0x2000)
                \_SB.PCI0.SBRG.EC0.VGAT = Zero
                Release (\_SB.PCI0.SBRG.EC0.MUT0)
                Acquire (\_SB.PCI0.SBRG.EC0.MUT1, 0x2000)
                If ((\UMFG != One))
                {
                    If ((GSTA () != One))
                    {
                        Return (Zero)
                    }

                    Local4 = M453 /* External reference */
                    M453 = Zero
                    If ((\_SB.PCI0.GPP0.TDGC == One))
                    {
                        CreateField (\_SB.PCI0.GPP0.TGPC, Zero, 0x03, GPPC)
                        If ((ToInteger (GPPC) == One))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6I ()
                        }
                        ElseIf ((ToInteger (GPPC) == 0x02))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6I ()
                        }
                    }
                    Else
                    {
                        Acquire (\_SB.PCI0.SBRG.EC0.MUT0, 0x2000)
                        Local0 = \_SB.PCI0.SBRG.EC0.VIUF /* External reference */
                        Release (\_SB.PCI0.SBRG.EC0.MUT0)
                        If ((Local0 == Zero))
                        {
                            \_SB.PCI0.GPP0.BPGF ()
                            \_SB.PCI0.GPP0.PEGP.LTRE = \_SB.PCI0.GPP0.LREN
                            Sleep (0x18)
                            Local1 = M019 (Zero, One, One, 0x54)
                            M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFC))
                            Sleep (One)
                            Local2 = M017 (Zero, One, One, 0x19, Zero, 0x08)
                            M028 (Local2, Zero, Zero, Zero)
                            Local3 = M027 (Local2, Zero, Zero)
                            M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFF))
                            \_SB.PCI0.GPP0.DL23 (PTOF)
                            \_SB.PCI0.GPP0.POFF ()
                            \_SB.PCI0.GPP0.APGF ()
                            M018 (Zero, One, One, 0x70, Zero, 0x10, Local2)
                            M023 (Zero, One, One)
                        }
                    }

                    M453 = Local4
                }

                Release (\_SB.PCI0.SBRG.EC0.MUT1)
            }
        }

        Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
        {
            PG00
        })
        Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
        {
            PG00
        })
        Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
        {
            PG00
        })
        Method (_S0W, 0, NotSerialized)  // _S0W: S0 Device Wake State
        {
            Return (0x04)
        }
    }

    Scope (\_SB.PCI0.GPP0.PEGP)
    {
        OperationRegion (PCIM, SystemMemory, EBAS, 0x0500)
        Field (PCIM, DWordAcc, Lock, Preserve)
        {
            NVID,   16, 
            NDID,   16, 
            CMDR,   8, 
            VGAR,   2008, 
            Offset (0x48B), 
                ,   1, 
            HDAE,   1
        }

        OperationRegion (MSID, SystemMemory, EBAS, 0x0500)
        Field (MSID, DWordAcc, NoLock, Preserve)
        {
            Offset (0x40), 
            SSSV,   32
        }

        Name (VGAB, Buffer (0xFA)
        {
             0x00                                             // .
        })
        Name (GPRF, Zero)
        Name (LTRE, Zero)
        Name (OMPR, 0x02)
        Name (DGPS, Zero)
        OperationRegion (NVHM, SystemMemory, NVHA, 0x00030400)
        Field (NVHM, DWordAcc, NoLock, Preserve)
        {
            NVSG,   128, 
            NVSZ,   32, 
            NVVR,   32, 
            NVHO,   32, 
            RVBS,   32, 
            RBF1,   262144, 
            RBF2,   262144, 
            RBF3,   262144, 
            RBF4,   262144, 
            RBF5,   262144, 
            RBF6,   262144, 
            MXML,   32, 
            MXM3,   1600
        }

        Method (SGST, 0, Serialized)
        {
            If ((NVID != 0xFFFF))
            {
                Return (0x0F)
            }

            Return (Zero)
        }

        Name (_PSC, Zero)  // _PSC: Power State Current
        Method (_PS0, 0, NotSerialized)  // _PS0: Power State 0
        {
            If ((\CUMA == Zero))
            {
                _PSC = Zero
                If ((DGPS != Zero))
                {
                    \_SB.PCI0.SBRG.EC0.NVPS = One
                    \_SB.PCI0.GPP0.PG00._ON ()
                    If ((GPRF != One))
                    {
                        VGAR = VGAB /* \_SB_.PCI0.GPP0.PEGP.VGAB */
                    }

                    DGPS = Zero
                    \_SB.PCI0.SBRG.EC0.NVPS = Zero
                }
            }
        }

        Method (_PS3, 0, NotSerialized)  // _PS3: Power State 3
        {
            If ((\CUMA == Zero))
            {
                If ((OMPR == 0x03))
                {
                    If ((GPRF != One))
                    {
                        VGAB = VGAR /* \_SB_.PCI0.GPP0.PEGP.VGAR */
                    }

                    \_SB.PCI0.GPP0.PG00._OFF ()
                    DGPS = One
                    OMPR = 0x02
                }

                _PSC = 0x03
            }
        }

        Method (NBCI, 4, Serialized)
        {
            Debug = "<<< NBCI >>>"
            If ((Arg1 != 0x0102))
            {
                Return (0x80000002)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Return (Buffer (0x04)
                    {
                         0x01, 0x00, 0x11, 0x00                           // ....
                    })
                }
                Case (0x10)
                {
                    CreateWordField (Arg3, 0x02, USRG)
                    OperationRegion (EDM2, SystemMemory, (\_SB.PCI0.SBRG.EC0.EDAD + 0x08), 0x0100)
                    Field (EDM2, AnyAcc, NoLock, Preserve)
                    {
                        PDVD,   32
                    }

                    Name (BF22, Buffer (0x04)
                    {
                         0x00                                             // .
                    })
                    If ((USRG == 0x4452))
                    {
                        Debug = "Get DR key"
                        If ((PDVD == 0x0B68E509))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0x2A, 0x28, 0xAE, 0x6D, 0x9F, 0xDE, 0x01, 0xAD,  // *(.m....
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x35, 0x00, 0x2A,  // .1.6.5.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x35, 0x00, 0x34,  // .1.4.5.4
                                /* 00A8 */  0x00, 0x35, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .5.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x34, 0x00, 0x30, 0x00, 0x30,  // .*.4.0.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x1574104D))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0x2A, 0x28, 0xAE, 0x6D, 0x9F, 0xDE, 0x01, 0xAD,  // *(.m....
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x35, 0x00, 0x2A,  // .1.6.5.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x35, 0x00, 0x34,  // .1.4.5.4
                                /* 00A8 */  0x00, 0x35, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .5.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x34, 0x00, 0x30, 0x00, 0x30,  // .*.4.0.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x1556AE0D))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0xA3, 0x7E, 0x9A, 0xE8, 0x4A, 0xE3, 0x16, 0x68,  // .~..J..h
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x34, 0x00, 0x2A,  // .1.4.4.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x36, 0x00, 0x36,  // .1.6.6.6
                                /* 00A8 */  0x00, 0x37, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .7.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x32, 0x00, 0x35, 0x00, 0x30,  // .*.2.5.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0xD2A2AF06))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0xB7, 0xAD, 0x06, 0xB9, 0x93, 0xDA, 0xE9, 0x87,  // ........
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x34, 0x00, 0x2A,  // .1.4.4.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x35, 0x00, 0x38, 0x00, 0x37,  // .1.5.8.7
                                /* 00A8 */  0x00, 0x33, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .3.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x33, 0x00, 0x33, 0x00, 0x33,  // .*.3.3.3
                                /* 00B8 */  0x00, 0x33, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00,  // .3.3....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x17026F0E))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0x7E, 0x78, 0x9F, 0x8B, 0x09, 0x99, 0x0D, 0x16,  // ~x......
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x32, 0x00, 0x34, 0x00, 0x30, 0x00, 0x2A,  // .2.4.0.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30,  // .1.0.0.0
                                /* 00A8 */  0x00, 0x30, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .0.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x34, 0x00, 0x30, 0x00, 0x30,  // .*.4.0.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x0B69E509))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0x7E, 0x78, 0x9F, 0x8B, 0x09, 0x99, 0x0D, 0x16,  // ~x......
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x32, 0x00, 0x34, 0x00, 0x30, 0x00, 0x2A,  // .2.4.0.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x30, 0x00, 0x30, 0x00, 0x30,  // .1.0.0.0
                                /* 00A8 */  0x00, 0x30, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .0.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x34, 0x00, 0x30, 0x00, 0x30,  // .*.4.0.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x0B6BE509))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0x31, 0x6D, 0xC3, 0x9A, 0xD6, 0xF3, 0x45, 0x91,  // 1m....E.
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x34, 0x00, 0x2A,  // .1.4.4.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x36, 0x00, 0x36,  // .1.6.6.6
                                /* 00A8 */  0x00, 0x36, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .6.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x34, 0x00, 0x30, 0x00, 0x30,  // .*.4.0.0
                                /* 00B8 */  0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,  // .0.0....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0xDDA7AF06))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0xB7, 0xAD, 0x06, 0xB9, 0x93, 0xDA, 0xE9, 0x87,  // ........
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x34, 0x00, 0x2A,  // .1.4.4.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x35, 0x00, 0x38, 0x00, 0x37,  // .1.5.8.7
                                /* 00A8 */  0x00, 0x33, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .3.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x33, 0x00, 0x33, 0x00, 0x33,  // .*.3.3.3
                                /* 00B8 */  0x00, 0x33, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00,  // .3.3....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x0B70E509))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0xA8, 0x12, 0xBA, 0x5F, 0x96, 0x41, 0x8E, 0x29,  // ..._.A.)
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x34, 0x00, 0x2A,  // .1.4.4.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x36, 0x00, 0x36,  // .1.6.6.6
                                /* 00A8 */  0x00, 0x37, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .7.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x33, 0x00, 0x35, 0x00, 0x37,  // .*.3.5.7
                                /* 00B8 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00,  // .1.4....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }

                        If ((PDVD == 0x1555AE0D))
                        {
                            Return (Buffer (0xD7)
                            {
                                /* 0000 */  0xBB, 0x82, 0x1B, 0x12, 0xEF, 0x27, 0x9E, 0x89,  // .....'..
                                /* 0008 */  0x52, 0x44, 0xD7, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
                                /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
                                /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
                                /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
                                /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
                                /* 0038 */  0x87, 0x00, 0x00, 0x00, 0x04, 0x00, 0x85, 0x00,  // ........
                                /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x83, 0x00, 0x00, 0x00,  // ........
                                /* 0048 */  0x06, 0x00, 0x81, 0x00, 0x00, 0x00, 0x07, 0x00,  // ........
                                /* 0050 */  0x7F, 0x00, 0x00, 0x00, 0x08, 0x00, 0x7D, 0x00,  // ......}.
                                /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
                                /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
                                /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
                                /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
                                /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
                                /* 0080 */  0x00, 0x02, 0x00, 0x00, 0x00, 0xE0, 0x7C, 0x97,  // ......|.
                                /* 0088 */  0x01, 0xC4, 0xD5, 0xC4, 0x32, 0x00, 0x00, 0x00,  // ....2...
                                /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00,  // .....&..
                                /* 0098 */  0x00, 0x31, 0x00, 0x36, 0x00, 0x35, 0x00, 0x2A,  // .1.6.5.*
                                /* 00A0 */  0x00, 0x31, 0x00, 0x34, 0x00, 0x37, 0x00, 0x30,  // .1.4.7.0
                                /* 00A8 */  0x00, 0x35, 0x00, 0x3B, 0x00, 0x36, 0x00, 0x30,  // .5.;.6.0
                                /* 00B0 */  0x00, 0x2A, 0x00, 0x33, 0x00, 0x33, 0x00, 0x33,  // .*.3.3.3
                                /* 00B8 */  0x00, 0x33, 0x00, 0x33, 0x00, 0x00, 0x00, 0x00,  // .3.3....
                                /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         // .......
                            })
                        }
                    }

                    If ((USRG == 0x564B))
                    {
                        Debug = "Get VK key"
                        If ((PDVD == 0x0B68E509))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x31, 0x80, 0x2F, 0x3A, 0xD0, 0x4C, 0x11, 0x74,  // 1./:.L.t
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x56, 0x57,  // m for VW
                                /* 0050 */  0x55, 0x41, 0x44, 0x5A, 0x4B, 0x49, 0x49, 0x4D,  // UADZKIIM
                                /* 0058 */  0x4A, 0x45, 0x57, 0x4B, 0x45, 0x4F, 0x55, 0x47,  // JEWKEOUG
                                /* 0060 */  0x4C, 0x46, 0x20, 0x2D, 0x20, 0x33, 0x3E, 0x42,  // LF - 3>B
                                /* 0068 */  0x58, 0x49, 0x54, 0x27, 0x43, 0x40, 0x35, 0x46,  // XIT'C@5F
                                /* 0070 */  0x33, 0x5C, 0x4C, 0x24, 0x40, 0x54, 0x4C, 0x32,  // 3\L$@TL2
                                /* 0078 */  0x5D, 0x2C, 0x36, 0x5E, 0x5A, 0x28, 0x41, 0x4C,  // ],6^Z(AL
                                /* 0080 */  0x5C, 0x21, 0x5B, 0x2E, 0x20, 0x48, 0x5E, 0x4C,  // \![. H^L
                                /* 0088 */  0x3F, 0x38, 0x42, 0x44, 0x25, 0x20, 0x2D, 0x20,  // ?8BD% - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }

                        If ((PDVD == 0x1574104D))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x09, 0x2C, 0x11, 0x6C, 0x1E, 0x63, 0x6A, 0xC7,  // .,.l.cj.
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x50, 0x49,  // m for PI
                                /* 0050 */  0x41, 0x45, 0x41, 0x55, 0x4B, 0x55, 0x44, 0x47,  // AEAUKUDG
                                /* 0058 */  0x50, 0x4E, 0x4C, 0x4B, 0x44, 0x45, 0x46, 0x57,  // PNLKDEFW
                                /* 0060 */  0x4C, 0x54, 0x20, 0x2D, 0x20, 0x2F, 0x58, 0x3B,  // LT - /X;
                                /* 0068 */  0x33, 0x3C, 0x46, 0x37, 0x32, 0x4C, 0x3F, 0x4E,  // 3<F72L?N
                                /* 0070 */  0x35, 0x40, 0x4E, 0x24, 0x42, 0x50, 0x46, 0x2A,  // 5@N$BPF*
                                /* 0078 */  0x4B, 0x20, 0x38, 0x2E, 0x28, 0x34, 0x2B, 0x54,  // K 8.(4+T
                                /* 0080 */  0x26, 0x44, 0x39, 0x4E, 0x46, 0x59, 0x35, 0x20,  // &D9NFY5 
                                /* 0088 */  0x5F, 0x2A, 0x24, 0x35, 0x5E, 0x20, 0x2D, 0x20,  // _*$5^ - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }

                        If ((PDVD == 0x1556AE0D))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x3C, 0x38, 0x52, 0x8D, 0xD7, 0xAE, 0x3A, 0xFD,  // <8R...:.
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x47, 0x4B,  // m for GK
                                /* 0050 */  0x57, 0x48, 0x53, 0x48, 0x43, 0x4B, 0x53, 0x45,  // WHSHCKSE
                                /* 0058 */  0x47, 0x50, 0x58, 0x5A, 0x41, 0x41, 0x49, 0x53,  // GPXZAAIS
                                /* 0060 */  0x4E, 0x4C, 0x20, 0x2D, 0x20, 0x47, 0x33, 0x57,  // NL - G3W
                                /* 0068 */  0x2D, 0x5C, 0x21, 0x52, 0x48, 0x57, 0x49, 0x53,  // -\!RHWIS
                                /* 0070 */  0x47, 0x53, 0x59, 0x59, 0x5B, 0x2B, 0x41, 0x2F,  // GSYY[+A/
                                /* 0078 */  0x48, 0x21, 0x3B, 0x2B, 0x2F, 0x3F, 0x56, 0x59,  // H!;+/?VY
                                /* 0080 */  0x57, 0x3A, 0x2E, 0x23, 0x5D, 0x43, 0x25, 0x41,  // W:.#]C%A
                                /* 0088 */  0x3E, 0x4D, 0x41, 0x52, 0x24, 0x20, 0x2D, 0x20,  // >MAR$ - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }

                        If ((PDVD == 0xD2A2AF06))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x95, 0x7D, 0x5D, 0x33, 0xDD, 0x65, 0x39, 0x72,  // .}]3.e9r
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x53, 0x49,  // m for SI
                                /* 0050 */  0x41, 0x4F, 0x48, 0x59, 0x59, 0x44, 0x51, 0x41,  // AOHYYDQA
                                /* 0058 */  0x49, 0x51, 0x55, 0x43, 0x4F, 0x45, 0x42, 0x44,  // IQUCOEBD
                                /* 0060 */  0x4C, 0x41, 0x20, 0x2D, 0x20, 0x29, 0x52, 0x31,  // LA - )R1
                                /* 0068 */  0x48, 0x36, 0x58, 0x29, 0x3C, 0x46, 0x39, 0x40,  // H6X)<F9@
                                /* 0070 */  0x33, 0x5A, 0x40, 0x22, 0x58, 0x2A, 0x40, 0x2C,  // 3Z@"X*@,
                                /* 0078 */  0x4D, 0x3E, 0x41, 0x24, 0x22, 0x4F, 0x25, 0x2E,  // M>A$"O%.
                                /* 0080 */  0x3C, 0x42, 0x37, 0x44, 0x3F, 0x23, 0x38, 0x3E,  // <B7D?#8>
                                /* 0088 */  0x23, 0x51, 0x39, 0x3A, 0x56, 0x20, 0x2D, 0x20,  // #Q9:V - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }

                        If ((PDVD == 0x17026F0E)) {}
                        If ((PDVD == 0x0B69E509)) {}
                        If ((PDVD == 0x0B6BE509)) {}
                        If ((PDVD == 0xDDA7AF06)) {}
                        If ((PDVD == 0x0B70E509))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x76, 0x51, 0x57, 0xFB, 0xC8, 0x43, 0x76, 0x4C,  // vQW..CvL
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x5A, 0x48,  // m for ZH
                                /* 0050 */  0x59, 0x46, 0x49, 0x49, 0x56, 0x47, 0x4A, 0x4B,  // YFIIVGJK
                                /* 0058 */  0x43, 0x45, 0x44, 0x52, 0x53, 0x51, 0x48, 0x4B,  // CEDRSQHK
                                /* 0060 */  0x45, 0x43, 0x20, 0x2D, 0x20, 0x54, 0x40, 0x20,  // EC - T@ 
                                /* 0068 */  0x39, 0x29, 0x4A, 0x45, 0x20, 0x3C, 0x2F, 0x38,  // 9)JE </8
                                /* 0070 */  0x2B, 0x34, 0x3B, 0x39, 0x3B, 0x49, 0x3F, 0x4C,  // +4;9;I?L
                                /* 0078 */  0x2D, 0x5F, 0x24, 0x43, 0x46, 0x55, 0x4B, 0x36,  // -_$CFUK6
                                /* 0080 */  0x44, 0x28, 0x5D, 0x33, 0x2C, 0x55, 0x58, 0x4D,  // D(]3,UXM
                                /* 0088 */  0x3F, 0x37, 0x3F, 0x58, 0x29, 0x20, 0x2D, 0x20,  // ?7?X) - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }

                        If ((PDVD == 0x1555AE0D))
                        {
                            Return (Buffer (0xD5)
                            {
                                /* 0000 */  0x9B, 0x21, 0x9D, 0xE3, 0xA3, 0x3F, 0xB2, 0x45,  // .!...?.E
                                /* 0008 */  0x4B, 0x56, 0xD5, 0x00, 0x00, 0x00, 0x01, 0x00,  // KV......
                                /* 0010 */  0x37, 0x35, 0x31, 0x31, 0x31, 0x35, 0x38, 0x37,  // 75111587
                                /* 0018 */  0x39, 0x38, 0x34, 0x39, 0x47, 0x65, 0x6E, 0x75,  // 9849Genu
                                /* 0020 */  0x69, 0x6E, 0x65, 0x20, 0x4E, 0x56, 0x49, 0x44,  // ine NVID
                                /* 0028 */  0x49, 0x41, 0x20, 0x43, 0x65, 0x72, 0x74, 0x69,  // IA Certi
                                /* 0030 */  0x66, 0x69, 0x65, 0x64, 0x20, 0x47, 0x53, 0x79,  // fied GSy
                                /* 0038 */  0x6E, 0x63, 0x20, 0x52, 0x65, 0x61, 0x64, 0x79,  // nc Ready
                                /* 0040 */  0x20, 0x50, 0x6C, 0x61, 0x74, 0x66, 0x6F, 0x72,  //  Platfor
                                /* 0048 */  0x6D, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x42, 0x4A,  // m for BJ
                                /* 0050 */  0x46, 0x49, 0x45, 0x53, 0x54, 0x4D, 0x52, 0x5A,  // FIESTMRZ
                                /* 0058 */  0x4B, 0x4D, 0x48, 0x44, 0x47, 0x51, 0x46, 0x51,  // KMHDGQFQ
                                /* 0060 */  0x53, 0x4A, 0x20, 0x2D, 0x20, 0x42, 0x30, 0x50,  // SJ - B0P
                                /* 0068 */  0x2E, 0x5B, 0x26, 0x51, 0x55, 0x52, 0x46, 0x50,  // .[&QURFP
                                /* 0070 */  0x42, 0x2E, 0x5E, 0x5A, 0x56, 0x26, 0x5E, 0x2C,  // B.^ZV&^,
                                /* 0078 */  0x4F, 0x22, 0x38, 0x2C, 0x2C, 0x3A, 0x53, 0x5E,  // O"8,,:S^
                                /* 0080 */  0x52, 0x37, 0x29, 0x5C, 0x52, 0x3F, 0x58, 0x5E,  // R7)\R?X^
                                /* 0088 */  0x33, 0x48, 0x44, 0x51, 0x3A, 0x20, 0x2D, 0x20,  // 3HDQ: - 
                                /* 0090 */  0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68,  // Copyrigh
                                /* 0098 */  0x74, 0x20, 0x32, 0x30, 0x31, 0x34, 0x20, 0x4E,  // t 2014 N
                                /* 00A0 */  0x56, 0x49, 0x44, 0x49, 0x41, 0x20, 0x43, 0x6F,  // VIDIA Co
                                /* 00A8 */  0x72, 0x70, 0x6F, 0x72, 0x61, 0x74, 0x69, 0x6F,  // rporatio
                                /* 00B0 */  0x6E, 0x20, 0x41, 0x6C, 0x6C, 0x20, 0x52, 0x69,  // n All Ri
                                /* 00B8 */  0x67, 0x68, 0x74, 0x73, 0x20, 0x52, 0x65, 0x73,  // ghts Res
                                /* 00C0 */  0x65, 0x72, 0x76, 0x65, 0x64, 0x2D, 0x35, 0x38,  // erved-58
                                /* 00C8 */  0x39, 0x36, 0x38, 0x34, 0x30, 0x32, 0x39, 0x33,  // 96840293
                                /* 00D0 */  0x38, 0x35, 0x28, 0x52, 0x29                     // 85(R)
                            })
                        }
                    }

                    Return (0x80000002)
                }
                Case (0x14)
                {
                    Return (Package (0x20)
                    {
                        0x8000A450, 
                        0x0200, 
                        Zero, 
                        Zero, 
                        0x05, 
                        One, 
                        0xC8, 
                        0x32, 
                        0x03E8, 
                        0x0B, 
                        0x32, 
                        0x64, 
                        0x96, 
                        0xC8, 
                        0x012C, 
                        0x0190, 
                        0x01FE, 
                        0x0276, 
                        0x02F8, 
                        0x0366, 
                        0x03E8, 
                        Zero, 
                        0x64, 
                        0xC8, 
                        0x012C, 
                        0x0190, 
                        0x01F4, 
                        0x0258, 
                        0x02BC, 
                        0x0320, 
                        0x0384, 
                        0x03E8
                    })
                }

            }

            Return (0x80000002)
        }

        Method (NVOP, 4, Serialized)
        {
            Debug = "<<< NVOP >>>"
            If ((Arg1 != 0x0100))
            {
                Return (0x80000002)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Return (Buffer (0x04)
                    {
                         0x01, 0x00, 0x00, 0x04                           // ....
                    })
                }
                Case (0x1A)
                {
                    CreateField (Arg3, Zero, One, FLCH)
                    CreateField (Arg3, One, One, DVSR)
                    CreateField (Arg3, 0x02, One, DVSC)
                    CreateField (Arg3, 0x18, 0x02, OPCE)
                    If ((ToInteger (FLCH) & (ToInteger (OPCE) != OMPR)))
                    {
                        \_SB.PCI0.GPP0.PEGP.OMPR = OPCE /* \_SB_.PCI0.GPP0.PEGP.NVOP.OPCE */
                    }

                    Local0 = Buffer (0x04)
                        {
                             0x00, 0x00, 0x00, 0x00                           // ....
                        }
                    CreateField (Local0, Zero, One, OPEN)
                    CreateField (Local0, 0x03, 0x02, CGCS)
                    CreateField (Local0, 0x06, One, SHPC)
                    CreateField (Local0, 0x08, One, SNSR)
                    CreateField (Local0, 0x18, 0x03, DGPC)
                    CreateField (Local0, 0x1B, 0x02, OHAC)
                    OPEN = One
                    SHPC = One
                    DGPC = One
                    OHAC = 0x03
                    If (ToInteger (DVSC))
                    {
                        If (ToInteger (DVSR))
                        {
                            GPRF = One
                        }
                        Else
                        {
                            GPRF = Zero
                        }
                    }

                    SNSR = GPRF /* \_SB_.PCI0.GPP0.PEGP.GPRF */
                    If ((DGPS == Zero))
                    {
                        CGCS = 0x03
                    }
                    Else
                    {
                        CGCS = Zero
                    }

                    Return (Local0)
                }
                Case (0x1B)
                {
                    Name (BUFF, Zero)
                    CreateField (Arg3, Zero, One, OACC)
                    CreateField (Arg3, One, One, UOAC)
                    CreateField (Arg3, 0x02, 0x08, OPDA)
                    CreateField (Arg3, 0x0A, One, OPDE)
                    Local1 = Zero
                    Local1 = \_SB.PCI0.GPP0.PEGP.HDAE
                    Return (Local1)
                }

            }

            Return (0x80000002)
        }

        Method (_ROM, 2, NotSerialized)  // _ROM: Read-Only Memory
        {
            Local0 = Arg0
            Local1 = Arg1
            If ((Local1 > 0x1000))
            {
                Local1 = 0x1000
            }

            If ((Local0 > 0x00030000))
            {
                Return (Buffer (Local1)
                {
                     0x00                                             // .
                })
            }

            Local3 = (Local1 * 0x08)
            Name (ROM1, Buffer (0x8000)
            {
                 0x00                                             // .
            })
            Name (ROM2, Buffer (Local1)
            {
                 0x00                                             // .
            })
            If ((Local0 < 0x8000))
            {
                ROM1 = RBF1 /* \_SB_.PCI0.GPP0.PEGP.RBF1 */
            }
            ElseIf ((Local0 < 0x00010000))
            {
                Local0 -= 0x8000
                ROM1 = RBF2 /* \_SB_.PCI0.GPP0.PEGP.RBF2 */
            }
            ElseIf ((Local0 < 0x00018000))
            {
                Local0 -= 0x00010000
                ROM1 = RBF3 /* \_SB_.PCI0.GPP0.PEGP.RBF3 */
            }
            ElseIf ((Local0 < 0x00020000))
            {
                Local0 -= 0x00018000
                ROM1 = RBF4 /* \_SB_.PCI0.GPP0.PEGP.RBF4 */
            }
            ElseIf ((Local0 < 0x00028000))
            {
                Local0 -= 0x00020000
                ROM1 = RBF5 /* \_SB_.PCI0.GPP0.PEGP.RBF5 */
            }
            ElseIf ((Local0 < 0x00030000))
            {
                Local0 -= 0x00028000
                ROM1 = RBF6 /* \_SB_.PCI0.GPP0.PEGP.RBF6 */
            }

            Local2 = (Local0 * 0x08)
            CreateField (ROM1, Local2, Local3, TMPB)
            ROM2 = TMPB /* \_SB_.PCI0.GPP0.PEGP._ROM.TMPB */
            Return (ROM2) /* \_SB_.PCI0.GPP0.PEGP._ROM.ROM2 */
        }

        Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
        {
            CreateByteField (Arg0, 0x03, GUID)
            If ((Arg0 == ToUUID ("d4a50b75-65c7-46f7-bfb7-41514cea0244") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NBCI (Arg0, Arg1, Arg2, Arg3))
            }

            If ((Arg0 == ToUUID ("a3132d01-8cda-49ba-a52e-bc9d46df6b81") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.GPS (Arg0, Arg1, Arg2, Arg3))
            }

            If ((Arg0 == ToUUID ("cbeca351-067b-4924-9cbd-b46b00b86f34") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NVJT (Arg0, Arg1, Arg2, Arg3))
            }

            If ((Arg0 == ToUUID ("a486d8f8-0bda-471b-a72b-6042a6b5bee0") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NVOP (Arg0, Arg1, Arg2, Arg3))
            }

            Return (0x80000001)
        }
    }

    Scope (\_SB.PCI0.GPP0.PEGP)
    {
        Method (GC6I, 0, Serialized)
        {
            Debug = "<<< GC6I >>>"
            \_SB.PCI0.GPP0.PEGP.LTRE = \_SB.PCI0.GPP0.LREN
            \_SB.PCI0.GPP0.DL23 (One)
            Sleep (0x0A)
            M010 (REST, Zero)
        }

        Method (GC6O, 0, Serialized)
        {
            Debug = "<<< GC6O >>>"
            M010 (REST, One)
            Sleep (0x0A)
            \_SB.PCI0.GPP0.L23D (One)
            \_SB.PCI0.GPP0.CMDR |= 0x04
            \_SB.PCI0.GPP0.D0ST = Zero
            While ((\_SB.PCI0.GPP0.PEGP.NVID != 0x10DE))
            {
                Sleep (One)
            }

            \_SB.PCI0.GPP0.LREN = \_SB.PCI0.GPP0.PEGP.LTRE
            \_SB.PCI0.GPP0.CEDR = One
        }

        Method (NVJT, 4, Serialized)
        {
            Debug = "------- NV JT DSM --------"
            If ((Arg1 < 0x0100))
            {
                Return (0x80000001)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Debug = "JT fun0 JT_FUNC_SUPPORT"
                    Return (Buffer (0x04)
                    {
                         0x1B, 0x00, 0x00, 0x00                           // ....
                    })
                }
                Case (One)
                {
                    Debug = "JT fun1 JT_FUNC_CAPS"
                    Name (JTCA, Buffer (0x04)
                    {
                         0x00                                             // .
                    })
                    CreateField (JTCA, Zero, One, JTEN)
                    CreateField (JTCA, One, 0x02, SREN)
                    CreateField (JTCA, 0x03, 0x02, PLPR)
                    CreateField (JTCA, 0x05, One, SRPR)
                    CreateField (JTCA, 0x06, 0x02, FBPR)
                    CreateField (JTCA, 0x08, 0x02, GUPR)
                    CreateField (JTCA, 0x0A, One, GC6R)
                    CreateField (JTCA, 0x0B, One, PTRH)
                    CreateField (JTCA, 0x0D, One, MHYB)
                    CreateField (JTCA, 0x0E, One, RPCL)
                    CreateField (JTCA, 0x0F, 0x02, GC6V)
                    CreateField (JTCA, 0x11, One, GEIS)
                    CreateField (JTCA, 0x12, One, GSWS)
                    CreateField (JTCA, 0x14, 0x0C, JTRV)
                    JTEN = One
                    GC6R = Zero
                    RPCL = One
                    SREN = One
                    FBPR = Zero
                    MHYB = One
                    GC6V = 0x02
                    JTRV = 0x0200
                    Return (JTCA) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTCA */
                }
                Case (0x02)
                {
                    Debug = "JT fun2 JT_FUNC_POLICYSELECT"
                    Return (0x80000002)
                }
                Case (0x03)
                {
                    Debug = "JT fun3 JT_FUNC_POWERCONTROL"
                    CreateField (Arg3, Zero, 0x03, GPPC)
                    CreateField (Arg3, 0x04, One, PLPC)
                    CreateField (Arg3, 0x07, One, ECOC)
                    CreateField (Arg3, 0x0E, 0x02, DFGC)
                    CreateField (Arg3, 0x10, 0x03, GPCX)
                    \_SB.PCI0.GPP0.TGPC = Arg3
                    If (((ToInteger (GPPC) != Zero) || (ToInteger (DFGC
                        ) != Zero)))
                    {
                        TDGC = ToInteger (DFGC)
                        DGCX = ToInteger (GPCX)
                    }

                    Name (JTPC, Buffer (0x04)
                    {
                         0x00                                             // .
                    })
                    CreateField (JTPC, Zero, 0x03, GUPS)
                    CreateField (JTPC, 0x03, One, GPWO)
                    CreateField (JTPC, 0x07, One, PLST)
                    If ((ToInteger (DFGC) != Zero))
                    {
                        GPWO = One
                        GUPS = One
                        Return (JTPC) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTPC */
                    }

                    If ((ToInteger (GPPC) == One))
                    {
                        GC6I ()
                        PLST = One
                        GUPS = Zero
                    }
                    ElseIf ((ToInteger (GPPC) == 0x02))
                    {
                        GC6I ()
                        If ((ToInteger (PLPC) == Zero))
                        {
                            PLST = Zero
                        }

                        GUPS = Zero
                    }
                    ElseIf ((ToInteger (GPPC) == 0x03))
                    {
                        GC6O ()
                        If ((ToInteger (PLPC) != Zero))
                        {
                            PLST = Zero
                        }

                        GPWO = One
                        GUPS = One
                    }
                    ElseIf ((ToInteger (GPPC) == 0x04))
                    {
                        GC6O ()
                        If ((ToInteger (PLPC) != Zero))
                        {
                            PLST = Zero
                        }

                        GPWO = One
                        GUPS = One
                    }
                    ElseIf ((M009 (0x28) == One))
                    {
                        Debug = "   JT GETS() return 0x1"
                        GPWO = One
                        GUPS = One
                    }
                    Else
                    {
                        Debug = "   JT GETS() return 0x3"
                        GPWO = Zero
                        GUPS = 0x03
                    }

                    Return (JTPC) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTPC */
                }
                Case (0x04)
                {
                    Debug = "   JT fun4 JT_FUNC_PLATPOLICY"
                    CreateField (Arg3, 0x02, One, PAUD)
                    CreateField (Arg3, 0x03, One, PADM)
                    CreateField (Arg3, 0x04, 0x04, PDGS)
                    Local0 = Zero
                    Local0 = (\_SB.PCI0.GPP0.PEGP.HDAE << 0x02)
                    Return (Local0)
                }

            }

            Return (0x80000002)
        }
    }

    Scope (\_SB.PCI0.GPP0.PEGP)
    {
        Name (NLIM, Zero)
        Name (PSLS, Zero)
        Name (CTGP, Zero)
        Name (GPSP, Buffer (0x28) {})
        CreateDWordField (GPSP, Zero, RETN)
        CreateDWordField (GPSP, 0x04, VRV1)
        CreateDWordField (GPSP, 0x08, TGPU)
        CreateDWordField (GPSP, 0x0C, PDTS)
        CreateDWordField (GPSP, 0x10, SFAN)
        CreateDWordField (GPSP, 0x14, SKNT)
        CreateDWordField (GPSP, 0x18, CPUE)
        CreateDWordField (GPSP, 0x1C, TMP1)
        CreateDWordField (GPSP, 0x20, TMP2)
        Method (GPS, 4, Serialized)
        {
            Debug = "------- NV GPS DSM Peg GN20--------"
            If ((Arg1 != 0x0200))
            {
                Return (0x80000002)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Debug = "GPS fun 0"
                    Return (Buffer (0x08)
                    {
                         0x01, 0x00, 0x08, 0x00, 0x01, 0x04, 0x00, 0x00   // ........
                    })
                }
                Case (0x13)
                {
                    Debug = "   GPS fun 19"
                    If (\_SB.PCI0.SBRG.EC0.ECOK)
                    {
                        Acquire (\_SB.PCI0.SBRG.EC0.MUT0, 0x2000)
                        \_SB.PCI0.SBRG.EC0.SDNT = One
                        Release (\_SB.PCI0.SBRG.EC0.MUT0)
                    }

                    CreateDWordField (Arg3, Zero, TEMP)
                    If ((TEMP == Zero))
                    {
                        Return (0x04)
                    }

                    If ((TEMP && 0x04))
                    {
                        Return (0x04)
                    }
                }
                Case (0x20)
                {
                    Debug = "   GPS fun 32"
                    Name (RET1, Zero)
                    CreateBitField (Arg3, 0x02, SPBI)
                    If (NLIM)
                    {
                        RET1 |= One
                    }

                    If (PSLS)
                    {
                        RET1 |= 0x02
                    }

                    If (CTGP)
                    {
                        RET1 |= 0x00400000
                    }

                    Return (RET1) /* \_SB_.PCI0.GPP0.PEGP.GPS_.RET1 */
                }
                Case (0x2A)
                {
                    Debug = "   GPS fun 42"
                    CreateField (Arg3, Zero, 0x04, PSH0)
                    CreateBitField (Arg3, 0x08, GPUT)
                    VRV1 = 0x00010000
                    Switch (ToInteger (PSH0))
                    {
                        Case (Zero)
                        {
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (One)
                        {
                            RETN = 0x0100
                            RETN |= ToInteger (PSH0)
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (0x02)
                        {
                            RETN = 0x0102
                            If ((NBFM == One))
                            {
                                NBFM = Zero
                                TGPU = GTPM /* External reference */
                            }
                            Else
                            {
                                If ((TGPU == Zero))
                                {
                                    Local0 = \_SB.PCI0.SBRG.EC0.FTBL /* External reference */
                                    Switch (ToInteger (Local0))
                                    {
                                        Case (0x02)
                                        {
                                            TGPU = 0x4B
                                        }
                                        Case (One)
                                        {
                                            TGPU = 0x57
                                        }
                                        Case (Zero)
                                        {
                                            TGPU = 0x57
                                        }
                                        Default
                                        {
                                            TGPU = 0x57
                                        }

                                    }
                                }
                                Else
                                {
                                }

                                NLIM = Zero
                            }

                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }

                    }

                    Return (0x80000002)
                }

            }

            Return (0x80000002)
        }
    }

    Scope (\_SB)
    {
        Device (NPCF)
        {
            Name (ACBT, Zero)
            Name (DCBT, Zero)
            Name (DBAC, Zero)
            Name (DBDC, Zero)
            Name (AMAT, 0xC8)
            Name (AMIT, 0xFFB0)
            Name (ATPP, 0x0168)
            Name (DTPP, Zero)
            Name (TPPL, Zero)
            Name (DROS, Zero)
            Name (CDIS, Zero)
            Name (CUSL, Zero)
            Name (CUCT, Zero)
            Name (WM2M, Zero)
            Name (CTDI, Zero)
            Name (GTDI, Zero)
            Name (AVGF, Zero)
            Name (AVGI, Zero)
            Name (AVG0, Zero)
            Name (AVG1, Zero)
            Name (AVG2, Zero)
            Name (AVG3, Zero)
            Name (AVG4, Zero)
            Name (SFTN, Zero)
            Method (GMIN, 2, Serialized)
            {
                If ((Arg0 > Arg1))
                {
                    Return (Arg1)
                }
                Else
                {
                    Return (Arg0)
                }
            }

            Method (GMAX, 2, Serialized)
            {
                If ((Arg0 > Arg1))
                {
                    Return (Arg0)
                }
                Else
                {
                    Return (Arg1)
                }
            }

            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                CDIS = Zero
                Return ("NVDA0820")
            }

            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
                SFTN = 0x06
            }

            Name (_UID, "NPCF")  // _UID: Unique ID
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((CDIS == One))
                {
                    Return (0x0D)
                }

                Return (0x0F)
            }

            Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
            {
                CDIS = One
            }

            Method (CMPC, 2, NotSerialized)
            {
                Local1 = SizeOf (Arg0)
                If ((Local1 != SizeOf (Arg1)))
                {
                    Return (Zero)
                }

                Local0 = Zero
                While ((Local0 < Local1))
                {
                    If ((DerefOf (Arg0 [Local0]) != DerefOf (Arg1 [Local0]
                        )))
                    {
                        Return (Zero)
                    }

                    Local0++
                }

                Return (One)
            }

            Name (SCFI, Buffer (0x0C)
            {
                /* 0000 */  0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C, 0x3D, 0x41,  // ..-27<=A
                /* 0008 */  0x42, 0x46, 0x47, 0x4B                           // BFGK
            })
            Name (SGFI, Buffer (0x0C)
            {
                /* 0000 */  0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C, 0x3D, 0x41,  // ..-27<=A
                /* 0008 */  0x42, 0x46, 0x47, 0x4B                           // BFGK
            })
            Method (MAVT, 1, Serialized)
            {
                Switch (ToInteger (AVGI))
                {
                    Case (Zero)
                    {
                        AVG0 = Arg0
                    }
                    Case (One)
                    {
                        AVG1 = Arg0
                    }
                    Case (0x02)
                    {
                        AVG2 = Arg0
                    }
                    Case (0x03)
                    {
                        AVG3 = Arg0
                    }
                    Case (0x04)
                    {
                        AVG4 = Arg0
                    }

                }

                If ((AVGI >= 0x04))
                {
                    AVGI = Zero
                    AVGF = One
                }
                Else
                {
                    AVGI += One
                }

                If ((AVGF >= One))
                {
                    Divide ((AVG0 + (AVG1 + (AVG2 + (AVG3 + AVG4))
                        )), 0x05, Local1, Local0)
                }
                Else
                {
                    Divide ((AVG0 + (AVG1 + (AVG2 + (AVG3 + AVG4))
                        )), AVGI, Local1, Local0)
                }

                Return (Local0)
            }

            Method (FCPI, 1, Serialized)
            {
                Local0 = CTDI /* \_SB_.NPCF.CTDI */
                While ((Local0 < SFTN))
                {
                    Local1 = ((Local0 * 0x02) + One)
                    If ((Arg0 >= DerefOf (SCFI [Local1])))
                    {
                        CTDI = Local0
                        Local0++
                    }
                    Else
                    {
                        Break
                    }
                }

                If ((CTDI == Local0))
                {
                    While ((Local0 > Zero))
                    {
                        Local1 = (Local0 * 0x02)
                        If ((Arg0 <= DerefOf (SCFI [Local1])))
                        {
                            Local0--
                            CTDI = Local0
                        }
                        Else
                        {
                            Break
                        }
                    }
                }

                Return (CTDI) /* \_SB_.NPCF.CTDI */
            }

            Method (FGPI, 1, Serialized)
            {
                Local0 = GTDI /* \_SB_.NPCF.GTDI */
                While ((Local0 < SFTN))
                {
                    Local1 = ((Local0 * 0x02) + One)
                    If ((Arg0 >= DerefOf (SGFI [Local1])))
                    {
                        GTDI = Local0
                        Local0++
                    }
                    Else
                    {
                        Break
                    }
                }

                If ((GTDI == Local0))
                {
                    While ((Local0 > Zero))
                    {
                        Local1 = (Local0 * 0x02)
                        If ((Arg0 <= DerefOf (SGFI [Local1])))
                        {
                            Local0--
                            GTDI = Local0
                        }
                        Else
                        {
                            Break
                        }
                    }
                }

                Return (GTDI) /* \_SB_.NPCF.GTDI */
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If ((Arg0 == ToUUID ("36b49710-2483-11e7-9598-0800200c9a66") /* Unknown UUID */))
                {
                    Return (NPCF (Arg0, Arg1, Arg2, Arg3))
                }
            }

            Method (NTCU, 0, Serialized)
            {
                Switch (ToInteger (TCNT))
                {
                    Case (0x20)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00F, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C010, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C011, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C012, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C013, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C014, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C015, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C016, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C017, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C018, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C019, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C01F, 0x85) // Device-Specific
                    }
                    Case (0x18)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00F, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C010, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C011, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C012, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C013, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C014, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C015, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C016, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C017, 0x85) // Device-Specific
                    }
                    Case (0x14)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00F, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C010, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C011, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C012, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C013, 0x85) // Device-Specific
                    }
                    Case (0x12)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00F, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C010, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C011, 0x85) // Device-Specific
                    }
                    Case (0x10)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00E, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00F, 0x85) // Device-Specific
                    }
                    Case (0x0E)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00C, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00D, 0x85) // Device-Specific
                    }
                    Case (0x0C)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00A, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C00B, 0x85) // Device-Specific
                    }
                    Case (0x0A)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C008, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C009, 0x85) // Device-Specific
                    }
                    Case (0x08)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C007, 0x85) // Device-Specific
                    }
                    Case (0x07)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C006, 0x85) // Device-Specific
                    }
                    Case (0x06)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C005, 0x85) // Device-Specific
                    }
                    Case (0x05)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C004, 0x85) // Device-Specific
                    }
                    Case (0x04)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C003, 0x85) // Device-Specific
                    }
                    Case (0x03)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C002, 0x85) // Device-Specific
                    }
                    Case (0x02)
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                        Notify (\_SB.PLTF.C001, 0x85) // Device-Specific
                    }
                    Default
                    {
                        Notify (\_SB.PLTF.C000, 0x85) // Device-Specific
                    }

                }
            }

            Method (NPCF, 4, Serialized)
            {
                Debug = "------- NVPCF DSM --------"
                If ((ToInteger (Arg1) != 0x0200))
                {
                    Return (0x80000001)
                }

                Switch (ToInteger (Arg2))
                {
                    Case (Zero)
                    {
                        Debug = "   NVPCF sub-func#0"
                        Return (Buffer (0x04)
                        {
                             0x87, 0x07, 0x00, 0x00                           // ....
                        })
                    }
                    Case (One)
                    {
                        Debug = "   NVPCF sub-func#1"
                        Return (Buffer (0x0E)
                        {
                            /* 0000 */  0x20, 0x03, 0x01, 0x01, 0x23, 0x04, 0x05, 0x01,  //  ...#...
                            /* 0008 */  0x01, 0x01, 0x00, 0x00, 0x00, 0xAC               // ......
                        })
                    }
                    Case (0x02)
                    {
                        Debug = "   NVPCF sub-func#2"
                        Name (PBD2, Buffer (0x31)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBD2, Zero, PTV2)
                        CreateByteField (PBD2, One, PHB2)
                        CreateByteField (PBD2, 0x02, GSB2)
                        CreateByteField (PBD2, 0x03, CTB2)
                        CreateByteField (PBD2, 0x04, NCE2)
                        PTV2 = 0x23
                        PHB2 = 0x05
                        GSB2 = 0x10
                        CTB2 = 0x1C
                        NCE2 = One
                        CreateWordField (PBD2, 0x05, TGPA)
                        CreateWordField (PBD2, 0x07, TGPD)
                        CreateByteField (PBD2, 0x15, PC01)
                        CreateByteField (PBD2, 0x16, PC02)
                        CreateWordField (PBD2, 0x19, TPPA)
                        CreateWordField (PBD2, 0x1B, TPPD)
                        CreateWordField (PBD2, 0x1D, MAGA)
                        CreateWordField (PBD2, 0x1F, MAGD)
                        CreateWordField (PBD2, 0x21, MIGA)
                        CreateWordField (PBD2, 0x23, MIGD)
                        CreateDWordField (PBD2, 0x25, DROP)
                        CreateDWordField (PBD2, 0x29, PA5O)
                        CreateDWordField (PBD2, 0x2D, PA6O)
                        CreateField (Arg3, 0x28, 0x02, NIGS)
                        CreateByteField (Arg3, 0x15, IORC)
                        CreateField (Arg3, 0xB0, One, PWCS)
                        CreateField (Arg3, 0xB1, One, PWTS)
                        CreateField (Arg3, 0xB2, One, CGPS)
                        If ((ToInteger (NIGS) == Zero))
                        {
                            TGPA = ACBT /* \_SB_.NPCF.ACBT */
                            TGPD = DCBT /* \_SB_.NPCF.DCBT */
                            PC01 = Zero
                            PC02 = (DBAC | (DBDC << One))
                            TPPA = ATPP /* \_SB_.NPCF.ATPP */
                            TPPD = DTPP /* \_SB_.NPCF.DTPP */
                            MAGA = AMAT /* \_SB_.NPCF.AMAT */
                            MIGA = AMIT /* \_SB_.NPCF.AMIT */
                            DROP = DROS /* \_SB_.NPCF.DROS */
                        }

                        If ((ToInteger (NIGS) == One))
                        {
                            If ((ToInteger (PWCS) == One)) {}
                            Else
                            {
                            }

                            If ((ToInteger (PWTS) == One)) {}
                            Else
                            {
                            }

                            If ((ToInteger (CGPS) == One)) {}
                            Else
                            {
                            }

                            TGPA = Zero
                            TGPD = Zero
                            PC01 = Zero
                            PC02 = Zero
                            TPPA = Zero
                            TPPD = Zero
                            MAGA = Zero
                            MIGA = Zero
                            MAGD = Zero
                            MIGD = Zero
                        }

                        Return (PBD2) /* \_SB_.NPCF.NPCF.PBD2 */
                    }
                    Case (0x03)
                    {
                        Debug = "   NVPCF sub-func#3"
                        If (((\_SB.NPCF.WM2M & One) == One))
                        {
                            Return (Buffer (0x1E)
                            {
                                /* 0000 */  0x11, 0x04, 0x0D, 0x02, 0x00, 0xFF, 0x00, 0x2D,  // .......-
                                /* 0008 */  0x32, 0x37, 0x3C, 0x3D, 0x41, 0x42, 0x46, 0x47,  // 27<=ABFG
                                /* 0010 */  0x4B, 0x05, 0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C,  // K...-27<
                                /* 0018 */  0x3D, 0x41, 0x42, 0x46, 0x47, 0x4B               // =ABFGK
                            })
                        }
                    }
                    Case (0x04)
                    {
                        Debug = "   NVPCF sub-func#4"
                        If (((\_SB.NPCF.WM2M & One) == One))
                        {
                            Return (Buffer (0x32)
                            {
                                /* 0000 */  0x11, 0x04, 0x2E, 0x01, 0x05, 0x00, 0x01, 0x02,  // ........
                                /* 0008 */  0x03, 0x04, 0x03, 0x00, 0x01, 0x02, 0x00, 0x01,  // ........
                                /* 0010 */  0x02, 0x03, 0x03, 0x03, 0x01, 0x01, 0x02, 0x03,  // ........
                                /* 0018 */  0x03, 0x03, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03,  // ........
                                /* 0020 */  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,  // ........
                                /* 0028 */  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,  // ........
                                /* 0030 */  0x03, 0x03                                       // ..
                            })
                        }
                    }
                    Case (0x05)
                    {
                        Debug = "   NVPCF sub-func#5"
                        Name (PBD5, Buffer (0x28)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBD5, Zero, PTV5)
                        CreateByteField (PBD5, One, PHB5)
                        CreateByteField (PBD5, 0x02, TEB5)
                        CreateByteField (PBD5, 0x03, NTE5)
                        PTV5 = 0x11
                        PHB5 = 0x04
                        TEB5 = 0x24
                        NTE5 = One
                        CreateDWordField (PBD5, 0x04, F5O0)
                        CreateDWordField (PBD5, 0x08, F5O1)
                        CreateDWordField (PBD5, 0x0C, F5O2)
                        CreateDWordField (PBD5, 0x10, F5O3)
                        CreateDWordField (PBD5, 0x14, F5O4)
                        CreateDWordField (PBD5, 0x18, F5O5)
                        CreateDWordField (PBD5, 0x1C, F5O6)
                        CreateDWordField (PBD5, 0x20, F5O7)
                        CreateDWordField (PBD5, 0x24, F5O8)
                        CreateField (Arg3, 0x20, 0x03, INC5)
                        CreateDWordField (Arg3, 0x08, F5P1)
                        CreateDWordField (Arg3, 0x0C, F5P2)
                        Switch (ToInteger (INC5))
                        {
                            Case (Zero)
                            {
                                F5O0 = WM2M /* \_SB_.NPCF.WM2M */
                                F5O1 = Zero
                                F5O2 = Zero
                                F5O3 = Zero
                            }
                            Case (One)
                            {
                                F5O0 = 0x0C
                                F5O1 = Zero
                                F5O2 = Zero
                                F5O3 = Zero
                            }
                            Case (0x02)
                            {
                                F5O0 = Zero
                                Local0 = \_SB.PCI0.SBRG.EC0.CTMP /* External reference */
                                Local1 = \_SB.PCI0.SBRG.EC0.VRTT /* External reference */
                                Local0 = MAVT (Local0)
                                Local2 = FCPI (Local0)
                                F5O1 = ((Local0 << 0x10) | (Local2 & 0xFF))
                                Local2 = FGPI (Local1)
                                F5O2 = ((Local1 << 0x10) | (Local2 & 0xFF))
                                F5O3 = Zero
                                F5O4 = Zero
                                F5O5 = Zero
                                F5O6 = Zero
                                F5O7 = Zero
                                F5O8 = Zero
                            }
                            Case (0x03)
                            {
                                CUSL = (F5P1 & 0xFF)
                            }
                            Case (0x04)
                            {
                                CUCT = F5P2 /* \_SB_.NPCF.NPCF.F5P2 */
                            }
                            Default
                            {
                                Return (0x80000002)
                            }

                        }

                        Return (PBD5) /* \_SB_.NPCF.NPCF.PBD5 */
                    }
                    Case (0x07)
                    {
                        Debug = "   NVPCF sub-func#7"
                        CreateDWordField (Arg3, 0x05, AMAX)
                        CreateDWordField (Arg3, 0x09, ARAT)
                        CreateDWordField (Arg3, 0x0D, DMAX)
                        CreateDWordField (Arg3, 0x11, DRAT)
                        CreateDWordField (Arg3, 0x15, TGPM)
                        Return (Zero)
                    }
                    Case (0x08)
                    {
                        Debug = "   NVPCF sub-func#8"
                        If ((\_SB.PCI0.SBRG.EC0.BAY1 == One))
                        {
                            Return (Buffer (0x6A)
                            {
                                /* 0000 */  0x10, 0x04, 0x11, 0x06, 0x64, 0x94, 0x11, 0x00,  // ....d...
                                /* 0008 */  0x00, 0x30, 0x75, 0x00, 0x00, 0x28, 0x23, 0x00,  // .0u..(#.
                                /* 0010 */  0x00, 0x50, 0x46, 0x00, 0x00, 0x50, 0x94, 0x11,  // .PF..P..
                                /* 0018 */  0x00, 0x00, 0x30, 0x75, 0x00, 0x00, 0x40, 0x1F,  // ..0u..@.
                                /* 0020 */  0x00, 0x00, 0x80, 0x3E, 0x00, 0x00, 0x3C, 0x94,  // ...>..<.
                                /* 0028 */  0x11, 0x00, 0x00, 0x30, 0x75, 0x00, 0x00, 0x58,  // ...0u..X
                                /* 0030 */  0x1B, 0x00, 0x00, 0x98, 0x3A, 0x00, 0x00, 0x32,  // ....:..2
                                /* 0038 */  0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75, 0x00, 0x00,  // ....0u..
                                /* 0040 */  0x70, 0x17, 0x00, 0x00, 0xE0, 0x2E, 0x00, 0x00,  // p.......
                                /* 0048 */  0x19, 0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75, 0x00,  // .....0u.
                                /* 0050 */  0x00, 0x88, 0x13, 0x00, 0x00, 0xF8, 0x2A, 0x00,  // ......*.
                                /* 0058 */  0x00, 0x0A, 0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75,  // ......0u
                                /* 0060 */  0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x10, 0x27,  // .......'
                                /* 0068 */  0x00, 0x00                                       // ..
                            })
                        }
                        ElseIf ((\_SB.PCI0.SBRG.EC0.BAY2 == One))
                        {
                            Return (Buffer (0x6A)
                            {
                                /* 0000 */  0x10, 0x04, 0x11, 0x06, 0x64, 0x7C, 0x15, 0x00,  // ....d|..
                                /* 0008 */  0x00, 0xB8, 0x88, 0x00, 0x00, 0xF8, 0x2A, 0x00,  // ......*.
                                /* 0010 */  0x00, 0xF0, 0x55, 0x00, 0x00, 0x50, 0x7C, 0x15,  // ..U..P|.
                                /* 0018 */  0x00, 0x00, 0xB8, 0x88, 0x00, 0x00, 0x10, 0x27,  // .......'
                                /* 0020 */  0x00, 0x00, 0x20, 0x4E, 0x00, 0x00, 0x3C, 0x7C,  // .. N..<|
                                /* 0028 */  0x15, 0x00, 0x00, 0xB8, 0x88, 0x00, 0x00, 0x28,  // .......(
                                /* 0030 */  0x23, 0x00, 0x00, 0x50, 0x46, 0x00, 0x00, 0x32,  // #..PF..2
                                /* 0038 */  0xEC, 0x13, 0x00, 0x00, 0xB8, 0x88, 0x00, 0x00,  // ........
                                /* 0040 */  0x40, 0x1F, 0x00, 0x00, 0x80, 0x3E, 0x00, 0x00,  // @....>..
                                /* 0048 */  0x19, 0xEC, 0x13, 0x00, 0x00, 0xB8, 0x88, 0x00,  // ........
                                /* 0050 */  0x00, 0x40, 0x1F, 0x00, 0x00, 0x98, 0x3A, 0x00,  // .@....:.
                                /* 0058 */  0x00, 0x0A, 0xEC, 0x13, 0x00, 0x00, 0xB8, 0x88,  // ........
                                /* 0060 */  0x00, 0x00, 0x58, 0x1B, 0x00, 0x00, 0xB0, 0x36,  // ..X....6
                                /* 0068 */  0x00, 0x00                                       // ..
                            })
                        }
                        Else
                        {
                            Return (Buffer (0x6A)
                            {
                                /* 0000 */  0x10, 0x04, 0x11, 0x06, 0x64, 0x94, 0x11, 0x00,  // ....d...
                                /* 0008 */  0x00, 0x30, 0x75, 0x00, 0x00, 0x28, 0x23, 0x00,  // .0u..(#.
                                /* 0010 */  0x00, 0x50, 0x46, 0x00, 0x00, 0x50, 0x94, 0x11,  // .PF..P..
                                /* 0018 */  0x00, 0x00, 0x30, 0x75, 0x00, 0x00, 0x40, 0x1F,  // ..0u..@.
                                /* 0020 */  0x00, 0x00, 0x80, 0x3E, 0x00, 0x00, 0x3C, 0x94,  // ...>..<.
                                /* 0028 */  0x11, 0x00, 0x00, 0x30, 0x75, 0x00, 0x00, 0x58,  // ...0u..X
                                /* 0030 */  0x1B, 0x00, 0x00, 0x98, 0x3A, 0x00, 0x00, 0x32,  // ....:..2
                                /* 0038 */  0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75, 0x00, 0x00,  // ....0u..
                                /* 0040 */  0x70, 0x17, 0x00, 0x00, 0xE0, 0x2E, 0x00, 0x00,  // p.......
                                /* 0048 */  0x19, 0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75, 0x00,  // .....0u.
                                /* 0050 */  0x00, 0x88, 0x13, 0x00, 0x00, 0xF8, 0x2A, 0x00,  // ......*.
                                /* 0058 */  0x00, 0x0A, 0xA0, 0x0F, 0x00, 0x00, 0x30, 0x75,  // ......0u
                                /* 0060 */  0x00, 0x00, 0x88, 0x13, 0x00, 0x00, 0x10, 0x27,  // .......'
                                /* 0068 */  0x00, 0x00                                       // ..
                            })
                        }
                    }
                    Case (0x09)
                    {
                        Debug = "   NVPCF sub-func#9"
                        CreateDWordField (Arg3, 0x03, CPTD)
                        Local0 = CPTD /* \_SB_.NPCF.NPCF.CPTD */
                        Divide (Local0, 0x03E8, Local1, Local2)
                        \_SB.PCI0.SBRG.EC0.NDF9 = Local2
                        Debug = Concatenate (Concatenate ("NVPCF F09 set CPU power limit = ", Local2), " hW")
                        Return (Zero)
                    }
                    Case (0x0A)
                    {
                        Debug = "   NVPCF sub-func#10"
                        Name (PBDA, Buffer (0x08)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBDA, Zero, DTTV)
                        CreateByteField (PBDA, One, DTSH)
                        CreateByteField (PBDA, 0x02, DTSE)
                        CreateByteField (PBDA, 0x03, DTTE)
                        CreateDWordField (PBDA, 0x04, DTTL)
                        DTTV = 0x10
                        DTSH = 0x04
                        DTSE = 0x04
                        DTTE = One
                        DTTL = TPPL /* \_SB_.NPCF.TPPL */
                        Return (PBDA) /* \_SB_.NPCF.NPCF.PBDA */
                    }

                }

                Return (0x80000002)
            }
        }
    }
}

