/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt1.dat, Mon Jun 26 21:07:02 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000A919 (43289)
 *     Revision         0x02
 *     Checksum         0xF8
 *     OEM ID           "AMD"
 *     OEM Table ID     "DISCUSB4"
 *     OEM Revision     0x00000002 (2)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "AMD", "DISCUSB4", 0x00000002)
{
    External (_ADR, IntObj)
    External (_GPE, DeviceObj)
    External (_GPE.OL02, MethodObj)    // 0 Arguments
    External (_GPE.OL32, MethodObj)    // 0 Arguments
    External (_SB_.ALIB, MethodObj)    // 2 Arguments
    External (_SB_.PCI0.DADR, IntObj)
    External (_SB_.PCI0.EBUS, IntObj)
    External (_SB_.PCI0.FLG0, IntObj)
    External (_SB_.PCI0.GPP7, DeviceObj)
    External (_SB_.PCI0.GPP7._ADR, IntObj)
    External (_SB_.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0, DeviceObj)
    External (_SB_.PCI0.M641, IntObj)
    External (M000, MethodObj)    // 1 Arguments
    External (M009, MethodObj)    // 1 Arguments
    External (M010, MethodObj)    // 2 Arguments
    External (M011, MethodObj)    // 4 Arguments
    External (M013, MethodObj)    // 4 Arguments
    External (M014, MethodObj)    // 5 Arguments
    External (M037, DeviceObj)
    External (M046, IntObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M04A, MethodObj)    // 2 Arguments
    External (M04B, MethodObj)    // 2 Arguments
    External (M04C, MethodObj)    // 3 Arguments
    External (M04D, MethodObj)    // 3 Arguments
    External (M04E, MethodObj)    // 3 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M23A, FieldUnitObj)
    External (M249, MethodObj)    // 4 Arguments
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M29A, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M31C, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M401, MethodObj)    // 3 Arguments
    External (M402, MethodObj)    // 3 Arguments
    External (M403, MethodObj)    // 4 Arguments
    External (M404, BuffObj)
    External (M408, MutexObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)
    External (M453, FieldUnitObj)
    External (M454, FieldUnitObj)
    External (M455, FieldUnitObj)
    External (M456, FieldUnitObj)
    External (M457, FieldUnitObj)
    External (M460, MethodObj)    // 7 Arguments
    External (M471, MethodObj)    // 3 Arguments
    External (M472, MethodObj)    // 4 Arguments
    External (M4C0, FieldUnitObj)
    External (M4F0, FieldUnitObj)
    External (M610, FieldUnitObj)
    External (M620, FieldUnitObj)
    External (M624, MethodObj)    // 1 Arguments
    External (M631, FieldUnitObj)
    External (M646, MethodObj)    // 2 Arguments
    External (M647, MethodObj)    // 2 Arguments
    External (M648, MethodObj)    // 3 Arguments
    External (M650, MethodObj)    // 2 Arguments
    External (M651, MethodObj)    // 4 Arguments

    Scope (\_SB)
    {
        Name (M623, 0x55555555)
        Name (M626, 0x55555555)
        Mutex (M627, 0x00)
        Method (M625, 2, Serialized)
        {
            Acquire (M627, 0xFFFF)
            Local7 = One
            If ((M620 != Zero))
            {
                If ((\_SB.M623 == 0x55555555))
                {
                    If ((\_SB.M626 == 0x55555555))
                    {
                        Local0 = M049 (M620, 0x12)
                        Local1 = M049 (M620, 0x13)
                        Local2 = M049 (M620, 0x14)
                        Local3 = M624 (Local0)
                        Local4 = (M083 + (Local3 << 0x14))
                        Local4 += (Local1 << 0x0F)
                        Local4 += (Local2 << 0x0C)
                        \_SB.M626 = Local4
                    }
                    Else
                    {
                        Local4 = \_SB.M626
                    }

                    Local5 = M011 (Local4, 0x19, Zero, 0x08)
                    If ((Local5 == Zero))
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 CpmSendDUSB4Command (0x%X, %dms) = 0x%X no USP bus number FAIL !!!\n", Arg0, Arg1, Local7, Zero, Zero, Zero)
                        Release (M627)
                        Return (Local7)
                    }

                    Local6 = (M083 + (Local5 << 0x14))
                    Local6 += 0x0420
                    \_SB.M623 = Local6
                }
                Else
                {
                    Local6 = \_SB.M623
                }

                Local4 = M650 ((Local6 - 0x0420), Zero)
                OperationRegion (VARM, SystemMemory, Local6, 0x14)
                Field (VARM, DWordAcc, NoLock, Preserve)
                {
                    M621,   32, 
                    Offset (0x10), 
                    M622,   32
                }

                M621 = Arg0
                Local0 = Zero
                Local1 = M621 /* \_SB_.M625.M621 */
                While (((Arg1 > Local0) && ((Local1 & 0x0F00) == 0x0100)))
                {
                    Local0++
                    Local5 = 0x0A
                    While ((Local5 > Zero))
                    {
                        Local5 = (Local5 - One)
                        Stall (0x63)
                    }

                    Local1 = M621 /* \_SB_.M625.M621 */
                }

                If ((Local0 >= Arg1))
                {
                    Local4 = M650 ((Local6 - 0x0420), Local4)
                    M460 ("  FEA-ASL-DiscreteUSB4 CpmSendDUSB4Command (0x%X, 0x%X, %dms) = 0x%X Write Timeout FAIL !!!\n", Local6, Arg0, Arg1, Local7, Zero, Zero)
                    Release (M627)
                    Return (Local7)
                }

                Local2 = Zero
                Local3 = M622 /* \_SB_.M625.M622 */
                While (((Arg1 > Local2) && ((Local3 & 0x0F00) == Zero)))
                {
                    Local2++
                    Local5 = 0x0A
                    While ((Local5 > Zero))
                    {
                        Local5 = (Local5 - One)
                        Stall (0x63)
                    }

                    Local3 = M622 /* \_SB_.M625.M622 */
                }

                M622 = Zero
                If ((Local2 >= Arg1))
                {
                    Local4 = M650 ((Local6 - 0x0420), Local4)
                    M460 ("  FEA-ASL-DiscreteUSB4 CpmSendDUSB4Command (0x%X, 0x%X, %dms) = 0x%X Read Timeout FAIL !!!\n", Local6, Arg0, Arg1, Local7, Zero, Zero)
                    Release (M627)
                    Return (Local7)
                }

                Local7 = Local3
                M460 ("  FEA-ASL-DiscreteUSB4 CpmSendDUSB4Command (0x%X, 0x%X, %dms) = 0x%X", Local6, Arg0, Arg1, Local7, Zero, Zero)
                M460 ("  WriteAckTime = %dms  ReadAckTime = %dms\n", Local0, Local2, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 CpmSendDUSB4Command (0x%X, %d) = 0x%X CpmDiscreteUSB4Table does NOT exist FAIL !!!\n", Arg0, Arg1, Local7, Zero, Zero, Zero)
            }

            Local4 = M650 ((Local6 - 0x0420), Local4)
            Release (M627)
            Return (Local7)
        }

        Method (BPTS, 1, Serialized)
        {
            M460 ("  FEA-ASL-DiscreteUSB4 \\_SB.BPTS (%d)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
            Local0 = M650 (\_SB.PCI0.GPP7.DADR, Zero)
            M647 (\_SB.PCI0.GPP7.DADR, 0x0540)
            If ((M049 (M620, 0x44) != 0x04))
            {
                Local1 = M650 (\_SB.PCI0.GPP7.U4UP.DADR, Zero)
                Local2 = M650 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, Zero)
                Local3 = M650 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, Zero)
                Local4 = M650 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, Zero)
                Local5 = M650 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, Zero)
                M647 (\_SB.PCI0.GPP7.U4UP.DADR, 0x0340)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, 0x0340)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, 0x0340)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, 0x0340)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, 0x0340)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P2.UXHC.DADR, 0x01A0)
                M647 (\_SB.PCI0.GPP7.U4UP.U4P3.UHI0.DADR, 0x01A0)
                Local5 = M650 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, Local5)
                Local4 = M650 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, Local4)
                Local3 = M650 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, Local3)
                Local2 = M650 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, Local2)
                Local1 = M650 (\_SB.PCI0.GPP7.U4UP.DADR, Local1)
            }

            Local0 = M650 (\_SB.PCI0.GPP7.DADR, Local0)
            M000 (0x0DEF)
        }

        Method (BWAK, 1, Serialized)
        {
            M460 ("  FEA-ASL-DiscreteUSB4 \\_SB.BWAK (%d)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
            Local0 = M650 (\_SB.PCI0.GPP7.DADR, Zero)
            Local1 = M650 (\_SB.PCI0.GPP7.U4UP.DADR, Zero)
            Local2 = M650 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, Zero)
            Local3 = M650 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, Zero)
            Local4 = M650 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, Zero)
            Local5 = M650 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, Zero)
            M647 (\_SB.PCI0.GPP7.DADR, 0x0540)
            M647 (\_SB.PCI0.GPP7.U4UP.DADR, 0x0340)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, 0x0340)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, 0x0340)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, 0x0340)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, 0x0340)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P2.UXHC.DADR, 0x01A0)
            M647 (\_SB.PCI0.GPP7.U4UP.U4P3.UHI0.DADR, 0x01A0)
            Local5 = M650 (\_SB.PCI0.GPP7.U4UP.U4P3.DADR, Local5)
            Local4 = M650 (\_SB.PCI0.GPP7.U4UP.U4P2.DADR, Local4)
            Local3 = M650 (\_SB.PCI0.GPP7.U4UP.U4P1.DADR, Local3)
            Local2 = M650 (\_SB.PCI0.GPP7.U4UP.U4P0.DADR, Local2)
            Local1 = M650 (\_SB.PCI0.GPP7.U4UP.DADR, Local1)
            Local0 = M650 (\_SB.PCI0.GPP7.DADR, Local0)
            M000 (0x0DF0)
        }
    }

    Scope (\_SB.PCI0.GPP7)
    {
        Name (XDSD, Package (0x08)
        {
            ToUUID ("6b4ad420-8fd3-4364-acf8-eb94876fd9eb") /* Unknown UUID */, 
            Package (0x00) {}, 
            ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "HotPlugSupportInD3", 
                    One
                }
            }, 

            ToUUID ("fdf06fad-f744-4451-bb64-ecd792215b10") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "FundamentalDeviceResetTriggeredOnD3ToD0", 
                    One
                }
            }, 

            ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
            Package (0x02)
            {
                Package (0x02)
                {
                    "ExternalFacingPort", 
                    One
                }, 

                Package (0x02)
                {
                    "UID", 
                    0x03
                }
            }
        })
        Name (YDSD, Package (0x06)
        {
            ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "HotPlugSupportInD3", 
                    One
                }
            }, 

            ToUUID ("fdf06fad-f744-4451-bb64-ecd792215b10") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "FundamentalDeviceResetTriggeredOnD3ToD0", 
                    One
                }
            }, 

            ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
            Package (0x02)
            {
                Package (0x02)
                {
                    "ExternalFacingPort", 
                    One
                }, 

                Package (0x02)
                {
                    "UID", 
                    0x03
                }
            }
        })
        Name (_DSD, Package (0x06)  // _DSD: Device-Specific Data
        {
            ToUUID ("6b4ad420-8fd3-4364-acf8-eb94876fd9eb") /* Unknown UUID */, 
            Package (0x00) {}, 
            ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "HotPlugSupportInD3", 
                    One
                }
            }, 

            ToUUID ("fdf06fad-f744-4451-bb64-ecd792215b10") /* Unknown UUID */, 
            Package (0x01)
            {
                Package (0x02)
                {
                    "FundamentalDeviceResetTriggeredOnD3ToD0", 
                    One
                }
            }
        })
        Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
        {
            Local0 = M049 (M620, 0x44)
            If ((Local0 == 0x03))
            {
                Local0 = 0x04
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }

            Return (Local0)
        }

        Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
        {
            Local0 = 0x04
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }

            Return (Local0)
        }

        Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
        {
            Local0 = 0x04
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }

            Return (Local0)
        }

        Name (DPRW, Package (0x02)
        {
            0x16, 
            0x03
        })
        Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
        {
            Local0 = M049 (M620, 0x3D)
            If ((Local0 != 0x16))
            {
                DPRW [Zero] = Local0
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
            }

            Return (DPRW) /* \_SB_.PCI0.GPP7.DPRW */
        }

        Method (M628, 0, Serialized)
        {
            M460 ("  FEA-ASL-CpmDiscreteUSB4 PowerOn () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
            If ((M049 (M620, 0x44) == 0x04))
            {
                Local7 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
                M014 (DADR, (M641 + 0x18), Zero, 0x20, (Local7 & 0xFFFF0000))
                Local1 = M04B (M620, 0x31)
                M010 (Local1, One)
            }

            Local0 = M049 (M620, 0x12)
            Local1 = M049 (M620, 0x13)
            Local2 = M049 (M620, 0x14)
            If ((M049 (M620, 0x44) == 0x03))
            {
                Local4 = M401 (Local0, Local1, Local2)
                If ((Local4 != 0xFF))
                {
                    Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                    Local5 = M249 (Zero, Zero, Zero, Local6)
                    Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                    M472 (Local0, Local1, Local2, Zero)
                }
            }

            Sleep (0x64)
            Local3 = M04B (M620, 0x2D)
            M010 (Local3, One)
            Local5 = M009 (Local3)
            If ((M049 (M620, 0x44) == 0x04))
            {
                If ((M085 >= 0x10))
                {
                    Local4 = M401 (Local0, Local1, Local2)
                    If ((Local4 != 0xFF))
                    {
                        Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                        Local5 = M249 (Zero, Zero, Zero, Local6)
                        Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                        Local3 = M249 (Zero, Zero, Zero, Local6)
                    }

                    Local3 = ((DADR >> 0x0C) & 0xFF)
                    M460 ("  FEA-ASL-DiscreteUSB4 PowerOn () call _SB.ALIB (0x13, 0x%X)\n", Local3, Zero, Zero, Zero, Zero, Zero)
                    \_SB.ALIB (0x13, Local3)
                    If ((Local4 != 0xFF))
                    {
                        Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                        If ((M249 (Zero, Zero, Zero, Local6) == Local5))
                        {
                            M472 (Local0, Local1, Local2, Zero)
                        }

                        Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                        Local3 = M249 (Zero, Zero, Zero, Local6)
                    }
                }
                Else
                {
                    M403 (Local0, Local1, Local2, One)
                }
            }

            Local5 = ((M013 (DADR, (M641 + 0x10), Zero, 0x20) >> 0x18
                ) & 0xFF)
            Local5 = 0x65
            While ((Local5 > Zero))
            {
                Local5 = (Local5 - One)
                Stall (0x63)
            }

            Local5 = ((M013 (DADR, (M641 + 0x10), Zero, 0x20) >> 0x18
                ) & 0xFF)
            Local4 = 0x13BB
            While ((((Local5 & 0x28) != 0x20) && (Local4 > Zero)))
            {
                If (((Local4 & 0xFF) == Zero))
                {
                    Local5 = ((M013 (DADR, (M641 + 0x10), Zero, 0x20) >> 0x18
                        ) & 0xFF)
                }
                Else
                {
                    Local5 = M049 (DADR, (M641 + 0x13))
                }

                Local4 = (Local4 - One)
                Stall (0x63)
            }

            Local5 = ((M013 (DADR, (M641 + 0x10), Zero, 0x20) >> 0x18
                ) & 0xFF)
            If ((Local4 == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PowerOn () DL_ACTIVE and LINK_TRAINING FAIL !!!\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }

            If ((M049 (M620, 0x44) == 0x04))
            {
                If ((M085 < 0x10))
                {
                    M403 (Local0, Local1, Local2, Zero)
                }

                Local5 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
                M014 (DADR, (M641 + 0x18), Zero, 0x20, (Local7 | Local5))
                Local5 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
            }

            If ((M049 (M620, 0x44) == 0x03))
            {
                Local4 = M401 (Local0, Local1, Local2)
                If ((Local4 != 0xFF))
                {
                    Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                    Local5 = M249 (Zero, Zero, Zero, Local6)
                    Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                }
            }

            Local6 = (DADR + M646 (DADR, One))
            If ((Local6 != DADR))
            {
                Local7 = M013 (Local6, 0x04, Zero, 0x20)
                If (((Local7 & 0x8000) != Zero))
                {
                    M014 (Local6, 0x04, Zero, 0x20, Local7)
                    Local7 = M013 (Local6, 0x04, Zero, 0x20)
                }
            }

            Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
            If (((Local7 & 0x00030000) != Zero))
            {
                M014 (DADR, (M641 + 0x20), Zero, 0x20, Local7)
                Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
                If (((Local7 & 0x00030000) != Zero))
                {
                    M014 (DADR, (M641 + 0x20), Zero, 0x20, Local7)
                    Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
                }
            }

            M460 ("  FEA-ASL-CpmDiscreteUSB4 PowerOn () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
        }

        Method (M629, 0, Serialized)
        {
            M460 ("  FEA-ASL-CpmDiscreteUSB4 PowerOff () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Local5 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
            M014 (DADR, (M641 + 0x18), Zero, 0x20, (Local5 & 0xFFFF0000))
            Local0 = M049 (M620, 0x12)
            Local1 = M049 (M620, 0x13)
            Local2 = M049 (M620, 0x14)
            M402 (Local0, Local1, Local2)
            If ((M085 >= 0x10))
            {
                Local4 = M401 (Local0, Local1, Local2)
                If ((Local4 != 0xFF))
                {
                    Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                    Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                }

                Local3 = ((DADR >> 0x0C) & 0xFF)
                M460 ("  FEA-ASL-DiscreteUSB4 PowerOff () call _SB.ALIB (0x12, 0x%X)\n", Local3, Zero, Zero, Zero, Zero, Zero)
                \_SB.ALIB (0x12, Local3)
                If ((Local4 != 0xFF))
                {
                    Local6 = (M471 (Local0, Local4, 0x08) + 0x0428)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                    Local6 = (M471 (Local0, Local4, 0x04) + 0x0294)
                    Local3 = M249 (Zero, Zero, Zero, Local6)
                }
            }

            Local1 = M04B (M620, 0x31)
            M010 (Local1, Zero)
            Local6 = 0xCA
            While ((Local6 > Zero))
            {
                Local6 = (Local6 - One)
                Stall (0x63)
            }

            Local0 = M04B (M620, 0x2D)
            M010 (Local0, Zero)
            Local7 = M009 (Local0)
            Local6 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
            M014 (DADR, (M641 + 0x18), Zero, 0x20, (Local5 | Local6))
            Local6 = M013 (DADR, (M641 + 0x18), Zero, 0x20)
            Local6 = (DADR + M646 (DADR, One))
            If ((Local6 != DADR))
            {
                Local7 = M013 (Local6, 0x04, Zero, 0x20)
                If (((Local7 & 0x8000) != Zero))
                {
                    M014 (Local6, 0x04, Zero, 0x20, Local7)
                    Local7 = M013 (Local6, 0x04, Zero, 0x20)
                }
            }

            Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
            If (((Local7 & 0x00030000) != Zero))
            {
                M014 (DADR, (M641 + 0x20), Zero, 0x20, Local7)
                Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
                If (((Local7 & 0x00030000) != Zero))
                {
                    M014 (DADR, (M641 + 0x20), Zero, 0x20, Local7)
                    Local7 = M013 (DADR, (M641 + 0x20), Zero, 0x20)
                }
            }

            M460 ("  FEA-ASL-CpmDiscreteUSB4 PowerOff () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
        }

        Method (SSTA, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
        }

        Method (SINI, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M647 (DADR, 0x0540)
            }
            Else
            {
                M647 (DADR, 0x02A0)
            }
        }

        Method (SREG, 2, Serialized)
        {
            If (((Arg0 == 0x02) && (Arg1 == Zero)))
            {
                If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                {
                    If ((M04B (M620, 0x15) == Zero))
                    {
                        M651 (DADR, M631, 0x10, Zero)
                    }
                    Else
                    {
                        M651 (DADR, M631, 0x50, Zero)
                    }
                }
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0540)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x02A0)
            }
        }

        Method (SDSW, 3, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
            }
        }

        Method (PPS0, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _PS0 () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0540)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _PS0 () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x02A0)
            }

            Local0 = M049 (M620, 0x44)
            Local1 = M009 (M04B (M620, 0x31))
            Local2 = M009 (M04B (M620, 0x2D))
            If (((Local0 == 0x04) || ((Local0 == 0x03) && (Local2 == Zero))))
            {
                M628 ()
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _PS0 () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0540)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _PS0 () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x02A0)
            }
        }

        Method (PPS3, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _PS3 () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0540)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _PS3 () Start\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x02A0)
            }

            If ((M049 (M620, 0x44) == 0x04))
            {
                M629 ()
            }

            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _PS3 () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0540)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _PS3 () End\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x02A0)
            }
        }

        Method (P_ON, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
        }

        Method (P_OF, 0, Serialized)
        {
            If ((M04B (M620, 0x15) == Zero))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 GPP _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
            Else
            {
                M460 ("  FEA-ASL-DiscreteUSB4 PROM DSP _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }
        }

        Name (DADR, 0xEEEEEEEE)
        Name (DSTA, 0xEE)
        Name (DBUS, 0xEEEE)
        Name (PCSA, Zero)
        Name (PWST, 0xD3)
        Name (ESTA, 0xEE)
        Name (EBUS, 0xEEEE)
        Name (PW3S, Zero)
        Name (FLG0, 0xEE)
        Name (M640, 0x33)
        Name (M641, 0x33)
        Name (M642, 0x33)
        Method (_STA, 0, Serialized)  // _STA: Status
        {
            If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
            {
                If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                {
                    ESTA = 0xEE
                    DBUS = 0xEEEE
                    DADR = 0xEEEEEEEE
                }
            }

            If (((DSTA == 0xEE) || (ESTA == 0xEE)))
            {
                If ((DBUS == 0xEEEE))
                {
                    If (CondRefOf (^^EBUS))
                    {
                        DBUS = ^^EBUS /* External reference */
                    }
                    Else
                    {
                        DBUS = Zero
                    }
                }

                If ((DBUS != 0xEEEE))
                {
                    If ((DADR == 0xEEEEEEEE))
                    {
                        Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                            One) & 0x000F8000))
                        Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                        DADR = (M083 + Local0)
                    }

                    Local0 = 0x7FFFFFFF
                    Local0 |= 0x80000000
                    Local1 = M04B (DADR, Zero)
                    If (((Local1 == Local0) || (Local1 == Zero)))
                    {
                        DSTA = Zero
                        ESTA = Zero
                        If ((DBUS != Zero))
                        {
                            If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                            {
                                If ((^^M641 == 0x33))
                                {
                                    Local2 = M646 (^^DADR, 0x10)
                                    ^^M641 = Local2
                                }
                                Else
                                {
                                    Local2 = ^^M641 /* External reference */
                                }

                                If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                            }
                        }
                    }
                    ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                    {
                        EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                        If ((M641 == 0x33))
                        {
                            Local2 = M646 (DADR, 0x10)
                            M641 = Local2
                        }
                        Else
                        {
                            Local2 = M641 /* \_SB_.PCI0.GPP7.M641 */
                        }

                        Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                        If ((Local3 == 0x05))
                        {
                            If ((EBUS == Zero))
                            {
                                DSTA = 0x0F
                                ESTA = 0xEE
                            }
                            ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                            {
                                DSTA = Zero
                                ESTA = Zero
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = 0x0F
                            }
                        }
                        ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                            Zero))
                        {
                            If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                            {
                                DSTA = Zero
                                ESTA = Zero
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = 0xEE
                            }
                        }
                        ElseIf ((EBUS == Zero))
                        {
                            DSTA = 0x0F
                            ESTA = 0xEE
                        }
                        Else
                        {
                            DSTA = 0x0F
                            ESTA = 0x0F
                        }

                        If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                        {
                            If ((M642 == 0x33))
                            {
                                Local4 = M646 (DADR, 0x15)
                            }
                            Else
                            {
                                Local4 = M642 /* \_SB_.PCI0.GPP7.M642 */
                            }

                            If ((Local4 != Zero))
                            {
                                FLG0 = 0x16
                            }
                        }
                    }
                    Else
                    {
                        DSTA = 0x0F
                        ESTA = Zero
                    }
                }
                Else
                {
                    DSTA = Zero
                    ESTA = 0xEE
                }
            }

            If ((FLG0 == 0x16))
            {
                If ((M049 (DADR, 0x19) == Zero))
                {
                    ESTA = 0xEE
                }
            }

            If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
            {
                PWST = 0xD0
            }

            If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
            {
                PWST = 0xD3
            }

            If (CondRefOf (SSTA))
            {
                SSTA ()
            }

            If ((DADR != 0xEEEEEEEE))
            {
                M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
            }

            Return (DSTA) /* \_SB_.PCI0.GPP7.DSTA */
        }

        Method (_INI, 0, Serialized)  // _INI: Initialize
        {
            Local0 = 0x7FFFFFFF
            Local0 |= 0x80000000
            Local1 = M04B (DADR, Zero)
            If (((Local1 == Local0) || (Local1 == Zero)))
            {
                PCSA = Zero
                PWST = 0xD3
                PW3S = Zero
            }
            Else
            {
                PCSA = One
                PWST = 0xD0
                PW3S = One
            }

            M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
            If (CondRefOf (SINI))
            {
                SINI ()
            }
        }

        Method (_REG, 2, Serialized)  // _REG: Region Availability
        {
            If (((Arg0 == 0x02) && (Arg1 == One)))
            {
                If ((DSTA == 0x0F))
                {
                    If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                    {
                        PCSA = One
                    }
                }
            }

            If (((Arg0 == 0x02) && (Arg1 == Zero)))
            {
                PCSA = Zero
            }

            If (CondRefOf (SREG))
            {
                SREG (Arg0, Arg1)
            }

            M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
        }

        Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
        {
            If (CondRefOf (SDSW))
            {
                SDSW (Arg0, Arg1, Arg2)
            }

            If ((DADR != 0xEEEEEEEE))
            {
                M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
            }
        }

        Method (_PS0, 0, Serialized)  // _PS0: Power State 0
        {
            M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
            If (CondRefOf (PPS0))
            {
                PPS0 ()
            }
        }

        Method (_PS3, 0, Serialized)  // _PS3: Power State 3
        {
            M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
            If (CondRefOf (PPS3))
            {
                PPS3 ()
            }
        }

        PowerResource (PWRS, 0x00, 0x0000)
        {
            Method (_STA, 0, Serialized)  // _STA: Status
            {
                If ((DADR != 0xEEEEEEEE))
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                }

                Return (PW3S) /* \_SB_.PCI0.GPP7.PW3S */
            }

            Method (_ON, 0, Serialized)  // _ON_: Power On
            {
                If ((PW3S == Zero))
                {
                    PW3S = One
                    M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                    If (CondRefOf (P_ON))
                    {
                        P_ON ()
                    }
                }
            }

            Method (_OFF, 0, Serialized)  // _OFF: Power Off
            {
                If ((PW3S == One))
                {
                    PW3S = Zero
                    M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                    If (CondRefOf (P_OF))
                    {
                        P_OF ()
                    }
                }
            }
        }

        Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
        {
            PWRS
        })
        Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
        {
            PWRS
        })
        Device (U4UP)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Method (_RMV, 0, Serialized)  // _RMV: Removal Status
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return (Zero)
            }

            Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
            {
                Local0 = M049 (M620, 0x44)
                If ((Local0 == 0x03))
                {
                    Local0 = 0x04
                }

                M460 ("  FEA-ASL-DiscreteUSB4 USP _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                Return (Local0)
            }

            Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
            {
                Local0 = 0x04
                M460 ("  FEA-ASL-DiscreteUSB4 USP _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                Return (Local0)
            }

            Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
            {
                Local0 = 0x04
                M460 ("  FEA-ASL-DiscreteUSB4 USP _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                Return (Local0)
            }

            Name (DPRW, Package (0x02)
            {
                0x16, 
                0x03
            })
            Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
            {
                Local0 = M049 (M620, 0x3D)
                If ((Local0 != 0x16))
                {
                    DPRW [Zero] = Local0
                }

                M460 ("  FEA-ASL-DiscreteUSB4 USP _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.DPRW */
            }

            Method (SSTA, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }

            Method (SINI, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0340)
            }

            Method (SREG, 2, Serialized)
            {
                If (((Arg0 == 0x02) && (Arg1 == Zero)))
                {
                    If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                    {
                        M651 (DADR, M631, 0x70, One)
                    }
                }

                M460 ("  FEA-ASL-DiscreteUSB4 USP _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0340)
            }

            Method (SDSW, 3, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                If ((Arg0 == Zero))
                {
                    Local0 = \_SB.M625 (0x00010193, 0x03E8)
                }
                Else
                {
                    Local0 = \_SB.M625 (0x00010192, 0x03E8)
                }
            }

            Method (PPS0, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0340)
                Local0 = \_SB.M625 (0x00010196, 0x03E8)
                Local0 = \_SB.M625 (0x00010194, 0x03E8)
            }

            Method (PPS3, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M647 (DADR, 0x0340)
                Local0 = \_SB.M625 (0x00010195, 0x03E8)
                Local0 = \_SB.M625 (0x00010199, 0x03E8)
            }

            Method (P_ON, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }

            Method (P_OF, 0, Serialized)
            {
                M460 ("  FEA-ASL-DiscreteUSB4 USP _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
            }

            Name (DADR, 0xEEEEEEEE)
            Name (DSTA, 0xEE)
            Name (DBUS, 0xEEEE)
            Name (PCSA, Zero)
            Name (PWST, 0xD3)
            Name (ESTA, 0xEE)
            Name (EBUS, 0xEEEE)
            Name (PW3S, Zero)
            Name (FLG0, 0xEE)
            Name (M640, 0x33)
            Name (M641, 0x33)
            Name (M642, 0x33)
            Method (_STA, 0, Serialized)  // _STA: Status
            {
                If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                {
                    If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                    {
                        ESTA = 0xEE
                        DBUS = 0xEEEE
                        DADR = 0xEEEEEEEE
                    }
                }

                If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                {
                    If ((DBUS == 0xEEEE))
                    {
                        If (CondRefOf (^^EBUS))
                        {
                            DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.EBUS */
                        }
                        Else
                        {
                            DBUS = Zero
                        }
                    }

                    If ((DBUS != 0xEEEE))
                    {
                        If ((DADR == 0xEEEEEEEE))
                        {
                            Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                One) & 0x000F8000))
                            Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                            DADR = (M083 + Local0)
                        }

                        Local0 = 0x7FFFFFFF
                        Local0 |= 0x80000000
                        Local1 = M04B (DADR, Zero)
                        If (((Local1 == Local0) || (Local1 == Zero)))
                        {
                            DSTA = Zero
                            ESTA = Zero
                            If ((DBUS != Zero))
                            {
                                If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                {
                                    If ((^^M641 == 0x33))
                                    {
                                        Local2 = M646 (^^DADR, 0x10)
                                        ^^M641 = Local2
                                    }
                                    Else
                                    {
                                        Local2 = ^^M641 /* \_SB_.PCI0.GPP7.M641 */
                                    }

                                    If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                }
                            }
                        }
                        ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                        {
                            EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                            If ((M641 == 0x33))
                            {
                                Local2 = M646 (DADR, 0x10)
                                M641 = Local2
                            }
                            Else
                            {
                                Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.M641 */
                            }

                            Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                            If ((Local3 == 0x05))
                            {
                                If ((EBUS == Zero))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                                ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0x0F
                                }
                            }
                            ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                Zero))
                            {
                                If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                            }
                            ElseIf ((EBUS == Zero))
                            {
                                DSTA = 0x0F
                                ESTA = 0xEE
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = 0x0F
                            }

                            If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                            {
                                If ((M642 == 0x33))
                                {
                                    Local4 = M646 (DADR, 0x15)
                                }
                                Else
                                {
                                    Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.M642 */
                                }

                                If ((Local4 != Zero))
                                {
                                    FLG0 = 0x16
                                }
                            }
                        }
                        Else
                        {
                            DSTA = 0x0F
                            ESTA = Zero
                        }
                    }
                    Else
                    {
                        DSTA = Zero
                        ESTA = 0xEE
                    }
                }

                If ((FLG0 == 0x16))
                {
                    If ((M049 (DADR, 0x19) == Zero))
                    {
                        ESTA = 0xEE
                    }
                }

                If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                {
                    PWST = 0xD0
                }

                If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                {
                    PWST = 0xD3
                }

                If (CondRefOf (SSTA))
                {
                    SSTA ()
                }

                If ((DADR != 0xEEEEEEEE))
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                }

                Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.DSTA */
            }

            Method (_INI, 0, Serialized)  // _INI: Initialize
            {
                Local0 = 0x7FFFFFFF
                Local0 |= 0x80000000
                Local1 = M04B (DADR, Zero)
                If (((Local1 == Local0) || (Local1 == Zero)))
                {
                    PCSA = Zero
                    PWST = 0xD3
                    PW3S = Zero
                }
                Else
                {
                    PCSA = One
                    PWST = 0xD0
                    PW3S = One
                }

                M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                If (CondRefOf (SINI))
                {
                    SINI ()
                }
            }

            Method (_REG, 2, Serialized)  // _REG: Region Availability
            {
                If (((Arg0 == 0x02) && (Arg1 == One)))
                {
                    If ((DSTA == 0x0F))
                    {
                        If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                        {
                            PCSA = One
                        }
                    }
                }

                If (((Arg0 == 0x02) && (Arg1 == Zero)))
                {
                    PCSA = Zero
                }

                If (CondRefOf (SREG))
                {
                    SREG (Arg0, Arg1)
                }

                M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
            }

            Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
            {
                If (CondRefOf (SDSW))
                {
                    SDSW (Arg0, Arg1, Arg2)
                }

                If ((DADR != 0xEEEEEEEE))
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                }
            }

            Method (_PS0, 0, Serialized)  // _PS0: Power State 0
            {
                M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                If (CondRefOf (PPS0))
                {
                    PPS0 ()
                }
            }

            Method (_PS3, 0, Serialized)  // _PS3: Power State 3
            {
                M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                If (CondRefOf (PPS3))
                {
                    PPS3 ()
                }
            }

            PowerResource (PWRS, 0x00, 0x0000)
            {
                Method (_STA, 0, Serialized)  // _STA: Status
                {
                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                    }

                    Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.PW3S */
                }

                Method (_ON, 0, Serialized)  // _ON_: Power On
                {
                    If ((PW3S == Zero))
                    {
                        PW3S = One
                        M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        If (CondRefOf (P_ON))
                        {
                            P_ON ()
                        }
                    }
                }

                Method (_OFF, 0, Serialized)  // _OFF: Power Off
                {
                    If ((PW3S == One))
                    {
                        PW3S = Zero
                        M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        If (CondRefOf (P_OF))
                        {
                            P_OF ()
                        }
                    }
                }
            }

            Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
            {
                PWRS
            })
            Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
            {
                PWRS
            })
            Device (U4P0)
            {
                Name (_ADR, Zero)  // _ADR: Address
                Name (XDSD, Package (0x0A)
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x04
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }, 

                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            Zero
                        }
                    }
                })
                Name (YDSD, Package (0x0A)
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x04
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }, 

                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            Zero
                        }
                    }
                })
                Name (_DSD, Package (0x08)  // _DSD: Device-Specific Data
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x04
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }
                })
                Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }

                Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                {
                    Local0 = M049 (M620, 0x44)
                    If ((Local0 == 0x03))
                    {
                        Local0 = 0x04
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Name (DPRW, Package (0x02)
                {
                    0x16, 
                    0x03
                })
                Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                {
                    Local0 = M049 (M620, 0x3D)
                    If ((Local0 != 0x16))
                    {
                        DPRW [Zero] = Local0
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P0.DPRW */
                }

                Method (SSTA, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (SINI, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SREG, 2, Serialized)
                {
                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                        {
                            M651 (DADR, M631, 0xA8, One)
                        }
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SDSW, 3, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    If ((Arg0 == Zero))
                    {
                        Local0 = \_SB.M625 (0x00020193, 0x03E8)
                    }
                    Else
                    {
                        Local0 = \_SB.M625 (0x00020192, 0x03E8)
                    }
                }

                Method (PPS0, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00020196, 0x03E8)
                    Local0 = \_SB.M625 (0x00020194, 0x03E8)
                }

                Method (PPS3, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00020195, 0x03E8)
                    Local0 = \_SB.M625 (0x00020199, 0x03E8)
                }

                Method (P_ON, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (P_OF, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP0 _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Name (DADR, 0xEEEEEEEE)
                Name (DSTA, 0xEE)
                Name (DBUS, 0xEEEE)
                Name (PCSA, Zero)
                Name (PWST, 0xD3)
                Name (ESTA, 0xEE)
                Name (EBUS, 0xEEEE)
                Name (PW3S, Zero)
                Name (FLG0, 0xEE)
                Name (M640, 0x33)
                Name (M641, 0x33)
                Name (M642, 0x33)
                Method (_STA, 0, Serialized)  // _STA: Status
                {
                    If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                    {
                        If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                        {
                            ESTA = 0xEE
                            DBUS = 0xEEEE
                            DADR = 0xEEEEEEEE
                        }
                    }

                    If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                    {
                        If ((DBUS == 0xEEEE))
                        {
                            If (CondRefOf (^^EBUS))
                            {
                                DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.EBUS */
                            }
                            Else
                            {
                                DBUS = Zero
                            }
                        }

                        If ((DBUS != 0xEEEE))
                        {
                            If ((DADR == 0xEEEEEEEE))
                            {
                                Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                    One) & 0x000F8000))
                                Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                DADR = (M083 + Local0)
                            }

                            Local0 = 0x7FFFFFFF
                            Local0 |= 0x80000000
                            Local1 = M04B (DADR, Zero)
                            If (((Local1 == Local0) || (Local1 == Zero)))
                            {
                                DSTA = Zero
                                ESTA = Zero
                                If ((DBUS != Zero))
                                {
                                    If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                    {
                                        If ((^^M641 == 0x33))
                                        {
                                            Local2 = M646 (^^DADR, 0x10)
                                            ^^M641 = Local2
                                        }
                                        Else
                                        {
                                            Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.M641 */
                                        }

                                        If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                }
                            }
                            ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                            {
                                EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                If ((M641 == 0x33))
                                {
                                    Local2 = M646 (DADR, 0x10)
                                    M641 = Local2
                                }
                                Else
                                {
                                    Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P0.M641 */
                                }

                                Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                If ((Local3 == 0x05))
                                {
                                    If ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }
                                }
                                ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                    Zero))
                                {
                                    If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                }
                                ElseIf ((EBUS == Zero))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0x0F
                                }

                                If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                {
                                    If ((M642 == 0x33))
                                    {
                                        Local4 = M646 (DADR, 0x15)
                                    }
                                    Else
                                    {
                                        Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P0.M642 */
                                    }

                                    If ((Local4 != Zero))
                                    {
                                        FLG0 = 0x16
                                    }
                                }
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = Zero
                            }
                        }
                        Else
                        {
                            DSTA = Zero
                            ESTA = 0xEE
                        }
                    }

                    If ((FLG0 == 0x16))
                    {
                        If ((M049 (DADR, 0x19) == Zero))
                        {
                            ESTA = 0xEE
                        }
                    }

                    If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                    {
                        PWST = 0xD0
                    }

                    If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                    {
                        PWST = 0xD3
                    }

                    If (CondRefOf (SSTA))
                    {
                        SSTA ()
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                    }

                    Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P0.DSTA */
                }

                Method (_INI, 0, Serialized)  // _INI: Initialize
                {
                    Local0 = 0x7FFFFFFF
                    Local0 |= 0x80000000
                    Local1 = M04B (DADR, Zero)
                    If (((Local1 == Local0) || (Local1 == Zero)))
                    {
                        PCSA = Zero
                        PWST = 0xD3
                        PW3S = Zero
                    }
                    Else
                    {
                        PCSA = One
                        PWST = 0xD0
                        PW3S = One
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                    If (CondRefOf (SINI))
                    {
                        SINI ()
                    }
                }

                Method (_REG, 2, Serialized)  // _REG: Region Availability
                {
                    If (((Arg0 == 0x02) && (Arg1 == One)))
                    {
                        If ((DSTA == 0x0F))
                        {
                            If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                            {
                                PCSA = One
                            }
                        }
                    }

                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        PCSA = Zero
                    }

                    If (CondRefOf (SREG))
                    {
                        SREG (Arg0, Arg1)
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                }

                Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                {
                    If (CondRefOf (SDSW))
                    {
                        SDSW (Arg0, Arg1, Arg2)
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                    }
                }

                Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS0))
                    {
                        PPS0 ()
                    }
                }

                Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS3))
                    {
                        PPS3 ()
                    }
                }

                PowerResource (PWRS, 0x00, 0x0000)
                {
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        }

                        Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P0.PW3S */
                    }

                    Method (_ON, 0, Serialized)  // _ON_: Power On
                    {
                        If ((PW3S == Zero))
                        {
                            PW3S = One
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_ON))
                            {
                                P_ON ()
                            }
                        }
                    }

                    Method (_OFF, 0, Serialized)  // _OFF: Power Off
                    {
                        If ((PW3S == One))
                        {
                            PW3S = Zero
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_OF))
                            {
                                P_OF ()
                            }
                        }
                    }
                }

                Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                {
                    PWRS
                })
                Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                {
                    PWRS
                })
                Device (UP0D)
                {
                    Name (_ADR, Zero)  // _ADR: Address
                    Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return (Zero)
                    }

                    Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                    {
                        Local0 = M049 (M620, 0x44)
                        If ((Local0 == 0x03))
                        {
                            Local0 = 0x04
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Name (DPRW, Package (0x02)
                    {
                        0x16, 
                        0x03
                    })
                    Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                    {
                        Local0 = M049 (M620, 0x3D)
                        If ((Local0 != 0x16))
                        {
                            DPRW [Zero] = Local0
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P0.UP0D.DPRW */
                    }

                    Method (SSTA, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (SINI, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (SREG, 2, Serialized)
                    {
                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                            {
                                M651 (DADR, M631, 0x0188, One)
                            }
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (SDSW, 3, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    }

                    Method (PPS0, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (PPS3, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (P_ON, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (P_OF, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP0 EP _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Name (DADR, 0xEEEEEEEE)
                    Name (DSTA, 0xEE)
                    Name (DBUS, 0xEEEE)
                    Name (PCSA, Zero)
                    Name (PWST, 0xD3)
                    Name (ESTA, 0xEE)
                    Name (EBUS, 0xEEEE)
                    Name (PW3S, Zero)
                    Name (FLG0, 0xEE)
                    Name (M640, 0x33)
                    Name (M641, 0x33)
                    Name (M642, 0x33)
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                        {
                            If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                            {
                                ESTA = 0xEE
                                DBUS = 0xEEEE
                                DADR = 0xEEEEEEEE
                            }
                        }

                        If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                        {
                            If ((DBUS == 0xEEEE))
                            {
                                If (CondRefOf (^^EBUS))
                                {
                                    DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.U4P0.EBUS */
                                }
                                Else
                                {
                                    DBUS = Zero
                                }
                            }

                            If ((DBUS != 0xEEEE))
                            {
                                If ((DADR == 0xEEEEEEEE))
                                {
                                    Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                        One) & 0x000F8000))
                                    Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                    DADR = (M083 + Local0)
                                }

                                Local0 = 0x7FFFFFFF
                                Local0 |= 0x80000000
                                Local1 = M04B (DADR, Zero)
                                If (((Local1 == Local0) || (Local1 == Zero)))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                    If ((DBUS != Zero))
                                    {
                                        If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                        {
                                            If ((^^M641 == 0x33))
                                            {
                                                Local2 = M646 (^^DADR, 0x10)
                                                ^^M641 = Local2
                                            }
                                            Else
                                            {
                                                Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.U4P0.M641 */
                                            }

                                            If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                            {
                                                DSTA = 0x0F
                                                ESTA = 0xEE
                                            }
                                        }
                                    }
                                }
                                ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                                {
                                    EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                    If ((M641 == 0x33))
                                    {
                                        Local2 = M646 (DADR, 0x10)
                                        M641 = Local2
                                    }
                                    Else
                                    {
                                        Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P0.UP0D.M641 */
                                    }

                                    Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                    If ((Local3 == 0x05))
                                    {
                                        If ((EBUS == Zero))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                        ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0x0F
                                        }
                                    }
                                    ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                        Zero))
                                    {
                                        If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                    ElseIf ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }

                                    If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                    {
                                        If ((M642 == 0x33))
                                        {
                                            Local4 = M646 (DADR, 0x15)
                                        }
                                        Else
                                        {
                                            Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P0.UP0D.M642 */
                                        }

                                        If ((Local4 != Zero))
                                        {
                                            FLG0 = 0x16
                                        }
                                    }
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = Zero
                                }
                            }
                            Else
                            {
                                DSTA = Zero
                                ESTA = 0xEE
                            }
                        }

                        If ((FLG0 == 0x16))
                        {
                            If ((M049 (DADR, 0x19) == Zero))
                            {
                                ESTA = 0xEE
                            }
                        }

                        If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                        {
                            PWST = 0xD0
                        }

                        If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                        {
                            PWST = 0xD3
                        }

                        If (CondRefOf (SSTA))
                        {
                            SSTA ()
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                        }

                        Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P0.UP0D.DSTA */
                    }

                    Method (_INI, 0, Serialized)  // _INI: Initialize
                    {
                        Local0 = 0x7FFFFFFF
                        Local0 |= 0x80000000
                        Local1 = M04B (DADR, Zero)
                        If (((Local1 == Local0) || (Local1 == Zero)))
                        {
                            PCSA = Zero
                            PWST = 0xD3
                            PW3S = Zero
                        }
                        Else
                        {
                            PCSA = One
                            PWST = 0xD0
                            PW3S = One
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                        If (CondRefOf (SINI))
                        {
                            SINI ()
                        }
                    }

                    Method (_REG, 2, Serialized)  // _REG: Region Availability
                    {
                        If (((Arg0 == 0x02) && (Arg1 == One)))
                        {
                            If ((DSTA == 0x0F))
                            {
                                If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                                {
                                    PCSA = One
                                }
                            }
                        }

                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            PCSA = Zero
                        }

                        If (CondRefOf (SREG))
                        {
                            SREG (Arg0, Arg1)
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                    }

                    Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                    {
                        If (CondRefOf (SDSW))
                        {
                            SDSW (Arg0, Arg1, Arg2)
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                        }
                    }

                    Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS0))
                        {
                            PPS0 ()
                        }
                    }

                    Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS3))
                        {
                            PPS3 ()
                        }
                    }

                    PowerResource (PWRS, 0x00, 0x0000)
                    {
                        Method (_STA, 0, Serialized)  // _STA: Status
                        {
                            If ((DADR != 0xEEEEEEEE))
                            {
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            }

                            Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P0.UP0D.PW3S */
                        }

                        Method (_ON, 0, Serialized)  // _ON_: Power On
                        {
                            If ((PW3S == Zero))
                            {
                                PW3S = One
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_ON))
                                {
                                    P_ON ()
                                }
                            }
                        }

                        Method (_OFF, 0, Serialized)  // _OFF: Power Off
                        {
                            If ((PW3S == One))
                            {
                                PW3S = Zero
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_OF))
                                {
                                    P_OF ()
                                }
                            }
                        }
                    }

                    Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                    {
                        PWRS
                    })
                    Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                    {
                        PWRS
                    })
                }
            }

            Device (U4P1)
            {
                Name (_ADR, 0x00010000)  // _ADR: Address
                Name (XDSD, Package (0x0A)
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x05
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }, 

                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            One
                        }
                    }
                })
                Name (YDSD, Package (0x0A)
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x05
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }, 

                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            One
                        }
                    }
                })
                Name (_DSD, Package (0x08)  // _DSD: Device-Specific Data
                {
                    ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "HotPlugSupportInD3", 
                            One
                        }
                    }, 

                    ToUUID ("efcc06cc-73ac-4bc3-bff0-76143807c389") /* Unknown UUID */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "ExternalFacingPort", 
                            One
                        }, 

                        Package (0x02)
                        {
                            "UID", 
                            0x05
                        }
                    }, 

                    ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "IMR_VALID", 
                            One
                        }
                    }, 

                    ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                    Package (0x01)
                    {
                        Package (0x02)
                        {
                            "WAKE_SUPPORTED", 
                            One
                        }
                    }
                })
                Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }

                Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                {
                    Local0 = M049 (M620, 0x44)
                    If ((Local0 == 0x03))
                    {
                        Local0 = 0x04
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Name (DPRW, Package (0x02)
                {
                    0x16, 
                    0x03
                })
                Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                {
                    Local0 = M049 (M620, 0x3D)
                    If ((Local0 != 0x16))
                    {
                        DPRW [Zero] = Local0
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P1.DPRW */
                }

                Method (SSTA, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (SINI, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SREG, 2, Serialized)
                {
                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                        {
                            M651 (DADR, M631, 0xE0, One)
                        }
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SDSW, 3, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    If ((Arg0 == Zero))
                    {
                        Local0 = \_SB.M625 (0x00040193, 0x03E8)
                    }
                    Else
                    {
                        Local0 = \_SB.M625 (0x00040192, 0x03E8)
                    }
                }

                Method (PPS0, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00040196, 0x03E8)
                    Local0 = \_SB.M625 (0x00040194, 0x03E8)
                }

                Method (PPS3, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00040195, 0x03E8)
                    Local0 = \_SB.M625 (0x00040199, 0x03E8)
                }

                Method (P_ON, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (P_OF, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP1 _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Name (DADR, 0xEEEEEEEE)
                Name (DSTA, 0xEE)
                Name (DBUS, 0xEEEE)
                Name (PCSA, Zero)
                Name (PWST, 0xD3)
                Name (ESTA, 0xEE)
                Name (EBUS, 0xEEEE)
                Name (PW3S, Zero)
                Name (FLG0, 0xEE)
                Name (M640, 0x33)
                Name (M641, 0x33)
                Name (M642, 0x33)
                Method (_STA, 0, Serialized)  // _STA: Status
                {
                    If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                    {
                        If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                        {
                            ESTA = 0xEE
                            DBUS = 0xEEEE
                            DADR = 0xEEEEEEEE
                        }
                    }

                    If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                    {
                        If ((DBUS == 0xEEEE))
                        {
                            If (CondRefOf (^^EBUS))
                            {
                                DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.EBUS */
                            }
                            Else
                            {
                                DBUS = Zero
                            }
                        }

                        If ((DBUS != 0xEEEE))
                        {
                            If ((DADR == 0xEEEEEEEE))
                            {
                                Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                    One) & 0x000F8000))
                                Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                DADR = (M083 + Local0)
                            }

                            Local0 = 0x7FFFFFFF
                            Local0 |= 0x80000000
                            Local1 = M04B (DADR, Zero)
                            If (((Local1 == Local0) || (Local1 == Zero)))
                            {
                                DSTA = Zero
                                ESTA = Zero
                                If ((DBUS != Zero))
                                {
                                    If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                    {
                                        If ((^^M641 == 0x33))
                                        {
                                            Local2 = M646 (^^DADR, 0x10)
                                            ^^M641 = Local2
                                        }
                                        Else
                                        {
                                            Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.M641 */
                                        }

                                        If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                }
                            }
                            ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                            {
                                EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                If ((M641 == 0x33))
                                {
                                    Local2 = M646 (DADR, 0x10)
                                    M641 = Local2
                                }
                                Else
                                {
                                    Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P1.M641 */
                                }

                                Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                If ((Local3 == 0x05))
                                {
                                    If ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }
                                }
                                ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                    Zero))
                                {
                                    If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                }
                                ElseIf ((EBUS == Zero))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0x0F
                                }

                                If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                {
                                    If ((M642 == 0x33))
                                    {
                                        Local4 = M646 (DADR, 0x15)
                                    }
                                    Else
                                    {
                                        Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P1.M642 */
                                    }

                                    If ((Local4 != Zero))
                                    {
                                        FLG0 = 0x16
                                    }
                                }
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = Zero
                            }
                        }
                        Else
                        {
                            DSTA = Zero
                            ESTA = 0xEE
                        }
                    }

                    If ((FLG0 == 0x16))
                    {
                        If ((M049 (DADR, 0x19) == Zero))
                        {
                            ESTA = 0xEE
                        }
                    }

                    If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                    {
                        PWST = 0xD0
                    }

                    If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                    {
                        PWST = 0xD3
                    }

                    If (CondRefOf (SSTA))
                    {
                        SSTA ()
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                    }

                    Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P1.DSTA */
                }

                Method (_INI, 0, Serialized)  // _INI: Initialize
                {
                    Local0 = 0x7FFFFFFF
                    Local0 |= 0x80000000
                    Local1 = M04B (DADR, Zero)
                    If (((Local1 == Local0) || (Local1 == Zero)))
                    {
                        PCSA = Zero
                        PWST = 0xD3
                        PW3S = Zero
                    }
                    Else
                    {
                        PCSA = One
                        PWST = 0xD0
                        PW3S = One
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                    If (CondRefOf (SINI))
                    {
                        SINI ()
                    }
                }

                Method (_REG, 2, Serialized)  // _REG: Region Availability
                {
                    If (((Arg0 == 0x02) && (Arg1 == One)))
                    {
                        If ((DSTA == 0x0F))
                        {
                            If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                            {
                                PCSA = One
                            }
                        }
                    }

                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        PCSA = Zero
                    }

                    If (CondRefOf (SREG))
                    {
                        SREG (Arg0, Arg1)
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                }

                Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                {
                    If (CondRefOf (SDSW))
                    {
                        SDSW (Arg0, Arg1, Arg2)
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                    }
                }

                Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS0))
                    {
                        PPS0 ()
                    }
                }

                Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS3))
                    {
                        PPS3 ()
                    }
                }

                PowerResource (PWRS, 0x00, 0x0000)
                {
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        }

                        Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P1.PW3S */
                    }

                    Method (_ON, 0, Serialized)  // _ON_: Power On
                    {
                        If ((PW3S == Zero))
                        {
                            PW3S = One
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_ON))
                            {
                                P_ON ()
                            }
                        }
                    }

                    Method (_OFF, 0, Serialized)  // _OFF: Power Off
                    {
                        If ((PW3S == One))
                        {
                            PW3S = Zero
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_OF))
                            {
                                P_OF ()
                            }
                        }
                    }
                }

                Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                {
                    PWRS
                })
                Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                {
                    PWRS
                })
                Device (UP1D)
                {
                    Name (_ADR, Zero)  // _ADR: Address
                    Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return (Zero)
                    }

                    Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                    {
                        Local0 = M049 (M620, 0x44)
                        If ((Local0 == 0x03))
                        {
                            Local0 = 0x04
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Name (DPRW, Package (0x02)
                    {
                        0x16, 
                        0x03
                    })
                    Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                    {
                        Local0 = M049 (M620, 0x3D)
                        If ((Local0 != 0x16))
                        {
                            DPRW [Zero] = Local0
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P1.UP1D.DPRW */
                    }

                    Method (SSTA, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (SINI, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (SREG, 2, Serialized)
                    {
                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                            {
                                M651 (DADR, M631, 0x01C0, One)
                            }
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (SDSW, 3, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    }

                    Method (PPS0, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (PPS3, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x0B20)
                    }

                    Method (P_ON, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (P_OF, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP1 EP _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Name (DADR, 0xEEEEEEEE)
                    Name (DSTA, 0xEE)
                    Name (DBUS, 0xEEEE)
                    Name (PCSA, Zero)
                    Name (PWST, 0xD3)
                    Name (ESTA, 0xEE)
                    Name (EBUS, 0xEEEE)
                    Name (PW3S, Zero)
                    Name (FLG0, 0xEE)
                    Name (M640, 0x33)
                    Name (M641, 0x33)
                    Name (M642, 0x33)
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                        {
                            If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                            {
                                ESTA = 0xEE
                                DBUS = 0xEEEE
                                DADR = 0xEEEEEEEE
                            }
                        }

                        If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                        {
                            If ((DBUS == 0xEEEE))
                            {
                                If (CondRefOf (^^EBUS))
                                {
                                    DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.U4P1.EBUS */
                                }
                                Else
                                {
                                    DBUS = Zero
                                }
                            }

                            If ((DBUS != 0xEEEE))
                            {
                                If ((DADR == 0xEEEEEEEE))
                                {
                                    Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                        One) & 0x000F8000))
                                    Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                    DADR = (M083 + Local0)
                                }

                                Local0 = 0x7FFFFFFF
                                Local0 |= 0x80000000
                                Local1 = M04B (DADR, Zero)
                                If (((Local1 == Local0) || (Local1 == Zero)))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                    If ((DBUS != Zero))
                                    {
                                        If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                        {
                                            If ((^^M641 == 0x33))
                                            {
                                                Local2 = M646 (^^DADR, 0x10)
                                                ^^M641 = Local2
                                            }
                                            Else
                                            {
                                                Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.U4P1.M641 */
                                            }

                                            If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                            {
                                                DSTA = 0x0F
                                                ESTA = 0xEE
                                            }
                                        }
                                    }
                                }
                                ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                                {
                                    EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                    If ((M641 == 0x33))
                                    {
                                        Local2 = M646 (DADR, 0x10)
                                        M641 = Local2
                                    }
                                    Else
                                    {
                                        Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P1.UP1D.M641 */
                                    }

                                    Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                    If ((Local3 == 0x05))
                                    {
                                        If ((EBUS == Zero))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                        ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0x0F
                                        }
                                    }
                                    ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                        Zero))
                                    {
                                        If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                    ElseIf ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }

                                    If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                    {
                                        If ((M642 == 0x33))
                                        {
                                            Local4 = M646 (DADR, 0x15)
                                        }
                                        Else
                                        {
                                            Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P1.UP1D.M642 */
                                        }

                                        If ((Local4 != Zero))
                                        {
                                            FLG0 = 0x16
                                        }
                                    }
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = Zero
                                }
                            }
                            Else
                            {
                                DSTA = Zero
                                ESTA = 0xEE
                            }
                        }

                        If ((FLG0 == 0x16))
                        {
                            If ((M049 (DADR, 0x19) == Zero))
                            {
                                ESTA = 0xEE
                            }
                        }

                        If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                        {
                            PWST = 0xD0
                        }

                        If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                        {
                            PWST = 0xD3
                        }

                        If (CondRefOf (SSTA))
                        {
                            SSTA ()
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                        }

                        Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P1.UP1D.DSTA */
                    }

                    Method (_INI, 0, Serialized)  // _INI: Initialize
                    {
                        Local0 = 0x7FFFFFFF
                        Local0 |= 0x80000000
                        Local1 = M04B (DADR, Zero)
                        If (((Local1 == Local0) || (Local1 == Zero)))
                        {
                            PCSA = Zero
                            PWST = 0xD3
                            PW3S = Zero
                        }
                        Else
                        {
                            PCSA = One
                            PWST = 0xD0
                            PW3S = One
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                        If (CondRefOf (SINI))
                        {
                            SINI ()
                        }
                    }

                    Method (_REG, 2, Serialized)  // _REG: Region Availability
                    {
                        If (((Arg0 == 0x02) && (Arg1 == One)))
                        {
                            If ((DSTA == 0x0F))
                            {
                                If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                                {
                                    PCSA = One
                                }
                            }
                        }

                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            PCSA = Zero
                        }

                        If (CondRefOf (SREG))
                        {
                            SREG (Arg0, Arg1)
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                    }

                    Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                    {
                        If (CondRefOf (SDSW))
                        {
                            SDSW (Arg0, Arg1, Arg2)
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                        }
                    }

                    Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS0))
                        {
                            PPS0 ()
                        }
                    }

                    Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS3))
                        {
                            PPS3 ()
                        }
                    }

                    PowerResource (PWRS, 0x00, 0x0000)
                    {
                        Method (_STA, 0, Serialized)  // _STA: Status
                        {
                            If ((DADR != 0xEEEEEEEE))
                            {
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            }

                            Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P1.UP1D.PW3S */
                        }

                        Method (_ON, 0, Serialized)  // _ON_: Power On
                        {
                            If ((PW3S == Zero))
                            {
                                PW3S = One
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_ON))
                                {
                                    P_ON ()
                                }
                            }
                        }

                        Method (_OFF, 0, Serialized)  // _OFF: Power Off
                        {
                            If ((PW3S == One))
                            {
                                PW3S = Zero
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_OF))
                                {
                                    P_OF ()
                                }
                            }
                        }
                    }

                    Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                    {
                        PWRS
                    })
                    Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                    {
                        PWRS
                    })
                }
            }

            Device (U4P2)
            {
                Name (_ADR, 0x00020000)  // _ADR: Address
                Name (XDSD, Package (0x02)
                {
                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            0x02
                        }
                    }
                })
                Name (YDSD, Package (0x02)
                {
                    ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                    Package (0x02)
                    {
                        Package (0x02)
                        {
                            "usb4-host-interface", 
                            \_SB.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0
                        }, 

                        Package (0x02)
                        {
                            "usb4-port-number", 
                            0x02
                        }
                    }
                })
                Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }

                Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                {
                    Local0 = M049 (M620, 0x44)
                    If ((Local0 == 0x03))
                    {
                        Local0 = 0x04
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Name (DPRW, Package (0x02)
                {
                    0x16, 
                    0x03
                })
                Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                {
                    Local0 = M049 (M620, 0x3D)
                    If ((Local0 != 0x16))
                    {
                        DPRW [Zero] = Local0
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P2.DPRW */
                }

                Method (SSTA, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (SINI, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SREG, 2, Serialized)
                {
                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                        {
                            M651 (DADR, M631, 0x0118, One)
                        }
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SDSW, 3, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    If ((Arg0 == Zero))
                    {
                        Local0 = \_SB.M625 (0x00080193, 0x03E8)
                    }
                    Else
                    {
                        Local0 = \_SB.M625 (0x00080192, 0x03E8)
                    }
                }

                Method (PPS0, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00080196, 0x03E8)
                    Local0 = \_SB.M625 (0x00080194, 0x03E8)
                }

                Method (PPS3, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00080195, 0x03E8)
                    Local0 = \_SB.M625 (0x00080199, 0x03E8)
                }

                Method (P_ON, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (P_OF, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP2 _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Name (DADR, 0xEEEEEEEE)
                Name (DSTA, 0xEE)
                Name (DBUS, 0xEEEE)
                Name (PCSA, Zero)
                Name (PWST, 0xD3)
                Name (ESTA, 0xEE)
                Name (EBUS, 0xEEEE)
                Name (PW3S, Zero)
                Name (FLG0, 0xEE)
                Name (M640, 0x33)
                Name (M641, 0x33)
                Name (M642, 0x33)
                Method (_STA, 0, Serialized)  // _STA: Status
                {
                    If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                    {
                        If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                        {
                            ESTA = 0xEE
                            DBUS = 0xEEEE
                            DADR = 0xEEEEEEEE
                        }
                    }

                    If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                    {
                        If ((DBUS == 0xEEEE))
                        {
                            If (CondRefOf (^^EBUS))
                            {
                                DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.EBUS */
                            }
                            Else
                            {
                                DBUS = Zero
                            }
                        }

                        If ((DBUS != 0xEEEE))
                        {
                            If ((DADR == 0xEEEEEEEE))
                            {
                                Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                    One) & 0x000F8000))
                                Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                DADR = (M083 + Local0)
                            }

                            Local0 = 0x7FFFFFFF
                            Local0 |= 0x80000000
                            Local1 = M04B (DADR, Zero)
                            If (((Local1 == Local0) || (Local1 == Zero)))
                            {
                                DSTA = Zero
                                ESTA = Zero
                                If ((DBUS != Zero))
                                {
                                    If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                    {
                                        If ((^^M641 == 0x33))
                                        {
                                            Local2 = M646 (^^DADR, 0x10)
                                            ^^M641 = Local2
                                        }
                                        Else
                                        {
                                            Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.M641 */
                                        }

                                        If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                }
                            }
                            ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                            {
                                EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                If ((M641 == 0x33))
                                {
                                    Local2 = M646 (DADR, 0x10)
                                    M641 = Local2
                                }
                                Else
                                {
                                    Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P2.M641 */
                                }

                                Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                If ((Local3 == 0x05))
                                {
                                    If ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }
                                }
                                ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                    Zero))
                                {
                                    If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                }
                                ElseIf ((EBUS == Zero))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0x0F
                                }

                                If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                {
                                    If ((M642 == 0x33))
                                    {
                                        Local4 = M646 (DADR, 0x15)
                                    }
                                    Else
                                    {
                                        Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P2.M642 */
                                    }

                                    If ((Local4 != Zero))
                                    {
                                        FLG0 = 0x16
                                    }
                                }
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = Zero
                            }
                        }
                        Else
                        {
                            DSTA = Zero
                            ESTA = 0xEE
                        }
                    }

                    If ((FLG0 == 0x16))
                    {
                        If ((M049 (DADR, 0x19) == Zero))
                        {
                            ESTA = 0xEE
                        }
                    }

                    If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                    {
                        PWST = 0xD0
                    }

                    If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                    {
                        PWST = 0xD3
                    }

                    If (CondRefOf (SSTA))
                    {
                        SSTA ()
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                    }

                    Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P2.DSTA */
                }

                Method (_INI, 0, Serialized)  // _INI: Initialize
                {
                    Local0 = 0x7FFFFFFF
                    Local0 |= 0x80000000
                    Local1 = M04B (DADR, Zero)
                    If (((Local1 == Local0) || (Local1 == Zero)))
                    {
                        PCSA = Zero
                        PWST = 0xD3
                        PW3S = Zero
                    }
                    Else
                    {
                        PCSA = One
                        PWST = 0xD0
                        PW3S = One
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                    If (CondRefOf (SINI))
                    {
                        SINI ()
                    }
                }

                Method (_REG, 2, Serialized)  // _REG: Region Availability
                {
                    If (((Arg0 == 0x02) && (Arg1 == One)))
                    {
                        If ((DSTA == 0x0F))
                        {
                            If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                            {
                                PCSA = One
                            }
                        }
                    }

                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        PCSA = Zero
                    }

                    If (CondRefOf (SREG))
                    {
                        SREG (Arg0, Arg1)
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                }

                Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                {
                    If (CondRefOf (SDSW))
                    {
                        SDSW (Arg0, Arg1, Arg2)
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                    }
                }

                Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS0))
                    {
                        PPS0 ()
                    }
                }

                Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS3))
                    {
                        PPS3 ()
                    }
                }

                PowerResource (PWRS, 0x00, 0x0000)
                {
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        }

                        Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P2.PW3S */
                    }

                    Method (_ON, 0, Serialized)  // _ON_: Power On
                    {
                        If ((PW3S == Zero))
                        {
                            PW3S = One
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_ON))
                            {
                                P_ON ()
                            }
                        }
                    }

                    Method (_OFF, 0, Serialized)  // _OFF: Power Off
                    {
                        If ((PW3S == One))
                        {
                            PW3S = Zero
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_OF))
                            {
                                P_OF ()
                            }
                        }
                    }
                }

                Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                {
                    PWRS
                })
                Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                {
                    PWRS
                })
                Device (UXHC)
                {
                    Name (_ADR, Zero)  // _ADR: Address
                    Name (_STR, Unicode ("USB4 xHC Controller"))  // _STR: Description String
                    Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return (Zero)
                    }

                    Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                    {
                        Local0 = M049 (M620, 0x44)
                        If ((Local0 == 0x03))
                        {
                            Local0 = 0x04
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Name (DPRW, Package (0x02)
                    {
                        0x16, 
                        0x03
                    })
                    Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                    {
                        Local0 = M049 (M620, 0x3D)
                        If ((Local0 != 0x16))
                        {
                            DPRW [Zero] = Local0
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.DPRW */
                    }

                    Method (SSTA, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (SINI, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                    }

                    Method (SREG, 2, Serialized)
                    {
                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                            {
                                M04E (M631, 0x01F8, M04B (DADR, 0x10))
                                M04D (M631, 0x01FC, M04A (DADR, 0x04))
                                M04C (M631, 0x01FE, M049 (DADR, 0x0C))
                                M04C (M631, 0x01FF, M049 (DADR, 0x3C))
                                Local0 = (DADR + M646 (DADR, One))
                                If ((Local0 != DADR))
                                {
                                    M04D (M631, 0x0200, M04A (Local0, 0x04))
                                }
                            }
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                    }

                    Method (SDSW, 3, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    }

                    Method (PPS0, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                        Local0 = \_SB.M625 (0x00200196, 0x03E8)
                        Local0 = \_SB.M625 (0x00200194, 0x03E8)
                    }

                    Method (PPS3, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                        Local0 = \_SB.M625 (0x00200195, 0x03E8)
                        Local0 = \_SB.M625 (0x00200199, 0x03E8)
                    }

                    Method (P_ON, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (P_OF, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP2 XHCI _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Name (DADR, 0xEEEEEEEE)
                    Name (DSTA, 0xEE)
                    Name (DBUS, 0xEEEE)
                    Name (PCSA, Zero)
                    Name (PWST, 0xD3)
                    Name (ESTA, 0xEE)
                    Name (EBUS, 0xEEEE)
                    Name (PW3S, Zero)
                    Name (FLG0, 0xEE)
                    Name (M640, 0x33)
                    Name (M641, 0x33)
                    Name (M642, 0x33)
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                        {
                            If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                            {
                                ESTA = 0xEE
                                DBUS = 0xEEEE
                                DADR = 0xEEEEEEEE
                            }
                        }

                        If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                        {
                            If ((DBUS == 0xEEEE))
                            {
                                If (CondRefOf (^^EBUS))
                                {
                                    DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.U4P2.EBUS */
                                }
                                Else
                                {
                                    DBUS = Zero
                                }
                            }

                            If ((DBUS != 0xEEEE))
                            {
                                If ((DADR == 0xEEEEEEEE))
                                {
                                    Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                        One) & 0x000F8000))
                                    Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                    DADR = (M083 + Local0)
                                }

                                Local0 = 0x7FFFFFFF
                                Local0 |= 0x80000000
                                Local1 = M04B (DADR, Zero)
                                If (((Local1 == Local0) || (Local1 == Zero)))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                    If ((DBUS != Zero))
                                    {
                                        If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                        {
                                            If ((^^M641 == 0x33))
                                            {
                                                Local2 = M646 (^^DADR, 0x10)
                                                ^^M641 = Local2
                                            }
                                            Else
                                            {
                                                Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.U4P2.M641 */
                                            }

                                            If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                            {
                                                DSTA = 0x0F
                                                ESTA = 0xEE
                                            }
                                        }
                                    }
                                }
                                ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                                {
                                    EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                    If ((M641 == 0x33))
                                    {
                                        Local2 = M646 (DADR, 0x10)
                                        M641 = Local2
                                    }
                                    Else
                                    {
                                        Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.M641 */
                                    }

                                    Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                    If ((Local3 == 0x05))
                                    {
                                        If ((EBUS == Zero))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                        ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0x0F
                                        }
                                    }
                                    ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                        Zero))
                                    {
                                        If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                    ElseIf ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }

                                    If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                    {
                                        If ((M642 == 0x33))
                                        {
                                            Local4 = M646 (DADR, 0x15)
                                        }
                                        Else
                                        {
                                            Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.M642 */
                                        }

                                        If ((Local4 != Zero))
                                        {
                                            FLG0 = 0x16
                                        }
                                    }
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = Zero
                                }
                            }
                            Else
                            {
                                DSTA = Zero
                                ESTA = 0xEE
                            }
                        }

                        If ((FLG0 == 0x16))
                        {
                            If ((M049 (DADR, 0x19) == Zero))
                            {
                                ESTA = 0xEE
                            }
                        }

                        If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                        {
                            PWST = 0xD0
                        }

                        If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                        {
                            PWST = 0xD3
                        }

                        If (CondRefOf (SSTA))
                        {
                            SSTA ()
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                        }

                        Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.DSTA */
                    }

                    Method (_INI, 0, Serialized)  // _INI: Initialize
                    {
                        Local0 = 0x7FFFFFFF
                        Local0 |= 0x80000000
                        Local1 = M04B (DADR, Zero)
                        If (((Local1 == Local0) || (Local1 == Zero)))
                        {
                            PCSA = Zero
                            PWST = 0xD3
                            PW3S = Zero
                        }
                        Else
                        {
                            PCSA = One
                            PWST = 0xD0
                            PW3S = One
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                        If (CondRefOf (SINI))
                        {
                            SINI ()
                        }
                    }

                    Method (_REG, 2, Serialized)  // _REG: Region Availability
                    {
                        If (((Arg0 == 0x02) && (Arg1 == One)))
                        {
                            If ((DSTA == 0x0F))
                            {
                                If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                                {
                                    PCSA = One
                                }
                            }
                        }

                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            PCSA = Zero
                        }

                        If (CondRefOf (SREG))
                        {
                            SREG (Arg0, Arg1)
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                    }

                    Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                    {
                        If (CondRefOf (SDSW))
                        {
                            SDSW (Arg0, Arg1, Arg2)
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                        }
                    }

                    Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS0))
                        {
                            PPS0 ()
                        }
                    }

                    Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS3))
                        {
                            PPS3 ()
                        }
                    }

                    PowerResource (PWRS, 0x00, 0x0000)
                    {
                        Method (_STA, 0, Serialized)  // _STA: Status
                        {
                            If ((DADR != 0xEEEEEEEE))
                            {
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            }

                            Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.PW3S */
                        }

                        Method (_ON, 0, Serialized)  // _ON_: Power On
                        {
                            If ((PW3S == Zero))
                            {
                                PW3S = One
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_ON))
                                {
                                    P_ON ()
                                }
                            }
                        }

                        Method (_OFF, 0, Serialized)  // _OFF: Power Off
                        {
                            If ((PW3S == One))
                            {
                                PW3S = Zero
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_OF))
                                {
                                    P_OF ()
                                }
                            }
                        }
                    }

                    Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                    {
                        PWRS
                    })
                    Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                    {
                        PWRS
                    })
                    Device (UHUB)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (UHS1)
                        {
                            Name (_ADR, One)  // _ADR: Address
                            Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                            {
                                Name (XUPC, Package (0x04)
                                {
                                    0xFF, 
                                    0x09, 
                                    Zero, 
                                    Zero
                                })
                                Return (XUPC) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.UHUB.UHS1._UPC.XUPC */
                            }

                            Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                            {
                                Name (XPLD, Package (0x01)
                                {
                                    Buffer (0x14)
                                    {
                                        /* 0000 */  0x82, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                        /* 0008 */  0x61, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // a.......
                                        /* 0010 */  0xFF, 0xFF, 0xFF, 0xFF                           // ....
                                    }
                                })
                                CreateField (DerefOf (XPLD [Zero]), 0x40, One, VISI)
                                VISI = One
                                CreateField (DerefOf (XPLD [Zero]), 0x57, 0x08, GPOS)
                                GPOS = 0x06
                                Return (XPLD) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.UHUB.UHS1._PLD.XPLD */
                            }
                        }

                        Device (UHS2)
                        {
                            Name (_ADR, 0x02)  // _ADR: Address
                        }

                        Device (USS1)
                        {
                            Name (_ADR, 0x03)  // _ADR: Address
                            Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                            {
                                Name (XUPC, Package (0x04)
                                {
                                    0xFF, 
                                    0x09, 
                                    Zero, 
                                    Zero
                                })
                                Return (XUPC) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.UHUB.USS1._UPC.XUPC */
                            }

                            Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                            {
                                Name (XPLD, Package (0x01)
                                {
                                    Buffer (0x14)
                                    {
                                        /* 0000 */  0x82, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                        /* 0008 */  0x61, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // a.......
                                        /* 0010 */  0xFF, 0xFF, 0xFF, 0xFF                           // ....
                                    }
                                })
                                CreateField (DerefOf (XPLD [Zero]), 0x40, One, VISI)
                                VISI = One
                                CreateField (DerefOf (XPLD [Zero]), 0x57, 0x08, GPOS)
                                GPOS = 0x06
                                Return (XPLD) /* \_SB_.PCI0.GPP7.U4UP.U4P2.UXHC.UHUB.USS1._PLD.XPLD */
                            }

                            Name (XDSD, Package (0x02)
                            {
                                ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                                Package (0x02)
                                {
                                    Package (0x02)
                                    {
                                        "usb4-host-interface", 
                                        \_SB.PCI0.GPP7.U4UP.U4P3.UHI0
                                    }, 

                                    Package (0x02)
                                    {
                                        "usb4-port-number", 
                                        0x03
                                    }
                                }
                            })
                            Name (YDSD, Package (0x02)
                            {
                                ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                                Package (0x02)
                                {
                                    Package (0x02)
                                    {
                                        "usb4-host-interface", 
                                        \_SB.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0
                                    }, 

                                    Package (0x02)
                                    {
                                        "usb4-port-number", 
                                        0x03
                                    }
                                }
                            })
                        }

                        Device (USS2)
                        {
                            Name (_ADR, 0x04)  // _ADR: Address
                            Name (XDSD, Package (0x02)
                            {
                                ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                                Package (0x02)
                                {
                                    Package (0x02)
                                    {
                                        "usb4-host-interface", 
                                        \_SB.PCI0.GPP7.U4UP.U4P3.UHI0
                                    }, 

                                    Package (0x02)
                                    {
                                        "usb4-port-number", 
                                        0x04
                                    }
                                }
                            })
                            Name (YDSD, Package (0x02)
                            {
                                ToUUID ("daffd814-6eba-4d8c-8a91-bc9bbf4aa301") /* Device Properties for _DSD */, 
                                Package (0x02)
                                {
                                    Package (0x02)
                                    {
                                        "usb4-host-interface", 
                                        \_SB.PCI0.GPP7.UP00.DP00.U4UP.U4P3.UHI0
                                    }, 

                                    Package (0x02)
                                    {
                                        "usb4-port-number", 
                                        0x04
                                    }
                                }
                            })
                        }
                    }
                }
            }

            Device (U4P3)
            {
                Name (_ADR, 0x00030000)  // _ADR: Address
                Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }

                Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                {
                    Local0 = M049 (M620, 0x44)
                    If ((Local0 == 0x03))
                    {
                        Local0 = 0x04
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                {
                    Local0 = 0x04
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }

                Name (DPRW, Package (0x02)
                {
                    0x16, 
                    0x03
                })
                Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                {
                    Local0 = M049 (M620, 0x3D)
                    If ((Local0 != 0x16))
                    {
                        DPRW [Zero] = Local0
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P3.DPRW */
                }

                Method (SSTA, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (SINI, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SREG, 2, Serialized)
                {
                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                        {
                            M651 (DADR, M631, 0x0150, One)
                        }
                    }

                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                }

                Method (SDSW, 3, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    If ((Arg0 == Zero))
                    {
                        Local0 = \_SB.M625 (0x00100193, 0x03E8)
                    }
                    Else
                    {
                        Local0 = \_SB.M625 (0x00100192, 0x03E8)
                    }
                }

                Method (PPS0, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00100196, 0x03E8)
                    Local0 = \_SB.M625 (0x00100194, 0x03E8)
                }

                Method (PPS3, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M647 (DADR, 0x0340)
                    Local0 = \_SB.M625 (0x00100195, 0x03E8)
                    Local0 = \_SB.M625 (0x00100199, 0x03E8)
                }

                Method (P_ON, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Method (P_OF, 0, Serialized)
                {
                    M460 ("  FEA-ASL-DiscreteUSB4 DSP3 _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                }

                Name (DADR, 0xEEEEEEEE)
                Name (DSTA, 0xEE)
                Name (DBUS, 0xEEEE)
                Name (PCSA, Zero)
                Name (PWST, 0xD3)
                Name (ESTA, 0xEE)
                Name (EBUS, 0xEEEE)
                Name (PW3S, Zero)
                Name (FLG0, 0xEE)
                Name (M640, 0x33)
                Name (M641, 0x33)
                Name (M642, 0x33)
                Method (_STA, 0, Serialized)  // _STA: Status
                {
                    If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                    {
                        If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                        {
                            ESTA = 0xEE
                            DBUS = 0xEEEE
                            DADR = 0xEEEEEEEE
                        }
                    }

                    If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                    {
                        If ((DBUS == 0xEEEE))
                        {
                            If (CondRefOf (^^EBUS))
                            {
                                DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.EBUS */
                            }
                            Else
                            {
                                DBUS = Zero
                            }
                        }

                        If ((DBUS != 0xEEEE))
                        {
                            If ((DADR == 0xEEEEEEEE))
                            {
                                Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                    One) & 0x000F8000))
                                Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                DADR = (M083 + Local0)
                            }

                            Local0 = 0x7FFFFFFF
                            Local0 |= 0x80000000
                            Local1 = M04B (DADR, Zero)
                            If (((Local1 == Local0) || (Local1 == Zero)))
                            {
                                DSTA = Zero
                                ESTA = Zero
                                If ((DBUS != Zero))
                                {
                                    If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                    {
                                        If ((^^M641 == 0x33))
                                        {
                                            Local2 = M646 (^^DADR, 0x10)
                                            ^^M641 = Local2
                                        }
                                        Else
                                        {
                                            Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.M641 */
                                        }

                                        If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                }
                            }
                            ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                            {
                                EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                If ((M641 == 0x33))
                                {
                                    Local2 = M646 (DADR, 0x10)
                                    M641 = Local2
                                }
                                Else
                                {
                                    Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P3.M641 */
                                }

                                Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                If ((Local3 == 0x05))
                                {
                                    If ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }
                                }
                                ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                    Zero))
                                {
                                    If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                    {
                                        DSTA = Zero
                                        ESTA = Zero
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                }
                                ElseIf ((EBUS == Zero))
                                {
                                    DSTA = 0x0F
                                    ESTA = 0xEE
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = 0x0F
                                }

                                If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                {
                                    If ((M642 == 0x33))
                                    {
                                        Local4 = M646 (DADR, 0x15)
                                    }
                                    Else
                                    {
                                        Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P3.M642 */
                                    }

                                    If ((Local4 != Zero))
                                    {
                                        FLG0 = 0x16
                                    }
                                }
                            }
                            Else
                            {
                                DSTA = 0x0F
                                ESTA = Zero
                            }
                        }
                        Else
                        {
                            DSTA = Zero
                            ESTA = 0xEE
                        }
                    }

                    If ((FLG0 == 0x16))
                    {
                        If ((M049 (DADR, 0x19) == Zero))
                        {
                            ESTA = 0xEE
                        }
                    }

                    If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                    {
                        PWST = 0xD0
                    }

                    If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                    {
                        PWST = 0xD3
                    }

                    If (CondRefOf (SSTA))
                    {
                        SSTA ()
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                    }

                    Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P3.DSTA */
                }

                Method (_INI, 0, Serialized)  // _INI: Initialize
                {
                    Local0 = 0x7FFFFFFF
                    Local0 |= 0x80000000
                    Local1 = M04B (DADR, Zero)
                    If (((Local1 == Local0) || (Local1 == Zero)))
                    {
                        PCSA = Zero
                        PWST = 0xD3
                        PW3S = Zero
                    }
                    Else
                    {
                        PCSA = One
                        PWST = 0xD0
                        PW3S = One
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                    If (CondRefOf (SINI))
                    {
                        SINI ()
                    }
                }

                Method (_REG, 2, Serialized)  // _REG: Region Availability
                {
                    If (((Arg0 == 0x02) && (Arg1 == One)))
                    {
                        If ((DSTA == 0x0F))
                        {
                            If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                            {
                                PCSA = One
                            }
                        }
                    }

                    If (((Arg0 == 0x02) && (Arg1 == Zero)))
                    {
                        PCSA = Zero
                    }

                    If (CondRefOf (SREG))
                    {
                        SREG (Arg0, Arg1)
                    }

                    M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                }

                Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                {
                    If (CondRefOf (SDSW))
                    {
                        SDSW (Arg0, Arg1, Arg2)
                    }

                    If ((DADR != 0xEEEEEEEE))
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                    }
                }

                Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS0))
                    {
                        PPS0 ()
                    }
                }

                Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                {
                    M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                    If (CondRefOf (PPS3))
                    {
                        PPS3 ()
                    }
                }

                PowerResource (PWRS, 0x00, 0x0000)
                {
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                        }

                        Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P3.PW3S */
                    }

                    Method (_ON, 0, Serialized)  // _ON_: Power On
                    {
                        If ((PW3S == Zero))
                        {
                            PW3S = One
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_ON))
                            {
                                P_ON ()
                            }
                        }
                    }

                    Method (_OFF, 0, Serialized)  // _OFF: Power Off
                    {
                        If ((PW3S == One))
                        {
                            PW3S = Zero
                            M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            If (CondRefOf (P_OF))
                            {
                                P_OF ()
                            }
                        }
                    }
                }

                Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                {
                    PWRS
                })
                Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                {
                    PWRS
                })
                Device (UHI0)
                {
                    Name (_ADR, Zero)  // _ADR: Address
                    Name (_STR, Unicode ("USB4 Host Interface Controller"))  // _STR: Description String
                    Name (XDSD, Package (0x04)
                    {
                        ToUUID ("c44d002f-69f9-4e7d-a904-a7baabdf43f7") /* Unknown UUID */, 
                        Package (0x01)
                        {
                            Package (0x02)
                            {
                                "IMR_VALID", 
                                One
                            }
                        }, 

                        ToUUID ("6c501103-c189-4296-ba72-9bf5a26ebe5d") /* Unknown UUID */, 
                        Package (0x01)
                        {
                            Package (0x02)
                            {
                                "WAKE_SUPPORTED", 
                                One
                            }
                        }
                    })
                    Method (_RMV, 0, Serialized)  // _RMV: Removal Status
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _RMV ()  Return 0\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return (Zero)
                    }

                    Method (_S0W, 0, Serialized)  // _S0W: S0 Device Wake State
                    {
                        Local0 = M049 (M620, 0x44)
                        If ((Local0 == 0x03))
                        {
                            Local0 = 0x04
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _S0W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3W, 0, Serialized)  // _S3W: S3 Device Wake State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _S3W ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Method (_S3D, 0, Serialized)  // _S3D: S3 Device State
                    {
                        Local0 = 0x04
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _S3D ()  Return %d\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }

                    Name (DPRW, Package (0x02)
                    {
                        0x16, 
                        0x03
                    })
                    Method (_PRW, 0, Serialized)  // _PRW: Power Resources for Wake
                    {
                        Local0 = M049 (M620, 0x3D)
                        If ((Local0 != 0x16))
                        {
                            DPRW [Zero] = Local0
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _PRW ()  Return Package (2) {0x%X, 0x04}\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (DPRW) /* \_SB_.PCI0.GPP7.U4UP.U4P3.UHI0.DPRW */
                    }

                    Method (SSTA, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _STA ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (SINI, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _INI ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                    }

                    Method (SREG, 2, Serialized)
                    {
                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            If (((M04B (M620, 0x52) & 0x0100) == 0x0100))
                            {
                                M04E (M631, 0x0204, M04B (DADR, 0x10))
                                M04E (M631, 0x0208, M04B (DADR, 0x18))
                                M04D (M631, 0x020C, M04A (DADR, 0x04))
                                M04C (M631, 0x020E, M049 (DADR, 0x0C))
                                M04C (M631, 0x020F, M049 (DADR, 0x3C))
                                Local0 = (DADR + M646 (DADR, One))
                                If ((Local0 != DADR))
                                {
                                    M04D (M631, 0x0210, M04A (Local0, 0x04))
                                }
                            }
                        }

                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _REG (%d, %d)\n", Arg0, Arg1, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                    }

                    Method (SDSW, 3, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _DSW (%d, %d, %d)\n", Arg0, Arg1, Arg2, Zero, Zero, Zero)
                    }

                    Method (PPS0, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _PS0 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                        Local0 = \_SB.M625 (0x00400196, 0x03E8)
                        Local0 = \_SB.M625 (0x00400194, 0x03E8)
                    }

                    Method (PPS3, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _PS3 ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        M647 (DADR, 0x01A0)
                        Local0 = \_SB.M625 (0x00400195, 0x03E8)
                        Local0 = \_SB.M625 (0x00400199, 0x03E8)
                    }

                    Method (P_ON, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _ON ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Method (P_OF, 0, Serialized)
                    {
                        M460 ("  FEA-ASL-DiscreteUSB4 DSP3 U4RT _OFF ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    }

                    Name (DADR, 0xEEEEEEEE)
                    Name (DSTA, 0xEE)
                    Name (DBUS, 0xEEEE)
                    Name (PCSA, Zero)
                    Name (PWST, 0xD3)
                    Name (ESTA, 0xEE)
                    Name (EBUS, 0xEEEE)
                    Name (PW3S, Zero)
                    Name (FLG0, 0xEE)
                    Name (M640, 0x33)
                    Name (M641, 0x33)
                    Name (M642, 0x33)
                    Method (_STA, 0, Serialized)  // _STA: Status
                    {
                        If ((CondRefOf (^^FLG0) && CondRefOf (^^EBUS)))
                        {
                            If (((^^FLG0 == 0x16) && (^^EBUS != DBUS)))
                            {
                                ESTA = 0xEE
                                DBUS = 0xEEEE
                                DADR = 0xEEEEEEEE
                            }
                        }

                        If (((DSTA == 0xEE) || (ESTA == 0xEE)))
                        {
                            If ((DBUS == 0xEEEE))
                            {
                                If (CondRefOf (^^EBUS))
                                {
                                    DBUS = ^^EBUS /* \_SB_.PCI0.GPP7.U4UP.U4P3.EBUS */
                                }
                                Else
                                {
                                    DBUS = Zero
                                }
                            }

                            If ((DBUS != 0xEEEE))
                            {
                                If ((DADR == 0xEEEEEEEE))
                                {
                                    Local0 = (((_ADR << 0x0C) & 0x7000) | ((_ADR >> 
                                        One) & 0x000F8000))
                                    Local0 |= ((DBUS << 0x14) & 0x0FF00000)
                                    DADR = (M083 + Local0)
                                }

                                Local0 = 0x7FFFFFFF
                                Local0 |= 0x80000000
                                Local1 = M04B (DADR, Zero)
                                If (((Local1 == Local0) || (Local1 == Zero)))
                                {
                                    DSTA = Zero
                                    ESTA = Zero
                                    If ((DBUS != Zero))
                                    {
                                        If ((CondRefOf (^^M641) && CondRefOf (^^DADR)))
                                        {
                                            If ((^^M641 == 0x33))
                                            {
                                                Local2 = M646 (^^DADR, 0x10)
                                                ^^M641 = Local2
                                            }
                                            Else
                                            {
                                                Local2 = ^^M641 /* \_SB_.PCI0.GPP7.U4UP.U4P3.M641 */
                                            }

                                            If (((M049 (^^DADR, (Local2 + 0x14)) & 0x40) == 0x40))
                                            {
                                                DSTA = 0x0F
                                                ESTA = 0xEE
                                            }
                                        }
                                    }
                                }
                                ElseIf ((M04A (DADR, 0x0A) == 0x0604))
                                {
                                    EBUS = ((M04B (DADR, 0x18) >> 0x08) & 0xFF)
                                    If ((M641 == 0x33))
                                    {
                                        Local2 = M646 (DADR, 0x10)
                                        M641 = Local2
                                    }
                                    Else
                                    {
                                        Local2 = M641 /* \_SB_.PCI0.GPP7.U4UP.U4P3.UHI0.M641 */
                                    }

                                    Local3 = ((M049 (DADR, (Local2 + 0x02)) >> 0x04) & 0x0F)
                                    If ((Local3 == 0x05))
                                    {
                                        If ((EBUS == Zero))
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                        ElseIf ((M648 (EBUS, 0x03, Zero) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0x0F
                                        }
                                    }
                                    ElseIf (((M049 (DADR, (Local2 + 0x1A)) & 0x40) == 
                                        Zero))
                                    {
                                        If (((M049 (DADR, (Local2 + 0x14)) & 0x40) == Zero))
                                        {
                                            DSTA = Zero
                                            ESTA = Zero
                                        }
                                        Else
                                        {
                                            DSTA = 0x0F
                                            ESTA = 0xEE
                                        }
                                    }
                                    ElseIf ((EBUS == Zero))
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0xEE
                                    }
                                    Else
                                    {
                                        DSTA = 0x0F
                                        ESTA = 0x0F
                                    }

                                    If (((FLG0 == 0xEE) && (Local3 == 0x06)))
                                    {
                                        If ((M642 == 0x33))
                                        {
                                            Local4 = M646 (DADR, 0x15)
                                        }
                                        Else
                                        {
                                            Local4 = M642 /* \_SB_.PCI0.GPP7.U4UP.U4P3.UHI0.M642 */
                                        }

                                        If ((Local4 != Zero))
                                        {
                                            FLG0 = 0x16
                                        }
                                    }
                                }
                                Else
                                {
                                    DSTA = 0x0F
                                    ESTA = Zero
                                }
                            }
                            Else
                            {
                                DSTA = Zero
                                ESTA = 0xEE
                            }
                        }

                        If ((FLG0 == 0x16))
                        {
                            If ((M049 (DADR, 0x19) == Zero))
                            {
                                ESTA = 0xEE
                            }
                        }

                        If ((((PCSA == One) && (PWST == 0xD3)) && (PW3S == One)))
                        {
                            PWST = 0xD0
                        }

                        If ((((PCSA == Zero) && (PWST == 0xD0)) && (PW3S == Zero)))
                        {
                            PWST = 0xD3
                        }

                        If (CondRefOf (SSTA))
                        {
                            SSTA ()
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._STA = 0x%X  PWST = 0x%X  DBUS = 0x%X  ESTA = 0x%X  EBUS = 0x%X\n", DADR, DSTA, PWST, DBUS, ESTA, EBUS)
                        }

                        Return (DSTA) /* \_SB_.PCI0.GPP7.U4UP.U4P3.UHI0.DSTA */
                    }

                    Method (_INI, 0, Serialized)  // _INI: Initialize
                    {
                        Local0 = 0x7FFFFFFF
                        Local0 |= 0x80000000
                        Local1 = M04B (DADR, Zero)
                        If (((Local1 == Local0) || (Local1 == Zero)))
                        {
                            PCSA = Zero
                            PWST = 0xD3
                            PW3S = Zero
                        }
                        Else
                        {
                            PCSA = One
                            PWST = 0xD0
                            PW3S = One
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._INI  PCSA = 0x%X  PWST = 0x%X  PW3S = 0x%X\n", DADR, PCSA, PWST, PW3S, Zero, Zero)
                        If (CondRefOf (SINI))
                        {
                            SINI ()
                        }
                    }

                    Method (_REG, 2, Serialized)  // _REG: Region Availability
                    {
                        If (((Arg0 == 0x02) && (Arg1 == One)))
                        {
                            If ((DSTA == 0x0F))
                            {
                                If (((M049 (DADR, 0x04) & 0xFC) == 0x04))
                                {
                                    PCSA = One
                                }
                            }
                        }

                        If (((Arg0 == 0x02) && (Arg1 == Zero)))
                        {
                            PCSA = Zero
                        }

                        If (CondRefOf (SREG))
                        {
                            SREG (Arg0, Arg1)
                        }

                        M460 ("  FEA-ASL-PCIe Address (0x%X)._REG (%d %d)  PCSA = %d\n", DADR, Arg0, Arg1, PCSA, Zero, Zero)
                    }

                    Method (_DSW, 3, Serialized)  // _DSW: Device Sleep Wake
                    {
                        If (CondRefOf (SDSW))
                        {
                            SDSW (Arg0, Arg1, Arg2)
                        }

                        If ((DADR != 0xEEEEEEEE))
                        {
                            M460 ("  FEA-ASL-PCIe Address (0x%X)._DSW (%d %d %d)  PCSA = %d\n", DADR, Arg0, Arg1, Arg2, PCSA, Zero)
                        }
                    }

                    Method (_PS0, 0, Serialized)  // _PS0: Power State 0
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS0\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS0))
                        {
                            PPS0 ()
                        }
                    }

                    Method (_PS3, 0, Serialized)  // _PS3: Power State 3
                    {
                        M460 ("  FEA-ASL-PCIe Address (0x%X)._PS3\n", DADR, Zero, Zero, Zero, Zero, Zero)
                        If (CondRefOf (PPS3))
                        {
                            PPS3 ()
                        }
                    }

                    PowerResource (PWRS, 0x00, 0x0000)
                    {
                        Method (_STA, 0, Serialized)  // _STA: Status
                        {
                            If ((DADR != 0xEEEEEEEE))
                            {
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._STA = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                            }

                            Return (PW3S) /* \_SB_.PCI0.GPP7.U4UP.U4P3.UHI0.PW3S */
                        }

                        Method (_ON, 0, Serialized)  // _ON_: Power On
                        {
                            If ((PW3S == Zero))
                            {
                                PW3S = One
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._ON = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_ON))
                                {
                                    P_ON ()
                                }
                            }
                        }

                        Method (_OFF, 0, Serialized)  // _OFF: Power Off
                        {
                            If ((PW3S == One))
                            {
                                PW3S = Zero
                                M460 ("  FEA-ASL-PCIe Address (0x%X).PWRS._OFF = 0x%X\n", DADR, PW3S, Zero, Zero, Zero, Zero)
                                If (CondRefOf (P_OF))
                                {
                                    P_OF ()
                                }
                            }
                        }
                    }

                    Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                    {
                        PWRS
                    })
                    Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                    {
                        PWRS
                    })
                }
            }
        }
    }

    Scope (\_GPE)
    {
        Method (NTAL, 0, NotSerialized)
        {
            M460 ("  FEA-ASL-DiscreteUSB4 \\_GPE.NTAL\n", Zero, Zero, Zero, Zero, Zero, Zero)
            M460 ("    Notify (\\_SB.PCI0.GPP7, 0x0)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7, Zero) // Bus Check
            Sleep (0x01F4)
            M460 ("    Notify (\\_SB.PCI0.GPP7, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P0, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P0, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P1, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P1, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P2, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P2, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P3, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P3, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P0.UP0D, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P0.UP0D, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P1.UP1D, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P1.UP1D, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P2.UXHC, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P2.UXHC, 0x02) // Device Wake
            M460 ("    Notify (\\_SB.PCI0.GPP7.U4UP.U4P3.UHI0, 0x2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
            Notify (\_SB.PCI0.GPP7.U4UP.U4P3.UHI0, 0x02) // Device Wake
        }

        Method (_L02, 0, NotSerialized)  // _Lxx: Level-Triggered GPE, xx=0x00-0xFF
        {
            M460 ("  FEA-ASL-DiscreteUSB4 \\_GPE._L02\n", Zero, Zero, Zero, Zero, Zero, Zero)
            NTAL ()
            If (CondRefOf (\_GPE.OL02))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 Callout \\_GPE.OL02\n", Zero, Zero, Zero, Zero, Zero, Zero)
                \_GPE.OL02 ()
            }
        }

        Method (_E0F, 0, NotSerialized)  // _Exx: Edge-Triggered GPE, xx=0x00-0xFF
        {
            M460 ("  FEA-ASL-DiscreteUSB4 \\_GPE._E0F\n", Zero, Zero, Zero, Zero, Zero, Zero)
            NTAL ()
            If (CondRefOf (\_GPE.OL32))
            {
                M460 ("  FEA-ASL-DiscreteUSB4 Callout \\_GPE.OL32\n", Zero, Zero, Zero, Zero, Zero, Zero)
                \_GPE.OL32 ()
            }
        }
    }
}

