/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00002D09 (11529)
 *     Revision         0x02
 *     Checksum         0x38
 *     OEM ID           "DptfTb"
 *     OEM Table ID     "DptfTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "DptfTb", "DptfTabl", 0x00001000)
{
    External (_SB_.AAC0, FieldUnitObj)
    External (_SB_.ACRT, FieldUnitObj)
    External (_SB_.APSV, FieldUnitObj)
    External (_SB_.CBMI, FieldUnitObj)
    External (_SB_.CFGD, FieldUnitObj)
    External (_SB_.CLVL, FieldUnitObj)
    External (_SB_.CPPC, FieldUnitObj)
    External (_SB_.CTC0, FieldUnitObj)
    External (_SB_.CTC1, FieldUnitObj)
    External (_SB_.CTC2, FieldUnitObj)
    External (_SB_.OSCP, IntObj)
    External (_SB_.PAGD, DeviceObj)
    External (_SB_.PAGD._PUR, PkgObj)
    External (_SB_.PAGD._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.LPCB, DeviceObj)
    External (_SB_.PC00.LPCB.BATM, FieldUnitObj)
    External (_SB_.PC00.LPCB.CLOT, FieldUnitObj)
    External (_SB_.PC00.LPCB.CTMP, FieldUnitObj)
    External (_SB_.PC00.LPCB.ECOK, IntObj)
    External (_SB_.PC00.LPCB.M662, FieldUnitObj)
    External (_SB_.PC00.LPCB.MUT0, MutexObj)
    External (_SB_.PC00.LPCB.Q4HI, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4HU, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LO, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LU, FieldUnitObj)
    External (_SB_.PC00.LPCB.SEN2, DeviceObj)
    External (_SB_.PC00.LPCB.SEN3, DeviceObj)
    External (_SB_.PC00.LPCB.VRTT, UnknownObj)
    External (_SB_.PC00.MC__.MHBR, FieldUnitObj)
    External (_SB_.PC00.TCPU, DeviceObj)
    External (_SB_.PL10, FieldUnitObj)
    External (_SB_.PL11, FieldUnitObj)
    External (_SB_.PL12, FieldUnitObj)
    External (_SB_.PL20, FieldUnitObj)
    External (_SB_.PL21, FieldUnitObj)
    External (_SB_.PL22, FieldUnitObj)
    External (_SB_.PLW0, FieldUnitObj)
    External (_SB_.PLW1, FieldUnitObj)
    External (_SB_.PLW2, FieldUnitObj)
    External (_SB_.PR00, ProcessorObj)
    External (_SB_.PR00._PSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TPC, IntObj)
    External (_SB_.PR00._TSD, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00.LPSS, PkgObj)
    External (_SB_.PR00.TPSS, PkgObj)
    External (_SB_.PR00.TSMC, PkgObj)
    External (_SB_.PR00.TSMF, PkgObj)
    External (_SB_.PR01, ProcessorObj)
    External (_SB_.PR02, ProcessorObj)
    External (_SB_.PR03, ProcessorObj)
    External (_SB_.PR04, ProcessorObj)
    External (_SB_.PR05, ProcessorObj)
    External (_SB_.PR06, ProcessorObj)
    External (_SB_.PR07, ProcessorObj)
    External (_SB_.PR08, ProcessorObj)
    External (_SB_.PR09, ProcessorObj)
    External (_SB_.PR10, ProcessorObj)
    External (_SB_.PR11, ProcessorObj)
    External (_SB_.PR12, ProcessorObj)
    External (_SB_.PR13, ProcessorObj)
    External (_SB_.PR14, ProcessorObj)
    External (_SB_.PR15, ProcessorObj)
    External (_SB_.PR16, ProcessorObj)
    External (_SB_.PR17, ProcessorObj)
    External (_SB_.PR18, ProcessorObj)
    External (_SB_.PR19, ProcessorObj)
    External (_SB_.PR20, ProcessorObj)
    External (_SB_.PR21, ProcessorObj)
    External (_SB_.PR22, ProcessorObj)
    External (_SB_.PR23, ProcessorObj)
    External (_SB_.PR24, ProcessorObj)
    External (_SB_.PR25, ProcessorObj)
    External (_SB_.PR26, ProcessorObj)
    External (_SB_.PR27, ProcessorObj)
    External (_SB_.PR28, ProcessorObj)
    External (_SB_.PR29, ProcessorObj)
    External (_SB_.PR30, ProcessorObj)
    External (_SB_.PR31, ProcessorObj)
    External (_SB_.SLPB, DeviceObj)
    External (_SB_.TAR0, FieldUnitObj)
    External (_SB_.TAR1, FieldUnitObj)
    External (_SB_.TAR2, FieldUnitObj)
    External (_TZ_.ETMD, IntObj)
    External (_TZ_.THRM, ThermalZoneObj)
    External (ACTT, IntObj)
    External (ATPC, IntObj)
    External (BATR, IntObj)
    External (CHGE, IntObj)
    External (CRTT, IntObj)
    External (DCFE, IntObj)
    External (DPTF, IntObj)
    External (ECON, IntObj)
    External (FND1, IntObj)
    External (FND2, IntObj)
    External (FND3, IntObj)
    External (HIDW, MethodObj)    // 4 Arguments
    External (HIWC, MethodObj)    // 1 Arguments
    External (IN34, IntObj)
    External (ODV0, IntObj)
    External (ODV1, IntObj)
    External (ODV2, IntObj)
    External (ODV3, IntObj)
    External (ODV4, IntObj)
    External (ODV5, IntObj)
    External (PCHE, FieldUnitObj)
    External (PF00, IntObj)
    External (PLID, IntObj)
    External (PNHM, IntObj)
    External (PPPR, IntObj)
    External (PPSZ, IntObj)
    External (PSVT, IntObj)
    External (PTPC, IntObj)
    External (PWRE, IntObj)
    External (PWRS, IntObj)
    External (S1DE, IntObj)
    External (S2DE, IntObj)
    External (S3DE, IntObj)
    External (S4DE, IntObj)
    External (S5DE, IntObj)
    External (S6DE, IntObj)
    External (S6P2, IntObj)
    External (SADE, IntObj)
    External (SSP1, IntObj)
    External (SSP2, IntObj)
    External (SSP3, IntObj)
    External (SSP4, IntObj)
    External (SSP5, IntObj)
    External (TCNT, IntObj)
    External (TSOD, IntObj)

    Scope (\_SB)
    {
        Device (IETM)
        {
            Method (GHID, 1, Serialized)
            {
                If ((Arg0 == "IETM"))
                {
                    Return ("INTC1041")
                }

                If ((Arg0 == "SEN1"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN2"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN3"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN4"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN5"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "TPCH"))
                {
                    Return ("INTC1049")
                }

                If ((Arg0 == "TFN1"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN2"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN3"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TPWR"))
                {
                    Return ("INTC1060")
                }

                If ((Arg0 == "1"))
                {
                    Return ("INTC1061")
                }

                If ((Arg0 == "CHRG"))
                {
                    Return ("INTC1046")
                }

                Return ("XXXX9999")
            }

            Name (_UID, "IETM")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If (CondRefOf (HIWC))
                {
                    If (HIWC (Arg0))
                    {
                        If (CondRefOf (HIDW))
                        {
                            Return (HIDW (Arg0, Arg1, Arg2, Arg3))
                        }
                    }
                }

                Return (Buffer (One)
                {
                     0x00                                             // .
                })
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If (((\DPTF == One) && (\IN34 == One)))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Name (PTRP, Zero)
            Name (PSEM, Zero)
            Name (ATRP, Zero)
            Name (ASEM, Zero)
            Name (YTRP, Zero)
            Name (YSEM, Zero)
            Method (_OSC, 4, Serialized)  // _OSC: Operating System Capabilities
            {
                CreateDWordField (Arg3, Zero, STS1)
                CreateDWordField (Arg3, 0x04, CAP1)
                If ((Arg1 != One))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x0A
                    Return (Arg3)
                }

                If ((Arg2 != 0x02))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x02
                    Return (Arg3)
                }

                If (CondRefOf (\_SB.APSV))
                {
                    If ((PSEM == Zero))
                    {
                        PSEM = One
                        PTRP = \_SB.APSV /* External reference */
                    }
                }

                If (CondRefOf (\_SB.AAC0))
                {
                    If ((ASEM == Zero))
                    {
                        ASEM = One
                        ATRP = \_SB.AAC0 /* External reference */
                    }
                }

                If (CondRefOf (\_SB.ACRT))
                {
                    If ((YSEM == Zero))
                    {
                        YSEM = One
                        YTRP = \_SB.ACRT /* External reference */
                    }
                }

                If ((Arg0 == ToUUID ("b23ba85d-c8b7-3542-88de-8de2ffcfd698") /* Unknown UUID */))
                {
                    If (~(STS1 & One))
                    {
                        If ((CAP1 & One))
                        {
                            If ((CAP1 & 0x02))
                            {
                                \_SB.AAC0 = 0x6E
                                \_TZ.ETMD = Zero
                            }
                            Else
                            {
                                \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                                \_TZ.ETMD = One
                            }

                            If ((CAP1 & 0x04))
                            {
                                \_SB.APSV = 0x6E
                            }
                            Else
                            {
                                \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            }

                            If ((CAP1 & 0x08))
                            {
                                \_SB.ACRT = 0xD2
                            }
                            Else
                            {
                                \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            }
                        }
                        Else
                        {
                            \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                            \_TZ.ETMD = One
                        }

                        Notify (\_TZ.THRM, 0x81) // Information Change
                    }

                    Return (Arg3)
                }

                Return (Arg3)
            }

            Method (DCFG, 0, NotSerialized)
            {
                Return (\DCFE) /* External reference */
            }

            Name (ODVX, Package (0x06)
            {
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero
            })
            Method (ODVP, 0, Serialized)
            {
                ODVX [Zero] = \ODV0 /* External reference */
                ODVX [One] = \ODV1 /* External reference */
                ODVX [0x02] = \ODV2 /* External reference */
                ODVX [0x03] = \ODV3 /* External reference */
                ODVX [0x04] = \ODV4 /* External reference */
                ODVX [0x05] = \ODV5 /* External reference */
                Return (ODVX) /* \_SB_.IETM.ODVX */
            }

            Scope (\_SB.PC00.TCPU)
            {
                Name (PFLG, Zero)
                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    If ((\SADE == One))
                    {
                        Return (0x0F)
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                OperationRegion (CPWR, SystemMemory, ((\_SB.PC00.MC.MHBR << 0x0F) + 0x5000), 0x1000)
                Field (CPWR, ByteAcc, NoLock, Preserve)
                {
                    Offset (0x930), 
                    PTDP,   15, 
                    Offset (0x932), 
                    PMIN,   15, 
                    Offset (0x934), 
                    PMAX,   15, 
                    Offset (0x936), 
                    TMAX,   7, 
                    Offset (0x938), 
                    PWRU,   4, 
                    Offset (0x939), 
                    EGYU,   5, 
                    Offset (0x93A), 
                    TIMU,   4, 
                    Offset (0x958), 
                    Offset (0x95C), 
                    LPMS,   1, 
                    CTNL,   2, 
                    Offset (0x978), 
                    PCTP,   8, 
                    Offset (0x998), 
                    RP0C,   8, 
                    RP1C,   8, 
                    RPNC,   8, 
                    Offset (0xF3C), 
                    TRAT,   8, 
                    Offset (0xF40), 
                    PTD1,   15, 
                    Offset (0xF42), 
                    TRA1,   8, 
                    Offset (0xF44), 
                    PMX1,   15, 
                    Offset (0xF46), 
                    PMN1,   15, 
                    Offset (0xF48), 
                    PTD2,   15, 
                    Offset (0xF4A), 
                    TRA2,   8, 
                    Offset (0xF4C), 
                    PMX2,   15, 
                    Offset (0xF4E), 
                    PMN2,   15, 
                    Offset (0xF50), 
                    CTCL,   2, 
                        ,   29, 
                    CLCK,   1, 
                    MNTR,   8
                }

                Name (XPCC, Zero)
                Method (PPCC, 0, Serialized)
                {
                    If (((XPCC == Zero) && CondRefOf (\_SB.CBMI)))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                If (((\_SB.CLVL >= One) && (\_SB.CLVL <= 0x03)))
                                {
                                    CPL0 ()
                                    XPCC = One
                                }
                            }
                            Case (One)
                            {
                                If (((\_SB.CLVL == 0x02) || (\_SB.CLVL == 0x03)))
                                {
                                    CPL1 ()
                                    XPCC = One
                                }
                            }
                            Case (0x02)
                            {
                                If ((\_SB.CLVL == 0x03))
                                {
                                    CPL2 ()
                                    XPCC = One
                                }
                            }

                        }
                    }

                    Return (NPCC) /* \_SB_.PC00.TCPU.NPCC */
                }

                Name (NPCC, Package (0x03)
                {
                    0x02, 
                    Package (0x06)
                    {
                        Zero, 
                        0x88B8, 
                        0xAFC8, 
                        0x6D60, 
                        0x7D00, 
                        0x03E8
                    }, 

                    Package (0x06)
                    {
                        One, 
                        0xDBBA, 
                        0xDBBA, 
                        Zero, 
                        Zero, 
                        0x03E8
                    }
                })
                Method (CPNU, 2, Serialized)
                {
                    Name (CNVT, Zero)
                    Name (PPUU, Zero)
                    Name (RMDR, Zero)
                    If ((PWRU == Zero))
                    {
                        PPUU = One
                    }
                    Else
                    {
                        PPUU = (PWRU-- << 0x02)
                    }

                    Divide (Arg0, PPUU, RMDR, CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    If ((Arg1 == Zero))
                    {
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                    Else
                    {
                        CNVT *= 0x03E8
                        RMDR *= 0x03E8
                        RMDR /= PPUU
                        CNVT += RMDR /* \_SB_.PC00.TCPU.CPNU.RMDR */
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                }

                Method (CPL0, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL10, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW0 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW0 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL1, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL11, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW1 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW1 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL2, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL12, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW2 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW2 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Name (LSTM, Zero)
                Name (_PPC, Zero)  // _PPC: Performance Present Capabilities
                Method (SPPC, 1, Serialized)
                {
                    If (CondRefOf (\_SB.CPPC))
                    {
                        \_SB.CPPC = Arg0
                    }

                    If ((ToInteger (\TCNT) > Zero))
                    {
                        Notify (\_SB.PR00, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > One))
                    {
                        Notify (\_SB.PR01, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x02))
                    {
                        Notify (\_SB.PR02, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x03))
                    {
                        Notify (\_SB.PR03, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x04))
                    {
                        Notify (\_SB.PR04, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x05))
                    {
                        Notify (\_SB.PR05, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x06))
                    {
                        Notify (\_SB.PR06, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x07))
                    {
                        Notify (\_SB.PR07, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x08))
                    {
                        Notify (\_SB.PR08, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x09))
                    {
                        Notify (\_SB.PR09, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0A))
                    {
                        Notify (\_SB.PR10, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0B))
                    {
                        Notify (\_SB.PR11, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0C))
                    {
                        Notify (\_SB.PR12, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0D))
                    {
                        Notify (\_SB.PR13, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0E))
                    {
                        Notify (\_SB.PR14, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0F))
                    {
                        Notify (\_SB.PR15, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x10))
                    {
                        Notify (\_SB.PR16, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x11))
                    {
                        Notify (\_SB.PR17, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x12))
                    {
                        Notify (\_SB.PR18, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x13))
                    {
                        Notify (\_SB.PR19, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x14))
                    {
                        Notify (\_SB.PR20, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x15))
                    {
                        Notify (\_SB.PR21, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x16))
                    {
                        Notify (\_SB.PR22, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x17))
                    {
                        Notify (\_SB.PR23, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x18))
                    {
                        Notify (\_SB.PR24, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x19))
                    {
                        Notify (\_SB.PR25, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1A))
                    {
                        Notify (\_SB.PR26, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1B))
                    {
                        Notify (\_SB.PR27, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1C))
                    {
                        Notify (\_SB.PR28, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1D))
                    {
                        Notify (\_SB.PR29, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1E))
                    {
                        Notify (\_SB.PR30, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1F))
                    {
                        Notify (\_SB.PR31, 0x80) // Status Change
                    }
                }

                Method (SPUR, 1, NotSerialized)
                {
                    If ((Arg0 <= \TCNT))
                    {
                        If ((\_SB.PAGD._STA () == 0x0F))
                        {
                            \_SB.PAGD._PUR [One] = Arg0
                            Notify (\_SB.PAGD, 0x80) // Status Change
                        }
                    }
                }

                Method (PCCC, 0, Serialized)
                {
                    PCCX [Zero] = One
                    Switch (ToInteger (CPNU (PTDP, Zero)))
                    {
                        Case (0x39)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xA7F8
                            DerefOf (PCCX [One]) [One] = 0x00017318
                        }
                        Case (0x2F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x9858
                            DerefOf (PCCX [One]) [One] = 0x00014C08
                        }
                        Case (0x25)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x7148
                            DerefOf (PCCX [One]) [One] = 0xD6D8
                        }
                        Case (0x19)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x3E80
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0B)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x61A8
                        }
                        Default
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xFF
                            DerefOf (PCCX [One]) [One] = 0xFF
                        }

                    }

                    Return (PCCX) /* \_SB_.PC00.TCPU.PCCX */
                }

                Name (PCCX, Package (0x02)
                {
                    0x80000000, 
                    Package (0x02)
                    {
                        0x80000000, 
                        0x80000000
                    }
                })
                Name (KEFF, Package (0x1E)
                {
                    Package (0x02)
                    {
                        0x01BC, 
                        Zero
                    }, 

                    Package (0x02)
                    {
                        0x01CF, 
                        0x27
                    }, 

                    Package (0x02)
                    {
                        0x01E1, 
                        0x4B
                    }, 

                    Package (0x02)
                    {
                        0x01F3, 
                        0x6C
                    }, 

                    Package (0x02)
                    {
                        0x0206, 
                        0x8B
                    }, 

                    Package (0x02)
                    {
                        0x0218, 
                        0xA8
                    }, 

                    Package (0x02)
                    {
                        0x022A, 
                        0xC3
                    }, 

                    Package (0x02)
                    {
                        0x023D, 
                        0xDD
                    }, 

                    Package (0x02)
                    {
                        0x024F, 
                        0xF4
                    }, 

                    Package (0x02)
                    {
                        0x0261, 
                        0x010B
                    }, 

                    Package (0x02)
                    {
                        0x0274, 
                        0x011F
                    }, 

                    Package (0x02)
                    {
                        0x032C, 
                        0x01BD
                    }, 

                    Package (0x02)
                    {
                        0x03D7, 
                        0x0227
                    }, 

                    Package (0x02)
                    {
                        0x048B, 
                        0x026D
                    }, 

                    Package (0x02)
                    {
                        0x053E, 
                        0x02A1
                    }, 

                    Package (0x02)
                    {
                        0x05F7, 
                        0x02C6
                    }, 

                    Package (0x02)
                    {
                        0x06A8, 
                        0x02E6
                    }, 

                    Package (0x02)
                    {
                        0x075D, 
                        0x02FF
                    }, 

                    Package (0x02)
                    {
                        0x0818, 
                        0x0311
                    }, 

                    Package (0x02)
                    {
                        0x08CF, 
                        0x0322
                    }, 

                    Package (0x02)
                    {
                        0x179C, 
                        0x0381
                    }, 

                    Package (0x02)
                    {
                        0x2DDC, 
                        0x039C
                    }, 

                    Package (0x02)
                    {
                        0x44A8, 
                        0x039E
                    }, 

                    Package (0x02)
                    {
                        0x5C35, 
                        0x0397
                    }, 

                    Package (0x02)
                    {
                        0x747D, 
                        0x038D
                    }, 

                    Package (0x02)
                    {
                        0x8D7F, 
                        0x0382
                    }, 

                    Package (0x02)
                    {
                        0xA768, 
                        0x0376
                    }, 

                    Package (0x02)
                    {
                        0xC23B, 
                        0x0369
                    }, 

                    Package (0x02)
                    {
                        0xDE26, 
                        0x035A
                    }, 

                    Package (0x02)
                    {
                        0xFB7C, 
                        0x034A
                    }
                })
                Name (CEUP, Package (0x06)
                {
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000
                })
                Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                {
                    LSTM = Arg0
                    Notify (\_SB.PC00.TCPU, 0x91) // Device-Specific
                }

                Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                {
                    Return (0x0ADE)
                }

                Name (PTYP, Zero)
                Method (_PSS, 0, NotSerialized)  // _PSS: Performance Supported States
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        Return (\_SB.PR00._PSS ())
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }, 

                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TSS, 0, NotSerialized)  // _TSS: Throttling Supported States
                {
                    If (CondRefOf (\_SB.PR00._TSS))
                    {
                        Return (\_SB.PR00._TSS ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                One, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TPC, 0, NotSerialized)  // _TPC: Throttling Present Capabilities
                {
                    If (CondRefOf (\_SB.PR00._TPC))
                    {
                        Return (\_SB.PR00._TPC) /* External reference */
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PTC, 0, NotSerialized)  // _PTC: Processor Throttling Control
                {
                    If ((CondRefOf (\PF00) && (\PF00 != 0x80000000)))
                    {
                        If ((\PF00 & 0x04))
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }
                            })
                        }
                        Else
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }
                            })
                        }
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }, 

                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }
                        })
                    }
                }

                Method (_TSD, 0, NotSerialized)  // _TSD: Throttling State Dependencies
                {
                    If (CondRefOf (\_SB.PR00._TSD))
                    {
                        Return (\_SB.PR00._TSD ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                0x05, 
                                Zero, 
                                Zero, 
                                0xFC, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TDL, 0, NotSerialized)  // _TDL: T-State Depth Limit
                {
                    If ((CondRefOf (\_SB.PR00._TSS) && CondRefOf (\_SB.CFGD)))
                    {
                        If ((\_SB.CFGD & 0x2000))
                        {
                            Return ((SizeOf (\_SB.PR00.TSMF) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.TSMC) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PDL, 0, NotSerialized)  // _PDL: P-state Depth Limit
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        If ((\_SB.OSCP & 0x0400))
                        {
                            Return ((SizeOf (\_SB.PR00.TPSS) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.LPSS) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }
            }

            Scope (\_SB.IETM)
            {
                Name (CTSP, Package (0x01)
                {
                    ToUUID ("e145970a-e4c1-4d73-900e-c9c5a69dd067") /* Unknown UUID */
                })
            }

            Scope (\_SB.PC00.TCPU)
            {
                Method (TDPL, 0, Serialized)
                {
                    Name (AAAA, Zero)
                    Name (BBBB, Zero)
                    Name (CCCC, Zero)
                    Local0 = CTNL /* \_SB_.PC00.TCPU.CTNL */
                    If (((Local0 == One) || (Local0 == 0x02)))
                    {
                        Local0 = \_SB.CLVL /* External reference */
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Zero
                        })
                    }

                    If ((CLCK == One))
                    {
                        Local0 = One
                    }

                    AAAA = CPNU (\_SB.PL10, One)
                    BBBB = CPNU (\_SB.PL11, One)
                    CCCC = CPNU (\_SB.PL12, One)
                    Name (TMP1, Package (0x01)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP2, Package (0x02)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP3, Package (0x03)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    If ((Local0 == 0x03))
                    {
                        If ((AAAA > BBBB))
                        {
                            If ((AAAA > CCCC))
                            {
                                If ((BBBB > CCCC))
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local4 = One
                                    LEV1 = One
                                    Local5 = 0x02
                                    LEV2 = 0x02
                                }
                                Else
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local5 = One
                                    LEV1 = 0x02
                                    Local4 = 0x02
                                    LEV2 = One
                                }
                            }
                            Else
                            {
                                Local5 = Zero
                                LEV0 = 0x02
                                Local3 = One
                                LEV1 = Zero
                                Local4 = 0x02
                                LEV2 = One
                            }
                        }
                        ElseIf ((BBBB > CCCC))
                        {
                            If ((AAAA > CCCC))
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local3 = One
                                LEV1 = Zero
                                Local5 = 0x02
                                LEV2 = 0x02
                            }
                            Else
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local5 = One
                                LEV1 = 0x02
                                Local3 = 0x02
                                LEV2 = Zero
                            }
                        }
                        Else
                        {
                            Local5 = Zero
                            LEV0 = 0x02
                            Local4 = One
                            LEV1 = One
                            Local3 = 0x02
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP3 [Local3]) [One] = Local2
                        DerefOf (TMP3 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP3 [Local3]) [0x03] = Local1
                        DerefOf (TMP3 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP3 [Local4]) [One] = Local2
                        DerefOf (TMP3 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP3 [Local4]) [0x03] = Local1
                        DerefOf (TMP3 [Local4]) [0x04] = Zero
                        Local1 = (\_SB.TAR2 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local5]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                        DerefOf (TMP3 [Local5]) [One] = Local2
                        DerefOf (TMP3 [Local5]) [0x02] = \_SB.CTC2 /* External reference */
                        DerefOf (TMP3 [Local5]) [0x03] = Local1
                        DerefOf (TMP3 [Local5]) [0x04] = Zero
                        Return (TMP3) /* \_SB_.PC00.TCPU.TDPL.TMP3 */
                    }

                    If ((Local0 == 0x02))
                    {
                        If ((AAAA > BBBB))
                        {
                            Local3 = Zero
                            Local4 = One
                            LEV0 = Zero
                            LEV1 = One
                            LEV2 = Zero
                        }
                        Else
                        {
                            Local4 = Zero
                            Local3 = One
                            LEV0 = One
                            LEV1 = Zero
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP2 [Local3]) [One] = Local2
                        DerefOf (TMP2 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP2 [Local3]) [0x03] = Local1
                        DerefOf (TMP2 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP2 [Local4]) [One] = Local2
                        DerefOf (TMP2 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP2 [Local4]) [0x03] = Local1
                        DerefOf (TMP2 [Local4]) [0x04] = Zero
                        Return (TMP2) /* \_SB_.PC00.TCPU.TDPL.TMP2 */
                    }

                    If ((Local0 == One))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                Local1 = (\_SB.TAR0 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC0 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = Zero
                                LEV1 = Zero
                                LEV2 = Zero
                            }
                            Case (One)
                            {
                                Local1 = (\_SB.TAR1 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC1 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = One
                                LEV1 = One
                                LEV2 = One
                            }
                            Case (0x02)
                            {
                                Local1 = (\_SB.TAR2 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC2 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = 0x02
                                LEV1 = 0x02
                                LEV2 = 0x02
                            }

                        }

                        Return (TMP1) /* \_SB_.PC00.TCPU.TDPL.TMP1 */
                    }

                    Return (Zero)
                }

                Name (MAXT, Zero)
                Method (TDPC, 0, NotSerialized)
                {
                    Return (MAXT) /* \_SB_.PC00.TCPU.MAXT */
                }

                Name (LEV0, Zero)
                Name (LEV1, Zero)
                Name (LEV2, Zero)
                Method (STDP, 1, Serialized)
                {
                    If ((Arg0 >= \_SB.CLVL))
                    {
                        Return (Zero)
                    }

                    Switch (ToInteger (Arg0))
                    {
                        Case (Zero)
                        {
                            Local0 = LEV0 /* \_SB_.PC00.TCPU.LEV0 */
                        }
                        Case (One)
                        {
                            Local0 = LEV1 /* \_SB_.PC00.TCPU.LEV1 */
                        }
                        Case (0x02)
                        {
                            Local0 = LEV2 /* \_SB_.PC00.TCPU.LEV2 */
                        }

                    }

                    Switch (ToInteger (Local0))
                    {
                        Case (Zero)
                        {
                            CPL0 ()
                        }
                        Case (One)
                        {
                            CPL1 ()
                        }
                        Case (0x02)
                        {
                            CPL2 ()
                        }

                    }

                    Notify (\_SB.PC00.TCPU, 0x83) // Device-Specific Change
                }
            }

            Scope (\_SB.IETM)
            {
                Name (PTTL, 0x14)
                Name (PSVT, Package (0x04)
                {
                    0x02, 
                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.TCPU, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN2, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN3, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }
                })
            }

            Scope (\_SB.PC00.LPCB)
            {
                Device (SEN3)
                {
                    Name (_HID, "INTC1046")  // _HID: Hardware ID
                    Name (_UID, "SEN3")  // _UID: Unique ID
                    Name (_STR, Unicode ("Remote GT Sensor"))  // _STR: Description String
                    Name (PTYP, 0x03)
                    Name (CTYP, Zero)
                    Name (PFLG, Zero)
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        If ((\S3DE == One))
                        {
                            Return (0x0F)
                        }
                        Else
                        {
                            Return (Zero)
                        }
                    }

                    Method (_TMP, 0, Serialized)  // _TMP: Temperature
                    {
                        Return (\_SB.IETM.CTOK (\_SB.PC00.LPCB.M662))
                    }

                    Name (PATC, 0x02)
                    Method (PAT0, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4LO = Local1
                        \_SB.PC00.LPCB.Q4LU = One
                    }

                    Method (PAT1, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4HI = Local1
                        \_SB.PC00.LPCB.Q4HU = One
                    }

                    Name (GTSH, 0x14)
                    Name (LSTM, Zero)
                    Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                    {
                        LSTM = Arg0
                        Notify (\_SB.PC00.LPCB.SEN3, 0x91) // Device-Specific
                    }

                    Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                    {
                        Return (0x0ADE)
                    }

                    Name (S3AC, 0x3C)
                    Name (S3A1, 0x32)
                    Name (S3A2, 0x28)
                    Name (S3PV, 0x41)
                    Name (S3CC, 0x50)
                    Name (S3C3, 0x46)
                    Name (S3HP, 0x4B)
                    Name (SSP3, Zero)
                    Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
                    {
                        Return (SSP3) /* \_SB_.PC00.LPCB.SEN3.SSP3 */
                    }

                    Method (_AC3, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Local1 = \_SB.IETM.CTOK (S3AC)
                        If ((LSTM >= Local1))
                        {
                            Return ((Local1 - 0x14))
                        }
                        Else
                        {
                            Return (Local1)
                        }
                    }

                    Method (_AC4, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A1))
                    }

                    Method (_AC5, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A2))
                    }

                    Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3PV))
                    }

                    Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3CC))
                    }

                    Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3C3))
                    }

                    Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3HP))
                    }
                }
            }

            Method (GDDV, 0, Serialized)
            {
                Return (Package (0x01)
                {
                    Buffer (0x05E9)
                    {
                        /* 0000 */  0xE5, 0x1F, 0x94, 0x00, 0x00, 0x00, 0x00, 0x02,  // ........
                        /* 0008 */  0x00, 0x00, 0x00, 0x40, 0x67, 0x64, 0x64, 0x76,  // ...@gddv
                        /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0020 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0028 */  0x00, 0x00, 0x00, 0x00, 0x4F, 0x45, 0x4D, 0x20,  // ....OEM 
                        /* 0030 */  0x45, 0x78, 0x70, 0x6F, 0x72, 0x74, 0x65, 0x64,  // Exported
                        /* 0038 */  0x20, 0x44, 0x61, 0x74, 0x61, 0x56, 0x61, 0x75,  //  DataVau
                        /* 0040 */  0x6C, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // lt......
                        /* 0048 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0050 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0058 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0060 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0068 */  0x00, 0x00, 0x00, 0x00, 0x78, 0x77, 0x17, 0x05,  // ....xw..
                        /* 0070 */  0x6E, 0xD1, 0xD1, 0xFD, 0x7A, 0x32, 0x66, 0xC9,  // n...z2f.
                        /* 0078 */  0xE2, 0x68, 0xCE, 0x77, 0x3E, 0x9A, 0x94, 0xFC,  // .h.w>...
                        /* 0080 */  0x6E, 0x64, 0xFB, 0x47, 0x94, 0x83, 0xFA, 0x43,  // nd.G...C
                        /* 0088 */  0x07, 0x97, 0x1E, 0xA8, 0x55, 0x05, 0x00, 0x00,  // ....U...
                        /* 0090 */  0x52, 0x45, 0x50, 0x4F, 0x5D, 0x00, 0x00, 0x00,  // REPO]...
                        /* 0098 */  0x01, 0xA6, 0x4D, 0x00, 0x00, 0x00, 0x00, 0x00,  // ..M.....
                        /* 00A0 */  0x00, 0x00, 0x72, 0x87, 0xCD, 0xFF, 0x6D, 0x24,  // ..r...m$
                        /* 00A8 */  0x47, 0xDB, 0x3D, 0x24, 0x92, 0xB4, 0x16, 0x6F,  // G.=$...o
                        /* 00B0 */  0x45, 0xD8, 0xC3, 0xF5, 0x66, 0x14, 0x9F, 0x22,  // E...f.."
                        /* 00B8 */  0xD7, 0xF7, 0xDE, 0x67, 0x90, 0x9A, 0xA2, 0x0D,  // ...g....
                        /* 00C0 */  0x39, 0x25, 0xAD, 0xC3, 0x1A, 0xAD, 0x52, 0x0B,  // 9%....R.
                        /* 00C8 */  0x75, 0x38, 0xE1, 0xA4, 0x14, 0x41, 0x89, 0x20,  // u8...A. 
                        /* 00D0 */  0xE7, 0xBF, 0x0A, 0xD0, 0x30, 0xFC, 0xA9, 0xB8,  // ....0...
                        /* 00D8 */  0x14, 0xF5, 0xDF, 0xAE, 0xF2, 0xFA, 0x10, 0x08,  // ........
                        /* 00E0 */  0xED, 0xB9, 0x65, 0x37, 0xCB, 0x2A, 0x71, 0xFE,  // ..e7.*q.
                        /* 00E8 */  0x87, 0xB2, 0x54, 0x65, 0x29, 0xA7, 0x66, 0x31,  // ..Te).f1
                        /* 00F0 */  0x59, 0x71, 0x81, 0x9E, 0x38, 0x47, 0x35, 0x0F,  // Yq..8G5.
                        /* 00F8 */  0xFC, 0x36, 0xEB, 0xC1, 0x3B, 0x1F, 0xE6, 0x6D,  // .6..;..m
                        /* 0100 */  0x9C, 0x01, 0xB8, 0x68, 0x27, 0xEB, 0x08, 0x1D,  // ...h'...
                        /* 0108 */  0x83, 0x84, 0xAB, 0x28, 0x82, 0x78, 0xE6, 0x71,  // ...(.x.q
                        /* 0110 */  0xEC, 0x47, 0xA4, 0x89, 0x48, 0x62, 0x9E, 0xA5,  // .G..Hb..
                        /* 0118 */  0x29, 0x49, 0x82, 0xD9, 0x38, 0xB6, 0x7A, 0x1B,  // )I..8.z.
                        /* 0120 */  0x28, 0x65, 0xD5, 0x48, 0x2C, 0x77, 0x72, 0x7F,  // (e.H,wr.
                        /* 0128 */  0x2D, 0xAE, 0xCE, 0xA8, 0x55, 0x10, 0x6E, 0x9E,  // -...U.n.
                        /* 0130 */  0x8C, 0x6B, 0x64, 0x74, 0xE4, 0x9D, 0xF1, 0xCC,  // .kdt....
                        /* 0138 */  0xB6, 0x83, 0x76, 0x16, 0xFD, 0x71, 0x42, 0x58,  // ..v..qBX
                        /* 0140 */  0xAE, 0x8A, 0x75, 0xE5, 0x90, 0x61, 0xFC, 0x04,  // ..u..a..
                        /* 0148 */  0xBC, 0xF0, 0x9B, 0x74, 0x58, 0x4C, 0x53, 0xCB,  // ...tXLS.
                        /* 0150 */  0x07, 0x15, 0xD4, 0x58, 0xA8, 0x97, 0xD4, 0xDE,  // ...X....
                        /* 0158 */  0x67, 0xF0, 0xB6, 0x77, 0x4C, 0xBD, 0x30, 0x9B,  // g..wL.0.
                        /* 0160 */  0x62, 0x50, 0x6B, 0x31, 0xA0, 0x92, 0x9F, 0xAD,  // bPk1....
                        /* 0168 */  0xAC, 0x42, 0xCA, 0x1D, 0x1D, 0x37, 0xD1, 0xB4,  // .B...7..
                        /* 0170 */  0x9E, 0x5F, 0xE4, 0xD9, 0x95, 0xB5, 0x00, 0x7E,  // ._.....~
                        /* 0178 */  0x8D, 0x22, 0x2B, 0xF0, 0x83, 0xC1, 0xA7, 0x63,  // ."+....c
                        /* 0180 */  0x1C, 0xD7, 0x37, 0x17, 0x5E, 0x01, 0x2C, 0xBD,  // ..7.^.,.
                        /* 0188 */  0x13, 0x59, 0x9B, 0x4A, 0xA4, 0x22, 0x22, 0x32,  // .Y.J.""2
                        /* 0190 */  0xB8, 0x1B, 0x4F, 0x23, 0xAB, 0x7E, 0x0B, 0xB1,  // ..O#.~..
                        /* 0198 */  0xAB, 0x90, 0x97, 0xE2, 0x42, 0x13, 0x90, 0xB7,  // ....B...
                        /* 01A0 */  0xED, 0xC5, 0xE8, 0x31, 0x95, 0x0E, 0x44, 0x17,  // ...1..D.
                        /* 01A8 */  0xBF, 0x4E, 0xB6, 0x75, 0x23, 0xEB, 0xF5, 0x5F,  // .N.u#.._
                        /* 01B0 */  0x37, 0x40, 0x00, 0xD9, 0xBB, 0x1D, 0x38, 0xD0,  // 7@....8.
                        /* 01B8 */  0xE0, 0x9C, 0xA6, 0x8D, 0x82, 0x66, 0xCE, 0x8C,  // .....f..
                        /* 01C0 */  0x80, 0x78, 0xB6, 0x63, 0xC6, 0xEB, 0xDF, 0x56,  // .x.c...V
                        /* 01C8 */  0xB2, 0x61, 0x9F, 0xC6, 0x7A, 0x29, 0xF2, 0x91,  // .a..z)..
                        /* 01D0 */  0x07, 0x30, 0xDC, 0x3E, 0x92, 0xA0, 0x55, 0xA8,  // .0.>..U.
                        /* 01D8 */  0x7A, 0x54, 0xB1, 0xA1, 0x53, 0x95, 0x3C, 0xA0,  // zT..S.<.
                        /* 01E0 */  0xEF, 0x17, 0x21, 0x76, 0x19, 0x2B, 0x9A, 0xA3,  // ..!v.+..
                        /* 01E8 */  0x2F, 0x08, 0x55, 0x7D, 0x38, 0x42, 0x1C, 0xE3,  // /.U}8B..
                        /* 01F0 */  0x7F, 0x77, 0x7D, 0x7A, 0x5E, 0xCF, 0x91, 0x73,  // .w}z^..s
                        /* 01F8 */  0x94, 0x54, 0xFE, 0x0C, 0x52, 0x5C, 0xC1, 0x81,  // .T..R\..
                        /* 0200 */  0xCA, 0x27, 0x1A, 0xF4, 0xCA, 0x1B, 0x74, 0x12,  // .'....t.
                        /* 0208 */  0x66, 0xAA, 0x10, 0xF7, 0x2F, 0xCA, 0xDB, 0x37,  // f.../..7
                        /* 0210 */  0x16, 0xB1, 0x43, 0x27, 0xC0, 0x35, 0x97, 0x02,  // ..C'.5..
                        /* 0218 */  0x3C, 0xF6, 0x36, 0xFA, 0xB9, 0x06, 0x7D, 0xA7,  // <.6...}.
                        /* 0220 */  0x88, 0x5D, 0xDD, 0x0D, 0x34, 0x76, 0x71, 0x31,  // .]..4vq1
                        /* 0228 */  0x25, 0xE1, 0xE6, 0x57, 0x9E, 0x8F, 0x97, 0xBD,  // %..W....
                        /* 0230 */  0x9C, 0x5A, 0xBE, 0xFF, 0xFF, 0x44, 0x44, 0x8B,  // .Z...DD.
                        /* 0238 */  0xFC, 0xF0, 0x67, 0x96, 0x20, 0x90, 0x4F, 0x69,  // ..g. .Oi
                        /* 0240 */  0x41, 0x80, 0x05, 0x32, 0x76, 0x2E, 0x41, 0xED,  // A..2v.A.
                        /* 0248 */  0xED, 0x75, 0x5F, 0x72, 0xA9, 0x99, 0xEB, 0xDC,  // .u_r....
                        /* 0250 */  0xE1, 0x53, 0x56, 0x7B, 0xDA, 0x1A, 0x7B, 0xF4,  // .SV{..{.
                        /* 0258 */  0xE3, 0x19, 0xD9, 0xC0, 0x8B, 0x2F, 0x46, 0x54,  // ...../FT
                        /* 0260 */  0x28, 0x82, 0xF0, 0xC2, 0x30, 0x68, 0x51, 0x66,  // (...0hQf
                        /* 0268 */  0x69, 0x6B, 0x00, 0x52, 0x69, 0xF5, 0x20, 0xFC,  // ik.Ri. .
                        /* 0270 */  0xB6, 0x10, 0x9C, 0xD0, 0x26, 0x26, 0xFD, 0x56,  // ....&&.V
                        /* 0278 */  0x82, 0xB9, 0x33, 0x96, 0x5A, 0xCC, 0x6A, 0xA8,  // ..3.Z.j.
                        /* 0280 */  0x66, 0x24, 0xA7, 0x07, 0x70, 0x58, 0x02, 0x78,  // f$..pX.x
                        /* 0288 */  0xD0, 0x87, 0x22, 0x47, 0xDF, 0xD7, 0xA1, 0xA1,  // .."G....
                        /* 0290 */  0x4A, 0xE7, 0x7B, 0xBD, 0xA4, 0xEB, 0x71, 0x82,  // J.{...q.
                        /* 0298 */  0x45, 0x17, 0x7C, 0x8C, 0x6B, 0xC7, 0xC3, 0xA3,  // E.|.k...
                        /* 02A0 */  0xBF, 0x26, 0xFB, 0xC4, 0xF4, 0xFC, 0xF0, 0x61,  // .&.....a
                        /* 02A8 */  0xDD, 0x85, 0x27, 0xFA, 0xC8, 0x44, 0x0D, 0x75,  // ..'..D.u
                        /* 02B0 */  0x5D, 0x37, 0xED, 0x27, 0x4B, 0xF7, 0xFA, 0x4E,  // ]7.'K..N
                        /* 02B8 */  0x80, 0x6F, 0x8F, 0xA1, 0x51, 0x56, 0x60, 0xE2,  // .o..QV`.
                        /* 02C0 */  0x93, 0xCA, 0xD4, 0x05, 0x58, 0xCE, 0x09, 0x5E,  // ....X..^
                        /* 02C8 */  0xE4, 0x91, 0x66, 0x18, 0x89, 0x89, 0x18, 0x36,  // ..f....6
                        /* 02D0 */  0xE9, 0x45, 0x5E, 0xC3, 0x23, 0x99, 0x12, 0xC5,  // .E^.#...
                        /* 02D8 */  0xD9, 0xD5, 0x9D, 0xDC, 0xB5, 0x91, 0xBC, 0xC7,  // ........
                        /* 02E0 */  0x18, 0xB7, 0x67, 0xF6, 0x95, 0x43, 0x6A, 0x8D,  // ..g..Cj.
                        /* 02E8 */  0x5C, 0x59, 0x9B, 0x0C, 0xD9, 0x10, 0x4C, 0x8E,  // \Y....L.
                        /* 02F0 */  0x7A, 0xFF, 0xA5, 0xBF, 0xE6, 0x31, 0x31, 0x1E,  // z....11.
                        /* 02F8 */  0x9F, 0xA6, 0xE1, 0xC8, 0xBE, 0x8E, 0x94, 0xE5,  // ........
                        /* 0300 */  0xD3, 0xA1, 0xE3, 0x2B, 0x8D, 0xE6, 0x30, 0x29,  // ...+..0)
                        /* 0308 */  0xE7, 0xC5, 0x12, 0xD3, 0x9D, 0xC7, 0x2F, 0x94,  // ....../.
                        /* 0310 */  0xCE, 0x35, 0x34, 0x7B, 0x8A, 0x1C, 0x43, 0xE1,  // .54{..C.
                        /* 0318 */  0x56, 0x6B, 0xEC, 0x9B, 0x50, 0xD7, 0x7E, 0x9B,  // Vk..P.~.
                        /* 0320 */  0xA8, 0xEA, 0x6A, 0x7A, 0x12, 0x0A, 0xF2, 0x2A,  // ..jz...*
                        /* 0328 */  0x8C, 0x0D, 0xCE, 0x39, 0xF0, 0x93, 0xD4, 0x0C,  // ...9....
                        /* 0330 */  0x97, 0xB3, 0x7B, 0x4A, 0x4F, 0xBC, 0xBA, 0xDC,  // ..{JO...
                        /* 0338 */  0x68, 0xFC, 0xCA, 0x7B, 0x67, 0xC3, 0x98, 0xA2,  // h..{g...
                        /* 0340 */  0x2B, 0xBF, 0xE3, 0x9D, 0x61, 0x60, 0xCC, 0xD3,  // +...a`..
                        /* 0348 */  0xCD, 0x40, 0x63, 0xB7, 0x50, 0x08, 0xAE, 0x34,  // .@c.P..4
                        /* 0350 */  0x84, 0xAC, 0x0D, 0xBE, 0x8A, 0x60, 0x1A, 0x8C,  // .....`..
                        /* 0358 */  0xE8, 0xE3, 0x4E, 0x98, 0xE5, 0x6D, 0xC8, 0xF9,  // ..N..m..
                        /* 0360 */  0x3E, 0xE4, 0x4E, 0x9F, 0xA7, 0x96, 0xA7, 0x96,  // >.N.....
                        /* 0368 */  0xE2, 0x85, 0x2A, 0x70, 0x4E, 0xFC, 0x28, 0xE6,  // ..*pN.(.
                        /* 0370 */  0xA8, 0x1C, 0xC9, 0xBE, 0x39, 0x15, 0x05, 0x9D,  // ....9...
                        /* 0378 */  0xAC, 0x3C, 0x81, 0x04, 0xF2, 0xA1, 0xAE, 0x0B,  // .<......
                        /* 0380 */  0xBC, 0xB5, 0xB4, 0x42, 0x7C, 0x43, 0x48, 0x06,  // ...B|CH.
                        /* 0388 */  0x69, 0x4C, 0x80, 0xA8, 0xA8, 0x9D, 0x3B, 0xF7,  // iL....;.
                        /* 0390 */  0x79, 0x50, 0xFB, 0xFF, 0xB8, 0xF7, 0xD6, 0x31,  // yP.....1
                        /* 0398 */  0x85, 0xF2, 0xA5, 0xA7, 0x18, 0x2E, 0x97, 0x8C,  // ........
                        /* 03A0 */  0xD2, 0x2F, 0x5B, 0x22, 0x22, 0x0A, 0x55, 0x04,  // ./["".U.
                        /* 03A8 */  0xF6, 0x21, 0xD1, 0x58, 0xA5, 0xB8, 0x8D, 0x17,  // .!.X....
                        /* 03B0 */  0xAC, 0xCC, 0xD2, 0x85, 0xD9, 0x21, 0x9C, 0x7A,  // .....!.z
                        /* 03B8 */  0xA2, 0xB3, 0x90, 0x11, 0x7B, 0x91, 0x5C, 0xB3,  // ....{.\.
                        /* 03C0 */  0x90, 0x68, 0xD6, 0x2F, 0xFF, 0xD6, 0x1F, 0x77,  // .h./...w
                        /* 03C8 */  0x89, 0x38, 0x75, 0x1E, 0x8D, 0x6D, 0xC7, 0xC7,  // .8u..m..
                        /* 03D0 */  0xCC, 0x20, 0xAF, 0xA0, 0x76, 0x9A, 0xAE, 0x8C,  // . ..v...
                        /* 03D8 */  0xDA, 0x4F, 0x7F, 0xCC, 0x17, 0x6B, 0x0A, 0xD8,  // .O...k..
                        /* 03E0 */  0xEA, 0x00, 0x2B, 0x43, 0x6D, 0x93, 0x0F, 0xEF,  // ..+Cm...
                        /* 03E8 */  0xF7, 0xB2, 0x42, 0x69, 0x59, 0xA5, 0xAA, 0xC3,  // ..BiY...
                        /* 03F0 */  0x37, 0x1A, 0x7B, 0xAE, 0xE9, 0xB2, 0x34, 0xC7,  // 7.{...4.
                        /* 03F8 */  0x3F, 0xDD, 0x6E, 0x6F, 0x75, 0x7C, 0x1A, 0x06,  // ?.nou|..
                        /* 0400 */  0x50, 0xAC, 0x18, 0x20, 0x6A, 0x81, 0x1A, 0x74,  // P.. j..t
                        /* 0408 */  0xB6, 0x22, 0xE9, 0xFF, 0x5C, 0xAE, 0xA3, 0xC1,  // ."..\...
                        /* 0410 */  0x84, 0x69, 0xC4, 0x20, 0x4E, 0x22, 0xD9, 0xFC,  // .i. N"..
                        /* 0418 */  0x71, 0x5B, 0x79, 0xCC, 0x5D, 0x4A, 0x71, 0x3B,  // q[y.]Jq;
                        /* 0420 */  0x47, 0x5E, 0xEE, 0x8A, 0x67, 0x9D, 0x06, 0x62,  // G^..g..b
                        /* 0428 */  0x06, 0x55, 0x46, 0xE5, 0x4A, 0x06, 0x33, 0xF8,  // .UF.J.3.
                        /* 0430 */  0x95, 0x71, 0xDC, 0x67, 0x3D, 0x76, 0x4A, 0x25,  // .q.g=vJ%
                        /* 0438 */  0xE3, 0xF8, 0xB2, 0x41, 0x61, 0xB7, 0x81, 0x3B,  // ...Aa..;
                        /* 0440 */  0x51, 0xFF, 0x85, 0x35, 0xF2, 0x32, 0xEF, 0x2D,  // Q..5.2.-
                        /* 0448 */  0x27, 0xEA, 0x28, 0x6B, 0x23, 0x83, 0x5E, 0x4C,  // '.(k#.^L
                        /* 0450 */  0x47, 0x92, 0xD5, 0x52, 0xDE, 0x62, 0x34, 0x8F,  // G..R.b4.
                        /* 0458 */  0x64, 0xDA, 0xF9, 0x04, 0xCA, 0x8B, 0xE7, 0x58,  // d......X
                        /* 0460 */  0xE9, 0x20, 0xF3, 0x34, 0x76, 0x9B, 0xEE, 0xDF,  // . .4v...
                        /* 0468 */  0x27, 0x24, 0xE6, 0xCF, 0x6D, 0x91, 0x10, 0x3E,  // '$..m..>
                        /* 0470 */  0xBA, 0x03, 0xF9, 0xF2, 0xBB, 0x68, 0x9E, 0x24,  // .....h.$
                        /* 0478 */  0x1D, 0xAA, 0xCB, 0x7C, 0x16, 0x12, 0x9A, 0x6F,  // ...|...o
                        /* 0480 */  0x73, 0xB9, 0xEC, 0x5F, 0xBE, 0x9E, 0x91, 0x29,  // s.._...)
                        /* 0488 */  0x70, 0x90, 0xCD, 0x46, 0xDB, 0x29, 0x3B, 0x13,  // p..F.);.
                        /* 0490 */  0x94, 0x8D, 0x34, 0x69, 0x82, 0x1A, 0x65, 0x23,  // ..4i..e#
                        /* 0498 */  0x84, 0x18, 0x12, 0xFB, 0x1E, 0x91, 0x64, 0xD9,  // ......d.
                        /* 04A0 */  0xBA, 0xC3, 0xFB, 0x2A, 0x7A, 0x50, 0x1A, 0x5A,  // ...*zP.Z
                        /* 04A8 */  0x68, 0xE2, 0x80, 0x19, 0x23, 0xA6, 0x4B, 0x8D,  // h...#.K.
                        /* 04B0 */  0x40, 0xFC, 0x62, 0x6A, 0xDC, 0x6A, 0xBA, 0xB6,  // @.bj.j..
                        /* 04B8 */  0xB0, 0x01, 0x30, 0xB7, 0x0B, 0xDC, 0x74, 0x81,  // ..0...t.
                        /* 04C0 */  0xB8, 0x6E, 0xA7, 0x36, 0x0D, 0x0E, 0x0C, 0x73,  // .n.6...s
                        /* 04C8 */  0x4F, 0x10, 0xBD, 0xA9, 0x79, 0x7B, 0x6A, 0xD8,  // O...y{j.
                        /* 04D0 */  0xEA, 0x43, 0x48, 0x91, 0x6E, 0x9B, 0x52, 0xC6,  // .CH.n.R.
                        /* 04D8 */  0x1A, 0xC8, 0x12, 0xCB, 0xFA, 0x0C, 0xCB, 0x15,  // ........
                        /* 04E0 */  0x69, 0xAA, 0xA4, 0x97, 0xC7, 0xB2, 0xEF, 0x86,  // i.......
                        /* 04E8 */  0x11, 0x2C, 0x6E, 0x5B, 0xCA, 0x73, 0xC7, 0xCB,  // .,n[.s..
                        /* 04F0 */  0xC4, 0xE8, 0x92, 0xAF, 0xB6, 0x01, 0x6E, 0xD7,  // ......n.
                        /* 04F8 */  0xAD, 0x2F, 0x7C, 0x72, 0xB4, 0x20, 0x94, 0x4A,  // ./|r. .J
                        /* 0500 */  0x1B, 0x86, 0x3B, 0x97, 0x40, 0x06, 0x77, 0x77,  // ..;.@.ww
                        /* 0508 */  0x09, 0xD0, 0x51, 0x09, 0xAB, 0x48, 0x34, 0x53,  // ..Q..H4S
                        /* 0510 */  0x29, 0x80, 0x1F, 0x2E, 0x80, 0xCD, 0xF1, 0x2D,  // )......-
                        /* 0518 */  0x09, 0x40, 0x51, 0xFB, 0xEB, 0x77, 0x07, 0xFE,  // .@Q..w..
                        /* 0520 */  0xE8, 0x21, 0x6A, 0x2C, 0xA6, 0x76, 0x63, 0x6A,  // .!j,.vcj
                        /* 0528 */  0xE0, 0x87, 0x3F, 0x86, 0x25, 0x64, 0xE1, 0x44,  // ..?.%d.D
                        /* 0530 */  0x3C, 0x06, 0xCA, 0x02, 0x2E, 0xA7, 0x5D, 0xAB,  // <.....].
                        /* 0538 */  0x28, 0x21, 0x81, 0xE4, 0xEB, 0xD0, 0x46, 0xA7,  // (!....F.
                        /* 0540 */  0x30, 0x94, 0xBA, 0xE6, 0x97, 0x3C, 0x28, 0x9B,  // 0....<(.
                        /* 0548 */  0xA1, 0x81, 0x73, 0xE7, 0x5F, 0xE1, 0x7E, 0x1E,  // ..s._.~.
                        /* 0550 */  0xA9, 0x5A, 0x07, 0x90, 0xD0, 0x5C, 0xE9, 0xB3,  // .Z...\..
                        /* 0558 */  0x6D, 0xC7, 0x6F, 0x65, 0xC2, 0xEB, 0x2C, 0xDF,  // m.oe..,.
                        /* 0560 */  0x0C, 0xB6, 0xB5, 0x8A, 0xF1, 0xA7, 0x64, 0x1C,  // ......d.
                        /* 0568 */  0x90, 0x51, 0xF2, 0x94, 0xD4, 0xCA, 0x5F, 0x42,  // .Q...._B
                        /* 0570 */  0x29, 0x0A, 0x50, 0xAD, 0x11, 0x67, 0xDB, 0x7A,  // ).P..g.z
                        /* 0578 */  0x69, 0x94, 0x66, 0xCF, 0x65, 0xEE, 0xA2, 0xF8,  // i.f.e...
                        /* 0580 */  0xD3, 0xB4, 0x3B, 0x54, 0x75, 0xD1, 0xBE, 0x28,  // ..;Tu..(
                        /* 0588 */  0x0E, 0xB1, 0x4F, 0x86, 0xF4, 0x68, 0x23, 0x92,  // ..O..h#.
                        /* 0590 */  0xED, 0x77, 0xB0, 0xD5, 0x6C, 0x03, 0x57, 0x0C,  // .w..l.W.
                        /* 0598 */  0x3D, 0xB2, 0x14, 0xC6, 0xD5, 0xA7, 0xF7, 0x9D,  // =.......
                        /* 05A0 */  0xDD, 0x0A, 0x03, 0x79, 0xF4, 0xF1, 0xA0, 0x58,  // ...y...X
                        /* 05A8 */  0xC5, 0xA8, 0x9D, 0xB9, 0xCC, 0x69, 0x32, 0xA4,  // .....i2.
                        /* 05B0 */  0xDD, 0x8B, 0x34, 0xF2, 0x14, 0x72, 0x5B, 0x48,  // ..4..r[H
                        /* 05B8 */  0x07, 0xFC, 0xBB, 0xBB, 0xBB, 0x23, 0xF8, 0x09,  // .....#..
                        /* 05C0 */  0x26, 0x2D, 0xDE, 0x27, 0x93, 0x08, 0x94, 0xF3,  // &-.'....
                        /* 05C8 */  0x2D, 0xB2, 0xF3, 0x09, 0x04, 0xF2, 0xD7, 0x68,  // -......h
                        /* 05D0 */  0x2E, 0xA2, 0x80, 0xFC, 0x05, 0xFD, 0x47, 0xA5,  // ......G.
                        /* 05D8 */  0x44, 0x81, 0xEC, 0xBE, 0xA3, 0xC0, 0xF9, 0xC3,  // D.......
                        /* 05E0 */  0x2A, 0x19, 0x1F, 0xD0, 0x7B, 0x06, 0x4C, 0x13,  // *...{.L.
                        /* 05E8 */  0x40                                             // @
                    }
                })
            }

            Method (IMOK, 1, NotSerialized)
            {
                Return (Arg0)
            }
        }
    }

    Scope (\_SB.IETM)
    {
        Method (KTOC, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return (((Arg0 - 0x0AAC) / 0x0A))
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (CTOK, 1, Serialized)
        {
            Return (((Arg0 * 0x0A) + 0x0AAC))
        }

        Method (C10K, 1, Serialized)
        {
            Name (TMP1, Buffer (0x10)
            {
                 0x00                                             // .
            })
            CreateByteField (TMP1, Zero, TMPL)
            CreateByteField (TMP1, One, TMPH)
            Local0 = (Arg0 + 0x0AAC)
            TMPL = (Local0 & 0xFF)
            TMPH = ((Local0 & 0xFF00) >> 0x08)
            ToInteger (TMP1, Local1)
            Return (Local1)
        }

        Method (K10C, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return ((Arg0 - 0x0AAC))
            }
            Else
            {
                Return (Zero)
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000210B (8459)
 *     Revision         0x02
 *     Checksum         0x60
 *     OEM ID           "_ASUS_"
 *     OEM Table ID     "TbtTypeC"
 *     OEM Revision     0x00000000 (0)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "_ASUS_", "TbtTypeC", 0x00000000)
{
    External (_SB_.PC00.RP01.PXSX, DeviceObj)
    External (_SB_.PC00.RP05.PXSX, DeviceObj)
    External (_SB_.PC00.RP09.PXSX, DeviceObj)
    External (_SB_.PC00.RP13.PXSX, DeviceObj)
    External (_SB_.PC00.RP17.PXSX, DeviceObj)
    External (_SB_.PC00.RP21.PXSX, DeviceObj)
    External (_SB_.PC00.RP25.PXSX, DeviceObj)
    External (_SB_.UBTC.RUCC, MethodObj)    // 2 Arguments
    External (DPM1, IntObj)
    External (DPM2, IntObj)
    External (DPM3, IntObj)
    External (DTFS, IntObj)
    External (NDUS, IntObj)
    External (NTUS, IntObj)
    External (RPS0, IntObj)
    External (RPS1, IntObj)
    External (TBSE, IntObj)
    External (TP1D, IntObj)
    External (TP1P, IntObj)
    External (TP1T, IntObj)
    External (TP2D, IntObj)
    External (TP2P, IntObj)
    External (TP2T, IntObj)
    External (TP3D, IntObj)
    External (TP3P, IntObj)
    External (TP3T, IntObj)
    External (TP4D, IntObj)
    External (TP4P, IntObj)
    External (TP4T, IntObj)
    External (TP5D, IntObj)
    External (TP5P, IntObj)
    External (TP5T, IntObj)
    External (TP6D, IntObj)
    External (TP6P, IntObj)
    External (TP6T, IntObj)

    If (CondRefOf (\DTFS))
    {
        If (((DTFS == One) && ((RPS0 == One) || (RPS1 == One))))
        {
            Scope (\_SB.PC00.RP01.PXSX)
            {
                Name (TURP, One)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP01.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x05) || (RPS1 == 0x05))))
        {
            Scope (\_SB.PC00.RP05.PXSX)
            {
                Name (TURP, 0x05)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP05.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x09) || (RPS1 == 0x09))))
        {
            Scope (\_SB.PC00.RP09.PXSX)
            {
                Name (TURP, 0x09)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP09.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x0D) || (RPS1 == 0x0D))))
        {
            Scope (\_SB.PC00.RP13.PXSX)
            {
                Name (TURP, 0x0D)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP13.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x11) || (RPS1 == 0x11))))
        {
            Scope (\_SB.PC00.RP17.PXSX)
            {
                Name (TURP, 0x11)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP17.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x15) || (RPS1 == 0x15))))
        {
            Scope (\_SB.PC00.RP21.PXSX)
            {
                Name (TURP, 0x15)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP21.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }

        If (((DTFS == One) && ((RPS0 == 0x19) || (RPS1 == 0x19))))
        {
            Scope (\_SB.PC00.RP25.PXSX)
            {
                Name (TURP, 0x19)
                Device (TBDU)
                {
                    Name (_ADR, 0x00020000)  // _ADR: Address
                    Device (XHCI)
                    {
                        Name (_ADR, Zero)  // _ADR: Address
                        Device (RHUB)
                        {
                            Name (_ADR, Zero)  // _ADR: Address
                            Method (SLMS, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return ((TP1D & One))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return ((TP2D & One))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return ((TP3D & One))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return ((TP4D & One))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return ((TP5D & One))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return ((TP6D & One))
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (FPCP, 2, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (\_SB.UBTC.RUCC (One, Arg1))
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x02, Arg1))
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x03, Arg1))
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x04, Arg1))
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x05, Arg1))
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (\_SB.UBTC.RUCC (0x06, Arg1))
                                }
                                ElseIf ((Arg1 == One))
                                {
                                    Return (TUPC (Zero, Zero))
                                }
                                Else
                                {
                                    Return (TPLD (Zero, Zero))
                                }
                            }

                            Method (FPSP, 1, Serialized)
                            {
                                Local0 = (TURP << 0x02)
                                Local0 |= One
                                If ((((TP1D >> One) == Local0) && (Arg0 == TP1T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP2D >> One) == Local0) && (Arg0 == TP2T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP3D >> One) == Local0) && (Arg0 == TP3T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP4D >> One) == Local0) && (Arg0 == TP4T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP5D >> One) == Local0) && (Arg0 == TP5T)))
                                {
                                    Return (One)
                                }
                                ElseIf ((((TP6D >> One) == Local0) && (Arg0 == TP6T)))
                                {
                                    Return (One)
                                }
                                Else
                                {
                                    Return (Zero)
                                }
                            }

                            Method (TPLD, 2, Serialized)
                            {
                                Name (PCKG, Package (0x01)
                                {
                                    Buffer (0x10) {}
                                })
                                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                                REV = One
                                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                                VISI = Arg0
                                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                                GPOS = Arg1
                                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                                SHAP = One
                                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                                WID = 0x08
                                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                                HGT = 0x03
                                Return (PCKG) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.TPLD.PCKG */
                            }

                            Method (TUPC, 2, Serialized)
                            {
                                Name (PCKG, Package (0x04)
                                {
                                    One, 
                                    Zero, 
                                    Zero, 
                                    Zero
                                })
                                PCKG [Zero] = Arg0
                                PCKG [One] = Arg1
                                Return (PCKG) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.TUPC.PCKG */
                            }

                            Name (UUNS, Package (0x04)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            })
                            Name (PUNS, Package (0x01)
                            {
                                Buffer (0x10)
                                {
                                    /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                                    /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
                                }
                            })
                            Device (HS01)
                            {
                                Name (_ADR, One)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (HS02)
                            {
                                Name (_ADR, 0x02)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }

                            Device (SS01)
                            {
                                Name (_ADR, 0x03)  // _ADR: Address
                                Name (UPCN, Package (0x04)
                                {
                                    0xFF, 
                                    0x0A, 
                                    Zero, 
                                    Zero
                                })
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UPCN) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.SS01.UPCN */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (TPLD (One, One))
                                }
                            }

                            Device (SS02)
                            {
                                Name (_ADR, 0x04)  // _ADR: Address
                                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                                {
                                    Return (UUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.UUNS */
                                }

                                Method (_PLD, 0, Serialized)  // _PLD: Physical Location of Device
                                {
                                    Return (PUNS) /* \_SB_.PC00.RP25.PXSX.TBDU.XHCI.RHUB.PUNS */
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000111C (4380)
 *     Revision         0x02
 *     Checksum         0x5B
 *     OEM ID           "_ASUS_"
 *     OEM Table ID     "UsbCTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "_ASUS_", "UsbCTabl", 0x00001000)
{
    External (_SB_.PC00.LPCB.CCI0, IntObj)
    External (_SB_.PC00.LPCB.CCI1, IntObj)
    External (_SB_.PC00.LPCB.CCI2, IntObj)
    External (_SB_.PC00.LPCB.CCI3, IntObj)
    External (_SB_.PC00.LPCB.CTL0, IntObj)
    External (_SB_.PC00.LPCB.CTL1, IntObj)
    External (_SB_.PC00.LPCB.CTL2, IntObj)
    External (_SB_.PC00.LPCB.CTL3, IntObj)
    External (_SB_.PC00.LPCB.CTL4, IntObj)
    External (_SB_.PC00.LPCB.CTL5, IntObj)
    External (_SB_.PC00.LPCB.CTL6, IntObj)
    External (_SB_.PC00.LPCB.CTL7, IntObj)
    External (_SB_.PC00.LPCB.MGI0, IntObj)
    External (_SB_.PC00.LPCB.MGI1, IntObj)
    External (_SB_.PC00.LPCB.MGI2, IntObj)
    External (_SB_.PC00.LPCB.MGI3, IntObj)
    External (_SB_.PC00.LPCB.MGI4, IntObj)
    External (_SB_.PC00.LPCB.MGI5, IntObj)
    External (_SB_.PC00.LPCB.MGI6, IntObj)
    External (_SB_.PC00.LPCB.MGI7, IntObj)
    External (_SB_.PC00.LPCB.MGI8, IntObj)
    External (_SB_.PC00.LPCB.MGI9, IntObj)
    External (_SB_.PC00.LPCB.MGIA, IntObj)
    External (_SB_.PC00.LPCB.MGIB, IntObj)
    External (_SB_.PC00.LPCB.MGIC, IntObj)
    External (_SB_.PC00.LPCB.MGID, IntObj)
    External (_SB_.PC00.LPCB.MGIE, IntObj)
    External (_SB_.PC00.LPCB.MGIF, IntObj)
    External (_SB_.PC00.LPCB.MGO0, IntObj)
    External (_SB_.PC00.LPCB.MGO1, IntObj)
    External (_SB_.PC00.LPCB.MGO2, IntObj)
    External (_SB_.PC00.LPCB.MGO3, IntObj)
    External (_SB_.PC00.LPCB.MGO4, IntObj)
    External (_SB_.PC00.LPCB.MGO5, IntObj)
    External (_SB_.PC00.LPCB.MGO6, IntObj)
    External (_SB_.PC00.LPCB.MGO7, IntObj)
    External (_SB_.PC00.LPCB.MGO8, IntObj)
    External (_SB_.PC00.LPCB.MGO9, IntObj)
    External (_SB_.PC00.LPCB.MGOA, IntObj)
    External (_SB_.PC00.LPCB.MGOB, IntObj)
    External (_SB_.PC00.LPCB.MGOC, IntObj)
    External (_SB_.PC00.LPCB.MGOD, IntObj)
    External (_SB_.PC00.LPCB.MGOE, IntObj)
    External (_SB_.PC00.LPCB.MGOF, IntObj)
    External (_SB_.PC00.LPCB.SEC1, MethodObj)    // 1 Arguments
    External (_SB_.PC00.XHCI.RHUB, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.TPLD, MethodObj)    // 2 Arguments
    External (P8XH, MethodObj)    // 2 Arguments
    External (TP1D, UnknownObj)
    External (TP1P, UnknownObj)
    External (TP1T, UnknownObj)
    External (TP1U, UnknownObj)
    External (TP2D, UnknownObj)
    External (TP2P, UnknownObj)
    External (TP2T, UnknownObj)
    External (TP2U, UnknownObj)
    External (TP3D, UnknownObj)
    External (TP3P, UnknownObj)
    External (TP3T, UnknownObj)
    External (TP3U, UnknownObj)
    External (TP4D, UnknownObj)
    External (TP4P, UnknownObj)
    External (TP4T, UnknownObj)
    External (TP4U, UnknownObj)
    External (TP5D, UnknownObj)
    External (TP5P, UnknownObj)
    External (TP5T, UnknownObj)
    External (TP5U, UnknownObj)
    External (TP6D, UnknownObj)
    External (TP6P, UnknownObj)
    External (TP6T, UnknownObj)
    External (TP6U, UnknownObj)
    External (TP7D, UnknownObj)
    External (TP7P, UnknownObj)
    External (TP7T, UnknownObj)
    External (TP7U, UnknownObj)
    External (TP8D, UnknownObj)
    External (TP8P, UnknownObj)
    External (TP8T, UnknownObj)
    External (TP8U, UnknownObj)
    External (TP9D, UnknownObj)
    External (TP9P, UnknownObj)
    External (TP9T, UnknownObj)
    External (TP9U, UnknownObj)
    External (TPAD, UnknownObj)
    External (TPAP, UnknownObj)
    External (TPAT, UnknownObj)
    External (TPAU, UnknownObj)
    External (TTUP, UnknownObj)
    External (UBCB, UnknownObj)
    External (UCMS, UnknownObj)
    External (UDRS, UnknownObj)
    External (USTC, UnknownObj)
    External (XDCE, UnknownObj)

    Scope (\_SB)
    {
        Device (UBTC)
        {
            Name (_HID, EisaId ("USBC000"))  // _HID: Hardware ID
            Name (_CID, EisaId ("PNP0CA0"))  // _CID: Compatible ID
            Name (_UID, Zero)  // _UID: Unique ID
            Name (_DDN, "USB Type C")  // _DDN: DOS Device Name
            Name (CRS, ResourceTemplate ()
            {
                Memory32Fixed (ReadWrite,
                    0x00000000,         // Address Base
                    0x00001000,         // Address Length
                    _Y73)
            })
            Device (CR01)
            {
                Name (_ADR, Zero)  // _ADR: Address
                Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
                {
                    If ((USTC == One))
                    {
                        Return (\_SB.PC00.XHCI.RHUB.TPLD (One, One))
                    }
                }

                Name (UPC3, Package (0x04)
                {
                    0xFF, 
                    0x0A, 
                    Zero, 
                    Zero
                })
                Method (_UPC, 0, Serialized)  // _UPC: USB Port Capabilities
                {
                    Return (UPC3) /* \_SB_.UBTC.CR01.UPC3 */
                }
            }

            Method (_CRS, 0, Serialized)  // _CRS: Current Resource Settings
            {
                CreateDWordField (CRS, \_SB.UBTC._Y73._BAS, CBAS)  // _BAS: Base Address
                CBAS = UBCB /* External reference */
                Return (CRS) /* \_SB_.UBTC.CRS_ */
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((USTC == One))
                {
                    If ((UCMS == One))
                    {
                        Return (0x0F)
                    }
                }

                Return (Zero)
            }

            Method (RUCC, 2, Serialized)
            {
                If (((Arg0 <= 0x0A) && (Arg0 >= One)))
                {
                    If ((Arg1 == One))
                    {
                        Return (\_SB.UBTC.TUPC (One, FTPT (Arg0)))
                    }
                    Else
                    {
                        Return (\_SB.UBTC.TPLD (One, FPMN (Arg0)))
                    }
                }
                ElseIf ((Arg1 == One))
                {
                    Return (\_SB.UBTC.TUPC (Zero, Zero))
                }
                Else
                {
                    Return (\_SB.UBTC.TPLD (Zero, Zero))
                }
            }

            Method (FTPT, 1, Serialized)
            {
                Switch (ToInteger (Arg0))
                {
                    Case (One)
                    {
                        Local0 = (TP1D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x02)
                    {
                        Local0 = (TP2D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x03)
                    {
                        Local0 = (TP3D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x04)
                    {
                        Local0 = (TP4D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x05)
                    {
                        Local0 = (TP5D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x06)
                    {
                        Local0 = (TP6D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x07)
                    {
                        Local0 = (TP7D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x08)
                    {
                        Local0 = (TP8D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x09)
                    {
                        Local0 = (TP9D >> One)
                        Local0 &= 0x03
                    }
                    Case (0x0A)
                    {
                        Local0 = (TPAD >> One)
                        Local0 &= 0x03
                    }
                    Default
                    {
                        Local0 = 0xFF
                    }

                }

                Switch (ToInteger (Local0))
                {
                    Case (Zero)
                    {
                        Return (0x09)
                    }
                    Case (One)
                    {
                        Return (0x09)
                    }
                    Case (0x02)
                    {
                        Return (0x09)
                    }
                    Case (0x03)
                    {
                        Return (Zero)
                    }

                }

                Return (0x09)
            }

            Method (FPMN, 1, Serialized)
            {
                Switch (ToInteger (Arg0))
                {
                    Case (One)
                    {
                        Local0 = (TP1D >> One)
                        Local0 &= 0x03
                        Local1 = (TP1D & One)
                        Local2 = TP1P /* External reference */
                        Local3 = TP1T /* External reference */
                    }
                    Case (0x02)
                    {
                        Local0 = (TP2D >> One)
                        Local0 &= 0x03
                        Local1 = (TP2D & One)
                        Local2 = TP2P /* External reference */
                        Local3 = TP2T /* External reference */
                    }
                    Case (0x03)
                    {
                        Local0 = (TP3D >> One)
                        Local0 &= 0x03
                        Local1 = (TP3D & One)
                        Local2 = TP3P /* External reference */
                        Local3 = TP3T /* External reference */
                    }
                    Case (0x04)
                    {
                        Local0 = (TP4D >> One)
                        Local0 &= 0x03
                        Local1 = (TP4D & One)
                        Local2 = TP4P /* External reference */
                        Local3 = TP4T /* External reference */
                    }
                    Case (0x05)
                    {
                        Local0 = (TP5D >> One)
                        Local0 &= 0x03
                        Local1 = (TP5D & One)
                        Local2 = TP5P /* External reference */
                        Local3 = TP5T /* External reference */
                    }
                    Case (0x06)
                    {
                        Local0 = (TP6D >> One)
                        Local0 &= 0x03
                        Local1 = (TP6D & One)
                        Local2 = TP6P /* External reference */
                        Local3 = TP6T /* External reference */
                    }
                    Case (0x07)
                    {
                        Local0 = (TP7D >> One)
                        Local0 &= 0x03
                        Local1 = (TP7D & One)
                        Local2 = TP7P /* External reference */
                        Local3 = TP7T /* External reference */
                    }
                    Case (0x08)
                    {
                        Local0 = (TP8D >> One)
                        Local0 &= 0x03
                        Local1 = (TP8D & One)
                        Local2 = TP8P /* External reference */
                        Local3 = TP8T /* External reference */
                    }
                    Case (0x09)
                    {
                        Local0 = (TP9D >> One)
                        Local0 &= 0x03
                        Local1 = (TP9D & One)
                        Local2 = TP9P /* External reference */
                        Local3 = TP9T /* External reference */
                    }
                    Case (0x0A)
                    {
                        Local0 = (TPAD >> One)
                        Local0 &= 0x03
                        Local1 = (TPAD & One)
                        Local2 = TPAP /* External reference */
                        Local3 = TPAT /* External reference */
                    }
                    Default
                    {
                        Local0 = 0xFF
                        Local1 = Zero
                        Local2 = Zero
                        Local3 = Zero
                    }

                }

                If ((Local0 == Zero))
                {
                    Return (Local2)
                }
                ElseIf (((Local0 == One) || ((Local0 == 0x02) || (Local0 == 
                    0x03))))
                {
                    If ((Local1 == One))
                    {
                        Return (Local2)
                    }
                    Else
                    {
                        Return (Local3)
                    }
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (TPLD, 2, Serialized)
            {
                Name (PCKG, Package (0x01)
                {
                    Buffer (0x10) {}
                })
                CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
                REV = One
                CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
                VISI = Arg0
                CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
                GPOS = Arg1
                CreateField (DerefOf (PCKG [Zero]), 0x4A, 0x04, SHAP)
                SHAP = One
                CreateField (DerefOf (PCKG [Zero]), 0x20, 0x10, WID)
                WID = 0x08
                CreateField (DerefOf (PCKG [Zero]), 0x30, 0x10, HGT)
                HGT = 0x03
                Return (PCKG) /* \_SB_.UBTC.TPLD.PCKG */
            }

            Method (TUPC, 2, Serialized)
            {
                Name (PCKG, Package (0x04)
                {
                    One, 
                    Zero, 
                    Zero, 
                    Zero
                })
                PCKG [Zero] = Arg0
                PCKG [One] = Arg1
                Return (PCKG) /* \_SB_.UBTC.TUPC.PCKG */
            }

            Method (ITCP, 1, Serialized)
            {
                Switch (ToInteger (FTPT (Arg0)))
                {
                    Case (Package (0x03)
                        {
                            0x08, 
                            0x09, 
                            0x0A
                        }

)
                    {
                        Return (One)
                    }
                    Default
                    {
                        Return (Zero)
                    }

                }
            }

            OperationRegion (USBC, SystemMemory, UBCB, 0x38)
            Field (USBC, ByteAcc, Lock, Preserve)
            {
                VER1,   8, 
                VER2,   8, 
                RSV1,   8, 
                RSV2,   8, 
                CCI0,   8, 
                CCI1,   8, 
                CCI2,   8, 
                CCI3,   8, 
                CTL0,   8, 
                CTL1,   8, 
                CTL2,   8, 
                CTL3,   8, 
                CTL4,   8, 
                CTL5,   8, 
                CTL6,   8, 
                CTL7,   8, 
                MGI0,   8, 
                MGI1,   8, 
                MGI2,   8, 
                MGI3,   8, 
                MGI4,   8, 
                MGI5,   8, 
                MGI6,   8, 
                MGI7,   8, 
                MGI8,   8, 
                MGI9,   8, 
                MGIA,   8, 
                MGIB,   8, 
                MGIC,   8, 
                MGID,   8, 
                MGIE,   8, 
                MGIF,   8, 
                MGO0,   8, 
                MGO1,   8, 
                MGO2,   8, 
                MGO3,   8, 
                MGO4,   8, 
                MGO5,   8, 
                MGO6,   8, 
                MGO7,   8, 
                MGO8,   8, 
                MGO9,   8, 
                MGOA,   8, 
                MGOB,   8, 
                MGOC,   8, 
                MGOD,   8, 
                MGOE,   8, 
                MGOF,   8
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If ((Arg0 == ToUUID ("6f8398c2-7ca4-11e4-ad36-631042b5008f") /* Unknown UUID */))
                {
                    Switch (ToInteger (Arg2))
                    {
                        Case (Zero)
                        {
                            Return (Buffer (One)
                            {
                                 0x1F                                             // .
                            })
                        }
                        Case (One)
                        {
                            \_SB.PC00.LPCB.MGO0 = MGO0 /* \_SB_.UBTC.MGO0 */
                            \_SB.PC00.LPCB.MGO1 = MGO1 /* \_SB_.UBTC.MGO1 */
                            \_SB.PC00.LPCB.MGO2 = MGO2 /* \_SB_.UBTC.MGO2 */
                            \_SB.PC00.LPCB.MGO3 = MGO3 /* \_SB_.UBTC.MGO3 */
                            \_SB.PC00.LPCB.MGO4 = MGO4 /* \_SB_.UBTC.MGO4 */
                            \_SB.PC00.LPCB.MGO5 = MGO5 /* \_SB_.UBTC.MGO5 */
                            \_SB.PC00.LPCB.MGO6 = MGO6 /* \_SB_.UBTC.MGO6 */
                            \_SB.PC00.LPCB.MGO7 = MGO7 /* \_SB_.UBTC.MGO7 */
                            \_SB.PC00.LPCB.MGO8 = MGO8 /* \_SB_.UBTC.MGO8 */
                            \_SB.PC00.LPCB.MGO9 = MGO9 /* \_SB_.UBTC.MGO9 */
                            \_SB.PC00.LPCB.MGOA = MGOA /* \_SB_.UBTC.MGOA */
                            \_SB.PC00.LPCB.MGOB = MGOB /* \_SB_.UBTC.MGOB */
                            \_SB.PC00.LPCB.MGOC = MGOC /* \_SB_.UBTC.MGOC */
                            \_SB.PC00.LPCB.MGOD = MGOD /* \_SB_.UBTC.MGOD */
                            \_SB.PC00.LPCB.MGOE = MGOE /* \_SB_.UBTC.MGOE */
                            \_SB.PC00.LPCB.MGOF = MGOF /* \_SB_.UBTC.MGOF */
                            \_SB.PC00.LPCB.CTL0 = CTL0 /* \_SB_.UBTC.CTL0 */
                            \_SB.PC00.LPCB.CTL1 = CTL1 /* \_SB_.UBTC.CTL1 */
                            \_SB.PC00.LPCB.CTL2 = CTL2 /* \_SB_.UBTC.CTL2 */
                            \_SB.PC00.LPCB.CTL3 = CTL3 /* \_SB_.UBTC.CTL3 */
                            \_SB.PC00.LPCB.CTL4 = CTL4 /* \_SB_.UBTC.CTL4 */
                            \_SB.PC00.LPCB.CTL5 = CTL5 /* \_SB_.UBTC.CTL5 */
                            \_SB.PC00.LPCB.CTL6 = CTL6 /* \_SB_.UBTC.CTL6 */
                            \_SB.PC00.LPCB.CTL7 = CTL7 /* \_SB_.UBTC.CTL7 */
                            \_SB.PC00.LPCB.SEC1 (0x18)
                            P8XH (Zero, 0xE0)
                        }
                        Case (0x02)
                        {
                            MGI0 = \_SB.PC00.LPCB.MGI0 /* External reference */
                            MGI1 = \_SB.PC00.LPCB.MGI1 /* External reference */
                            MGI2 = \_SB.PC00.LPCB.MGI2 /* External reference */
                            MGI3 = \_SB.PC00.LPCB.MGI3 /* External reference */
                            MGI4 = \_SB.PC00.LPCB.MGI4 /* External reference */
                            MGI5 = \_SB.PC00.LPCB.MGI5 /* External reference */
                            MGI6 = \_SB.PC00.LPCB.MGI6 /* External reference */
                            MGI7 = \_SB.PC00.LPCB.MGI7 /* External reference */
                            MGI8 = \_SB.PC00.LPCB.MGI8 /* External reference */
                            MGI9 = \_SB.PC00.LPCB.MGI9 /* External reference */
                            MGIA = \_SB.PC00.LPCB.MGIA /* External reference */
                            MGIB = \_SB.PC00.LPCB.MGIB /* External reference */
                            MGIC = \_SB.PC00.LPCB.MGIC /* External reference */
                            MGID = \_SB.PC00.LPCB.MGID /* External reference */
                            MGIE = \_SB.PC00.LPCB.MGIE /* External reference */
                            MGIF = \_SB.PC00.LPCB.MGIF /* External reference */
                            CCI0 = \_SB.PC00.LPCB.CCI0 /* External reference */
                            CCI1 = \_SB.PC00.LPCB.CCI1 /* External reference */
                            CCI2 = \_SB.PC00.LPCB.CCI2 /* External reference */
                            CCI3 = \_SB.PC00.LPCB.CCI3 /* External reference */
                        }
                        Case (0x03)
                        {
                            Return (XDCE) /* External reference */
                        }
                        Case (0x04)
                        {
                            Return (UDRS) /* External reference */
                        }

                    }
                }

                Return (Buffer (One)
                {
                     0x00                                             // .
                })
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00001227 (4647)
 *     Revision         0x02
 *     Checksum         0xBE
 *     OEM ID           "INTEL"
 *     OEM Table ID     "xh_rplsb"
 *     OEM Revision     0x00000000 (0)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "INTEL", "xh_rplsb", 0x00000000)
{
    External (_SB_.BTRK, MethodObj)    // 1 Arguments
    External (_SB_.GGIV, MethodObj)    // 1 Arguments
    External (_SB_.GGOV, MethodObj)    // 1 Arguments
    External (_SB_.PC00.RP08.PXSX.WIST, MethodObj)    // 0 Arguments
    External (_SB_.PC00.XHCI, DeviceObj)
    External (_SB_.PC00.XHCI.POVP, IntObj)
    External (_SB_.PC00.XHCI.PPOE, IntObj)
    External (_SB_.PC00.XHCI.PSG1, IntObj)
    External (_SB_.PC00.XHCI.PSG2, IntObj)
    External (_SB_.PC00.XHCI.RHUB.HS01, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS02, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS03, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS04, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS05, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS06, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS07, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS08, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS09, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS10, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS11, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS12, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS13, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS14, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.HS14.RDLY, UnknownObj)
    External (_SB_.PC00.XHCI.RHUB.SS01, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS02, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS03, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS04, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS05, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS06, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS07, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS08, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS09, DeviceObj)
    External (_SB_.PC00.XHCI.RHUB.SS10, DeviceObj)
    External (_SB_.PC00.XHCI.UCMS, IntObj)
    External (_SB_.SGOV, MethodObj)    // 2 Arguments
    External (_SB_.UBTC.RUCC, MethodObj)    // 2 Arguments
    External (ATDV, UnknownObj)
    External (BED2, UnknownObj)
    External (BED3, UnknownObj)
    External (BTBR, UnknownObj)
    External (BTL2, UnknownObj)
    External (BTLE, UnknownObj)
    External (BTLL, UnknownObj)
    External (BTSE, UnknownObj)
    External (CECV, UnknownObj)
    External (CNMT, UnknownObj)
    External (DTFS, IntObj)
    External (GPRV, MethodObj)    // 2 Arguments
    External (LEDU, UnknownObj)
    External (PPOE, IntObj)
    External (PSON, IntObj)
    External (PU2C, UnknownObj)
    External (PU3C, UnknownObj)
    External (SLEC, UnknownObj)
    External (TAPM, UnknownObj)
    External (TILE, UnknownObj)
    External (TIS0, UnknownObj)
    External (TIS4, UnknownObj)
    External (TIS5, UnknownObj)
    External (TS0X, UnknownObj)

    If ((PPOE != Zero))
    {
        Scope (\_SB.PC00.XHCI)
        {
            Name (PDLV, Zero)
            Name (PDLK, Zero)
            Method (PDDP, 0, Serialized)
            {
            }

            Method (PDBG, 0, Serialized)
            {
                PDDP ()
            }

            Method (PDOF, 0, Serialized)
            {
                PDDP ()
                If ((UCMS == 0x02))
                {
                    If ((\_SB.GGOV (PSG1) != One))
                    {
                        \_SB.SGOV (PSG1, One)
                        Local0 = Zero
                        While ((Local0 < 0x64))
                        {
                            Sleep (0x0A)
                            If ((\_SB.GGIV (PSG2) == One))
                            {
                                Break
                            }
                            Else
                            {
                                Local0++
                            }
                        }
                    }
                    Else
                    {
                    }
                }

                PDDP ()
            }

            Method (PDON, 0, NotSerialized)
            {
                PDDP ()
                If ((UCMS == 0x02))
                {
                    If ((\_SB.GGOV (PSG1) == One))
                    {
                        \_SB.SGOV (PSG1, Zero)
                        Local0 = Zero
                        While ((Local0 < 0x64))
                        {
                            Sleep (0x0A)
                            If ((\_SB.GGIV (PSG2) == Zero))
                            {
                                Break
                            }
                            Else
                            {
                                Local0++
                            }
                        }
                    }
                    Else
                    {
                    }
                }

                PDDP ()
            }

            Method (NCS1, 0, Serialized)
            {
                If ((PSON == One))
                {
                    PDOF ()
                }
                Else
                {
                }

                GPRV (0x02, Zero)
            }

            Method (XCS1, 0, Serialized)
            {
                PDON ()
            }

            Method (PPEN, 0, Serialized)
            {
                PDBG ()
                Switch (ToInteger (PDLV))
                {
                    Case (Zero)
                    {
                        PDLV++
                        If (((PPOE == 0x02) && (\_SB.GGIV (POVP) == Zero)))
                        {
                            GPRV (0x02, One)
                            Break
                        }
                        Else
                        {
                            NCS1 ()
                            Break
                        }
                    }
                    Case (One)
                    {
                        NCS1 ()
                        PDLV++
                        PDLK = One
                        Break
                    }
                    Default
                    {
                        If ((PDLK == Zero)) {}
                    }

                }

                PDBG ()
                Return (PDLV) /* \_SB_.PC00.XHCI.PDLV */
            }

            Method (PPEX, 0, Serialized)
            {
                PDBG ()
                If ((PDLK == One))
                {
                    Return (PDLV) /* \_SB_.PC00.XHCI.PDLV */
                }

                Switch (ToInteger (PDLV))
                {
                    Case (Zero)
                    {
                        Break
                    }
                    Case (One)
                    {
                        XCS1 ()
                        PDLV--
                        Break
                    }
                    Case (0x02)
                    {
                        PDLV--
                        Break
                    }
                    Default
                    {
                        If ((PDLK == Zero)) {}
                    }

                }

                PDBG ()
                Return (PDLV) /* \_SB_.PC00.XHCI.PDLV */
            }

            Method (PSLI, 1, Serialized)
            {
                Switch (ToInteger (Arg0))
                {
                    Case (0x05)
                    {
                        If ((PDLV < One))
                        {
                            PDLV = One
                        }

                        PPEN ()
                    }
                    Case (0x06)
                    {
                        PDLK = Zero
                        PDLV = One
                        PPEX ()
                    }
                    Default
                    {
                    }

                }
            }

            Method (PSLP, 1, Serialized)
            {
                PDLV = One
                PPEN ()
                PDLV = Zero
                PDLK = Zero
            }

            Name (PDSA, One)
            PowerResource (PDPG, 0x00, 0x0000)
            {
                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    Return (PDSA) /* \_SB_.PC00.XHCI.PDSA */
                }

                Method (_ON, 0, NotSerialized)  // _ON_: Power On
                {
                    PPEX ()
                    If ((PDLV == Zero))
                    {
                        PDSA = One
                    }
                }

                Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
                {
                    PPEN ()
                    If ((PDLV == 0x02))
                    {
                        PDSA = Zero
                    }
                }
            }

            Method (_PR0, 0, NotSerialized)  // _PR0: Power Resources for D0
            {
                Return (Package (0x01)
                {
                    PDPG
                })
            }

            Method (_PR3, 0, NotSerialized)  // _PR3: Power Resources for D3hot
            {
                Return (Package (0x01)
                {
                    PDPG
                })
            }
        }
    }

    Method (GPLD, 2, Serialized)
    {
        Name (PCKG, Package (0x01)
        {
            Buffer (0x10) {}
        })
        CreateField (DerefOf (PCKG [Zero]), Zero, 0x07, REV)
        REV = 0x02
        CreateField (DerefOf (PCKG [Zero]), 0x07, One, RGB)
        RGB = One
        CreateField (DerefOf (PCKG [Zero]), 0x40, One, VISI)
        VISI = Arg0
        CreateField (DerefOf (PCKG [Zero]), 0x57, 0x08, GPOS)
        GPOS = Arg1
        Return (PCKG) /* \GPLD.PCKG */
    }

    Method (GUPC, 2, Serialized)
    {
        Name (PCKG, Package (0x04)
        {
            0xFF, 
            0xFF, 
            Zero, 
            Zero
        })
        PCKG [Zero] = Arg0
        PCKG [One] = Arg1
        Return (PCKG) /* \GUPC.PCKG */
    }

    If ((One <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS01)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (\_SB.UBTC.RUCC (0x04, One))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (\_SB.UBTC.RUCC (0x04, 0x02))
            }
        }
    }

    If ((0x02 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS02)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x02))
            }
        }
    }

    If ((0x03 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS03)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, Zero))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x03))
            }
        }
    }

    If ((0x04 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS04)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x04))
            }
        }
    }

    If ((0x05 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS05)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x05))
            }
        }
    }

    If ((0x06 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS06)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (0x03, One))
                    }
                }

                Return (GUPC (One, 0x08))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (0x03, 0x02))
                    }
                }

                Return (GPLD (One, 0x06))
            }
        }
    }

    If ((0x07 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS07)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x07))
            }
        }
    }

    If ((0x08 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS08)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x08))
            }
        }
    }

    If ((0x09 <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS09)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (0x02, One))
                    }
                }

                Return (GUPC (One, 0x08))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (0x02, 0x02))
                    }
                }

                Return (GPLD (One, 0x09))
            }
        }
    }

    If ((0x0A <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS10)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x0A))
            }
        }
    }

    If ((0x0B <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS11)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (One, One))
                    }
                }

                Return (GUPC (One, 0x08))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                If (CondRefOf (\DTFS))
                {
                    If ((DTFS == One))
                    {
                        Return (\_SB.UBTC.RUCC (One, 0x02))
                    }
                }

                Return (GPLD (Zero, 0x0B))
            }
        }
    }

    If ((0x0C <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS12)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x0C))
            }
        }
    }

    If ((0x0D <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS13)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x0D))
            }
        }
    }

    If ((0x0E <= PU2C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.HS14)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x0E))
            }

            If (\_SB.PC00.RP08.PXSX.WIST ())
            {
                Name (SADX, Package (0x03)
                {
                    Zero, 
                    Package (0x02)
                    {
                        0x07, 
                        0x80000000
                    }, 

                    Package (0x02)
                    {
                        0x12, 
                        0x80000000
                    }
                })
                Method (SADS, 0, Serialized)
                {
                    DerefOf (SADX [One]) [One] = \ATDV /* External reference */
                    DerefOf (SADX [0x02]) [One] = \ATDV /* External reference */
                    Return (SADX) /* \_SB_.PC00.XHCI.RHUB.HS14.SADX */
                }

                Name (RDLY, 0x69)
                Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
                {
                    If ((Arg0 == ToUUID ("aa10f4e0-81ac-4233-abf6-3b2ac50e28d9") /* Unknown UUID */))
                    {
                        If ((Arg2 == Zero))
                        {
                            If ((Arg1 == Zero))
                            {
                                Return (Buffer (One)
                                {
                                     0x03                                             // .
                                })
                            }
                            Else
                            {
                                Return (Buffer (One)
                                {
                                     0x00                                             // .
                                })
                            }
                        }

                        If ((Arg2 == One))
                        {
                            RDLY = Arg3
                        }

                        Return (Zero)
                    }
                    ElseIf ((Arg0 == ToUUID ("2d19d3e1-5708-4696-bd5b-2c3dbae2d6a9") /* Unknown UUID */))
                    {
                        If ((Arg2 == Zero))
                        {
                            If ((Arg1 == Zero))
                            {
                                Return (Buffer (One)
                                {
                                     0x03                                             // .
                                })
                            }
                            Else
                            {
                                Return (Buffer (One)
                                {
                                     0x00                                             // .
                                })
                            }
                        }

                        If ((Arg2 == One)) {}
                        Return (Zero)
                    }
                    Else
                    {
                        Return (Buffer (One)
                        {
                             0x00                                             // .
                        })
                    }
                }

                PowerResource (BTRT, 0x05, 0x0000)
                {
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (One)
                    }

                    Method (_ON, 0, NotSerialized)  // _ON_: Power On
                    {
                    }

                    Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
                    {
                    }

                    Method (_RST, 0, NotSerialized)  // _RST: Device Reset
                    {
                        Local0 = Acquire (\CNMT, 0x03E8)
                        If ((Local0 == Zero))
                        {
                            \_SB.BTRK (Zero)
                            Sleep (RDLY)
                            \_SB.BTRK (One)
                            Sleep (RDLY)
                        }

                        Release (\CNMT)
                    }
                }

                Method (_PRR, 0, NotSerialized)  // _PRR: Power Resource for Reset
                {
                    Return (Package (0x01)
                    {
                        BTRT
                    })
                }

                Name (BRDY, Package (0x02)
                {
                    Zero, 
                    Package (0x08)
                    {
                        0x12, 
                        0x80, 
                        0x80, 
                        0x80, 
                        0x80, 
                        0x80, 
                        0x80, 
                        0x80
                    }
                })
                Method (BRDS, 0, Serialized)
                {
                    DerefOf (BRDY [One]) [One] = \BTSE /* External reference */
                    DerefOf (BRDY [One]) [0x02] = \BTBR /* External reference */
                    DerefOf (BRDY [One]) [0x03] = \BED2 /* External reference */
                    DerefOf (BRDY [One]) [0x04] = \BED3 /* External reference */
                    DerefOf (BRDY [One]) [0x05] = \BTLE /* External reference */
                    DerefOf (BRDY [One]) [0x06] = \BTL2 /* External reference */
                    DerefOf (BRDY [One]) [0x07] = \BTLL /* External reference */
                    Return (BRDY) /* \_SB_.PC00.XHCI.RHUB.HS14.BRDY */
                }

                Name (ECKY, Package (0x02)
                {
                    Zero, 
                    Package (0x02)
                    {
                        0x12, 
                        Zero
                    }
                })
                Method (ECKV, 0, Serialized)
                {
                    DerefOf (ECKY [One]) [One] = \CECV /* External reference */
                    Return (ECKY) /* \_SB_.PC00.XHCI.RHUB.HS14.ECKY */
                }

                Name (GPCX, Package (0x03)
                {
                    Zero, 
                    Package (0x02)
                    {
                        0x07, 
                        Package (0x03)
                        {
                            Zero, 
                            Zero, 
                            Zero
                        }
                    }, 

                    Package (0x02)
                    {
                        0x12, 
                        Package (0x03)
                        {
                            Zero, 
                            Zero, 
                            Zero
                        }
                    }
                })
                Method (GPC, 0, Serialized)
                {
                    Return (GPCX) /* \_SB_.PC00.XHCI.RHUB.HS14.GPCX */
                }

                Name (BTLY, Package (0x02)
                {
                    Zero, 
                    Package (0x0A)
                    {
                        0x12, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero
                    }
                })
                Method (BTLC, 0, Serialized)
                {
                    DerefOf (BTLY [One]) [One] = \TILE /* External reference */
                    DerefOf (BTLY [One]) [0x02] = \TIS0 /* External reference */
                    DerefOf (BTLY [One]) [0x03] = \TS0X /* External reference */
                    DerefOf (BTLY [One]) [0x04] = \TIS4 /* External reference */
                    DerefOf (BTLY [One]) [0x05] = \TIS5 /* External reference */
                    DerefOf (BTLY [One]) [0x06] = \SLEC /* External reference */
                    DerefOf (BTLY [One]) [0x07] = \LEDU /* External reference */
                    DerefOf (BTLY [One]) [0x08] = \TAPM /* External reference */
                    Return (BTLY) /* \_SB_.PC00.XHCI.RHUB.HS14.BTLY */
                }
            }
        }
    }

    If ((One <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS01)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x11))
            }
        }
    }

    If ((0x02 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS02)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x0C))
            }
        }
    }

    If ((0x03 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS03)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x08))
            }
        }
    }

    If ((0x04 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS04)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x12))
            }
        }
    }

    If ((0x05 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS05)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x13))
            }
        }
    }

    If ((0x06 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS06)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x14))
            }
        }
    }

    If ((0x07 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS07)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x15))
            }
        }
    }

    If ((0x08 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS08)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (One, 0x03))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (One, 0x04))
            }
        }
    }

    If ((0x09 <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS09)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x16))
            }
        }
    }

    If ((0x0A <= PU3C))
    {
        Scope (\_SB.PC00.XHCI.RHUB.SS10)
        {
            Method (_UPC, 0, NotSerialized)  // _UPC: USB Port Capabilities
            {
                Return (GUPC (Zero, 0xFF))
            }

            Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
            {
                Return (GPLD (Zero, 0x17))
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00003AEA (15082)
 *     Revision         0x02
 *     Checksum         0x32
 *     OEM ID           "SocGpe"
 *     OEM Table ID     "SocGpe "
 *     OEM Revision     0x00003000 (12288)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "SocGpe", "SocGpe ", 0x00003000)
{
    External (_GPE.P0L6, MethodObj)    // 0 Arguments
    External (_GPE.P1L6, MethodObj)    // 0 Arguments
    External (_GPE.P2L6, MethodObj)    // 0 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.CNVW.GPEH, MethodObj)    // 0 Arguments
    External (_SB_.PC00.D3C_, PowerResObj)
    External (_SB_.PC00.D3C_._ON_, MethodObj)    // 0 Arguments
    External (_SB_.PC00.D3C_._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00.GFX0, DeviceObj)
    External (_SB_.PC00.GFX0.GSCI, MethodObj)    // 0 Arguments
    External (_SB_.PC00.GFX0.GSSE, FieldUnitObj)
    External (_SB_.PC00.GLAN.GPEH, MethodObj)    // 0 Arguments
    External (_SB_.PC00.HDAS.GPEH, MethodObj)    // 0 Arguments
    External (_SB_.PC00.MC__, DeviceObj)
    External (_SB_.PC00.MC__.D1F0, FieldUnitObj)
    External (_SB_.PC00.MC__.D1F1, FieldUnitObj)
    External (_SB_.PC00.MC__.D6F0, FieldUnitObj)
    External (_SB_.PC00.PEG0, DeviceObj)
    External (_SB_.PC00.PEG0.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG0.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG0.PEGP, DeviceObj)
    External (_SB_.PC00.PEG1, DeviceObj)
    External (_SB_.PC00.PEG1.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG1.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG2, DeviceObj)
    External (_SB_.PC00.PEG2.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.PEG2.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP01, DeviceObj)
    External (_SB_.PC00.RP01.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP01.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP01.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP01.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP01.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP01.VDID, FieldUnitObj)
    External (_SB_.PC00.RP02, DeviceObj)
    External (_SB_.PC00.RP02.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP02.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP02.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP02.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP02.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP02.VDID, FieldUnitObj)
    External (_SB_.PC00.RP03, DeviceObj)
    External (_SB_.PC00.RP03.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP03.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP03.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP03.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP03.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP03.VDID, FieldUnitObj)
    External (_SB_.PC00.RP04, DeviceObj)
    External (_SB_.PC00.RP04.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP04.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP04.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP04.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP04.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP04.VDID, FieldUnitObj)
    External (_SB_.PC00.RP05, DeviceObj)
    External (_SB_.PC00.RP05.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP05.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP05.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP05.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP05.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP05.VDID, FieldUnitObj)
    External (_SB_.PC00.RP06, DeviceObj)
    External (_SB_.PC00.RP06.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP06.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP06.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP06.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP06.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP06.VDID, FieldUnitObj)
    External (_SB_.PC00.RP07, DeviceObj)
    External (_SB_.PC00.RP07.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP07.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP07.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP07.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP07.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP07.VDID, FieldUnitObj)
    External (_SB_.PC00.RP08, DeviceObj)
    External (_SB_.PC00.RP08.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP08.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP08.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP08.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP08.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP08.VDID, FieldUnitObj)
    External (_SB_.PC00.RP09, DeviceObj)
    External (_SB_.PC00.RP09.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP09.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP09.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP09.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP09.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP09.VDID, FieldUnitObj)
    External (_SB_.PC00.RP10, DeviceObj)
    External (_SB_.PC00.RP10.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP10.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP10.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP10.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP10.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP10.VDID, FieldUnitObj)
    External (_SB_.PC00.RP11, DeviceObj)
    External (_SB_.PC00.RP11.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP11.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP11.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP11.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP11.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP11.VDID, FieldUnitObj)
    External (_SB_.PC00.RP12, DeviceObj)
    External (_SB_.PC00.RP12.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP12.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP12.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP12.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP12.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP12.VDID, FieldUnitObj)
    External (_SB_.PC00.RP13, DeviceObj)
    External (_SB_.PC00.RP13.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP13.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP13.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP13.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP13.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP13.VDID, FieldUnitObj)
    External (_SB_.PC00.RP14, DeviceObj)
    External (_SB_.PC00.RP14.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP14.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP14.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP14.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP14.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP14.VDID, FieldUnitObj)
    External (_SB_.PC00.RP15, DeviceObj)
    External (_SB_.PC00.RP15.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP15.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP15.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP15.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP15.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP15.VDID, FieldUnitObj)
    External (_SB_.PC00.RP16, DeviceObj)
    External (_SB_.PC00.RP16.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP16.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP16.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP16.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP16.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP16.VDID, FieldUnitObj)
    External (_SB_.PC00.RP17, DeviceObj)
    External (_SB_.PC00.RP17.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP17.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP17.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP17.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP17.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP17.VDID, FieldUnitObj)
    External (_SB_.PC00.RP18, DeviceObj)
    External (_SB_.PC00.RP18.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP18.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP18.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP18.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP18.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP18.VDID, FieldUnitObj)
    External (_SB_.PC00.RP19, DeviceObj)
    External (_SB_.PC00.RP19.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP19.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP19.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP19.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP19.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP19.VDID, FieldUnitObj)
    External (_SB_.PC00.RP20, DeviceObj)
    External (_SB_.PC00.RP20.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP20.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP20.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP20.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP20.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP20.VDID, FieldUnitObj)
    External (_SB_.PC00.RP21, DeviceObj)
    External (_SB_.PC00.RP21.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP21.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP21.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP21.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP21.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP21.VDID, FieldUnitObj)
    External (_SB_.PC00.RP22, DeviceObj)
    External (_SB_.PC00.RP22.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP22.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP22.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP22.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP22.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP22.VDID, FieldUnitObj)
    External (_SB_.PC00.RP23, DeviceObj)
    External (_SB_.PC00.RP23.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP23.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP23.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP23.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP23.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP23.VDID, FieldUnitObj)
    External (_SB_.PC00.RP24, DeviceObj)
    External (_SB_.PC00.RP24.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP24.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP24.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP24.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP24.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP24.VDID, FieldUnitObj)
    External (_SB_.PC00.RP25, DeviceObj)
    External (_SB_.PC00.RP25.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP25.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP25.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP25.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP25.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP25.VDID, FieldUnitObj)
    External (_SB_.PC00.RP26, DeviceObj)
    External (_SB_.PC00.RP26.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP26.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP26.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP26.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP26.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP26.VDID, FieldUnitObj)
    External (_SB_.PC00.RP27, DeviceObj)
    External (_SB_.PC00.RP27.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP27.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP27.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP27.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP27.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP27.VDID, FieldUnitObj)
    External (_SB_.PC00.RP28, DeviceObj)
    External (_SB_.PC00.RP28.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.RP28.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP28.L0SE, FieldUnitObj)
    External (_SB_.PC00.RP28.PDCX, FieldUnitObj)
    External (_SB_.PC00.RP28.PDSX, FieldUnitObj)
    External (_SB_.PC00.RP28.VDID, FieldUnitObj)
    External (_SB_.PC00.TBT0, PowerResObj)
    External (_SB_.PC00.TBT0._OFF, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TBT0._ON_, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TBT1, PowerResObj)
    External (_SB_.PC00.TBT1._OFF, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TBT1._ON_, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TDM0, DeviceObj)
    External (_SB_.PC00.TDM0._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TDM0.MEMS, FieldUnitObj)
    External (_SB_.PC00.TDM0.PMES, FieldUnitObj)
    External (_SB_.PC00.TDM0.PMST, FieldUnitObj)
    External (_SB_.PC00.TDM0.STAT, IntObj)
    External (_SB_.PC00.TDM1, DeviceObj)
    External (_SB_.PC00.TDM1._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TDM1.MEMS, FieldUnitObj)
    External (_SB_.PC00.TDM1.PMES, FieldUnitObj)
    External (_SB_.PC00.TDM1.PMST, FieldUnitObj)
    External (_SB_.PC00.TDM1.STAT, IntObj)
    External (_SB_.PC00.TRP0, DeviceObj)
    External (_SB_.PC00.TRP0.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP0.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP1, DeviceObj)
    External (_SB_.PC00.TRP1.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP1.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP2, DeviceObj)
    External (_SB_.PC00.TRP2.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP2.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP3, DeviceObj)
    External (_SB_.PC00.TRP3.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP3.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TXDC, UnknownObj)
    External (_SB_.PC00.TXDC._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TXDC.D0I3, FieldUnitObj)
    External (_SB_.PC00.TXDC.PMES, FieldUnitObj)
    External (_SB_.PC00.TXHC, DeviceObj)
    External (_SB_.PC00.TXHC._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TXHC.D0D3, FieldUnitObj)
    External (_SB_.PC00.TXHC.PMES, FieldUnitObj)
    External (_SB_.PC00.XDCI.GPEH, MethodObj)    // 0 Arguments
    External (_SB_.PC00.XHCI.GPEH, MethodObj)    // 0 Arguments
    External (_SB_.PC01, DeviceObj)
    External (_SB_.PC01.TRP0, DeviceObj)
    External (_SB_.PC01.TRP0.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP0.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP1, DeviceObj)
    External (_SB_.PC01.TRP1.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP1.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP2, DeviceObj)
    External (_SB_.PC01.TRP2.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP2.HPME, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP3, DeviceObj)
    External (_SB_.PC01.TRP3.HPEV, MethodObj)    // 0 Arguments
    External (_SB_.PC01.TRP3.HPME, MethodObj)    // 0 Arguments
    External (AL6D, FieldUnitObj)
    External (CPRT, FieldUnitObj)
    External (GSMI, FieldUnitObj)
    External (P1GP, FieldUnitObj)
    External (P2GP, FieldUnitObj)
    External (PCHS, FieldUnitObj)
    External (PCHX, IntObj)
    External (PG0E, FieldUnitObj)
    External (PG1E, FieldUnitObj)
    External (PG2E, FieldUnitObj)
    External (SGGP, FieldUnitObj)

    Scope (\_GPE)
    {
        Method (SL61, 0, NotSerialized)
        {
            If ((AL6D == One))
            {
                If (CondRefOf (\_SB.PC00.TXHC))
                {
                    If (CondRefOf (\_SB.PC01))
                    {
                        \_SB.PC01.TRP0.HPEV ()
                        \_SB.PC01.TRP1.HPEV ()
                        \_SB.PC01.TRP2.HPEV ()
                        \_SB.PC01.TRP3.HPEV ()
                    }
                    Else
                    {
                        \_SB.PC00.TRP0.HPEV ()
                        \_SB.PC00.TRP1.HPEV ()
                        \_SB.PC00.TRP2.HPEV ()
                        \_SB.PC00.TRP3.HPEV ()
                    }
                }
            }

            Sleep (0x64)
            If (CondRefOf (\_SB.PC00.TXHC))
            {
                If (CondRefOf (\_SB.PC01))
                {
                    \_SB.PC01.TRP0.HPEV ()
                    \_SB.PC01.TRP1.HPEV ()
                    \_SB.PC01.TRP2.HPEV ()
                    \_SB.PC01.TRP3.HPEV ()
                }
                Else
                {
                    \_SB.PC00.TRP0.HPEV ()
                    \_SB.PC00.TRP1.HPEV ()
                    \_SB.PC00.TRP2.HPEV ()
                    \_SB.PC00.TRP3.HPEV ()
                }
            }

            If ((PG0E == One))
            {
                \_SB.PC00.PEG0.HPEV ()
            }

            If ((PG1E == One))
            {
                \_SB.PC00.PEG1.HPEV ()
            }

            If ((PG2E == One))
            {
                \_SB.PC00.PEG2.HPEV ()
            }

            If (CondRefOf (\_SB.PC00.RP01))
            {
                If (((\_SB.PC00.RP01.VDID != 0xFFFFFFFF) && \_SB.PC00.RP01.HPSX))
                {
                    If (\_SB.PC00.RP01.PDCX)
                    {
                        \_SB.PC00.RP01.PDCX = One
                        \_SB.PC00.RP01.HPSX = One
                        If (!\_SB.PC00.RP01.PDSX)
                        {
                            \_SB.PC00.RP01.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP01, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP01.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP02))
            {
                If (((\_SB.PC00.RP02.VDID != 0xFFFFFFFF) && \_SB.PC00.RP02.HPSX))
                {
                    If (\_SB.PC00.RP02.PDCX)
                    {
                        \_SB.PC00.RP02.PDCX = One
                        \_SB.PC00.RP02.HPSX = One
                        If (!\_SB.PC00.RP02.PDSX)
                        {
                            \_SB.PC00.RP02.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP02, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP02.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP03))
            {
                If (((\_SB.PC00.RP03.VDID != 0xFFFFFFFF) && \_SB.PC00.RP03.HPSX))
                {
                    If (\_SB.PC00.RP03.PDCX)
                    {
                        \_SB.PC00.RP03.PDCX = One
                        \_SB.PC00.RP03.HPSX = One
                        If (!\_SB.PC00.RP03.PDSX)
                        {
                            \_SB.PC00.RP03.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP03, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP03.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP04))
            {
                If (((\_SB.PC00.RP04.VDID != 0xFFFFFFFF) && \_SB.PC00.RP04.HPSX))
                {
                    If (\_SB.PC00.RP04.PDCX)
                    {
                        \_SB.PC00.RP04.PDCX = One
                        \_SB.PC00.RP04.HPSX = One
                        If (!\_SB.PC00.RP04.PDSX)
                        {
                            \_SB.PC00.RP04.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP04, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP04.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP05))
            {
                If (((\_SB.PC00.RP05.VDID != 0xFFFFFFFF) && \_SB.PC00.RP05.HPSX))
                {
                    If (\_SB.PC00.RP05.PDCX)
                    {
                        \_SB.PC00.RP05.PDCX = One
                        \_SB.PC00.RP05.HPSX = One
                        If (!\_SB.PC00.RP05.PDSX)
                        {
                            \_SB.PC00.RP05.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP05, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP05.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP06))
            {
                If (((\_SB.PC00.RP06.VDID != 0xFFFFFFFF) && \_SB.PC00.RP06.HPSX))
                {
                    If (\_SB.PC00.RP06.PDCX)
                    {
                        \_SB.PC00.RP06.PDCX = One
                        \_SB.PC00.RP06.HPSX = One
                        If (!\_SB.PC00.RP06.PDSX)
                        {
                            \_SB.PC00.RP06.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP06, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP06.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP07))
            {
                If (((\_SB.PC00.RP07.VDID != 0xFFFFFFFF) && \_SB.PC00.RP07.HPSX))
                {
                    If (\_SB.PC00.RP07.PDCX)
                    {
                        \_SB.PC00.RP07.PDCX = One
                        \_SB.PC00.RP07.HPSX = One
                        If (!\_SB.PC00.RP07.PDSX)
                        {
                            \_SB.PC00.RP07.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP07, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP07.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP08))
            {
                If (((\_SB.PC00.RP08.VDID != 0xFFFFFFFF) && \_SB.PC00.RP08.HPSX))
                {
                    If (\_SB.PC00.RP08.PDCX)
                    {
                        \_SB.PC00.RP08.PDCX = One
                        \_SB.PC00.RP08.HPSX = One
                        If (!\_SB.PC00.RP08.PDSX)
                        {
                            \_SB.PC00.RP08.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP08, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP08.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP09))
            {
                If (((\_SB.PC00.RP09.VDID != 0xFFFFFFFF) && \_SB.PC00.RP09.HPSX))
                {
                    If (\_SB.PC00.RP09.PDCX)
                    {
                        \_SB.PC00.RP09.PDCX = One
                        \_SB.PC00.RP09.HPSX = One
                        If (!\_SB.PC00.RP09.PDSX)
                        {
                            \_SB.PC00.RP09.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP09, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP09.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP10))
            {
                If (((\_SB.PC00.RP10.VDID != 0xFFFFFFFF) && \_SB.PC00.RP10.HPSX))
                {
                    If (\_SB.PC00.RP10.PDCX)
                    {
                        \_SB.PC00.RP10.PDCX = One
                        \_SB.PC00.RP10.HPSX = One
                        If (!\_SB.PC00.RP10.PDSX)
                        {
                            \_SB.PC00.RP10.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP10, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP10.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP11))
            {
                If (((\_SB.PC00.RP11.VDID != 0xFFFFFFFF) && \_SB.PC00.RP11.HPSX))
                {
                    If (\_SB.PC00.RP11.PDCX)
                    {
                        \_SB.PC00.RP11.PDCX = One
                        \_SB.PC00.RP11.HPSX = One
                        If (!\_SB.PC00.RP11.PDSX)
                        {
                            \_SB.PC00.RP11.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP11, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP11.HPSX = One
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.RP12))
            {
                If (((\_SB.PC00.RP12.VDID != 0xFFFFFFFF) && \_SB.PC00.RP12.HPSX))
                {
                    If (\_SB.PC00.RP12.PDCX)
                    {
                        \_SB.PC00.RP12.PDCX = One
                        \_SB.PC00.RP12.HPSX = One
                        If (!\_SB.PC00.RP12.PDSX)
                        {
                            \_SB.PC00.RP12.L0SE = Zero
                        }

                        Notify (\_SB.PC00.RP12, Zero) // Bus Check
                    }
                    Else
                    {
                        \_SB.PC00.RP12.HPSX = One
                    }
                }
            }

            If ((PCHS == PCHX))
            {
                If (CondRefOf (\_SB.PC00.RP13))
                {
                    If (((\_SB.PC00.RP13.VDID != 0xFFFFFFFF) && \_SB.PC00.RP13.HPSX))
                    {
                        If (\_SB.PC00.RP13.PDCX)
                        {
                            \_SB.PC00.RP13.PDCX = One
                            \_SB.PC00.RP13.HPSX = One
                            If (!\_SB.PC00.RP13.PDSX)
                            {
                                \_SB.PC00.RP13.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP13, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP13.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP14))
                {
                    If (((\_SB.PC00.RP14.VDID != 0xFFFFFFFF) && \_SB.PC00.RP14.HPSX))
                    {
                        If (\_SB.PC00.RP14.PDCX)
                        {
                            \_SB.PC00.RP14.PDCX = One
                            \_SB.PC00.RP14.HPSX = One
                            If (!\_SB.PC00.RP14.PDSX)
                            {
                                \_SB.PC00.RP14.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP14, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP14.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP15))
                {
                    If (((\_SB.PC00.RP15.VDID != 0xFFFFFFFF) && \_SB.PC00.RP15.HPSX))
                    {
                        If (\_SB.PC00.RP15.PDCX)
                        {
                            \_SB.PC00.RP15.PDCX = One
                            \_SB.PC00.RP15.HPSX = One
                            If (!\_SB.PC00.RP15.PDSX)
                            {
                                \_SB.PC00.RP15.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP15, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP15.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP16))
                {
                    If (((\_SB.PC00.RP16.VDID != 0xFFFFFFFF) && \_SB.PC00.RP16.HPSX))
                    {
                        If (\_SB.PC00.RP16.PDCX)
                        {
                            \_SB.PC00.RP16.PDCX = One
                            \_SB.PC00.RP16.HPSX = One
                            If (!\_SB.PC00.RP16.PDSX)
                            {
                                \_SB.PC00.RP16.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP16, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP16.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP17))
                {
                    If (((\_SB.PC00.RP17.VDID != 0xFFFFFFFF) && \_SB.PC00.RP17.HPSX))
                    {
                        If (\_SB.PC00.RP17.PDCX)
                        {
                            \_SB.PC00.RP17.PDCX = One
                            \_SB.PC00.RP17.HPSX = One
                            If (!\_SB.PC00.RP17.PDSX)
                            {
                                \_SB.PC00.RP17.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP17, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP17.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP18))
                {
                    If (((\_SB.PC00.RP18.VDID != 0xFFFFFFFF) && \_SB.PC00.RP18.HPSX))
                    {
                        If (\_SB.PC00.RP18.PDCX)
                        {
                            \_SB.PC00.RP18.PDCX = One
                            \_SB.PC00.RP18.HPSX = One
                            If (!\_SB.PC00.RP18.PDSX)
                            {
                                \_SB.PC00.RP18.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP18, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP18.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP19))
                {
                    If (((\_SB.PC00.RP19.VDID != 0xFFFFFFFF) && \_SB.PC00.RP19.HPSX))
                    {
                        If (\_SB.PC00.RP19.PDCX)
                        {
                            \_SB.PC00.RP19.PDCX = One
                            \_SB.PC00.RP19.HPSX = One
                            If (!\_SB.PC00.RP19.PDSX)
                            {
                                \_SB.PC00.RP19.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP19, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP19.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP20))
                {
                    If (((\_SB.PC00.RP20.VDID != 0xFFFFFFFF) && \_SB.PC00.RP20.HPSX))
                    {
                        If (\_SB.PC00.RP20.PDCX)
                        {
                            \_SB.PC00.RP20.PDCX = One
                            \_SB.PC00.RP20.HPSX = One
                            If (!\_SB.PC00.RP20.PDSX)
                            {
                                \_SB.PC00.RP20.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP20, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP20.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP21))
                {
                    If (((\_SB.PC00.RP21.VDID != 0xFFFFFFFF) && \_SB.PC00.RP21.HPSX))
                    {
                        If (\_SB.PC00.RP21.PDCX)
                        {
                            \_SB.PC00.RP21.PDCX = One
                            \_SB.PC00.RP21.HPSX = One
                            If (!\_SB.PC00.RP21.PDSX)
                            {
                                \_SB.PC00.RP21.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP21, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP21.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP22))
                {
                    If (((\_SB.PC00.RP22.VDID != 0xFFFFFFFF) && \_SB.PC00.RP22.HPSX))
                    {
                        If (\_SB.PC00.RP22.PDCX)
                        {
                            \_SB.PC00.RP22.PDCX = One
                            \_SB.PC00.RP22.HPSX = One
                            If (!\_SB.PC00.RP22.PDSX)
                            {
                                \_SB.PC00.RP22.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP22, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP22.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP23))
                {
                    If (((\_SB.PC00.RP23.VDID != 0xFFFFFFFF) && \_SB.PC00.RP23.HPSX))
                    {
                        If (\_SB.PC00.RP23.PDCX)
                        {
                            \_SB.PC00.RP23.PDCX = One
                            \_SB.PC00.RP23.HPSX = One
                            If (!\_SB.PC00.RP23.PDSX)
                            {
                                \_SB.PC00.RP23.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP23, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP23.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP24))
                {
                    If (((\_SB.PC00.RP24.VDID != 0xFFFFFFFF) && \_SB.PC00.RP24.HPSX))
                    {
                        If (\_SB.PC00.RP24.PDCX)
                        {
                            \_SB.PC00.RP24.PDCX = One
                            \_SB.PC00.RP24.HPSX = One
                            If (!\_SB.PC00.RP24.PDSX)
                            {
                                \_SB.PC00.RP24.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP24, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP24.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP25))
                {
                    If (((\_SB.PC00.RP25.VDID != 0xFFFFFFFF) && \_SB.PC00.RP25.HPSX))
                    {
                        If (\_SB.PC00.RP25.PDCX)
                        {
                            \_SB.PC00.RP25.PDCX = One
                            \_SB.PC00.RP25.HPSX = One
                            If (!\_SB.PC00.RP25.PDSX)
                            {
                                \_SB.PC00.RP25.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP25, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP25.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP26))
                {
                    If (((\_SB.PC00.RP26.VDID != 0xFFFFFFFF) && \_SB.PC00.RP26.HPSX))
                    {
                        If (\_SB.PC00.RP26.PDCX)
                        {
                            \_SB.PC00.RP26.PDCX = One
                            \_SB.PC00.RP26.HPSX = One
                            If (!\_SB.PC00.RP26.PDSX)
                            {
                                \_SB.PC00.RP26.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP26, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP26.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP27))
                {
                    If (((\_SB.PC00.RP27.VDID != 0xFFFFFFFF) && \_SB.PC00.RP27.HPSX))
                    {
                        If (\_SB.PC00.RP27.PDCX)
                        {
                            \_SB.PC00.RP27.PDCX = One
                            \_SB.PC00.RP27.HPSX = One
                            If (!\_SB.PC00.RP27.PDSX)
                            {
                                \_SB.PC00.RP27.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP27, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP27.HPSX = One
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.RP28))
                {
                    If (((\_SB.PC00.RP28.VDID != 0xFFFFFFFF) && \_SB.PC00.RP28.HPSX))
                    {
                        If (\_SB.PC00.RP28.PDCX)
                        {
                            \_SB.PC00.RP28.PDCX = One
                            \_SB.PC00.RP28.HPSX = One
                            If (!\_SB.PC00.RP28.PDSX)
                            {
                                \_SB.PC00.RP28.L0SE = Zero
                            }

                            Notify (\_SB.PC00.RP28, Zero) // Bus Check
                        }
                        Else
                        {
                            \_SB.PC00.RP28.HPSX = One
                        }
                    }
                }
            }

            If ((AL6D == One))
            {
                If (CondRefOf (\_SB.PC00.TXHC))
                {
                    If (CondRefOf (\_SB.PC01))
                    {
                        \_SB.PC01.TRP0.HPEV ()
                        \_SB.PC01.TRP1.HPEV ()
                        \_SB.PC01.TRP2.HPEV ()
                        \_SB.PC01.TRP3.HPEV ()
                    }
                    Else
                    {
                        \_SB.PC00.TRP0.HPEV ()
                        \_SB.PC00.TRP1.HPEV ()
                        \_SB.PC00.TRP2.HPEV ()
                        \_SB.PC00.TRP3.HPEV ()
                    }
                }
            }
        }

        Method (SL66, 0, NotSerialized)
        {
            If (CondRefOf (\_SB.PC00.GFX0))
            {
                If ((\_SB.PC00.GFX0.GSSE && !GSMI))
                {
                    \_SB.PC00.GFX0.GSCI ()
                }
            }
        }

        Method (SL69, 0, NotSerialized)
        {
            If (CondRefOf (\_SB.PC00.TXHC))
            {
                If (CondRefOf (\_SB.PC01))
                {
                    If ((\_SB.PC01.TRP0.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM0, 0x02) // Device Wake
                        Notify (\_SB.PC01.TRP0, 0x02) // Device Wake
                    }

                    If ((\_SB.PC01.TRP1.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM0, 0x02) // Device Wake
                        Notify (\_SB.PC01.TRP1, 0x02) // Device Wake
                    }

                    If ((\_SB.PC01.TRP2.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM1, 0x02) // Device Wake
                        Notify (\_SB.PC01.TRP2, 0x02) // Device Wake
                    }

                    If ((\_SB.PC01.TRP3.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM1, 0x02) // Device Wake
                        Notify (\_SB.PC01.TRP3, 0x02) // Device Wake
                    }
                }
                Else
                {
                    If ((\_SB.PC00.TRP0.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM0, 0x02) // Device Wake
                        Notify (\_SB.PC00.TRP0, 0x02) // Device Wake
                    }

                    If ((\_SB.PC00.TRP1.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM0, 0x02) // Device Wake
                        Notify (\_SB.PC00.TRP1, 0x02) // Device Wake
                    }

                    If ((\_SB.PC00.TRP2.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM1, 0x02) // Device Wake
                        Notify (\_SB.PC00.TRP2, 0x02) // Device Wake
                    }

                    If ((\_SB.PC00.TRP3.HPME () == One))
                    {
                        Notify (\_SB.PC00.TDM1, 0x02) // Device Wake
                        Notify (\_SB.PC00.TRP3, 0x02) // Device Wake
                    }
                }
            }

            \_SB.PC00.RP01.HPME ()
            \_SB.PC00.RP02.HPME ()
            \_SB.PC00.RP03.HPME ()
            \_SB.PC00.RP04.HPME ()
            \_SB.PC00.RP05.HPME ()
            \_SB.PC00.RP06.HPME ()
            \_SB.PC00.RP07.HPME ()
            \_SB.PC00.RP08.HPME ()
            \_SB.PC00.RP09.HPME ()
            \_SB.PC00.RP10.HPME ()
            \_SB.PC00.RP11.HPME ()
            \_SB.PC00.RP12.HPME ()
            If ((PCHS == PCHX))
            {
                \_SB.PC00.RP13.HPME ()
                \_SB.PC00.RP14.HPME ()
                \_SB.PC00.RP15.HPME ()
                \_SB.PC00.RP16.HPME ()
                \_SB.PC00.RP17.HPME ()
                \_SB.PC00.RP18.HPME ()
                \_SB.PC00.RP19.HPME ()
                \_SB.PC00.RP20.HPME ()
                \_SB.PC00.RP21.HPME ()
                \_SB.PC00.RP22.HPME ()
                \_SB.PC00.RP23.HPME ()
                \_SB.PC00.RP24.HPME ()
                \_SB.PC00.RP25.HPME ()
                \_SB.PC00.RP26.HPME ()
                \_SB.PC00.RP27.HPME ()
                \_SB.PC00.RP28.HPME ()
            }

            If ((\_SB.PC00.MC.D6F0 == One))
            {
                \_SB.PC00.PEG0.HPME ()
                Notify (\_SB.PC00.PEG0, 0x02) // Device Wake
                Notify (\_SB.PC00.PEG0.PEGP, 0x02) // Device Wake
            }

            If ((\_SB.PC00.MC.D1F0 == One))
            {
                \_SB.PC00.PEG1.HPME ()
                Notify (\_SB.PC00.PEG1, 0x02) // Device Wake
            }

            If ((\_SB.PC00.MC.D1F1 == One))
            {
                \_SB.PC00.PEG2.HPME ()
                Notify (\_SB.PC00.PEG2, 0x02) // Device Wake
            }
        }

        If ((AL6D == One))
        {
            Method (SL6D, 0, NotSerialized)
            {
                If (CondRefOf (\_SB.PC00.XHCI))
                {
                    \_SB.PC00.XHCI.GPEH ()
                }

                If (CondRefOf (\_SB.PC00.HDAS))
                {
                    \_SB.PC00.HDAS.GPEH ()
                }

                If (CondRefOf (\_SB.PC00.GLAN))
                {
                    \_SB.PC00.GLAN.GPEH ()
                }

                If (CondRefOf (\_SB.PC00.CNVW))
                {
                    \_SB.PC00.CNVW.GPEH ()
                }

                If (CondRefOf (\_SB.PC00.XDCI))
                {
                    \_SB.PC00.XDCI.GPEH ()
                }

                If (CondRefOf (\_SB.PC00.D3C))
                {
                    If ((\_SB.PC00.D3C._STA () == Zero))
                    {
                        \_SB.PC00.D3C._ON ()
                    }
                }

                If (CondRefOf (\_SB.PC00.TDM0))
                {
                    If ((\_SB.PC00.TDM0._STA () == 0x0F))
                    {
                        Local0 = Zero
                        If ((\_SB.PC00.TDM0.STAT == Zero))
                        {
                            \_SB.PC00.TBT0._ON ()
                            Local0 = One
                        }

                        Local1 = \_SB.PC00.TDM0.PMES /* External reference */
                        If ((Local1 == Zero))
                        {
                            If ((Local0 == One))
                            {
                                If ((\_SB.PC00.TDM0.PMST == 0x03))
                                {
                                    If ((\_SB.PC00.TDM0.MEMS == Zero))
                                    {
                                        If ((\_SB.PC00.TDM0.STAT == One))
                                        {
                                            \_SB.PC00.TBT0._OFF ()
                                        }
                                    }
                                }
                            }
                        }
                        Else
                        {
                            Notify (\_SB.PC00.TDM0, 0x02) // Device Wake
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.TDM1))
                {
                    If ((\_SB.PC00.TDM1._STA () == 0x0F))
                    {
                        Local0 = Zero
                        If ((\_SB.PC00.TDM1.STAT == Zero))
                        {
                            \_SB.PC00.TBT1._ON ()
                            Local0 = One
                        }

                        Local1 = \_SB.PC00.TDM1.PMES /* External reference */
                        If ((Local1 == Zero))
                        {
                            If ((Local0 == One))
                            {
                                If ((\_SB.PC00.TDM1.PMST == 0x03))
                                {
                                    If ((\_SB.PC00.TDM1.MEMS == Zero))
                                    {
                                        If ((\_SB.PC00.TDM1.STAT == One))
                                        {
                                            \_SB.PC00.TBT1._OFF ()
                                        }
                                    }
                                }
                            }
                        }
                        Else
                        {
                            Notify (\_SB.PC00.TDM1, 0x02) // Device Wake
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.TXDC))
                {
                    If ((\_SB.PC00.TXDC._STA () == 0x0F))
                    {
                        Local1 = \_SB.PC00.TXDC.PMES /* External reference */
                        If ((Local1 == One))
                        {
                            Notify (\_SB.PC00.TXDC, 0x02) // Device Wake
                        }
                    }
                }

                If (CondRefOf (\_SB.PC00.TXHC))
                {
                    If ((\_SB.PC00.TXHC._STA () == 0x0F))
                    {
                        Notify (\_SB.PC00.TXHC, 0x02) // Device Wake
                    }
                }
            }
        }

        Method (SL6F, 0, NotSerialized)
        {
            If ((SGGP == One))
            {
                If (CondRefOf (\_GPE.P0L6))
                {
                    \_GPE.P0L6 ()
                }
            }

            If ((P1GP == One))
            {
                If (CondRefOf (\_GPE.P1L6))
                {
                    \_GPE.P1L6 ()
                }
            }

            If ((P2GP == One))
            {
                If (CondRefOf (\_GPE.P2L6))
                {
                    \_GPE.P2L6 ()
                }
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x000039DA (14810)
 *     Revision         0x02
 *     Checksum         0xA2
 *     OEM ID           "SocCmn"
 *     OEM Table ID     "SocCmn "
 *     OEM Revision     0x00003000 (12288)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "SocCmn", "SocCmn ", 0x00003000)
{
    External (_SB_.CPPC, FieldUnitObj)
    External (_SB_.PC00.RP01, DeviceObj)
    External (_SB_.PC00.RP01.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP01.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP01.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP01.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP01.VDID, FieldUnitObj)
    External (_SB_.PC00.RP02, DeviceObj)
    External (_SB_.PC00.RP02.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP02.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP02.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP02.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP02.VDID, FieldUnitObj)
    External (_SB_.PC00.RP03, DeviceObj)
    External (_SB_.PC00.RP03.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP03.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP03.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP03.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP03.VDID, FieldUnitObj)
    External (_SB_.PC00.RP04, DeviceObj)
    External (_SB_.PC00.RP04.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP04.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP04.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP04.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP04.VDID, FieldUnitObj)
    External (_SB_.PC00.RP05, DeviceObj)
    External (_SB_.PC00.RP05.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP05.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP05.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP05.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP05.VDID, FieldUnitObj)
    External (_SB_.PC00.RP06, DeviceObj)
    External (_SB_.PC00.RP06.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP06.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP06.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP06.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP06.VDID, FieldUnitObj)
    External (_SB_.PC00.RP07, DeviceObj)
    External (_SB_.PC00.RP07.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP07.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP07.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP07.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP07.VDID, FieldUnitObj)
    External (_SB_.PC00.RP08, DeviceObj)
    External (_SB_.PC00.RP08.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP08.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP08.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP08.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP08.VDID, FieldUnitObj)
    External (_SB_.PC00.RP09, DeviceObj)
    External (_SB_.PC00.RP09.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP09.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP09.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP09.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP09.VDID, FieldUnitObj)
    External (_SB_.PC00.RP10, DeviceObj)
    External (_SB_.PC00.RP10.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP10.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP10.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP10.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP10.VDID, FieldUnitObj)
    External (_SB_.PC00.RP11, DeviceObj)
    External (_SB_.PC00.RP11.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP11.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP11.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP11.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP11.VDID, FieldUnitObj)
    External (_SB_.PC00.RP12, DeviceObj)
    External (_SB_.PC00.RP12.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP12.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP12.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP12.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP12.VDID, FieldUnitObj)
    External (_SB_.PC00.RP13, DeviceObj)
    External (_SB_.PC00.RP13.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP13.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP13.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP13.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP13.VDID, FieldUnitObj)
    External (_SB_.PC00.RP14, DeviceObj)
    External (_SB_.PC00.RP14.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP14.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP14.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP14.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP14.VDID, FieldUnitObj)
    External (_SB_.PC00.RP15, DeviceObj)
    External (_SB_.PC00.RP15.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP15.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP15.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP15.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP15.VDID, FieldUnitObj)
    External (_SB_.PC00.RP16, DeviceObj)
    External (_SB_.PC00.RP16.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP16.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP16.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP16.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP16.VDID, FieldUnitObj)
    External (_SB_.PC00.RP17, DeviceObj)
    External (_SB_.PC00.RP17.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP17.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP17.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP17.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP17.VDID, FieldUnitObj)
    External (_SB_.PC00.RP18, DeviceObj)
    External (_SB_.PC00.RP18.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP18.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP18.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP18.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP18.VDID, FieldUnitObj)
    External (_SB_.PC00.RP19, DeviceObj)
    External (_SB_.PC00.RP19.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP19.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP19.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP19.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP19.VDID, FieldUnitObj)
    External (_SB_.PC00.RP20, DeviceObj)
    External (_SB_.PC00.RP20.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP20.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP20.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP20.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP20.VDID, FieldUnitObj)
    External (_SB_.PC00.RP21, DeviceObj)
    External (_SB_.PC00.RP21.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP21.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP21.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP21.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP21.VDID, FieldUnitObj)
    External (_SB_.PC00.RP22, DeviceObj)
    External (_SB_.PC00.RP22.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP22.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP22.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP22.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP22.VDID, FieldUnitObj)
    External (_SB_.PC00.RP23, DeviceObj)
    External (_SB_.PC00.RP23.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP23.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP23.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP23.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP23.VDID, FieldUnitObj)
    External (_SB_.PC00.RP24, DeviceObj)
    External (_SB_.PC00.RP24.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP24.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP24.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP24.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP24.VDID, FieldUnitObj)
    External (_SB_.PC00.RP25, DeviceObj)
    External (_SB_.PC00.RP25.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP25.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP25.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP25.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP25.VDID, FieldUnitObj)
    External (_SB_.PC00.RP26, DeviceObj)
    External (_SB_.PC00.RP26.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP26.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP26.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP26.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP26.VDID, FieldUnitObj)
    External (_SB_.PC00.RP27, DeviceObj)
    External (_SB_.PC00.RP27.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP27.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP27.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP27.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP27.VDID, FieldUnitObj)
    External (_SB_.PC00.RP28, DeviceObj)
    External (_SB_.PC00.RP28.HPEX, FieldUnitObj)
    External (_SB_.PC00.RP28.HPSX, FieldUnitObj)
    External (_SB_.PC00.RP28.PMEX, FieldUnitObj)
    External (_SB_.PC00.RP28.PMSX, FieldUnitObj)
    External (_SB_.PC00.RP28.VDID, FieldUnitObj)
    External (_SB_.PC00.TCON, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TG0N, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TG1N, MethodObj)    // 0 Arguments
    External (_SB_.PC00.TRP0.HPEX, IntObj)
    External (_SB_.PC00.TRP0.HPSX, IntObj)
    External (_SB_.PC00.TRP0.PMEX, IntObj)
    External (_SB_.PC00.TRP0.PMSX, IntObj)
    External (_SB_.PC00.TRP1.HPEX, IntObj)
    External (_SB_.PC00.TRP1.HPSX, IntObj)
    External (_SB_.PC00.TRP1.PMEX, IntObj)
    External (_SB_.PC00.TRP1.PMSX, IntObj)
    External (_SB_.PC00.TRP2.HPEX, IntObj)
    External (_SB_.PC00.TRP2.HPSX, IntObj)
    External (_SB_.PC00.TRP2.PMEX, IntObj)
    External (_SB_.PC00.TRP2.PMSX, IntObj)
    External (_SB_.PC00.TRP3.HPEX, IntObj)
    External (_SB_.PC00.TRP3.HPSX, IntObj)
    External (_SB_.PC00.TRP3.PMEX, IntObj)
    External (_SB_.PC00.TRP3.PMSX, IntObj)
    External (_SB_.PC00.TXHC, DeviceObj)
    External (_SB_.PC01.TRP0.HPEX, IntObj)
    External (_SB_.PC01.TRP0.HPSX, IntObj)
    External (_SB_.PC01.TRP0.PMEX, IntObj)
    External (_SB_.PC01.TRP0.PMSX, IntObj)
    External (_SB_.PC01.TRP1.HPEX, IntObj)
    External (_SB_.PC01.TRP1.HPSX, IntObj)
    External (_SB_.PC01.TRP1.PMEX, IntObj)
    External (_SB_.PC01.TRP1.PMSX, IntObj)
    External (_SB_.PC01.TRP2.HPEX, IntObj)
    External (_SB_.PC01.TRP2.HPSX, IntObj)
    External (_SB_.PC01.TRP2.PMEX, IntObj)
    External (_SB_.PC01.TRP2.PMSX, IntObj)
    External (_SB_.PC01.TRP3.HPEX, IntObj)
    External (_SB_.PC01.TRP3.HPSX, IntObj)
    External (_SB_.PC01.TRP3.PMEX, IntObj)
    External (_SB_.PC01.TRP3.PMSX, IntObj)
    External (_SB_.PR00, DeviceObj)
    External (_SB_.PR00.LPSS, PkgObj)
    External (_SB_.PR00.TPSS, PkgObj)
    External (_SB_.PR01, DeviceObj)
    External (_SB_.PR02, DeviceObj)
    External (_SB_.PR03, DeviceObj)
    External (_SB_.PR04, DeviceObj)
    External (_SB_.PR05, DeviceObj)
    External (_SB_.PR06, DeviceObj)
    External (_SB_.PR07, DeviceObj)
    External (_SB_.PR08, DeviceObj)
    External (_SB_.PR09, DeviceObj)
    External (_SB_.PR10, DeviceObj)
    External (_SB_.PR11, DeviceObj)
    External (_SB_.PR12, DeviceObj)
    External (_SB_.PR13, DeviceObj)
    External (_SB_.PR14, DeviceObj)
    External (_SB_.PR15, DeviceObj)
    External (_SB_.PR16, DeviceObj)
    External (_SB_.PR17, DeviceObj)
    External (_SB_.PR18, DeviceObj)
    External (_SB_.PR19, DeviceObj)
    External (_SB_.PR20, DeviceObj)
    External (_SB_.PR21, DeviceObj)
    External (_SB_.PR22, DeviceObj)
    External (_SB_.PR23, DeviceObj)
    External (_SB_.PR24, DeviceObj)
    External (_SB_.PR25, DeviceObj)
    External (_SB_.PR26, DeviceObj)
    External (_SB_.PR27, DeviceObj)
    External (_SB_.PR28, DeviceObj)
    External (_SB_.PR29, DeviceObj)
    External (_SB_.PR30, DeviceObj)
    External (_SB_.PR31, DeviceObj)
    External (_SB_.PR32, DeviceObj)
    External (_SB_.PR33, DeviceObj)
    External (_SB_.PR34, DeviceObj)
    External (_SB_.PR35, DeviceObj)
    External (_SB_.PR36, DeviceObj)
    External (_SB_.PR37, DeviceObj)
    External (_SB_.PR38, DeviceObj)
    External (_SB_.PR39, DeviceObj)
    External (_SB_.PR40, DeviceObj)
    External (_SB_.PR41, DeviceObj)
    External (_SB_.PR42, DeviceObj)
    External (_SB_.PR43, DeviceObj)
    External (_SB_.PR44, DeviceObj)
    External (_SB_.PR45, DeviceObj)
    External (_SB_.PR46, DeviceObj)
    External (_SB_.PR47, DeviceObj)
    External (_SB_.PR48, DeviceObj)
    External (_SB_.PR49, DeviceObj)
    External (_SB_.PR50, DeviceObj)
    External (_SB_.PR51, DeviceObj)
    External (_SB_.PR52, DeviceObj)
    External (_SB_.PR53, DeviceObj)
    External (_SB_.PR54, DeviceObj)
    External (_SB_.PR55, DeviceObj)
    External (_SB_.PR56, DeviceObj)
    External (_SB_.PR57, DeviceObj)
    External (_SB_.PR58, DeviceObj)
    External (_SB_.PR59, DeviceObj)
    External (_SB_.PR60, DeviceObj)
    External (_SB_.PR61, DeviceObj)
    External (_SB_.PR62, DeviceObj)
    External (_SB_.PR63, DeviceObj)
    External (_SB_.TCWK, MethodObj)    // 1 Arguments
    External (ITRT, FieldUnitObj)
    External (NEXP, FieldUnitObj)
    External (OSCC, FieldUnitObj)
    External (PCHS, FieldUnitObj)
    External (PCHX, IntObj)
    External (PF00, IntObj)
    External (PF01, IntObj)
    External (PF02, IntObj)
    External (PF03, IntObj)
    External (PF04, IntObj)
    External (PF05, IntObj)
    External (PF06, IntObj)
    External (PF07, IntObj)
    External (PF08, IntObj)
    External (PF09, IntObj)
    External (PF10, IntObj)
    External (PF11, IntObj)
    External (PF12, IntObj)
    External (PF13, IntObj)
    External (PF14, IntObj)
    External (PF15, IntObj)
    External (PF16, IntObj)
    External (PF17, IntObj)
    External (PF18, IntObj)
    External (PF19, IntObj)
    External (PF20, IntObj)
    External (PF21, IntObj)
    External (PF22, IntObj)
    External (PF23, IntObj)
    External (PF24, IntObj)
    External (PF25, IntObj)
    External (PF26, IntObj)
    External (PF27, IntObj)
    External (PF28, IntObj)
    External (PF29, IntObj)
    External (PF30, IntObj)
    External (PF31, IntObj)
    External (PF32, IntObj)
    External (PF33, IntObj)
    External (PF34, IntObj)
    External (PF35, IntObj)
    External (PF36, IntObj)
    External (PF37, IntObj)
    External (PF38, IntObj)
    External (PF39, IntObj)
    External (PF40, IntObj)
    External (PF41, IntObj)
    External (PF42, IntObj)
    External (PF43, IntObj)
    External (PF44, IntObj)
    External (PF45, IntObj)
    External (PF46, IntObj)
    External (PF47, IntObj)
    External (PF48, IntObj)
    External (PF49, IntObj)
    External (PF50, IntObj)
    External (PF51, IntObj)
    External (PF52, IntObj)
    External (PF53, IntObj)
    External (PF54, IntObj)
    External (PF55, IntObj)
    External (PF56, IntObj)
    External (PF57, IntObj)
    External (PF58, IntObj)
    External (PF59, IntObj)
    External (PF60, IntObj)
    External (PF61, IntObj)
    External (PF62, IntObj)
    External (PF63, IntObj)
    External (TCNT, FieldUnitObj)
    External (TRTD, FieldUnitObj)

    Method (NHPG, 0, Serialized)
    {
        \_SB.PC00.RP01.HPEX = Zero
        \_SB.PC00.RP02.HPEX = Zero
        \_SB.PC00.RP03.HPEX = Zero
        \_SB.PC00.RP04.HPEX = Zero
        \_SB.PC00.RP05.HPEX = Zero
        \_SB.PC00.RP06.HPEX = Zero
        \_SB.PC00.RP07.HPEX = Zero
        \_SB.PC00.RP08.HPEX = Zero
        \_SB.PC00.RP09.HPEX = Zero
        \_SB.PC00.RP10.HPEX = Zero
        \_SB.PC00.RP11.HPEX = Zero
        \_SB.PC00.RP12.HPEX = Zero
        If ((PCHS == PCHX))
        {
            \_SB.PC00.RP13.HPEX = Zero
            \_SB.PC00.RP14.HPEX = Zero
            \_SB.PC00.RP15.HPEX = Zero
            \_SB.PC00.RP16.HPEX = Zero
            \_SB.PC00.RP17.HPEX = Zero
            \_SB.PC00.RP18.HPEX = Zero
            \_SB.PC00.RP19.HPEX = Zero
            \_SB.PC00.RP20.HPEX = Zero
            \_SB.PC00.RP21.HPEX = Zero
            \_SB.PC00.RP22.HPEX = Zero
            \_SB.PC00.RP23.HPEX = Zero
            \_SB.PC00.RP24.HPEX = Zero
            \_SB.PC00.RP25.HPEX = Zero
            \_SB.PC00.RP26.HPEX = Zero
            \_SB.PC00.RP27.HPEX = Zero
            \_SB.PC00.RP28.HPEX = Zero
        }

        If (CondRefOf (\_SB.PC00.TXHC))
        {
            If (CondRefOf (\_SB.PC01))
            {
                \_SB.PC01.TRP0.HPEX = Zero
                \_SB.PC01.TRP1.HPEX = Zero
                \_SB.PC01.TRP2.HPEX = Zero
                \_SB.PC01.TRP3.HPEX = Zero
            }
            Else
            {
                \_SB.PC00.TRP0.HPEX = Zero
                \_SB.PC00.TRP1.HPEX = Zero
                \_SB.PC00.TRP2.HPEX = Zero
                \_SB.PC00.TRP3.HPEX = Zero
            }
        }

        \_SB.PC00.RP01.HPSX = One
        \_SB.PC00.RP02.HPSX = One
        \_SB.PC00.RP03.HPSX = One
        \_SB.PC00.RP04.HPSX = One
        \_SB.PC00.RP05.HPSX = One
        \_SB.PC00.RP06.HPSX = One
        \_SB.PC00.RP07.HPSX = One
        \_SB.PC00.RP08.HPSX = One
        \_SB.PC00.RP09.HPSX = One
        \_SB.PC00.RP10.HPSX = One
        \_SB.PC00.RP11.HPSX = One
        \_SB.PC00.RP12.HPSX = One
        If ((PCHS == PCHX))
        {
            \_SB.PC00.RP13.HPSX = One
            \_SB.PC00.RP14.HPSX = One
            \_SB.PC00.RP15.HPSX = One
            \_SB.PC00.RP16.HPSX = One
            \_SB.PC00.RP17.HPSX = One
            \_SB.PC00.RP18.HPSX = One
            \_SB.PC00.RP19.HPSX = One
            \_SB.PC00.RP20.HPSX = One
            \_SB.PC00.RP21.HPSX = One
            \_SB.PC00.RP22.HPSX = One
            \_SB.PC00.RP23.HPSX = One
            \_SB.PC00.RP24.HPSX = One
            \_SB.PC00.RP25.HPSX = One
            \_SB.PC00.RP26.HPSX = One
            \_SB.PC00.RP27.HPSX = One
            \_SB.PC00.RP28.HPSX = One
        }

        If (CondRefOf (\_SB.PC00.TXHC))
        {
            If (CondRefOf (\_SB.PC01))
            {
                \_SB.PC01.TRP0.HPSX = One
                \_SB.PC01.TRP1.HPSX = One
                \_SB.PC01.TRP2.HPSX = One
                \_SB.PC01.TRP3.HPSX = One
            }
            Else
            {
                \_SB.PC00.TRP0.HPSX = One
                \_SB.PC00.TRP1.HPSX = One
                \_SB.PC00.TRP2.HPSX = One
                \_SB.PC00.TRP3.HPSX = One
            }
        }
    }

    Method (NPME, 0, Serialized)
    {
        \_SB.PC00.RP01.PMEX = Zero
        \_SB.PC00.RP02.PMEX = Zero
        \_SB.PC00.RP03.PMEX = Zero
        \_SB.PC00.RP04.PMEX = Zero
        \_SB.PC00.RP05.PMEX = Zero
        \_SB.PC00.RP06.PMEX = Zero
        \_SB.PC00.RP07.PMEX = Zero
        \_SB.PC00.RP08.PMEX = Zero
        \_SB.PC00.RP09.PMEX = Zero
        \_SB.PC00.RP10.PMEX = Zero
        \_SB.PC00.RP11.PMEX = Zero
        \_SB.PC00.RP12.PMEX = Zero
        If ((PCHS == PCHX))
        {
            \_SB.PC00.RP13.PMEX = Zero
            \_SB.PC00.RP14.PMEX = Zero
            \_SB.PC00.RP15.PMEX = Zero
            \_SB.PC00.RP16.PMEX = Zero
            \_SB.PC00.RP17.PMEX = Zero
            \_SB.PC00.RP18.PMEX = Zero
            \_SB.PC00.RP19.PMEX = Zero
            \_SB.PC00.RP20.PMEX = Zero
            \_SB.PC00.RP21.PMEX = Zero
            \_SB.PC00.RP22.PMEX = Zero
            \_SB.PC00.RP23.PMEX = Zero
            \_SB.PC00.RP24.PMEX = Zero
            \_SB.PC00.RP21.PMEX = Zero
            \_SB.PC00.RP22.PMEX = Zero
            \_SB.PC00.RP23.PMEX = Zero
            \_SB.PC00.RP24.PMEX = Zero
            \_SB.PC00.RP25.PMEX = Zero
            \_SB.PC00.RP26.PMEX = Zero
            \_SB.PC00.RP27.PMEX = Zero
            \_SB.PC00.RP28.PMEX = Zero
        }

        If (CondRefOf (\_SB.PC00.TXHC))
        {
            If (CondRefOf (\_SB.PC01))
            {
                \_SB.PC01.TRP0.PMEX = Zero
                \_SB.PC01.TRP1.PMEX = Zero
                \_SB.PC01.TRP2.PMEX = Zero
                \_SB.PC01.TRP3.PMEX = Zero
            }
            Else
            {
                \_SB.PC00.TRP0.PMEX = Zero
                \_SB.PC00.TRP1.PMEX = Zero
                \_SB.PC00.TRP2.PMEX = Zero
                \_SB.PC00.TRP3.PMEX = Zero
            }
        }

        \_SB.PC00.RP01.PMSX = One
        \_SB.PC00.RP02.PMSX = One
        \_SB.PC00.RP03.PMSX = One
        \_SB.PC00.RP04.PMSX = One
        \_SB.PC00.RP05.PMSX = One
        \_SB.PC00.RP06.PMSX = One
        \_SB.PC00.RP07.PMSX = One
        \_SB.PC00.RP08.PMSX = One
        \_SB.PC00.RP09.PMSX = One
        \_SB.PC00.RP10.PMSX = One
        \_SB.PC00.RP11.PMSX = One
        \_SB.PC00.RP12.PMSX = One
        If ((PCHS == PCHX))
        {
            \_SB.PC00.RP13.PMSX = One
            \_SB.PC00.RP14.PMSX = One
            \_SB.PC00.RP15.PMSX = One
            \_SB.PC00.RP16.PMSX = One
            \_SB.PC00.RP17.PMSX = One
            \_SB.PC00.RP18.PMSX = One
            \_SB.PC00.RP19.PMSX = One
            \_SB.PC00.RP20.PMSX = One
            \_SB.PC00.RP21.PMSX = One
            \_SB.PC00.RP22.PMSX = One
            \_SB.PC00.RP23.PMSX = One
            \_SB.PC00.RP24.PMSX = One
            \_SB.PC00.RP25.PMSX = One
            \_SB.PC00.RP26.PMSX = One
            \_SB.PC00.RP27.PMSX = One
            \_SB.PC00.RP28.PMSX = One
        }

        If (CondRefOf (\_SB.PC00.TXHC))
        {
            If (CondRefOf (\_SB.PC01))
            {
                \_SB.PC01.TRP0.PMSX = One
                \_SB.PC01.TRP1.PMSX = One
                \_SB.PC01.TRP2.PMSX = One
                \_SB.PC01.TRP3.PMSX = One
            }
            Else
            {
                \_SB.PC00.TRP0.PMSX = One
                \_SB.PC00.TRP1.PMSX = One
                \_SB.PC00.TRP2.PMSX = One
                \_SB.PC00.TRP3.PMSX = One
            }
        }
    }

    Method (SPNT, 0, Serialized)
    {
        If ((\PF00 & 0x0400))
        {
            If (CondRefOf (\_SB.PR00.TPSS))
            {
                \_SB.CPPC = Zero
            }
        }
        ElseIf (CondRefOf (\_SB.PR00.LPSS))
        {
            \_SB.CPPC = Zero
        }

        If ((TCNT > One))
        {
            If ((\PF00 & 0x08))
            {
                Notify (\_SB.PR00, 0x80) // Status Change
            }

            If ((\PF01 & 0x08))
            {
                Notify (\_SB.PR01, 0x80) // Status Change
            }

            If ((\PF02 & 0x08))
            {
                Notify (\_SB.PR02, 0x80) // Status Change
            }

            If ((\PF03 & 0x08))
            {
                Notify (\_SB.PR03, 0x80) // Status Change
            }

            If ((\PF04 & 0x08))
            {
                Notify (\_SB.PR04, 0x80) // Status Change
            }

            If ((\PF05 & 0x08))
            {
                Notify (\_SB.PR05, 0x80) // Status Change
            }

            If ((\PF06 & 0x08))
            {
                Notify (\_SB.PR06, 0x80) // Status Change
            }

            If ((\PF07 & 0x08))
            {
                Notify (\_SB.PR07, 0x80) // Status Change
            }

            If ((\PF08 & 0x08))
            {
                Notify (\_SB.PR08, 0x80) // Status Change
            }

            If ((\PF09 & 0x08))
            {
                Notify (\_SB.PR09, 0x80) // Status Change
            }

            If ((\PF10 & 0x08))
            {
                Notify (\_SB.PR10, 0x80) // Status Change
            }

            If ((\PF11 & 0x08))
            {
                Notify (\_SB.PR11, 0x80) // Status Change
            }

            If ((\PF12 & 0x08))
            {
                Notify (\_SB.PR12, 0x80) // Status Change
            }

            If ((\PF13 & 0x08))
            {
                Notify (\_SB.PR13, 0x80) // Status Change
            }

            If ((\PF14 & 0x08))
            {
                Notify (\_SB.PR14, 0x80) // Status Change
            }

            If ((\PF15 & 0x08))
            {
                Notify (\_SB.PR15, 0x80) // Status Change
            }

            If ((\PF16 & 0x08))
            {
                Notify (\_SB.PR16, 0x80) // Status Change
            }

            If ((\PF17 & 0x08))
            {
                Notify (\_SB.PR17, 0x80) // Status Change
            }

            If ((\PF18 & 0x08))
            {
                Notify (\_SB.PR18, 0x80) // Status Change
            }

            If ((\PF19 & 0x08))
            {
                Notify (\_SB.PR19, 0x80) // Status Change
            }

            If ((\PF20 & 0x08))
            {
                Notify (\_SB.PR20, 0x80) // Status Change
            }

            If ((\PF21 & 0x08))
            {
                Notify (\_SB.PR21, 0x80) // Status Change
            }

            If ((\PF22 & 0x08))
            {
                Notify (\_SB.PR22, 0x80) // Status Change
            }

            If ((\PF23 & 0x08))
            {
                Notify (\_SB.PR23, 0x80) // Status Change
            }

            If ((\PF24 & 0x08))
            {
                Notify (\_SB.PR24, 0x80) // Status Change
            }

            If ((\PF25 & 0x08))
            {
                Notify (\_SB.PR25, 0x80) // Status Change
            }

            If ((\PF26 & 0x08))
            {
                Notify (\_SB.PR26, 0x80) // Status Change
            }

            If ((\PF27 & 0x08))
            {
                Notify (\_SB.PR27, 0x80) // Status Change
            }

            If ((\PF28 & 0x08))
            {
                Notify (\_SB.PR28, 0x80) // Status Change
            }

            If ((\PF29 & 0x08))
            {
                Notify (\_SB.PR29, 0x80) // Status Change
            }

            If ((\PF30 & 0x08))
            {
                Notify (\_SB.PR30, 0x80) // Status Change
            }

            If ((\PF31 & 0x08))
            {
                Notify (\_SB.PR31, 0x80) // Status Change
            }

            If ((\PF32 & 0x08))
            {
                Notify (\_SB.PR32, 0x80) // Status Change
            }

            If ((\PF33 & 0x08))
            {
                Notify (\_SB.PR33, 0x80) // Status Change
            }

            If ((\PF34 & 0x08))
            {
                Notify (\_SB.PR34, 0x80) // Status Change
            }

            If ((\PF35 & 0x08))
            {
                Notify (\_SB.PR35, 0x80) // Status Change
            }

            If ((\PF36 & 0x08))
            {
                Notify (\_SB.PR36, 0x80) // Status Change
            }

            If ((\PF37 & 0x08))
            {
                Notify (\_SB.PR37, 0x80) // Status Change
            }

            If ((\PF38 & 0x08))
            {
                Notify (\_SB.PR38, 0x80) // Status Change
            }

            If ((\PF39 & 0x08))
            {
                Notify (\_SB.PR39, 0x80) // Status Change
            }

            If ((\PF40 & 0x08))
            {
                Notify (\_SB.PR40, 0x80) // Status Change
            }

            If ((\PF41 & 0x08))
            {
                Notify (\_SB.PR41, 0x80) // Status Change
            }

            If ((\PF42 & 0x08))
            {
                Notify (\_SB.PR42, 0x80) // Status Change
            }

            If ((\PF43 & 0x08))
            {
                Notify (\_SB.PR43, 0x80) // Status Change
            }

            If ((\PF44 & 0x08))
            {
                Notify (\_SB.PR44, 0x80) // Status Change
            }

            If ((\PF45 & 0x08))
            {
                Notify (\_SB.PR45, 0x80) // Status Change
            }

            If ((\PF46 & 0x08))
            {
                Notify (\_SB.PR46, 0x80) // Status Change
            }

            If ((\PF47 & 0x08))
            {
                Notify (\_SB.PR47, 0x80) // Status Change
            }

            If ((\PF48 & 0x08))
            {
                Notify (\_SB.PR48, 0x80) // Status Change
            }

            If ((\PF49 & 0x08))
            {
                Notify (\_SB.PR49, 0x80) // Status Change
            }

            If ((\PF50 & 0x08))
            {
                Notify (\_SB.PR50, 0x80) // Status Change
            }

            If ((\PF51 & 0x08))
            {
                Notify (\_SB.PR51, 0x80) // Status Change
            }

            If ((\PF52 & 0x08))
            {
                Notify (\_SB.PR52, 0x80) // Status Change
            }

            If ((\PF53 & 0x08))
            {
                Notify (\_SB.PR53, 0x80) // Status Change
            }

            If ((\PF54 & 0x08))
            {
                Notify (\_SB.PR54, 0x80) // Status Change
            }

            If ((\PF55 & 0x08))
            {
                Notify (\_SB.PR55, 0x80) // Status Change
            }

            If ((\PF56 & 0x08))
            {
                Notify (\_SB.PR56, 0x80) // Status Change
            }

            If ((\PF57 & 0x08))
            {
                Notify (\_SB.PR57, 0x80) // Status Change
            }

            If ((\PF58 & 0x08))
            {
                Notify (\_SB.PR58, 0x80) // Status Change
            }

            If ((\PF59 & 0x08))
            {
                Notify (\_SB.PR59, 0x80) // Status Change
            }

            If ((\PF60 & 0x08))
            {
                Notify (\_SB.PR60, 0x80) // Status Change
            }

            If ((\PF61 & 0x08))
            {
                Notify (\_SB.PR61, 0x80) // Status Change
            }

            If ((\PF62 & 0x08))
            {
                Notify (\_SB.PR62, 0x80) // Status Change
            }

            If ((\PF63 & 0x08))
            {
                Notify (\_SB.PR63, 0x80) // Status Change
            }
        }
        Else
        {
            Notify (\_SB.PR00, 0x80) // Status Change
        }

        If ((TCNT > One))
        {
            If (((\PF00 & 0x08) && (\PF00 & 0x10)))
            {
                Notify (\_SB.PR00, 0x81) // Information Change
            }

            If (((\PF01 & 0x08) && (\PF01 & 0x10)))
            {
                Notify (\_SB.PR01, 0x81) // Information Change
            }

            If (((\PF02 & 0x08) && (\PF02 & 0x10)))
            {
                Notify (\_SB.PR02, 0x81) // Information Change
            }

            If (((\PF03 & 0x08) && (\PF03 & 0x10)))
            {
                Notify (\_SB.PR03, 0x81) // Information Change
            }

            If (((\PF04 & 0x08) && (\PF04 & 0x10)))
            {
                Notify (\_SB.PR04, 0x81) // Information Change
            }

            If (((\PF05 & 0x08) && (\PF05 & 0x10)))
            {
                Notify (\_SB.PR05, 0x81) // Information Change
            }

            If (((\PF06 & 0x08) && (\PF06 & 0x10)))
            {
                Notify (\_SB.PR06, 0x81) // Information Change
            }

            If (((\PF07 & 0x08) && (\PF07 & 0x10)))
            {
                Notify (\_SB.PR07, 0x81) // Information Change
            }

            If (((\PF08 & 0x08) && (\PF08 & 0x10)))
            {
                Notify (\_SB.PR08, 0x81) // Information Change
            }

            If (((\PF09 & 0x08) && (\PF09 & 0x10)))
            {
                Notify (\_SB.PR09, 0x81) // Information Change
            }

            If (((\PF10 & 0x08) && (\PF10 & 0x10)))
            {
                Notify (\_SB.PR10, 0x81) // Information Change
            }

            If (((\PF11 & 0x08) && (\PF11 & 0x10)))
            {
                Notify (\_SB.PR11, 0x81) // Information Change
            }

            If (((\PF12 & 0x08) && (\PF12 & 0x10)))
            {
                Notify (\_SB.PR12, 0x81) // Information Change
            }

            If (((\PF13 & 0x08) && (\PF13 & 0x10)))
            {
                Notify (\_SB.PR13, 0x81) // Information Change
            }

            If (((\PF14 & 0x08) && (\PF14 & 0x10)))
            {
                Notify (\_SB.PR14, 0x81) // Information Change
            }

            If (((\PF15 & 0x08) && (\PF15 & 0x10)))
            {
                Notify (\_SB.PR15, 0x81) // Information Change
            }

            If (((\PF16 & 0x08) && (\PF16 & 0x10)))
            {
                Notify (\_SB.PR16, 0x81) // Information Change
            }

            If (((\PF17 & 0x08) && (\PF17 & 0x10)))
            {
                Notify (\_SB.PR17, 0x81) // Information Change
            }

            If (((\PF18 & 0x08) && (\PF18 & 0x10)))
            {
                Notify (\_SB.PR18, 0x81) // Information Change
            }

            If (((\PF19 & 0x08) && (\PF19 & 0x10)))
            {
                Notify (\_SB.PR19, 0x81) // Information Change
            }

            If (((\PF20 & 0x08) && (\PF20 & 0x10)))
            {
                Notify (\_SB.PR20, 0x81) // Information Change
            }

            If (((\PF21 & 0x08) && (\PF21 & 0x10)))
            {
                Notify (\_SB.PR21, 0x81) // Information Change
            }

            If (((\PF22 & 0x08) && (\PF22 & 0x10)))
            {
                Notify (\_SB.PR22, 0x81) // Information Change
            }

            If (((\PF23 & 0x08) && (\PF23 & 0x10)))
            {
                Notify (\_SB.PR23, 0x81) // Information Change
            }

            If (((\PF24 & 0x08) && (\PF24 & 0x10)))
            {
                Notify (\_SB.PR24, 0x81) // Information Change
            }

            If (((\PF25 & 0x08) && (\PF25 & 0x10)))
            {
                Notify (\_SB.PR25, 0x81) // Information Change
            }

            If (((\PF26 & 0x08) && (\PF26 & 0x10)))
            {
                Notify (\_SB.PR26, 0x81) // Information Change
            }

            If (((\PF27 & 0x08) && (\PF27 & 0x10)))
            {
                Notify (\_SB.PR27, 0x81) // Information Change
            }

            If (((\PF28 & 0x08) && (\PF28 & 0x10)))
            {
                Notify (\_SB.PR28, 0x81) // Information Change
            }

            If (((\PF29 & 0x08) && (\PF29 & 0x10)))
            {
                Notify (\_SB.PR29, 0x81) // Information Change
            }

            If (((\PF30 & 0x08) && (\PF30 & 0x10)))
            {
                Notify (\_SB.PR30, 0x81) // Information Change
            }

            If (((\PF31 & 0x08) && (\PF31 & 0x10)))
            {
                Notify (\_SB.PR31, 0x81) // Information Change
            }

            If (((\PF32 & 0x08) && (\PF32 & 0x10)))
            {
                Notify (\_SB.PR32, 0x81) // Information Change
            }

            If (((\PF33 & 0x08) && (\PF33 & 0x10)))
            {
                Notify (\_SB.PR33, 0x81) // Information Change
            }

            If (((\PF34 & 0x08) && (\PF34 & 0x10)))
            {
                Notify (\_SB.PR34, 0x81) // Information Change
            }

            If (((\PF35 & 0x08) && (\PF35 & 0x10)))
            {
                Notify (\_SB.PR35, 0x81) // Information Change
            }

            If (((\PF36 & 0x08) && (\PF36 & 0x10)))
            {
                Notify (\_SB.PR36, 0x81) // Information Change
            }

            If (((\PF37 & 0x08) && (\PF37 & 0x10)))
            {
                Notify (\_SB.PR37, 0x81) // Information Change
            }

            If (((\PF38 & 0x08) && (\PF38 & 0x10)))
            {
                Notify (\_SB.PR38, 0x81) // Information Change
            }

            If (((\PF39 & 0x08) && (\PF39 & 0x10)))
            {
                Notify (\_SB.PR39, 0x81) // Information Change
            }

            If (((\PF40 & 0x08) && (\PF40 & 0x10)))
            {
                Notify (\_SB.PR40, 0x81) // Information Change
            }

            If (((\PF41 & 0x08) && (\PF41 & 0x10)))
            {
                Notify (\_SB.PR41, 0x81) // Information Change
            }

            If (((\PF42 & 0x08) && (\PF42 & 0x10)))
            {
                Notify (\_SB.PR42, 0x81) // Information Change
            }

            If (((\PF43 & 0x08) && (\PF43 & 0x10)))
            {
                Notify (\_SB.PR43, 0x81) // Information Change
            }

            If (((\PF44 & 0x08) && (\PF44 & 0x10)))
            {
                Notify (\_SB.PR44, 0x81) // Information Change
            }

            If (((\PF45 & 0x08) && (\PF45 & 0x10)))
            {
                Notify (\_SB.PR45, 0x81) // Information Change
            }

            If (((\PF46 & 0x08) && (\PF46 & 0x10)))
            {
                Notify (\_SB.PR46, 0x81) // Information Change
            }

            If (((\PF47 & 0x08) && (\PF47 & 0x10)))
            {
                Notify (\_SB.PR47, 0x81) // Information Change
            }

            If (((\PF48 & 0x08) && (\PF48 & 0x10)))
            {
                Notify (\_SB.PR48, 0x81) // Information Change
            }

            If (((\PF49 & 0x08) && (\PF49 & 0x10)))
            {
                Notify (\_SB.PR49, 0x81) // Information Change
            }

            If (((\PF50 & 0x08) && (\PF50 & 0x10)))
            {
                Notify (\_SB.PR50, 0x81) // Information Change
            }

            If (((\PF51 & 0x08) && (\PF51 & 0x10)))
            {
                Notify (\_SB.PR51, 0x81) // Information Change
            }

            If (((\PF52 & 0x08) && (\PF52 & 0x10)))
            {
                Notify (\_SB.PR52, 0x81) // Information Change
            }

            If (((\PF53 & 0x08) && (\PF53 & 0x10)))
            {
                Notify (\_SB.PR53, 0x81) // Information Change
            }

            If (((\PF54 & 0x08) && (\PF54 & 0x10)))
            {
                Notify (\_SB.PR54, 0x81) // Information Change
            }

            If (((\PF55 & 0x08) && (\PF55 & 0x10)))
            {
                Notify (\_SB.PR55, 0x81) // Information Change
            }

            If (((\PF56 & 0x08) && (\PF56 & 0x10)))
            {
                Notify (\_SB.PR56, 0x81) // Information Change
            }

            If (((\PF57 & 0x08) && (\PF57 & 0x10)))
            {
                Notify (\_SB.PR57, 0x81) // Information Change
            }

            If (((\PF58 & 0x08) && (\PF58 & 0x10)))
            {
                Notify (\_SB.PR58, 0x81) // Information Change
            }

            If (((\PF59 & 0x08) && (\PF59 & 0x10)))
            {
                Notify (\_SB.PR59, 0x81) // Information Change
            }

            If (((\PF60 & 0x08) && (\PF60 & 0x10)))
            {
                Notify (\_SB.PR60, 0x81) // Information Change
            }

            If (((\PF61 & 0x08) && (\PF61 & 0x10)))
            {
                Notify (\_SB.PR61, 0x81) // Information Change
            }

            If (((\PF62 & 0x08) && (\PF62 & 0x10)))
            {
                Notify (\_SB.PR62, 0x81) // Information Change
            }

            If (((\PF63 & 0x08) && (\PF63 & 0x10)))
            {
                Notify (\_SB.PR63, 0x81) // Information Change
            }
        }
        Else
        {
            Notify (\_SB.PR00, 0x81) // Information Change
        }
    }

    Method (SPTS, 1, NotSerialized)
    {
        If (CondRefOf (\_SB.PC00.TXHC))
        {
            If (TRTD)
            {
                \_SB.PC00.TCON ()
            }

            If (ITRT)
            {
                \_SB.PC00.TG0N ()
                \_SB.PC00.TG1N ()
            }
        }
    }

    Method (SWAK, 2, NotSerialized)
    {
        If (NEXP)
        {
            If ((OSCC & One))
            {
                NHPG ()
            }

            If ((OSCC & 0x04))
            {
                NPME ()
            }
        }

        If (((Arg0 == 0x03) || (Arg0 == 0x04)))
        {
            If ((\_SB.PC00.RP01.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x02) != One))
                {
                    Notify (\_SB.PC00.RP01, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP02.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x04) != One))
                {
                    Notify (\_SB.PC00.RP02, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP03.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x08) != One))
                {
                    Notify (\_SB.PC00.RP03, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP04.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x10) != One))
                {
                    Notify (\_SB.PC00.RP04, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP05.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x20) != One))
                {
                    Notify (\_SB.PC00.RP05, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP06.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x40) != One))
                {
                    Notify (\_SB.PC00.RP06, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP07.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x80) != One))
                {
                    Notify (\_SB.PC00.RP07, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP08.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x0100) != One))
                {
                    Notify (\_SB.PC00.RP08, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP09.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x0200) != One))
                {
                    Notify (\_SB.PC00.RP09, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP10.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x0400) != One))
                {
                    Notify (\_SB.PC00.RP10, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP11.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x0800) != One))
                {
                    Notify (\_SB.PC00.RP11, Zero) // Bus Check
                }
            }

            If ((\_SB.PC00.RP12.VDID != 0xFFFFFFFF))
            {
                If (((Arg1 & 0x1000) != One))
                {
                    Notify (\_SB.PC00.RP12, Zero) // Bus Check
                }
            }

            If ((PCHS == PCHX))
            {
                If ((\_SB.PC00.RP13.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x2000) != One))
                    {
                        Notify (\_SB.PC00.RP13, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP14.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x4000) != One))
                    {
                        Notify (\_SB.PC00.RP14, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP15.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x8000) != One))
                    {
                        Notify (\_SB.PC00.RP15, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP16.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00010000) != One))
                    {
                        Notify (\_SB.PC00.RP16, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP17.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00020000) != One))
                    {
                        Notify (\_SB.PC00.RP17, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP18.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00040000) != One))
                    {
                        Notify (\_SB.PC00.RP18, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP19.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00080000) != One))
                    {
                        Notify (\_SB.PC00.RP19, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP20.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00100000) != One))
                    {
                        Notify (\_SB.PC00.RP20, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP21.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00200000) != One))
                    {
                        Notify (\_SB.PC00.RP21, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP22.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00400000) != One))
                    {
                        Notify (\_SB.PC00.RP22, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP23.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x00800000) != One))
                    {
                        Notify (\_SB.PC00.RP23, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP24.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x01000000) != One))
                    {
                        Notify (\_SB.PC00.RP24, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP25.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x02000000) != One))
                    {
                        Notify (\_SB.PC00.RP25, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP26.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x04000000) != One))
                    {
                        Notify (\_SB.PC00.RP26, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP27.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x08000000) != One))
                    {
                        Notify (\_SB.PC00.RP27, Zero) // Bus Check
                    }
                }

                If ((\_SB.PC00.RP28.VDID != 0xFFFFFFFF))
                {
                    If (((Arg1 & 0x10000000) != One))
                    {
                        Notify (\_SB.PC00.RP28, Zero) // Bus Check
                    }
                }
            }

            If (CondRefOf (\_SB.PC00.TXHC))
            {
                \_SB.TCWK (Arg0)
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x000000AE (174)
 *     Revision         0x02
 *     Checksum         0xCF
 *     OEM ID           "HgRef"
 *     OEM Table ID     "HgPeg"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "HgRef", "HgPeg", 0x00001000)
{
    External (_SB_.PC00.PEG1.PEGP, DeviceObj)
    External (_SB_.SGOV, MethodObj)    // 2 Arguments
    External (GBAS, UnknownObj)
    External (HGMD, UnknownObj)
    External (SGGP, UnknownObj)

    Scope (\_SB.PC00.PEG1.PEGP)
    {
        Method (SGPO, 4, Serialized)
        {
            If ((Arg2 == Zero))
            {
                Arg3 = ~Arg3
                Arg3 &= One
            }

            If ((SGGP == One))
            {
                If (CondRefOf (\_SB.SGOV))
                {
                    \_SB.SGOV (Arg1, Arg3)
                }
            }
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000144 (324)
 *     Revision         0x02
 *     Checksum         0xD5
 *     OEM ID           "Intel "
 *     OEM Table ID     "ADebTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "Intel ", "ADebTabl", 0x00001000)
{
    Scope (\)
    {
        Name (DPTR, 0x3EEAD000)
        Name (EPTR, 0x3EEBD000)
        Name (CPTR, 0x3EEAD020)
        Mutex (MMUT, 0x00)
        OperationRegion (ADBP, SystemIO, 0xB2, 0x02)
        Field (ADBP, ByteAcc, NoLock, Preserve)
        {
            B2PT,   8, 
            B3PT,   8
        }

        Method (MDBG, 1, Serialized)
        {
            OperationRegion (ADHD, SystemMemory, DPTR, 0x20)
            Field (ADHD, ByteAcc, NoLock, Preserve)
            {
                ASIG,   128, 
                ASIZ,   32, 
                ACHP,   32, 
                ACTP,   32, 
                SMIN,   8, 
                WRAP,   8, 
                SMMV,   8, 
                TRUN,   8
            }

            Local0 = Acquire (MMUT, 0x03E8)
            If ((Local0 == Zero))
            {
                OperationRegion (ABLK, SystemMemory, CPTR, 0x20)
                Field (ABLK, ByteAcc, NoLock, Preserve)
                {
                    AAAA,   256
                }

                ToHexString (Arg0, Local1)
                TRUN = Zero
                If ((SizeOf (Local1) >= 0x20))
                {
                    TRUN = One
                }

                Mid (Local1, Zero, 0x1F, AAAA) /* \MDBG.AAAA */
                CPTR += 0x20
                If ((CPTR >= EPTR))
                {
                    CPTR = (DPTR + 0x20)
                    WRAP = One
                }

                ACTP = CPTR /* \CPTR */
                If (SMMV)
                {
                    B2PT = SMIN /* \MDBG.SMIN */
                }

                Release (MMUT)
            }

            Return (Local0)
        }
    }
}

/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000733 (1843)
 *     Revision         0x01
 *     Checksum         0x10
 *     OEM ID           "NvDDS"
 *     OEM Table ID     "NvDDSN20"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 1, "NvDDS", "NvDDSN20", 0x00001000)
{
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.LPCB.HDRP, IntObj)
    External (_SB_.PC00.LPCB.IVGA, IntObj)
    External (_SB_.PC00.LPCB.MBDF, FieldUnitObj)
    External (_SB_.PC00.LPCB.NEDP, IntObj)
    External (_SB_.PC00.LPCB.SECC, MethodObj)    // 2 Arguments
    External (_SB_.PC00.PEG1.PEGP, DeviceObj)
    External (DPMF, UnknownObj)
    External (GDPM, UnknownObj)
    External (IEB0, UnknownObj)
    External (IEB1, UnknownObj)
    External (SDMF, UnknownObj)
    External (SDPM, UnknownObj)
    External (SSMP, UnknownObj)

    Scope (\_SB.PC00.PEG1.PEGP)
    {
        Name (MMID, Package (0x02)
        {
            Package (0x03)
            {
                Zero, 
                "CBTL06DP213", 
                0x00040001
            }, 

            Package (0x03)
            {
                One, 
                "NON-MUX or Error", 
                Zero
            }
        })
        Name (EDB1, Buffer (0x80)
        {
            /* 0000 */  0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,  // ........
            /* 0008 */  0x09, 0xE5, 0x09, 0x0A, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x0A, 0x1F, 0x01, 0x04, 0xA5, 0x26, 0x15, 0x78,  // .....&.x
            /* 0018 */  0x03, 0x0F, 0x95, 0xAE, 0x52, 0x43, 0xB0, 0x26,  // ....RC.&
            /* 0020 */  0x0F, 0x50, 0x54, 0x00, 0x00, 0x00, 0x01, 0x01,  // .PT.....
            /* 0028 */  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,  // ........
            /* 0030 */  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x09, 0x64,  // .......d
            /* 0038 */  0x00, 0xB0, 0xA0, 0xA0, 0x78, 0x50, 0x30, 0x20,  // ....xP0 
            /* 0040 */  0x36, 0x00, 0x7D, 0xD6, 0x10, 0x00, 0x00, 0x18,  // 6.}.....
            /* 0048 */  0x00, 0x00, 0x00, 0xFD, 0x0C, 0x30, 0xA5, 0x02,  // .....0..
            /* 0050 */  0x02, 0x46, 0x01, 0x0A, 0x20, 0x20, 0x20, 0x20,  // .F..    
            /* 0058 */  0x20, 0x20, 0x00, 0x00, 0x00, 0xFE, 0x00, 0x42,  //   .....B
            /* 0060 */  0x4F, 0x45, 0x20, 0x43, 0x51, 0x0A, 0x20, 0x20,  // OE CQ.  
            /* 0068 */  0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0xFE,  //     ....
            /* 0070 */  0x00, 0x4E, 0x45, 0x31, 0x37, 0x33, 0x51, 0x48,  // .NE173QH
            /* 0078 */  0x4D, 0x2D, 0x4E, 0x59, 0x33, 0x0A, 0x01, 0x6D   // M-NY3..m
        })
        Name (EDB2, Buffer (0x0100)
        {
            /* 0000 */  0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00,  // ........
            /* 0008 */  0x09, 0xE5, 0x09, 0x0A, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x0A, 0x1F, 0x01, 0x04, 0xA5, 0x26, 0x15, 0x78,  // .....&.x
            /* 0018 */  0x03, 0x0F, 0x95, 0xAE, 0x52, 0x43, 0xB0, 0x26,  // ....RC.&
            /* 0020 */  0x0F, 0x50, 0x54, 0x00, 0x00, 0x00, 0x01, 0x01,  // .PT.....
            /* 0028 */  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,  // ........
            /* 0030 */  0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x09, 0x64,  // .......d
            /* 0038 */  0x00, 0xB0, 0xA0, 0xA0, 0x78, 0x50, 0x30, 0x20,  // ....xP0 
            /* 0040 */  0x36, 0x00, 0x7D, 0xD6, 0x10, 0x00, 0x00, 0x18,  // 6.}.....
            /* 0048 */  0x00, 0x00, 0x00, 0xFD, 0x0C, 0x30, 0xA5, 0x02,  // .....0..
            /* 0050 */  0x02, 0x46, 0x01, 0x0A, 0x20, 0x20, 0x20, 0x20,  // .F..    
            /* 0058 */  0x20, 0x20, 0x00, 0x00, 0x00, 0xFE, 0x00, 0x42,  //   .....B
            /* 0060 */  0x4F, 0x45, 0x20, 0x43, 0x51, 0x0A, 0x20, 0x20,  // OE CQ.  
            /* 0068 */  0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0xFE,  //     ....
            /* 0070 */  0x00, 0x4E, 0x45, 0x31, 0x37, 0x33, 0x51, 0x48,  // .NE173QH
            /* 0078 */  0x4D, 0x2D, 0x4E, 0x59, 0x33, 0x0A, 0x01, 0x6D,  // M-NY3..m
            /* 0080 */  0x70, 0x13, 0x79, 0x00, 0x00, 0x03, 0x01, 0x14,  // p.y.....
            /* 0088 */  0x19, 0x13, 0x01, 0x84, 0xFF, 0x09, 0xAF, 0x00,  // ........
            /* 0090 */  0x2F, 0x00, 0x1F, 0x00, 0x9F, 0x05, 0x77, 0x00,  // /.....w.
            /* 0098 */  0x02, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00A0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00A8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00B0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00B8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00C0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00C8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00D0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00D8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00E0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00E8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00F0 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00F8 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x84, 0x90   // ........
        })
        OperationRegion (GIE0, SystemMemory, 0xE06A0870, 0x20)
        Field (GIE0, ByteAcc, Lock, Preserve)
        {
            WE15,   1, 
            RE15,   1, 
            Offset (0x10), 
            WE16,   1, 
            RE16,   1
        }

        Method (_DOD, 0, NotSerialized)  // _DOD: Display Output Devices
        {
            Return (Package (0x01)
            {
                0x8000A450
            })
        }

        Device (LCD0)
        {
            Method (_ADR, 0, Serialized)  // _ADR: Address
            {
                Return (0x8000A450)
            }

            Method (_DDC, 1, Serialized)  // _DDC: Display Data Current
            {
                Debug = "Mild DDS debug"
                Name (BUF2, Buffer (0x0180) {})
                CreateField (BUF2, Zero, 0x0400, EDB0)
                CreateField (BUF2, Zero, 0x0800, EDB1)
                CreateField (BUF2, Zero, 0x0C00, EDB2)
                EDB2 = IEB0 /* External reference */
                If ((Arg0 == One))
                {
                    Return (EDB0) /* \_SB_.PC00.PEG1.PEGP.LCD0._DDC.EDB0 */
                }

                If ((Arg0 == 0x02))
                {
                    Return (EDB1) /* \_SB_.PC00.PEG1.PEGP.LCD0._DDC.EDB1 */
                }

                If ((Arg0 == 0x03))
                {
                    Return (EDB2) /* \_SB_.PC00.PEG1.PEGP.LCD0._DDC.EDB2 */
                }
            }

            Method (MXDS, 1, NotSerialized)
            {
                Local0 = Arg0
                Local1 = (Local0 & 0x0F)
                Local2 = (Local0 & 0x10)
                If ((Local1 == Zero))
                {
                    If ((RE15 == Zero))
                    {
                        Return (One)
                    }
                    Else
                    {
                        Return (0x02)
                    }
                }
                ElseIf ((Local1 == One))
                {
                    If ((Local2 == 0x10))
                    {
                        WE15 = One
                        \_SB.PC00.LPCB.NEDP = One
                    }
                    Else
                    {
                        WE15 = Zero
                        \_SB.PC00.LPCB.NEDP = Zero
                    }

                    Return (One)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (MXDM, 1, NotSerialized)
            {
                Local0 = Arg0
                Local1 = (Local0 & 0x07)
                If ((Local1 == Zero))
                {
                    Local2 = DPMF /* External reference */
                    Return (Local2)
                }
                ElseIf ((Local1 < 0x05))
                {
                    SDMF = Local1
                    SSMP = 0xB8
                }
                Else
                {
                    Return (Zero)
                }

                Return (One)
            }

            Method (MXID, 1, NotSerialized)
            {
                If ((Arg0 == Zero))
                {
                    Local0 = DerefOf (DerefOf (MMID [Zero]) [0x02])
                    Return (Local0)
                }
            }

            Method (_BCL, 0, NotSerialized)  // _BCL: Brightness Control Levels
            {
                Return (Package (0x67)
                {
                    0x50, 
                    0x32, 
                    Zero, 
                    One, 
                    0x02, 
                    0x03, 
                    0x04, 
                    0x05, 
                    0x06, 
                    0x07, 
                    0x08, 
                    0x09, 
                    0x0A, 
                    0x0B, 
                    0x0C, 
                    0x0D, 
                    0x0E, 
                    0x0F, 
                    0x10, 
                    0x11, 
                    0x12, 
                    0x13, 
                    0x14, 
                    0x15, 
                    0x16, 
                    0x17, 
                    0x18, 
                    0x19, 
                    0x1A, 
                    0x1B, 
                    0x1C, 
                    0x1D, 
                    0x1E, 
                    0x1F, 
                    0x20, 
                    0x21, 
                    0x22, 
                    0x23, 
                    0x24, 
                    0x25, 
                    0x26, 
                    0x27, 
                    0x28, 
                    0x29, 
                    0x2A, 
                    0x2B, 
                    0x2C, 
                    0x2D, 
                    0x2E, 
                    0x2F, 
                    0x30, 
                    0x31, 
                    0x32, 
                    0x33, 
                    0x34, 
                    0x35, 
                    0x36, 
                    0x37, 
                    0x38, 
                    0x39, 
                    0x3A, 
                    0x3B, 
                    0x3C, 
                    0x3D, 
                    0x3E, 
                    0x3F, 
                    0x40, 
                    0x41, 
                    0x42, 
                    0x43, 
                    0x44, 
                    0x45, 
                    0x46, 
                    0x47, 
                    0x48, 
                    0x49, 
                    0x4A, 
                    0x4B, 
                    0x4C, 
                    0x4D, 
                    0x4E, 
                    0x4F, 
                    0x50, 
                    0x51, 
                    0x52, 
                    0x53, 
                    0x54, 
                    0x55, 
                    0x56, 
                    0x57, 
                    0x58, 
                    0x59, 
                    0x5A, 
                    0x5B, 
                    0x5C, 
                    0x5D, 
                    0x5E, 
                    0x5F, 
                    0x60, 
                    0x61, 
                    0x62, 
                    0x63, 
                    0x64
                })
            }

            Method (_BCM, 1, NotSerialized)  // _BCM: Brightness Control Method
            {
            }

            Method (LRST, 1, NotSerialized)
            {
                Local0 = Arg0
                Local1 = (Local0 & 0x07)
                If ((Local1 == Zero))
                {
                    If ((RE16 == Zero))
                    {
                        Return (One)
                    }
                    ElseIf ((RE16 == One))
                    {
                        Return (0x02)
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }
                ElseIf ((Local1 == One))
                {
                    WE16 = Zero
                }
                ElseIf ((Local1 == 0x02))
                {
                    WE16 = One
                }
                Else
                {
                    Return (Zero)
                }
            }
        }
    }

    Scope (\_SB.PC00)
    {
        Device (AWMI)
        {
            Name (_HID, "PNP0C14" /* Windows Management Instrumentation Device */)  // _HID: Hardware ID
            Name (_UID, "0x00")  // _UID: Unique ID
            Name (LEDC, 0x23)
            Name (_WDG, Buffer (0x28)
            {
                /* 0000 */  0x13, 0x96, 0x3E, 0x60, 0x25, 0xEF, 0x38, 0x43,  // ..>`%.8C
                /* 0008 */  0xA3, 0xD0, 0xC4, 0x61, 0x77, 0x51, 0x6D, 0xB7,  // ...awQm.
                /* 0010 */  0x41, 0x41, 0x01, 0x02, 0x21, 0x12, 0x90, 0x05,  // AA..!...
                /* 0018 */  0x66, 0xD5, 0xD1, 0x11, 0xB2, 0xF0, 0x00, 0xA0,  // f.......
                /* 0020 */  0xC9, 0x06, 0x29, 0x10, 0x30, 0x30, 0x01, 0x00   // ..).00..
            })
            Method (WMAA, 3, Serialized)
            {
                CreateByteField (Arg2, Zero, MODF)
                CreateDWordField (Arg2, 0x04, LEDB)
                Switch (Arg1)
                {
                    Case (One)
                    {
                        If ((MODF == Zero))
                        {
                            LEDB = LEDC /* \_SB_.PC00.AWMI.LEDC */
                            Return (LEDB) /* \_SB_.PC00.AWMI.WMAA.LEDB */
                        }
                        ElseIf ((MODF == One))
                        {
                            LEDC = (LEDB & 0xFF)
                            \_SB.PC00.LPCB.MBDF = LEDC /* \_SB_.PC00.AWMI.LEDC */
                            Return (Zero)
                        }
                        ElseIf ((MODF == 0x02))
                        {
                            LEDB = 0x64
                            Return (LEDB) /* \_SB_.PC00.AWMI.WMAA.LEDB */
                        }
                        Else
                        {
                            Return (One)
                        }
                    }
                    Case (0x02)
                    {
                        If ((MODF == Zero))
                        {
                            If ((\_SB.PC00.LPCB.HDRP == 0x33))
                            {
                                LEDB = 0x03
                            }
                            ElseIf ((\_SB.PC00.LPCB.IVGA == One))
                            {
                                LEDB = One
                            }
                            Else
                            {
                                LEDB = 0x02
                            }

                            Return (LEDB) /* \_SB_.PC00.AWMI.WMAA.LEDB */
                        }
                        ElseIf ((MODF == One))
                        {
                            If ((LEDB < 0x03))
                            {
                                Local0 = Zero
                                If ((LEDB == One))
                                {
                                    Local0 = One
                                }

                                \_SB.PC00.LPCB.SECC (0x40, Local0)
                                Return (Zero)
                            }

                            Return (One)
                        }
                        Else
                        {
                            Return (One)
                        }
                    }
                    Default
                    {
                        Return (One)
                    }

                }
            }
        }
    }
}

Firmware Error (ACPI): Failure creating named object [\_SB.PC00.PEG1.PEGP._DOD], AE_ALREADY_EXISTS (20220331/dswload-387)
Could not parse ACPI tables, AE_ALREADY_EXISTS
