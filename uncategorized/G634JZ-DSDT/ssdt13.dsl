/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt13.dat, Sat Jun 17 14:34:24 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00002D09 (11529)
 *     Revision         0x02
 *     Checksum         0x38
 *     OEM ID           "DptfTb"
 *     OEM Table ID     "DptfTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "DptfTb", "DptfTabl", 0x00001000)
{
    External (_SB_.AAC0, FieldUnitObj)
    External (_SB_.ACRT, FieldUnitObj)
    External (_SB_.APSV, FieldUnitObj)
    External (_SB_.CBMI, FieldUnitObj)
    External (_SB_.CFGD, FieldUnitObj)
    External (_SB_.CLVL, FieldUnitObj)
    External (_SB_.CPPC, FieldUnitObj)
    External (_SB_.CTC0, FieldUnitObj)
    External (_SB_.CTC1, FieldUnitObj)
    External (_SB_.CTC2, FieldUnitObj)
    External (_SB_.OSCP, IntObj)
    External (_SB_.PAGD, DeviceObj)
    External (_SB_.PAGD._PUR, PkgObj)
    External (_SB_.PAGD._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.LPCB, DeviceObj)
    External (_SB_.PC00.LPCB.BATM, FieldUnitObj)
    External (_SB_.PC00.LPCB.CLOT, FieldUnitObj)
    External (_SB_.PC00.LPCB.CTMP, FieldUnitObj)
    External (_SB_.PC00.LPCB.ECOK, IntObj)
    External (_SB_.PC00.LPCB.M662, FieldUnitObj)
    External (_SB_.PC00.LPCB.MUT0, MutexObj)
    External (_SB_.PC00.LPCB.Q4HI, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4HU, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LO, FieldUnitObj)
    External (_SB_.PC00.LPCB.Q4LU, FieldUnitObj)
    External (_SB_.PC00.LPCB.SEN2, DeviceObj)
    External (_SB_.PC00.LPCB.SEN3, DeviceObj)
    External (_SB_.PC00.LPCB.VRTT, UnknownObj)
    External (_SB_.PC00.MC__.MHBR, FieldUnitObj)
    External (_SB_.PC00.TCPU, DeviceObj)
    External (_SB_.PL10, FieldUnitObj)
    External (_SB_.PL11, FieldUnitObj)
    External (_SB_.PL12, FieldUnitObj)
    External (_SB_.PL20, FieldUnitObj)
    External (_SB_.PL21, FieldUnitObj)
    External (_SB_.PL22, FieldUnitObj)
    External (_SB_.PLW0, FieldUnitObj)
    External (_SB_.PLW1, FieldUnitObj)
    External (_SB_.PLW2, FieldUnitObj)
    External (_SB_.PR00, ProcessorObj)
    External (_SB_.PR00._PSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TPC, IntObj)
    External (_SB_.PR00._TSD, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00.LPSS, PkgObj)
    External (_SB_.PR00.TPSS, PkgObj)
    External (_SB_.PR00.TSMC, PkgObj)
    External (_SB_.PR00.TSMF, PkgObj)
    External (_SB_.PR01, ProcessorObj)
    External (_SB_.PR02, ProcessorObj)
    External (_SB_.PR03, ProcessorObj)
    External (_SB_.PR04, ProcessorObj)
    External (_SB_.PR05, ProcessorObj)
    External (_SB_.PR06, ProcessorObj)
    External (_SB_.PR07, ProcessorObj)
    External (_SB_.PR08, ProcessorObj)
    External (_SB_.PR09, ProcessorObj)
    External (_SB_.PR10, ProcessorObj)
    External (_SB_.PR11, ProcessorObj)
    External (_SB_.PR12, ProcessorObj)
    External (_SB_.PR13, ProcessorObj)
    External (_SB_.PR14, ProcessorObj)
    External (_SB_.PR15, ProcessorObj)
    External (_SB_.PR16, ProcessorObj)
    External (_SB_.PR17, ProcessorObj)
    External (_SB_.PR18, ProcessorObj)
    External (_SB_.PR19, ProcessorObj)
    External (_SB_.PR20, ProcessorObj)
    External (_SB_.PR21, ProcessorObj)
    External (_SB_.PR22, ProcessorObj)
    External (_SB_.PR23, ProcessorObj)
    External (_SB_.PR24, ProcessorObj)
    External (_SB_.PR25, ProcessorObj)
    External (_SB_.PR26, ProcessorObj)
    External (_SB_.PR27, ProcessorObj)
    External (_SB_.PR28, ProcessorObj)
    External (_SB_.PR29, ProcessorObj)
    External (_SB_.PR30, ProcessorObj)
    External (_SB_.PR31, ProcessorObj)
    External (_SB_.SLPB, DeviceObj)
    External (_SB_.TAR0, FieldUnitObj)
    External (_SB_.TAR1, FieldUnitObj)
    External (_SB_.TAR2, FieldUnitObj)
    External (_TZ_.ETMD, IntObj)
    External (_TZ_.THRM, ThermalZoneObj)
    External (ACTT, IntObj)
    External (ATPC, IntObj)
    External (BATR, IntObj)
    External (CHGE, IntObj)
    External (CRTT, IntObj)
    External (DCFE, IntObj)
    External (DPTF, IntObj)
    External (ECON, IntObj)
    External (FND1, IntObj)
    External (FND2, IntObj)
    External (FND3, IntObj)
    External (HIDW, MethodObj)    // 4 Arguments
    External (HIWC, MethodObj)    // 1 Arguments
    External (IN34, IntObj)
    External (ODV0, IntObj)
    External (ODV1, IntObj)
    External (ODV2, IntObj)
    External (ODV3, IntObj)
    External (ODV4, IntObj)
    External (ODV5, IntObj)
    External (PCHE, FieldUnitObj)
    External (PF00, IntObj)
    External (PLID, IntObj)
    External (PNHM, IntObj)
    External (PPPR, IntObj)
    External (PPSZ, IntObj)
    External (PSVT, IntObj)
    External (PTPC, IntObj)
    External (PWRE, IntObj)
    External (PWRS, IntObj)
    External (S1DE, IntObj)
    External (S2DE, IntObj)
    External (S3DE, IntObj)
    External (S4DE, IntObj)
    External (S5DE, IntObj)
    External (S6DE, IntObj)
    External (S6P2, IntObj)
    External (SADE, IntObj)
    External (SSP1, IntObj)
    External (SSP2, IntObj)
    External (SSP3, IntObj)
    External (SSP4, IntObj)
    External (SSP5, IntObj)
    External (TCNT, IntObj)
    External (TSOD, IntObj)

    Scope (\_SB)
    {
        Device (IETM)
        {
            Method (GHID, 1, Serialized)
            {
                If ((Arg0 == "IETM"))
                {
                    Return ("INTC1041")
                }

                If ((Arg0 == "SEN1"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN2"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN3"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN4"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN5"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "TPCH"))
                {
                    Return ("INTC1049")
                }

                If ((Arg0 == "TFN1"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN2"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN3"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TPWR"))
                {
                    Return ("INTC1060")
                }

                If ((Arg0 == "1"))
                {
                    Return ("INTC1061")
                }

                If ((Arg0 == "CHRG"))
                {
                    Return ("INTC1046")
                }

                Return ("XXXX9999")
            }

            Name (_UID, "IETM")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If (CondRefOf (HIWC))
                {
                    If (HIWC (Arg0))
                    {
                        If (CondRefOf (HIDW))
                        {
                            Return (HIDW (Arg0, Arg1, Arg2, Arg3))
                        }
                    }
                }

                Return (Buffer (One)
                {
                     0x00                                             // .
                })
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If (((\DPTF == One) && (\IN34 == One)))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Name (PTRP, Zero)
            Name (PSEM, Zero)
            Name (ATRP, Zero)
            Name (ASEM, Zero)
            Name (YTRP, Zero)
            Name (YSEM, Zero)
            Method (_OSC, 4, Serialized)  // _OSC: Operating System Capabilities
            {
                CreateDWordField (Arg3, Zero, STS1)
                CreateDWordField (Arg3, 0x04, CAP1)
                If ((Arg1 != One))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x0A
                    Return (Arg3)
                }

                If ((Arg2 != 0x02))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x02
                    Return (Arg3)
                }

                If (CondRefOf (\_SB.APSV))
                {
                    If ((PSEM == Zero))
                    {
                        PSEM = One
                        PTRP = \_SB.APSV /* External reference */
                    }
                }

                If (CondRefOf (\_SB.AAC0))
                {
                    If ((ASEM == Zero))
                    {
                        ASEM = One
                        ATRP = \_SB.AAC0 /* External reference */
                    }
                }

                If (CondRefOf (\_SB.ACRT))
                {
                    If ((YSEM == Zero))
                    {
                        YSEM = One
                        YTRP = \_SB.ACRT /* External reference */
                    }
                }

                If ((Arg0 == ToUUID ("b23ba85d-c8b7-3542-88de-8de2ffcfd698") /* Unknown UUID */))
                {
                    If (~(STS1 & One))
                    {
                        If ((CAP1 & One))
                        {
                            If ((CAP1 & 0x02))
                            {
                                \_SB.AAC0 = 0x6E
                                \_TZ.ETMD = Zero
                            }
                            Else
                            {
                                \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                                \_TZ.ETMD = One
                            }

                            If ((CAP1 & 0x04))
                            {
                                \_SB.APSV = 0x6E
                            }
                            Else
                            {
                                \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            }

                            If ((CAP1 & 0x08))
                            {
                                \_SB.ACRT = 0xD2
                            }
                            Else
                            {
                                \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            }
                        }
                        Else
                        {
                            \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                            \_TZ.ETMD = One
                        }

                        Notify (\_TZ.THRM, 0x81) // Information Change
                    }

                    Return (Arg3)
                }

                Return (Arg3)
            }

            Method (DCFG, 0, NotSerialized)
            {
                Return (\DCFE) /* External reference */
            }

            Name (ODVX, Package (0x06)
            {
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero
            })
            Method (ODVP, 0, Serialized)
            {
                ODVX [Zero] = \ODV0 /* External reference */
                ODVX [One] = \ODV1 /* External reference */
                ODVX [0x02] = \ODV2 /* External reference */
                ODVX [0x03] = \ODV3 /* External reference */
                ODVX [0x04] = \ODV4 /* External reference */
                ODVX [0x05] = \ODV5 /* External reference */
                Return (ODVX) /* \_SB_.IETM.ODVX */
            }

            Scope (\_SB.PC00.TCPU)
            {
                Name (PFLG, Zero)
                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    If ((\SADE == One))
                    {
                        Return (0x0F)
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                OperationRegion (CPWR, SystemMemory, ((\_SB.PC00.MC.MHBR << 0x0F) + 0x5000), 0x1000)
                Field (CPWR, ByteAcc, NoLock, Preserve)
                {
                    Offset (0x930), 
                    PTDP,   15, 
                    Offset (0x932), 
                    PMIN,   15, 
                    Offset (0x934), 
                    PMAX,   15, 
                    Offset (0x936), 
                    TMAX,   7, 
                    Offset (0x938), 
                    PWRU,   4, 
                    Offset (0x939), 
                    EGYU,   5, 
                    Offset (0x93A), 
                    TIMU,   4, 
                    Offset (0x958), 
                    Offset (0x95C), 
                    LPMS,   1, 
                    CTNL,   2, 
                    Offset (0x978), 
                    PCTP,   8, 
                    Offset (0x998), 
                    RP0C,   8, 
                    RP1C,   8, 
                    RPNC,   8, 
                    Offset (0xF3C), 
                    TRAT,   8, 
                    Offset (0xF40), 
                    PTD1,   15, 
                    Offset (0xF42), 
                    TRA1,   8, 
                    Offset (0xF44), 
                    PMX1,   15, 
                    Offset (0xF46), 
                    PMN1,   15, 
                    Offset (0xF48), 
                    PTD2,   15, 
                    Offset (0xF4A), 
                    TRA2,   8, 
                    Offset (0xF4C), 
                    PMX2,   15, 
                    Offset (0xF4E), 
                    PMN2,   15, 
                    Offset (0xF50), 
                    CTCL,   2, 
                        ,   29, 
                    CLCK,   1, 
                    MNTR,   8
                }

                Name (XPCC, Zero)
                Method (PPCC, 0, Serialized)
                {
                    If (((XPCC == Zero) && CondRefOf (\_SB.CBMI)))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                If (((\_SB.CLVL >= One) && (\_SB.CLVL <= 0x03)))
                                {
                                    CPL0 ()
                                    XPCC = One
                                }
                            }
                            Case (One)
                            {
                                If (((\_SB.CLVL == 0x02) || (\_SB.CLVL == 0x03)))
                                {
                                    CPL1 ()
                                    XPCC = One
                                }
                            }
                            Case (0x02)
                            {
                                If ((\_SB.CLVL == 0x03))
                                {
                                    CPL2 ()
                                    XPCC = One
                                }
                            }

                        }
                    }

                    Return (NPCC) /* \_SB_.PC00.TCPU.NPCC */
                }

                Name (NPCC, Package (0x03)
                {
                    0x02, 
                    Package (0x06)
                    {
                        Zero, 
                        0x88B8, 
                        0xAFC8, 
                        0x6D60, 
                        0x7D00, 
                        0x03E8
                    }, 

                    Package (0x06)
                    {
                        One, 
                        0xDBBA, 
                        0xDBBA, 
                        Zero, 
                        Zero, 
                        0x03E8
                    }
                })
                Method (CPNU, 2, Serialized)
                {
                    Name (CNVT, Zero)
                    Name (PPUU, Zero)
                    Name (RMDR, Zero)
                    If ((PWRU == Zero))
                    {
                        PPUU = One
                    }
                    Else
                    {
                        PPUU = (PWRU-- << 0x02)
                    }

                    Divide (Arg0, PPUU, RMDR, CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    If ((Arg1 == Zero))
                    {
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                    Else
                    {
                        CNVT *= 0x03E8
                        RMDR *= 0x03E8
                        RMDR /= PPUU
                        CNVT += RMDR /* \_SB_.PC00.TCPU.CPNU.RMDR */
                        Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
                    }
                }

                Method (CPL0, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL10, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW0 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW0 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL20, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL1, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL11, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW1 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW1 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL21, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Method (CPL2, 0, NotSerialized)
                {
                    \_SB.PC00.TCPU.NPCC [Zero] = 0x02
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL12, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW2 * 0x03E8)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW2 * 0x03E8
                        ) + 0x0FA0)
                    DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL22, One)
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
                    DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
                }

                Name (LSTM, Zero)
                Name (_PPC, Zero)  // _PPC: Performance Present Capabilities
                Method (SPPC, 1, Serialized)
                {
                    If (CondRefOf (\_SB.CPPC))
                    {
                        \_SB.CPPC = Arg0
                    }

                    If ((ToInteger (\TCNT) > Zero))
                    {
                        Notify (\_SB.PR00, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > One))
                    {
                        Notify (\_SB.PR01, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x02))
                    {
                        Notify (\_SB.PR02, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x03))
                    {
                        Notify (\_SB.PR03, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x04))
                    {
                        Notify (\_SB.PR04, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x05))
                    {
                        Notify (\_SB.PR05, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x06))
                    {
                        Notify (\_SB.PR06, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x07))
                    {
                        Notify (\_SB.PR07, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x08))
                    {
                        Notify (\_SB.PR08, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x09))
                    {
                        Notify (\_SB.PR09, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0A))
                    {
                        Notify (\_SB.PR10, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0B))
                    {
                        Notify (\_SB.PR11, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0C))
                    {
                        Notify (\_SB.PR12, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0D))
                    {
                        Notify (\_SB.PR13, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0E))
                    {
                        Notify (\_SB.PR14, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x0F))
                    {
                        Notify (\_SB.PR15, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x10))
                    {
                        Notify (\_SB.PR16, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x11))
                    {
                        Notify (\_SB.PR17, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x12))
                    {
                        Notify (\_SB.PR18, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x13))
                    {
                        Notify (\_SB.PR19, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x14))
                    {
                        Notify (\_SB.PR20, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x15))
                    {
                        Notify (\_SB.PR21, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x16))
                    {
                        Notify (\_SB.PR22, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x17))
                    {
                        Notify (\_SB.PR23, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x18))
                    {
                        Notify (\_SB.PR24, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x19))
                    {
                        Notify (\_SB.PR25, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1A))
                    {
                        Notify (\_SB.PR26, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1B))
                    {
                        Notify (\_SB.PR27, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1C))
                    {
                        Notify (\_SB.PR28, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1D))
                    {
                        Notify (\_SB.PR29, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1E))
                    {
                        Notify (\_SB.PR30, 0x80) // Status Change
                    }

                    If ((ToInteger (\TCNT) > 0x1F))
                    {
                        Notify (\_SB.PR31, 0x80) // Status Change
                    }
                }

                Method (SPUR, 1, NotSerialized)
                {
                    If ((Arg0 <= \TCNT))
                    {
                        If ((\_SB.PAGD._STA () == 0x0F))
                        {
                            \_SB.PAGD._PUR [One] = Arg0
                            Notify (\_SB.PAGD, 0x80) // Status Change
                        }
                    }
                }

                Method (PCCC, 0, Serialized)
                {
                    PCCX [Zero] = One
                    Switch (ToInteger (CPNU (PTDP, Zero)))
                    {
                        Case (0x39)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xA7F8
                            DerefOf (PCCX [One]) [One] = 0x00017318
                        }
                        Case (0x2F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x9858
                            DerefOf (PCCX [One]) [One] = 0x00014C08
                        }
                        Case (0x25)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x7148
                            DerefOf (PCCX [One]) [One] = 0xD6D8
                        }
                        Case (0x19)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x3E80
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0F)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x7D00
                        }
                        Case (0x0B)
                        {
                            DerefOf (PCCX [One]) [Zero] = 0x36B0
                            DerefOf (PCCX [One]) [One] = 0x61A8
                        }
                        Default
                        {
                            DerefOf (PCCX [One]) [Zero] = 0xFF
                            DerefOf (PCCX [One]) [One] = 0xFF
                        }

                    }

                    Return (PCCX) /* \_SB_.PC00.TCPU.PCCX */
                }

                Name (PCCX, Package (0x02)
                {
                    0x80000000, 
                    Package (0x02)
                    {
                        0x80000000, 
                        0x80000000
                    }
                })
                Name (KEFF, Package (0x1E)
                {
                    Package (0x02)
                    {
                        0x01BC, 
                        Zero
                    }, 

                    Package (0x02)
                    {
                        0x01CF, 
                        0x27
                    }, 

                    Package (0x02)
                    {
                        0x01E1, 
                        0x4B
                    }, 

                    Package (0x02)
                    {
                        0x01F3, 
                        0x6C
                    }, 

                    Package (0x02)
                    {
                        0x0206, 
                        0x8B
                    }, 

                    Package (0x02)
                    {
                        0x0218, 
                        0xA8
                    }, 

                    Package (0x02)
                    {
                        0x022A, 
                        0xC3
                    }, 

                    Package (0x02)
                    {
                        0x023D, 
                        0xDD
                    }, 

                    Package (0x02)
                    {
                        0x024F, 
                        0xF4
                    }, 

                    Package (0x02)
                    {
                        0x0261, 
                        0x010B
                    }, 

                    Package (0x02)
                    {
                        0x0274, 
                        0x011F
                    }, 

                    Package (0x02)
                    {
                        0x032C, 
                        0x01BD
                    }, 

                    Package (0x02)
                    {
                        0x03D7, 
                        0x0227
                    }, 

                    Package (0x02)
                    {
                        0x048B, 
                        0x026D
                    }, 

                    Package (0x02)
                    {
                        0x053E, 
                        0x02A1
                    }, 

                    Package (0x02)
                    {
                        0x05F7, 
                        0x02C6
                    }, 

                    Package (0x02)
                    {
                        0x06A8, 
                        0x02E6
                    }, 

                    Package (0x02)
                    {
                        0x075D, 
                        0x02FF
                    }, 

                    Package (0x02)
                    {
                        0x0818, 
                        0x0311
                    }, 

                    Package (0x02)
                    {
                        0x08CF, 
                        0x0322
                    }, 

                    Package (0x02)
                    {
                        0x179C, 
                        0x0381
                    }, 

                    Package (0x02)
                    {
                        0x2DDC, 
                        0x039C
                    }, 

                    Package (0x02)
                    {
                        0x44A8, 
                        0x039E
                    }, 

                    Package (0x02)
                    {
                        0x5C35, 
                        0x0397
                    }, 

                    Package (0x02)
                    {
                        0x747D, 
                        0x038D
                    }, 

                    Package (0x02)
                    {
                        0x8D7F, 
                        0x0382
                    }, 

                    Package (0x02)
                    {
                        0xA768, 
                        0x0376
                    }, 

                    Package (0x02)
                    {
                        0xC23B, 
                        0x0369
                    }, 

                    Package (0x02)
                    {
                        0xDE26, 
                        0x035A
                    }, 

                    Package (0x02)
                    {
                        0xFB7C, 
                        0x034A
                    }
                })
                Name (CEUP, Package (0x06)
                {
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000, 
                    0x80000000
                })
                Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                {
                    LSTM = Arg0
                    Notify (\_SB.PC00.TCPU, 0x91) // Device-Specific
                }

                Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                {
                    Return (0x0ADE)
                }

                Name (PTYP, Zero)
                Method (_PSS, 0, NotSerialized)  // _PSS: Performance Supported States
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        Return (\_SB.PR00._PSS ())
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }, 

                            Package (0x06)
                            {
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TSS, 0, NotSerialized)  // _TSS: Throttling Supported States
                {
                    If (CondRefOf (\_SB.PR00._TSS))
                    {
                        Return (\_SB.PR00._TSS ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                One, 
                                Zero, 
                                Zero, 
                                Zero, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TPC, 0, NotSerialized)  // _TPC: Throttling Present Capabilities
                {
                    If (CondRefOf (\_SB.PR00._TPC))
                    {
                        Return (\_SB.PR00._TPC) /* External reference */
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PTC, 0, NotSerialized)  // _PTC: Processor Throttling Control
                {
                    If ((CondRefOf (\PF00) && (\PF00 != 0x80000000)))
                    {
                        If ((\PF00 & 0x04))
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (FFixedHW, 
                                        0x00,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000000000, // Address
                                        ,)
                                }
                            })
                        }
                        Else
                        {
                            Return (Package (0x02)
                            {
                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }, 

                                ResourceTemplate ()
                                {
                                    Register (SystemIO, 
                                        0x05,               // Bit Width
                                        0x00,               // Bit Offset
                                        0x0000000000001810, // Address
                                        ,)
                                }
                            })
                        }
                    }
                    Else
                    {
                        Return (Package (0x02)
                        {
                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }, 

                            ResourceTemplate ()
                            {
                                Register (FFixedHW, 
                                    0x00,               // Bit Width
                                    0x00,               // Bit Offset
                                    0x0000000000000000, // Address
                                    ,)
                            }
                        })
                    }
                }

                Method (_TSD, 0, NotSerialized)  // _TSD: Throttling State Dependencies
                {
                    If (CondRefOf (\_SB.PR00._TSD))
                    {
                        Return (\_SB.PR00._TSD ())
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Package (0x05)
                            {
                                0x05, 
                                Zero, 
                                Zero, 
                                0xFC, 
                                Zero
                            }
                        })
                    }
                }

                Method (_TDL, 0, NotSerialized)  // _TDL: T-State Depth Limit
                {
                    If ((CondRefOf (\_SB.PR00._TSS) && CondRefOf (\_SB.CFGD)))
                    {
                        If ((\_SB.CFGD & 0x2000))
                        {
                            Return ((SizeOf (\_SB.PR00.TSMF) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.TSMC) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }

                Method (_PDL, 0, NotSerialized)  // _PDL: P-state Depth Limit
                {
                    If (CondRefOf (\_SB.PR00._PSS))
                    {
                        If ((\_SB.OSCP & 0x0400))
                        {
                            Return ((SizeOf (\_SB.PR00.TPSS) - One))
                        }
                        Else
                        {
                            Return ((SizeOf (\_SB.PR00.LPSS) - One))
                        }
                    }
                    Else
                    {
                        Return (Zero)
                    }
                }
            }

            Scope (\_SB.IETM)
            {
                Name (CTSP, Package (0x01)
                {
                    ToUUID ("e145970a-e4c1-4d73-900e-c9c5a69dd067") /* Unknown UUID */
                })
            }

            Scope (\_SB.PC00.TCPU)
            {
                Method (TDPL, 0, Serialized)
                {
                    Name (AAAA, Zero)
                    Name (BBBB, Zero)
                    Name (CCCC, Zero)
                    Local0 = CTNL /* \_SB_.PC00.TCPU.CTNL */
                    If (((Local0 == One) || (Local0 == 0x02)))
                    {
                        Local0 = \_SB.CLVL /* External reference */
                    }
                    Else
                    {
                        Return (Package (0x01)
                        {
                            Zero
                        })
                    }

                    If ((CLCK == One))
                    {
                        Local0 = One
                    }

                    AAAA = CPNU (\_SB.PL10, One)
                    BBBB = CPNU (\_SB.PL11, One)
                    CCCC = CPNU (\_SB.PL12, One)
                    Name (TMP1, Package (0x01)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP2, Package (0x02)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    Name (TMP3, Package (0x03)
                    {
                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }, 

                        Package (0x05)
                        {
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000, 
                            0x80000000
                        }
                    })
                    If ((Local0 == 0x03))
                    {
                        If ((AAAA > BBBB))
                        {
                            If ((AAAA > CCCC))
                            {
                                If ((BBBB > CCCC))
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local4 = One
                                    LEV1 = One
                                    Local5 = 0x02
                                    LEV2 = 0x02
                                }
                                Else
                                {
                                    Local3 = Zero
                                    LEV0 = Zero
                                    Local5 = One
                                    LEV1 = 0x02
                                    Local4 = 0x02
                                    LEV2 = One
                                }
                            }
                            Else
                            {
                                Local5 = Zero
                                LEV0 = 0x02
                                Local3 = One
                                LEV1 = Zero
                                Local4 = 0x02
                                LEV2 = One
                            }
                        }
                        ElseIf ((BBBB > CCCC))
                        {
                            If ((AAAA > CCCC))
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local3 = One
                                LEV1 = Zero
                                Local5 = 0x02
                                LEV2 = 0x02
                            }
                            Else
                            {
                                Local4 = Zero
                                LEV0 = One
                                Local5 = One
                                LEV1 = 0x02
                                Local3 = 0x02
                                LEV2 = Zero
                            }
                        }
                        Else
                        {
                            Local5 = Zero
                            LEV0 = 0x02
                            Local4 = One
                            LEV1 = One
                            Local3 = 0x02
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP3 [Local3]) [One] = Local2
                        DerefOf (TMP3 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP3 [Local3]) [0x03] = Local1
                        DerefOf (TMP3 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP3 [Local4]) [One] = Local2
                        DerefOf (TMP3 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP3 [Local4]) [0x03] = Local1
                        DerefOf (TMP3 [Local4]) [0x04] = Zero
                        Local1 = (\_SB.TAR2 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP3 [Local5]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                        DerefOf (TMP3 [Local5]) [One] = Local2
                        DerefOf (TMP3 [Local5]) [0x02] = \_SB.CTC2 /* External reference */
                        DerefOf (TMP3 [Local5]) [0x03] = Local1
                        DerefOf (TMP3 [Local5]) [0x04] = Zero
                        Return (TMP3) /* \_SB_.PC00.TCPU.TDPL.TMP3 */
                    }

                    If ((Local0 == 0x02))
                    {
                        If ((AAAA > BBBB))
                        {
                            Local3 = Zero
                            Local4 = One
                            LEV0 = Zero
                            LEV1 = One
                            LEV2 = Zero
                        }
                        Else
                        {
                            Local4 = Zero
                            Local3 = One
                            LEV0 = One
                            LEV1 = Zero
                            LEV2 = Zero
                        }

                        Local1 = (\_SB.TAR0 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local3]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                        DerefOf (TMP2 [Local3]) [One] = Local2
                        DerefOf (TMP2 [Local3]) [0x02] = \_SB.CTC0 /* External reference */
                        DerefOf (TMP2 [Local3]) [0x03] = Local1
                        DerefOf (TMP2 [Local3]) [0x04] = Zero
                        Local1 = (\_SB.TAR1 + One)
                        Local2 = (Local1 * 0x64)
                        DerefOf (TMP2 [Local4]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                        DerefOf (TMP2 [Local4]) [One] = Local2
                        DerefOf (TMP2 [Local4]) [0x02] = \_SB.CTC1 /* External reference */
                        DerefOf (TMP2 [Local4]) [0x03] = Local1
                        DerefOf (TMP2 [Local4]) [0x04] = Zero
                        Return (TMP2) /* \_SB_.PC00.TCPU.TDPL.TMP2 */
                    }

                    If ((Local0 == One))
                    {
                        Switch (ToInteger (\_SB.CBMI))
                        {
                            Case (Zero)
                            {
                                Local1 = (\_SB.TAR0 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = AAAA /* \_SB_.PC00.TCPU.TDPL.AAAA */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC0 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = Zero
                                LEV1 = Zero
                                LEV2 = Zero
                            }
                            Case (One)
                            {
                                Local1 = (\_SB.TAR1 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = BBBB /* \_SB_.PC00.TCPU.TDPL.BBBB */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC1 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = One
                                LEV1 = One
                                LEV2 = One
                            }
                            Case (0x02)
                            {
                                Local1 = (\_SB.TAR2 + One)
                                Local2 = (Local1 * 0x64)
                                DerefOf (TMP1 [Zero]) [Zero] = CCCC /* \_SB_.PC00.TCPU.TDPL.CCCC */
                                DerefOf (TMP1 [Zero]) [One] = Local2
                                DerefOf (TMP1 [Zero]) [0x02] = \_SB.CTC2 /* External reference */
                                DerefOf (TMP1 [Zero]) [0x03] = Local1
                                DerefOf (TMP1 [Zero]) [0x04] = Zero
                                LEV0 = 0x02
                                LEV1 = 0x02
                                LEV2 = 0x02
                            }

                        }

                        Return (TMP1) /* \_SB_.PC00.TCPU.TDPL.TMP1 */
                    }

                    Return (Zero)
                }

                Name (MAXT, Zero)
                Method (TDPC, 0, NotSerialized)
                {
                    Return (MAXT) /* \_SB_.PC00.TCPU.MAXT */
                }

                Name (LEV0, Zero)
                Name (LEV1, Zero)
                Name (LEV2, Zero)
                Method (STDP, 1, Serialized)
                {
                    If ((Arg0 >= \_SB.CLVL))
                    {
                        Return (Zero)
                    }

                    Switch (ToInteger (Arg0))
                    {
                        Case (Zero)
                        {
                            Local0 = LEV0 /* \_SB_.PC00.TCPU.LEV0 */
                        }
                        Case (One)
                        {
                            Local0 = LEV1 /* \_SB_.PC00.TCPU.LEV1 */
                        }
                        Case (0x02)
                        {
                            Local0 = LEV2 /* \_SB_.PC00.TCPU.LEV2 */
                        }

                    }

                    Switch (ToInteger (Local0))
                    {
                        Case (Zero)
                        {
                            CPL0 ()
                        }
                        Case (One)
                        {
                            CPL1 ()
                        }
                        Case (0x02)
                        {
                            CPL2 ()
                        }

                    }

                    Notify (\_SB.PC00.TCPU, 0x83) // Device-Specific Change
                }
            }

            Scope (\_SB.IETM)
            {
                Name (PTTL, 0x14)
                Name (PSVT, Package (0x04)
                {
                    0x02, 
                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.TCPU, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN2, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }, 

                    Package (0x0C)
                    {
                        \_SB.PC00.TCPU, 
                        \_SB.PC00.LPCB.SEN3, 
                        0x02, 
                        0x012C, 
                        0x0C6E, 
                        0x09, 
                        0x00010000, 
                        "MAX", 
                        0x01F4, 
                        0x0A, 
                        0x14, 
                        Zero
                    }
                })
            }

            Scope (\_SB.PC00.LPCB)
            {
                Device (SEN3)
                {
                    Name (_HID, "INTC1046")  // _HID: Hardware ID
                    Name (_UID, "SEN3")  // _UID: Unique ID
                    Name (_STR, Unicode ("Remote GT Sensor"))  // _STR: Description String
                    Name (PTYP, 0x03)
                    Name (CTYP, Zero)
                    Name (PFLG, Zero)
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        If ((\S3DE == One))
                        {
                            Return (0x0F)
                        }
                        Else
                        {
                            Return (Zero)
                        }
                    }

                    Method (_TMP, 0, Serialized)  // _TMP: Temperature
                    {
                        Return (\_SB.IETM.CTOK (\_SB.PC00.LPCB.M662))
                    }

                    Name (PATC, 0x02)
                    Method (PAT0, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4LO = Local1
                        \_SB.PC00.LPCB.Q4LU = One
                    }

                    Method (PAT1, 1, Serialized)
                    {
                        Local1 = \_SB.IETM.KTOC (Arg0)
                        \_SB.PC00.LPCB.Q4HI = Local1
                        \_SB.PC00.LPCB.Q4HU = One
                    }

                    Name (GTSH, 0x14)
                    Name (LSTM, Zero)
                    Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
                    {
                        LSTM = Arg0
                        Notify (\_SB.PC00.LPCB.SEN3, 0x91) // Device-Specific
                    }

                    Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
                    {
                        Return (0x0ADE)
                    }

                    Name (S3AC, 0x3C)
                    Name (S3A1, 0x32)
                    Name (S3A2, 0x28)
                    Name (S3PV, 0x41)
                    Name (S3CC, 0x50)
                    Name (S3C3, 0x46)
                    Name (S3HP, 0x4B)
                    Name (SSP3, Zero)
                    Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
                    {
                        Return (SSP3) /* \_SB_.PC00.LPCB.SEN3.SSP3 */
                    }

                    Method (_AC3, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Local1 = \_SB.IETM.CTOK (S3AC)
                        If ((LSTM >= Local1))
                        {
                            Return ((Local1 - 0x14))
                        }
                        Else
                        {
                            Return (Local1)
                        }
                    }

                    Method (_AC4, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A1))
                    }

                    Method (_AC5, 0, Serialized)  // _ACx: Active Cooling, x=0-9
                    {
                        Return (\_SB.IETM.CTOK (S3A2))
                    }

                    Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3PV))
                    }

                    Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3CC))
                    }

                    Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3C3))
                    }

                    Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
                    {
                        Return (\_SB.IETM.CTOK (S3HP))
                    }
                }
            }

            Method (GDDV, 0, Serialized)
            {
                Return (Package (0x01)
                {
                    Buffer (0x05E9)
                    {
                        /* 0000 */  0xE5, 0x1F, 0x94, 0x00, 0x00, 0x00, 0x00, 0x02,  // ........
                        /* 0008 */  0x00, 0x00, 0x00, 0x40, 0x67, 0x64, 0x64, 0x76,  // ...@gddv
                        /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0020 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0028 */  0x00, 0x00, 0x00, 0x00, 0x4F, 0x45, 0x4D, 0x20,  // ....OEM 
                        /* 0030 */  0x45, 0x78, 0x70, 0x6F, 0x72, 0x74, 0x65, 0x64,  // Exported
                        /* 0038 */  0x20, 0x44, 0x61, 0x74, 0x61, 0x56, 0x61, 0x75,  //  DataVau
                        /* 0040 */  0x6C, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // lt......
                        /* 0048 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0050 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0058 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0060 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0068 */  0x00, 0x00, 0x00, 0x00, 0x78, 0x77, 0x17, 0x05,  // ....xw..
                        /* 0070 */  0x6E, 0xD1, 0xD1, 0xFD, 0x7A, 0x32, 0x66, 0xC9,  // n...z2f.
                        /* 0078 */  0xE2, 0x68, 0xCE, 0x77, 0x3E, 0x9A, 0x94, 0xFC,  // .h.w>...
                        /* 0080 */  0x6E, 0x64, 0xFB, 0x47, 0x94, 0x83, 0xFA, 0x43,  // nd.G...C
                        /* 0088 */  0x07, 0x97, 0x1E, 0xA8, 0x55, 0x05, 0x00, 0x00,  // ....U...
                        /* 0090 */  0x52, 0x45, 0x50, 0x4F, 0x5D, 0x00, 0x00, 0x00,  // REPO]...
                        /* 0098 */  0x01, 0xA6, 0x4D, 0x00, 0x00, 0x00, 0x00, 0x00,  // ..M.....
                        /* 00A0 */  0x00, 0x00, 0x72, 0x87, 0xCD, 0xFF, 0x6D, 0x24,  // ..r...m$
                        /* 00A8 */  0x47, 0xDB, 0x3D, 0x24, 0x92, 0xB4, 0x16, 0x6F,  // G.=$...o
                        /* 00B0 */  0x45, 0xD8, 0xC3, 0xF5, 0x66, 0x14, 0x9F, 0x22,  // E...f.."
                        /* 00B8 */  0xD7, 0xF7, 0xDE, 0x67, 0x90, 0x9A, 0xA2, 0x0D,  // ...g....
                        /* 00C0 */  0x39, 0x25, 0xAD, 0xC3, 0x1A, 0xAD, 0x52, 0x0B,  // 9%....R.
                        /* 00C8 */  0x75, 0x38, 0xE1, 0xA4, 0x14, 0x41, 0x89, 0x20,  // u8...A. 
                        /* 00D0 */  0xE7, 0xBF, 0x0A, 0xD0, 0x30, 0xFC, 0xA9, 0xB8,  // ....0...
                        /* 00D8 */  0x14, 0xF5, 0xDF, 0xAE, 0xF2, 0xFA, 0x10, 0x08,  // ........
                        /* 00E0 */  0xED, 0xB9, 0x65, 0x37, 0xCB, 0x2A, 0x71, 0xFE,  // ..e7.*q.
                        /* 00E8 */  0x87, 0xB2, 0x54, 0x65, 0x29, 0xA7, 0x66, 0x31,  // ..Te).f1
                        /* 00F0 */  0x59, 0x71, 0x81, 0x9E, 0x38, 0x47, 0x35, 0x0F,  // Yq..8G5.
                        /* 00F8 */  0xFC, 0x36, 0xEB, 0xC1, 0x3B, 0x1F, 0xE6, 0x6D,  // .6..;..m
                        /* 0100 */  0x9C, 0x01, 0xB8, 0x68, 0x27, 0xEB, 0x08, 0x1D,  // ...h'...
                        /* 0108 */  0x83, 0x84, 0xAB, 0x28, 0x82, 0x78, 0xE6, 0x71,  // ...(.x.q
                        /* 0110 */  0xEC, 0x47, 0xA4, 0x89, 0x48, 0x62, 0x9E, 0xA5,  // .G..Hb..
                        /* 0118 */  0x29, 0x49, 0x82, 0xD9, 0x38, 0xB6, 0x7A, 0x1B,  // )I..8.z.
                        /* 0120 */  0x28, 0x65, 0xD5, 0x48, 0x2C, 0x77, 0x72, 0x7F,  // (e.H,wr.
                        /* 0128 */  0x2D, 0xAE, 0xCE, 0xA8, 0x55, 0x10, 0x6E, 0x9E,  // -...U.n.
                        /* 0130 */  0x8C, 0x6B, 0x64, 0x74, 0xE4, 0x9D, 0xF1, 0xCC,  // .kdt....
                        /* 0138 */  0xB6, 0x83, 0x76, 0x16, 0xFD, 0x71, 0x42, 0x58,  // ..v..qBX
                        /* 0140 */  0xAE, 0x8A, 0x75, 0xE5, 0x90, 0x61, 0xFC, 0x04,  // ..u..a..
                        /* 0148 */  0xBC, 0xF0, 0x9B, 0x74, 0x58, 0x4C, 0x53, 0xCB,  // ...tXLS.
                        /* 0150 */  0x07, 0x15, 0xD4, 0x58, 0xA8, 0x97, 0xD4, 0xDE,  // ...X....
                        /* 0158 */  0x67, 0xF0, 0xB6, 0x77, 0x4C, 0xBD, 0x30, 0x9B,  // g..wL.0.
                        /* 0160 */  0x62, 0x50, 0x6B, 0x31, 0xA0, 0x92, 0x9F, 0xAD,  // bPk1....
                        /* 0168 */  0xAC, 0x42, 0xCA, 0x1D, 0x1D, 0x37, 0xD1, 0xB4,  // .B...7..
                        /* 0170 */  0x9E, 0x5F, 0xE4, 0xD9, 0x95, 0xB5, 0x00, 0x7E,  // ._.....~
                        /* 0178 */  0x8D, 0x22, 0x2B, 0xF0, 0x83, 0xC1, 0xA7, 0x63,  // ."+....c
                        /* 0180 */  0x1C, 0xD7, 0x37, 0x17, 0x5E, 0x01, 0x2C, 0xBD,  // ..7.^.,.
                        /* 0188 */  0x13, 0x59, 0x9B, 0x4A, 0xA4, 0x22, 0x22, 0x32,  // .Y.J.""2
                        /* 0190 */  0xB8, 0x1B, 0x4F, 0x23, 0xAB, 0x7E, 0x0B, 0xB1,  // ..O#.~..
                        /* 0198 */  0xAB, 0x90, 0x97, 0xE2, 0x42, 0x13, 0x90, 0xB7,  // ....B...
                        /* 01A0 */  0xED, 0xC5, 0xE8, 0x31, 0x95, 0x0E, 0x44, 0x17,  // ...1..D.
                        /* 01A8 */  0xBF, 0x4E, 0xB6, 0x75, 0x23, 0xEB, 0xF5, 0x5F,  // .N.u#.._
                        /* 01B0 */  0x37, 0x40, 0x00, 0xD9, 0xBB, 0x1D, 0x38, 0xD0,  // 7@....8.
                        /* 01B8 */  0xE0, 0x9C, 0xA6, 0x8D, 0x82, 0x66, 0xCE, 0x8C,  // .....f..
                        /* 01C0 */  0x80, 0x78, 0xB6, 0x63, 0xC6, 0xEB, 0xDF, 0x56,  // .x.c...V
                        /* 01C8 */  0xB2, 0x61, 0x9F, 0xC6, 0x7A, 0x29, 0xF2, 0x91,  // .a..z)..
                        /* 01D0 */  0x07, 0x30, 0xDC, 0x3E, 0x92, 0xA0, 0x55, 0xA8,  // .0.>..U.
                        /* 01D8 */  0x7A, 0x54, 0xB1, 0xA1, 0x53, 0x95, 0x3C, 0xA0,  // zT..S.<.
                        /* 01E0 */  0xEF, 0x17, 0x21, 0x76, 0x19, 0x2B, 0x9A, 0xA3,  // ..!v.+..
                        /* 01E8 */  0x2F, 0x08, 0x55, 0x7D, 0x38, 0x42, 0x1C, 0xE3,  // /.U}8B..
                        /* 01F0 */  0x7F, 0x77, 0x7D, 0x7A, 0x5E, 0xCF, 0x91, 0x73,  // .w}z^..s
                        /* 01F8 */  0x94, 0x54, 0xFE, 0x0C, 0x52, 0x5C, 0xC1, 0x81,  // .T..R\..
                        /* 0200 */  0xCA, 0x27, 0x1A, 0xF4, 0xCA, 0x1B, 0x74, 0x12,  // .'....t.
                        /* 0208 */  0x66, 0xAA, 0x10, 0xF7, 0x2F, 0xCA, 0xDB, 0x37,  // f.../..7
                        /* 0210 */  0x16, 0xB1, 0x43, 0x27, 0xC0, 0x35, 0x97, 0x02,  // ..C'.5..
                        /* 0218 */  0x3C, 0xF6, 0x36, 0xFA, 0xB9, 0x06, 0x7D, 0xA7,  // <.6...}.
                        /* 0220 */  0x88, 0x5D, 0xDD, 0x0D, 0x34, 0x76, 0x71, 0x31,  // .]..4vq1
                        /* 0228 */  0x25, 0xE1, 0xE6, 0x57, 0x9E, 0x8F, 0x97, 0xBD,  // %..W....
                        /* 0230 */  0x9C, 0x5A, 0xBE, 0xFF, 0xFF, 0x44, 0x44, 0x8B,  // .Z...DD.
                        /* 0238 */  0xFC, 0xF0, 0x67, 0x96, 0x20, 0x90, 0x4F, 0x69,  // ..g. .Oi
                        /* 0240 */  0x41, 0x80, 0x05, 0x32, 0x76, 0x2E, 0x41, 0xED,  // A..2v.A.
                        /* 0248 */  0xED, 0x75, 0x5F, 0x72, 0xA9, 0x99, 0xEB, 0xDC,  // .u_r....
                        /* 0250 */  0xE1, 0x53, 0x56, 0x7B, 0xDA, 0x1A, 0x7B, 0xF4,  // .SV{..{.
                        /* 0258 */  0xE3, 0x19, 0xD9, 0xC0, 0x8B, 0x2F, 0x46, 0x54,  // ...../FT
                        /* 0260 */  0x28, 0x82, 0xF0, 0xC2, 0x30, 0x68, 0x51, 0x66,  // (...0hQf
                        /* 0268 */  0x69, 0x6B, 0x00, 0x52, 0x69, 0xF5, 0x20, 0xFC,  // ik.Ri. .
                        /* 0270 */  0xB6, 0x10, 0x9C, 0xD0, 0x26, 0x26, 0xFD, 0x56,  // ....&&.V
                        /* 0278 */  0x82, 0xB9, 0x33, 0x96, 0x5A, 0xCC, 0x6A, 0xA8,  // ..3.Z.j.
                        /* 0280 */  0x66, 0x24, 0xA7, 0x07, 0x70, 0x58, 0x02, 0x78,  // f$..pX.x
                        /* 0288 */  0xD0, 0x87, 0x22, 0x47, 0xDF, 0xD7, 0xA1, 0xA1,  // .."G....
                        /* 0290 */  0x4A, 0xE7, 0x7B, 0xBD, 0xA4, 0xEB, 0x71, 0x82,  // J.{...q.
                        /* 0298 */  0x45, 0x17, 0x7C, 0x8C, 0x6B, 0xC7, 0xC3, 0xA3,  // E.|.k...
                        /* 02A0 */  0xBF, 0x26, 0xFB, 0xC4, 0xF4, 0xFC, 0xF0, 0x61,  // .&.....a
                        /* 02A8 */  0xDD, 0x85, 0x27, 0xFA, 0xC8, 0x44, 0x0D, 0x75,  // ..'..D.u
                        /* 02B0 */  0x5D, 0x37, 0xED, 0x27, 0x4B, 0xF7, 0xFA, 0x4E,  // ]7.'K..N
                        /* 02B8 */  0x80, 0x6F, 0x8F, 0xA1, 0x51, 0x56, 0x60, 0xE2,  // .o..QV`.
                        /* 02C0 */  0x93, 0xCA, 0xD4, 0x05, 0x58, 0xCE, 0x09, 0x5E,  // ....X..^
                        /* 02C8 */  0xE4, 0x91, 0x66, 0x18, 0x89, 0x89, 0x18, 0x36,  // ..f....6
                        /* 02D0 */  0xE9, 0x45, 0x5E, 0xC3, 0x23, 0x99, 0x12, 0xC5,  // .E^.#...
                        /* 02D8 */  0xD9, 0xD5, 0x9D, 0xDC, 0xB5, 0x91, 0xBC, 0xC7,  // ........
                        /* 02E0 */  0x18, 0xB7, 0x67, 0xF6, 0x95, 0x43, 0x6A, 0x8D,  // ..g..Cj.
                        /* 02E8 */  0x5C, 0x59, 0x9B, 0x0C, 0xD9, 0x10, 0x4C, 0x8E,  // \Y....L.
                        /* 02F0 */  0x7A, 0xFF, 0xA5, 0xBF, 0xE6, 0x31, 0x31, 0x1E,  // z....11.
                        /* 02F8 */  0x9F, 0xA6, 0xE1, 0xC8, 0xBE, 0x8E, 0x94, 0xE5,  // ........
                        /* 0300 */  0xD3, 0xA1, 0xE3, 0x2B, 0x8D, 0xE6, 0x30, 0x29,  // ...+..0)
                        /* 0308 */  0xE7, 0xC5, 0x12, 0xD3, 0x9D, 0xC7, 0x2F, 0x94,  // ....../.
                        /* 0310 */  0xCE, 0x35, 0x34, 0x7B, 0x8A, 0x1C, 0x43, 0xE1,  // .54{..C.
                        /* 0318 */  0x56, 0x6B, 0xEC, 0x9B, 0x50, 0xD7, 0x7E, 0x9B,  // Vk..P.~.
                        /* 0320 */  0xA8, 0xEA, 0x6A, 0x7A, 0x12, 0x0A, 0xF2, 0x2A,  // ..jz...*
                        /* 0328 */  0x8C, 0x0D, 0xCE, 0x39, 0xF0, 0x93, 0xD4, 0x0C,  // ...9....
                        /* 0330 */  0x97, 0xB3, 0x7B, 0x4A, 0x4F, 0xBC, 0xBA, 0xDC,  // ..{JO...
                        /* 0338 */  0x68, 0xFC, 0xCA, 0x7B, 0x67, 0xC3, 0x98, 0xA2,  // h..{g...
                        /* 0340 */  0x2B, 0xBF, 0xE3, 0x9D, 0x61, 0x60, 0xCC, 0xD3,  // +...a`..
                        /* 0348 */  0xCD, 0x40, 0x63, 0xB7, 0x50, 0x08, 0xAE, 0x34,  // .@c.P..4
                        /* 0350 */  0x84, 0xAC, 0x0D, 0xBE, 0x8A, 0x60, 0x1A, 0x8C,  // .....`..
                        /* 0358 */  0xE8, 0xE3, 0x4E, 0x98, 0xE5, 0x6D, 0xC8, 0xF9,  // ..N..m..
                        /* 0360 */  0x3E, 0xE4, 0x4E, 0x9F, 0xA7, 0x96, 0xA7, 0x96,  // >.N.....
                        /* 0368 */  0xE2, 0x85, 0x2A, 0x70, 0x4E, 0xFC, 0x28, 0xE6,  // ..*pN.(.
                        /* 0370 */  0xA8, 0x1C, 0xC9, 0xBE, 0x39, 0x15, 0x05, 0x9D,  // ....9...
                        /* 0378 */  0xAC, 0x3C, 0x81, 0x04, 0xF2, 0xA1, 0xAE, 0x0B,  // .<......
                        /* 0380 */  0xBC, 0xB5, 0xB4, 0x42, 0x7C, 0x43, 0x48, 0x06,  // ...B|CH.
                        /* 0388 */  0x69, 0x4C, 0x80, 0xA8, 0xA8, 0x9D, 0x3B, 0xF7,  // iL....;.
                        /* 0390 */  0x79, 0x50, 0xFB, 0xFF, 0xB8, 0xF7, 0xD6, 0x31,  // yP.....1
                        /* 0398 */  0x85, 0xF2, 0xA5, 0xA7, 0x18, 0x2E, 0x97, 0x8C,  // ........
                        /* 03A0 */  0xD2, 0x2F, 0x5B, 0x22, 0x22, 0x0A, 0x55, 0x04,  // ./["".U.
                        /* 03A8 */  0xF6, 0x21, 0xD1, 0x58, 0xA5, 0xB8, 0x8D, 0x17,  // .!.X....
                        /* 03B0 */  0xAC, 0xCC, 0xD2, 0x85, 0xD9, 0x21, 0x9C, 0x7A,  // .....!.z
                        /* 03B8 */  0xA2, 0xB3, 0x90, 0x11, 0x7B, 0x91, 0x5C, 0xB3,  // ....{.\.
                        /* 03C0 */  0x90, 0x68, 0xD6, 0x2F, 0xFF, 0xD6, 0x1F, 0x77,  // .h./...w
                        /* 03C8 */  0x89, 0x38, 0x75, 0x1E, 0x8D, 0x6D, 0xC7, 0xC7,  // .8u..m..
                        /* 03D0 */  0xCC, 0x20, 0xAF, 0xA0, 0x76, 0x9A, 0xAE, 0x8C,  // . ..v...
                        /* 03D8 */  0xDA, 0x4F, 0x7F, 0xCC, 0x17, 0x6B, 0x0A, 0xD8,  // .O...k..
                        /* 03E0 */  0xEA, 0x00, 0x2B, 0x43, 0x6D, 0x93, 0x0F, 0xEF,  // ..+Cm...
                        /* 03E8 */  0xF7, 0xB2, 0x42, 0x69, 0x59, 0xA5, 0xAA, 0xC3,  // ..BiY...
                        /* 03F0 */  0x37, 0x1A, 0x7B, 0xAE, 0xE9, 0xB2, 0x34, 0xC7,  // 7.{...4.
                        /* 03F8 */  0x3F, 0xDD, 0x6E, 0x6F, 0x75, 0x7C, 0x1A, 0x06,  // ?.nou|..
                        /* 0400 */  0x50, 0xAC, 0x18, 0x20, 0x6A, 0x81, 0x1A, 0x74,  // P.. j..t
                        /* 0408 */  0xB6, 0x22, 0xE9, 0xFF, 0x5C, 0xAE, 0xA3, 0xC1,  // ."..\...
                        /* 0410 */  0x84, 0x69, 0xC4, 0x20, 0x4E, 0x22, 0xD9, 0xFC,  // .i. N"..
                        /* 0418 */  0x71, 0x5B, 0x79, 0xCC, 0x5D, 0x4A, 0x71, 0x3B,  // q[y.]Jq;
                        /* 0420 */  0x47, 0x5E, 0xEE, 0x8A, 0x67, 0x9D, 0x06, 0x62,  // G^..g..b
                        /* 0428 */  0x06, 0x55, 0x46, 0xE5, 0x4A, 0x06, 0x33, 0xF8,  // .UF.J.3.
                        /* 0430 */  0x95, 0x71, 0xDC, 0x67, 0x3D, 0x76, 0x4A, 0x25,  // .q.g=vJ%
                        /* 0438 */  0xE3, 0xF8, 0xB2, 0x41, 0x61, 0xB7, 0x81, 0x3B,  // ...Aa..;
                        /* 0440 */  0x51, 0xFF, 0x85, 0x35, 0xF2, 0x32, 0xEF, 0x2D,  // Q..5.2.-
                        /* 0448 */  0x27, 0xEA, 0x28, 0x6B, 0x23, 0x83, 0x5E, 0x4C,  // '.(k#.^L
                        /* 0450 */  0x47, 0x92, 0xD5, 0x52, 0xDE, 0x62, 0x34, 0x8F,  // G..R.b4.
                        /* 0458 */  0x64, 0xDA, 0xF9, 0x04, 0xCA, 0x8B, 0xE7, 0x58,  // d......X
                        /* 0460 */  0xE9, 0x20, 0xF3, 0x34, 0x76, 0x9B, 0xEE, 0xDF,  // . .4v...
                        /* 0468 */  0x27, 0x24, 0xE6, 0xCF, 0x6D, 0x91, 0x10, 0x3E,  // '$..m..>
                        /* 0470 */  0xBA, 0x03, 0xF9, 0xF2, 0xBB, 0x68, 0x9E, 0x24,  // .....h.$
                        /* 0478 */  0x1D, 0xAA, 0xCB, 0x7C, 0x16, 0x12, 0x9A, 0x6F,  // ...|...o
                        /* 0480 */  0x73, 0xB9, 0xEC, 0x5F, 0xBE, 0x9E, 0x91, 0x29,  // s.._...)
                        /* 0488 */  0x70, 0x90, 0xCD, 0x46, 0xDB, 0x29, 0x3B, 0x13,  // p..F.);.
                        /* 0490 */  0x94, 0x8D, 0x34, 0x69, 0x82, 0x1A, 0x65, 0x23,  // ..4i..e#
                        /* 0498 */  0x84, 0x18, 0x12, 0xFB, 0x1E, 0x91, 0x64, 0xD9,  // ......d.
                        /* 04A0 */  0xBA, 0xC3, 0xFB, 0x2A, 0x7A, 0x50, 0x1A, 0x5A,  // ...*zP.Z
                        /* 04A8 */  0x68, 0xE2, 0x80, 0x19, 0x23, 0xA6, 0x4B, 0x8D,  // h...#.K.
                        /* 04B0 */  0x40, 0xFC, 0x62, 0x6A, 0xDC, 0x6A, 0xBA, 0xB6,  // @.bj.j..
                        /* 04B8 */  0xB0, 0x01, 0x30, 0xB7, 0x0B, 0xDC, 0x74, 0x81,  // ..0...t.
                        /* 04C0 */  0xB8, 0x6E, 0xA7, 0x36, 0x0D, 0x0E, 0x0C, 0x73,  // .n.6...s
                        /* 04C8 */  0x4F, 0x10, 0xBD, 0xA9, 0x79, 0x7B, 0x6A, 0xD8,  // O...y{j.
                        /* 04D0 */  0xEA, 0x43, 0x48, 0x91, 0x6E, 0x9B, 0x52, 0xC6,  // .CH.n.R.
                        /* 04D8 */  0x1A, 0xC8, 0x12, 0xCB, 0xFA, 0x0C, 0xCB, 0x15,  // ........
                        /* 04E0 */  0x69, 0xAA, 0xA4, 0x97, 0xC7, 0xB2, 0xEF, 0x86,  // i.......
                        /* 04E8 */  0x11, 0x2C, 0x6E, 0x5B, 0xCA, 0x73, 0xC7, 0xCB,  // .,n[.s..
                        /* 04F0 */  0xC4, 0xE8, 0x92, 0xAF, 0xB6, 0x01, 0x6E, 0xD7,  // ......n.
                        /* 04F8 */  0xAD, 0x2F, 0x7C, 0x72, 0xB4, 0x20, 0x94, 0x4A,  // ./|r. .J
                        /* 0500 */  0x1B, 0x86, 0x3B, 0x97, 0x40, 0x06, 0x77, 0x77,  // ..;.@.ww
                        /* 0508 */  0x09, 0xD0, 0x51, 0x09, 0xAB, 0x48, 0x34, 0x53,  // ..Q..H4S
                        /* 0510 */  0x29, 0x80, 0x1F, 0x2E, 0x80, 0xCD, 0xF1, 0x2D,  // )......-
                        /* 0518 */  0x09, 0x40, 0x51, 0xFB, 0xEB, 0x77, 0x07, 0xFE,  // .@Q..w..
                        /* 0520 */  0xE8, 0x21, 0x6A, 0x2C, 0xA6, 0x76, 0x63, 0x6A,  // .!j,.vcj
                        /* 0528 */  0xE0, 0x87, 0x3F, 0x86, 0x25, 0x64, 0xE1, 0x44,  // ..?.%d.D
                        /* 0530 */  0x3C, 0x06, 0xCA, 0x02, 0x2E, 0xA7, 0x5D, 0xAB,  // <.....].
                        /* 0538 */  0x28, 0x21, 0x81, 0xE4, 0xEB, 0xD0, 0x46, 0xA7,  // (!....F.
                        /* 0540 */  0x30, 0x94, 0xBA, 0xE6, 0x97, 0x3C, 0x28, 0x9B,  // 0....<(.
                        /* 0548 */  0xA1, 0x81, 0x73, 0xE7, 0x5F, 0xE1, 0x7E, 0x1E,  // ..s._.~.
                        /* 0550 */  0xA9, 0x5A, 0x07, 0x90, 0xD0, 0x5C, 0xE9, 0xB3,  // .Z...\..
                        /* 0558 */  0x6D, 0xC7, 0x6F, 0x65, 0xC2, 0xEB, 0x2C, 0xDF,  // m.oe..,.
                        /* 0560 */  0x0C, 0xB6, 0xB5, 0x8A, 0xF1, 0xA7, 0x64, 0x1C,  // ......d.
                        /* 0568 */  0x90, 0x51, 0xF2, 0x94, 0xD4, 0xCA, 0x5F, 0x42,  // .Q...._B
                        /* 0570 */  0x29, 0x0A, 0x50, 0xAD, 0x11, 0x67, 0xDB, 0x7A,  // ).P..g.z
                        /* 0578 */  0x69, 0x94, 0x66, 0xCF, 0x65, 0xEE, 0xA2, 0xF8,  // i.f.e...
                        /* 0580 */  0xD3, 0xB4, 0x3B, 0x54, 0x75, 0xD1, 0xBE, 0x28,  // ..;Tu..(
                        /* 0588 */  0x0E, 0xB1, 0x4F, 0x86, 0xF4, 0x68, 0x23, 0x92,  // ..O..h#.
                        /* 0590 */  0xED, 0x77, 0xB0, 0xD5, 0x6C, 0x03, 0x57, 0x0C,  // .w..l.W.
                        /* 0598 */  0x3D, 0xB2, 0x14, 0xC6, 0xD5, 0xA7, 0xF7, 0x9D,  // =.......
                        /* 05A0 */  0xDD, 0x0A, 0x03, 0x79, 0xF4, 0xF1, 0xA0, 0x58,  // ...y...X
                        /* 05A8 */  0xC5, 0xA8, 0x9D, 0xB9, 0xCC, 0x69, 0x32, 0xA4,  // .....i2.
                        /* 05B0 */  0xDD, 0x8B, 0x34, 0xF2, 0x14, 0x72, 0x5B, 0x48,  // ..4..r[H
                        /* 05B8 */  0x07, 0xFC, 0xBB, 0xBB, 0xBB, 0x23, 0xF8, 0x09,  // .....#..
                        /* 05C0 */  0x26, 0x2D, 0xDE, 0x27, 0x93, 0x08, 0x94, 0xF3,  // &-.'....
                        /* 05C8 */  0x2D, 0xB2, 0xF3, 0x09, 0x04, 0xF2, 0xD7, 0x68,  // -......h
                        /* 05D0 */  0x2E, 0xA2, 0x80, 0xFC, 0x05, 0xFD, 0x47, 0xA5,  // ......G.
                        /* 05D8 */  0x44, 0x81, 0xEC, 0xBE, 0xA3, 0xC0, 0xF9, 0xC3,  // D.......
                        /* 05E0 */  0x2A, 0x19, 0x1F, 0xD0, 0x7B, 0x06, 0x4C, 0x13,  // *...{.L.
                        /* 05E8 */  0x40                                             // @
                    }
                })
            }

            Method (IMOK, 1, NotSerialized)
            {
                Return (Arg0)
            }
        }
    }

    Scope (\_SB.IETM)
    {
        Method (KTOC, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return (((Arg0 - 0x0AAC) / 0x0A))
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (CTOK, 1, Serialized)
        {
            Return (((Arg0 * 0x0A) + 0x0AAC))
        }

        Method (C10K, 1, Serialized)
        {
            Name (TMP1, Buffer (0x10)
            {
                 0x00                                             // .
            })
            CreateByteField (TMP1, Zero, TMPL)
            CreateByteField (TMP1, One, TMPH)
            Local0 = (Arg0 + 0x0AAC)
            TMPL = (Local0 & 0xFF)
            TMPH = ((Local0 & 0xFF00) >> 0x08)
            ToInteger (TMP1, Local1)
            Return (Local1)
        }

        Method (K10C, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return ((Arg0 - 0x0AAC))
            }
            Else
            {
                Return (Zero)
            }
        }
    }
}

