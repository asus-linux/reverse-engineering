sudo usbhid-dump -m 0b05:19b6

003:004:002:DESCRIPTOR         1682907346.538090
 05 01 09 06 A1 01 85 01 75 01 95 08 05 07 19 E0
 29 E7 15 00 25 01 81 02 95 01 75 08 81 03 95 05
 75 01 05 08 19 01 29 05 91 02 95 01 75 03 91 03
 95 06 75 08 15 00 26 FF 00 05 07 19 00 2A FF 00
 81 00 95 C0 75 01 05 07 19 00 29 EF 15 00 25 01
 81 02 C0

003:004:001:DESCRIPTOR         1682907346.539166
 06 82 FF 09 CF A1 01 85 C1 09 62 15 00 26 FF 00
 75 08 95 3C B1 02 09 66 15 00 26 FF 00 75 08 95
 10 81 02 09 61 15 00 26 FF 00 75 08 95 3C 91 02
 85 C2 09 8A 15 00 26 FF 00 75 08 95 10 81 02 09
 8E 15 00 26 FF 00 75 08 95 10 91 02 C0

003:004:000:DESCRIPTOR         1682907346.540287
 06 31 FF 09 76 A1 01 85 5A 19 00 2A FF 00 15 00
 26 FF 00 75 08 95 05 81 00 19 00 2A FF 00 15 00
 26 FF 00 75 08 95 3F B1 00 C0 05 0C 09 01 A1 01
 85 02 19 00 2A 3C 02 15 00 26 3C 02 75 10 95 02
 81 00 C0 06 31 FF 09 79 A1 01 85 5D 19 00 2A FF
 00 15 00 26 FF 00 75 08 95 1F 81 00 19 00 2A FF
 00 15 00 26 FF 00 75 08 95 3F 91 00 19 00 2A FF
 00 15 00 26 FF 00 75 08 95 3F B1 00 C0 06 31 FF
 09 80 A1 01 85 5E 19 00 2A FF 00 15 00 26 FF 00
 75 08 95 05 81 00 19 00 2A FF 00 15 00 26 FF 00
 75 08 95 3F B1 00 C0


https://eleccelerator.com/usbdescreqparser/

0x03, 0x04, 0x02, 0x05, 0x01,  // Unknown (bTag: 0x00, bType: 0x00)
0x09, 0x06,        // Usage (0x06)
0xA1, 0x01,        // Collection (Application)
0x85, 0x01,        //   Report ID (1)
0x75, 0x01,        //   Report Size (1)
0x95, 0x08,        //   Report Count (8)
0x05, 0x07,        //   Usage Page (Kbrd/Keypad)
0x19, 0xE0,        //   Usage Minimum (0xE0)
0x29, 0xE7,        //   Usage Maximum (0xE7)
0x15, 0x00,        //   Logical Minimum (0)
0x25, 0x01,        //   Logical Maximum (1)
0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x95, 0x01,        //   Report Count (1)
0x75, 0x08,        //   Report Size (8)
0x81, 0x03,        //   Input (Const,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x95, 0x05,        //   Report Count (5)
0x75, 0x01,        //   Report Size (1)
0x05, 0x08,        //   Usage Page (LEDs)
0x19, 0x01,        //   Usage Minimum (Num Lock)
0x29, 0x05,        //   Usage Maximum (Kana)
0x91, 0x02,        //   Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0x95, 0x01,        //   Report Count (1)
0x75, 0x03,        //   Report Size (3)
0x91, 0x03,        //   Output (Const,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0x95, 0x06,        //   Report Count (6)
0x75, 0x08,        //   Report Size (8)
0x15, 0x00,        //   Logical Minimum (0)
0x26, 0xFF, 0x00,  //   Logical Maximum (255)
0x05, 0x07,        //   Usage Page (Kbrd/Keypad)
0x19, 0x00,        //   Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  //   Usage Maximum (0xFF)
0x81, 0x00,        //   Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x95, 0xC0,        //   Report Count (-64)
0x75, 0x01,        //   Report Size (1)
0x05, 0x07,        //   Usage Page (Kbrd/Keypad)
0x19, 0x00,        //   Usage Minimum (0x00)
0x29, 0xEF,        //   Usage Maximum (0xEF)
0x15, 0x00,        //   Logical Minimum (0)
0x25, 0x01,        //   Logical Maximum (1)
0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0xC0,              // End Collection
0x03, 0x04, 0x01, 0x06, 0x82,  // Unknown (bTag: 0x00, bType: 0x00)
0xFF, 0x09, 0xCF, 0xA1, 0x01,  // Unknown (bTag: 0x0F, bType: 0x03)
0x85, 0xC1,        // Report ID (-63)
0x09, 0x62,        // Usage (0x62)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3C,        // Report Count (60)
0xB1, 0x02,        // Feature (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0x09, 0x66,        // Usage (0x66)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x10,        // Report Count (16)
0x81, 0x02,        // Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x09, 0x61,        // Usage (0x61)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3C,        // Report Count (60)
0x91, 0x02,        // Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0x85, 0xC2,        // Report ID (-62)
0x09, 0x8A,        // Usage (0x8A)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x10,        // Report Count (16)
0x81, 0x02,        // Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x09, 0x8E,        // Usage (0x8E)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x10,        // Report Count (16)
0x91, 0x02,        // Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0xC0,              // End Collection
0x03, 0x04, 0x00, 0x06, 0x31,  // Unknown (bTag: 0x00, bType: 0x00)
0xFF, 0x09, 0x76, 0xA1, 0x01,  // Unknown (bTag: 0x0F, bType: 0x03)
0x85, 0x5A,        // Report ID (90)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x05,        // Report Count (5)
0x81, 0x00,        // Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3F,        // Report Count (63)
0xB1, 0x00,        // Feature (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0xC0,              // End Collection
0x05, 0x0C,        // Usage Page (Consumer)
0x09, 0x01,        // Usage (Consumer Control)
0xA1, 0x01,        // Collection (Application)
0x85, 0x02,        // Report ID (2)
0x19, 0x00,        // Usage Minimum (Unassigned)
0x2A, 0x3C, 0x02,  // Usage Maximum (AC Format)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0x3C, 0x02,  // Logical Maximum (572)
0x75, 0x10,        // Report Size (16)
0x95, 0x02,        // Report Count (2)
0x81, 0x00,        // Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
0xC0,              // End Collection
0x06, 0x31, 0xFF,  // Usage Page (Vendor Defined 0xFF31)
0x09, 0x79,        // Usage (0x79)
0xA1, 0x01,        // Collection (Application)
0x85, 0x5D,        // Report ID (93)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x1F,        // Report Count (31)
0x81, 0x00,        // Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3F,        // Report Count (63)
0x91, 0x00,        // Output (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3F,        // Report Count (63)
0xB1, 0x00,        // Feature (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0xC0,              // End Collection
0x06, 0x31, 0xFF,  // Usage Page (Vendor Defined 0xFF31)
0x09, 0x80,        // Usage (0x80)
0xA1, 0x01,        // Collection (Application)
0x85, 0x5E,        // Report ID (94)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x05,        // Report Count (5)
0x81, 0x00,        // Input (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x19, 0x00,        // Usage Minimum (0x00)
0x2A, 0xFF, 0x00,  // Usage Maximum (0xFF)
0x15, 0x00,        // Logical Minimum (0)
0x26, 0xFF, 0x00,  // Logical Maximum (255)
0x75, 0x08,        // Report Size (8)
0x95, 0x3F,        // Report Count (63)
0xB1, 0x00,        // Feature (Data,Array,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
0xC0,              // End Collection

// 336 bytes

