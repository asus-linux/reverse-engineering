/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt5.dat, Thu Apr 27 00:07:23 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000457 (1111)
 *     Revision         0x02
 *     Checksum         0x04
 *     OEM ID           "HgRef"
 *     OEM Table ID     "HgRpSsdt"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "HgRef", "HgRpSsdt", 0x00001000)
{
    External (_SB_.GGOV, MethodObj)    // 1 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.RP01._ADR, MethodObj)    // 0 Arguments
    External (_SB_.SGOV, MethodObj)    // 2 Arguments
    External (DLHR, UnknownObj)
    External (DLPW, UnknownObj)
    External (EECP, UnknownObj)
    External (GBAS, UnknownObj)
    External (HGMD, UnknownObj)
    External (HRA0, UnknownObj)
    External (HRE0, UnknownObj)
    External (HRG0, UnknownObj)
    External (OSYS, UnknownObj)
    External (PWA0, UnknownObj)
    External (PWE0, UnknownObj)
    External (PWG0, UnknownObj)
    External (RPA1, UnknownObj)
    External (RPA5, UnknownObj)
    External (RPBA, UnknownObj)
    External (RPIN, UnknownObj)
    External (SGGP, UnknownObj)
    External (XBAS, UnknownObj)

    Scope (\_SB.PC00)
    {
        Name (IVID, 0xFFFF)
        Name (ELCT, Zero)
        Name (HVID, Zero)
        Name (HDID, Zero)
        Name (TCNT, Zero)
        Name (LTRE, Zero)
        OperationRegion (RPCF, SystemMemory, ((\XBAS + (((\_SB.PC00.RP01._ADR () & 0x00FF0000) >> 0x10) << 
            0x0F)) + ((\RPA1 & 0x0F) << 0x0C)), 0x1000)
        Field (RPCF, DWordAcc, NoLock, Preserve)
        {
            PVID,   16, 
            PDID,   16, 
            Offset (0x18), 
            PRBN,   8, 
            SCBN,   8, 
            Offset (0x4A), 
            CEDN,   1, 
            Offset (0x50), 
            ASPN,   2, 
                ,   2, 
            LKDN,   1, 
            Offset (0x69), 
                ,   2, 
            LREN,   1, 
            Offset (0x328), 
                ,   19, 
            LKSN,   4
        }

        OperationRegion (RTPN, SystemMemory, (\XBAS + (SCBN << 0x14)), 0xF0)
        Field (RTPN, AnyAcc, Lock, Preserve)
        {
            DVID,   16, 
            Offset (0x0B), 
            CBCN,   8, 
            Offset (0x2C), 
            SVID,   16, 
            SDID,   16
        }

        OperationRegion (PCAN, SystemMemory, ((\XBAS + (SCBN << 0x14)) + \EECP), 0x14)
        Field (PCAN, DWordAcc, NoLock, Preserve)
        {
            Offset (0x10), 
            LCTR,   16
        }

        OperationRegion (PCBN, SystemMemory, (((\XBAS + (SCBN << 0x14)) + 0x1000) + 
            \EECP), 0x14)
        Field (PCBN, DWordAcc, NoLock, Preserve)
        {
            Offset (0x10), 
            LCTZ,   16
        }

        Method (HGON, 0, Serialized)
        {
            If ((SGGP != One))
            {
                Return (Zero)
            }

            If ((CCHK (One) == Zero))
            {
                Return (Zero)
            }

            HGPO (PWE0, PWG0, PWA0, One)
            Sleep (DLPW)
            HGPO (HRE0, HRG0, HRA0, Zero)
            Sleep (DLHR)
            LKDN = Zero
            While ((LKSN < 0x07))
            {
                Sleep (One)
            }

            SVID = HVID /* \_SB_.PC00.HVID */
            SDID = HDID /* \_SB_.PC00.HDID */
            LCTR = ((ELCT & 0x43) | (LCTR & 0xFFBC))
            LCTZ = ((ELCT & 0x43) | (LCTZ & 0xFFBC))
            While ((\_SB.PC00.LKSN < 0x07))
            {
                Sleep (One)
            }

            LREN = LTRE /* \_SB_.PC00.LTRE */
            CEDN = One
            Return (Zero)
        }

        Method (HGOF, 0, Serialized)
        {
            If ((SGGP != One))
            {
                Return (Zero)
            }

            If ((CCHK (Zero) == Zero))
            {
                Return (Zero)
            }

            ELCT = LCTR /* \_SB_.PC00.LCTR */
            HVID = SVID /* \_SB_.PC00.SVID */
            HDID = SDID /* \_SB_.PC00.SDID */
            LTRE = LREN /* \_SB_.PC00.LREN */
            LKDN = One
            While ((LKSN != Zero))
            {
                Sleep (One)
            }

            HGPO (HRE0, HRG0, HRA0, One)
            Sleep (DLHR)
            HGPO (PWE0, PWG0, PWA0, Zero)
            Return (Zero)
        }

        Method (HGPO, 4, Serialized)
        {
            If ((Arg2 == Zero))
            {
                Arg3 = ~Arg3
                Arg3 &= One
            }

            If ((SGGP == One))
            {
                If (CondRefOf (\_SB.SGOV))
                {
                    \_SB.SGOV (Arg1, Arg3)
                }
            }
        }

        Method (HGPI, 4, Serialized)
        {
            If ((Arg0 == One))
            {
                If (CondRefOf (\_SB.GGOV))
                {
                    Local0 = \_SB.GGOV (Arg2)
                }
            }

            If ((Arg3 == Zero))
            {
                Local0 = ~Local0
            }

            Local0 &= One
            Return (Local0)
        }

        Method (CCHK, 1, NotSerialized)
        {
            If ((PVID == IVID))
            {
                Return (Zero)
            }

            If ((Arg0 == Zero))
            {
                If ((HGPI (SGGP, PWE0, PWG0, PWA0) == Zero))
                {
                    Return (Zero)
                }
            }
            ElseIf ((Arg0 == One))
            {
                If ((HGPI (SGGP, PWE0, PWG0, PWA0) == One))
                {
                    Return (Zero)
                }
            }

            Return (One)
        }
    }
}

