/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt11.dat, Thu Apr 27 00:07:22 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00002870 (10352)
 *     Revision         0x02
 *     Checksum         0xDB
 *     OEM ID           "DptfTb"
 *     OEM Table ID     "DptfTabl"
 *     OEM Revision     0x00001000 (4096)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "DptfTb", "DptfTabl", 0x00001000)
{
    External (_SB_.AAC0, FieldUnitObj)
    External (_SB_.ACRT, FieldUnitObj)
    External (_SB_.APSV, FieldUnitObj)
    External (_SB_.CBMI, FieldUnitObj)
    External (_SB_.CFGD, FieldUnitObj)
    External (_SB_.CLVL, FieldUnitObj)
    External (_SB_.CPPC, FieldUnitObj)
    External (_SB_.CTC0, FieldUnitObj)
    External (_SB_.CTC1, FieldUnitObj)
    External (_SB_.CTC2, FieldUnitObj)
    External (_SB_.OSCP, IntObj)
    External (_SB_.PAGD, DeviceObj)
    External (_SB_.PAGD._PUR, PkgObj)
    External (_SB_.PAGD._STA, MethodObj)    // 0 Arguments
    External (_SB_.PC00, DeviceObj)
    External (_SB_.PC00.LPCB.EC0_, DeviceObj)
    External (_SB_.PC00.LPCB.EC0_.BATM, FieldUnitObj)
    External (_SB_.PC00.LPCB.EC0_.CLOT, FieldUnitObj)
    External (_SB_.PC00.LPCB.EC0_.CTMP, FieldUnitObj)
    External (_SB_.PC00.LPCB.EC0_.ECOK, IntObj)
    External (_SB_.PC00.LPCB.EC0_.MUT0, MutexObj)
    External (_SB_.PC00.LPCB.EC0_.SEN2, DeviceObj)
    External (_SB_.PC00.LPCB.EC0_.SEN3, DeviceObj)
    External (_SB_.PC00.LPCB.EC0_.VLTT, UnknownObj)
    External (_SB_.PC00.LPCB.EC0_.VRTT, UnknownObj)
    External (_SB_.PC00.LPCB.H_EC.UVTH, FieldUnitObj)
    External (_SB_.PC00.MC__.MHBR, FieldUnitObj)
    External (_SB_.PC00.TCPU, DeviceObj)
    External (_SB_.PL10, FieldUnitObj)
    External (_SB_.PL11, FieldUnitObj)
    External (_SB_.PL12, FieldUnitObj)
    External (_SB_.PL20, FieldUnitObj)
    External (_SB_.PL21, FieldUnitObj)
    External (_SB_.PL22, FieldUnitObj)
    External (_SB_.PLW0, FieldUnitObj)
    External (_SB_.PLW1, FieldUnitObj)
    External (_SB_.PLW2, FieldUnitObj)
    External (_SB_.PR00, ProcessorObj)
    External (_SB_.PR00._PSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TPC, IntObj)
    External (_SB_.PR00._TSD, MethodObj)    // 0 Arguments
    External (_SB_.PR00._TSS, MethodObj)    // 0 Arguments
    External (_SB_.PR00.LPSS, PkgObj)
    External (_SB_.PR00.TPSS, PkgObj)
    External (_SB_.PR00.TSMC, PkgObj)
    External (_SB_.PR00.TSMF, PkgObj)
    External (_SB_.PR01, ProcessorObj)
    External (_SB_.PR02, ProcessorObj)
    External (_SB_.PR03, ProcessorObj)
    External (_SB_.PR04, ProcessorObj)
    External (_SB_.PR05, ProcessorObj)
    External (_SB_.PR06, ProcessorObj)
    External (_SB_.PR07, ProcessorObj)
    External (_SB_.PR08, ProcessorObj)
    External (_SB_.PR09, ProcessorObj)
    External (_SB_.PR10, ProcessorObj)
    External (_SB_.PR11, ProcessorObj)
    External (_SB_.PR12, ProcessorObj)
    External (_SB_.PR13, ProcessorObj)
    External (_SB_.PR14, ProcessorObj)
    External (_SB_.PR15, ProcessorObj)
    External (_SB_.PR16, ProcessorObj)
    External (_SB_.PR17, ProcessorObj)
    External (_SB_.PR18, ProcessorObj)
    External (_SB_.PR19, ProcessorObj)
    External (_SB_.PR20, ProcessorObj)
    External (_SB_.PR21, ProcessorObj)
    External (_SB_.PR22, ProcessorObj)
    External (_SB_.PR23, ProcessorObj)
    External (_SB_.PR24, ProcessorObj)
    External (_SB_.PR25, ProcessorObj)
    External (_SB_.PR26, ProcessorObj)
    External (_SB_.PR27, ProcessorObj)
    External (_SB_.PR28, ProcessorObj)
    External (_SB_.PR29, ProcessorObj)
    External (_SB_.PR30, ProcessorObj)
    External (_SB_.PR31, ProcessorObj)
    External (_SB_.SLPB, DeviceObj)
    External (_SB_.TAR0, FieldUnitObj)
    External (_SB_.TAR1, FieldUnitObj)
    External (_SB_.TAR2, FieldUnitObj)
    External (_TZ_.ETMD, IntObj)
    External (_TZ_.THRM, ThermalZoneObj)
    External (ACTT, IntObj)
    External (ATPC, IntObj)
    External (BATR, IntObj)
    External (CHGE, IntObj)
    External (CRTT, IntObj)
    External (DCFE, IntObj)
    External (DPTF, IntObj)
    External (ECON, IntObj)
    External (FND1, IntObj)
    External (FND2, IntObj)
    External (FND3, IntObj)
    External (HIDW, MethodObj)    // 4 Arguments
    External (HIWC, MethodObj)    // 1 Arguments
    External (IN34, IntObj)
    External (ODV0, IntObj)
    External (ODV1, IntObj)
    External (ODV2, IntObj)
    External (ODV3, IntObj)
    External (ODV4, IntObj)
    External (ODV5, IntObj)
    External (PCHE, FieldUnitObj)
    External (PF00, IntObj)
    External (PLID, IntObj)
    External (PNHM, IntObj)
    External (PPPR, IntObj)
    External (PPSZ, IntObj)
    External (PSVT, IntObj)
    External (PTPC, IntObj)
    External (PWRE, IntObj)
    External (PWRS, IntObj)
    External (S1DE, IntObj)
    External (S2DE, IntObj)
    External (S3DE, IntObj)
    External (S4DE, IntObj)
    External (S5DE, IntObj)
    External (S6DE, IntObj)
    External (S6P2, IntObj)
    External (SADE, IntObj)
    External (SSP1, IntObj)
    External (SSP2, IntObj)
    External (SSP3, IntObj)
    External (SSP4, IntObj)
    External (SSP5, IntObj)
    External (TCNT, IntObj)
    External (TSOD, IntObj)

    Scope (\_SB)
    {
        Device (IETM)
        {
            Method (GHID, 1, Serialized)
            {
                If ((Arg0 == "IETM"))
                {
                    Return ("INTC1041")
                }

                If ((Arg0 == "SEN1"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN2"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN3"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN4"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "SEN5"))
                {
                    Return ("INTC1046")
                }

                If ((Arg0 == "TPCH"))
                {
                    Return ("INTC1049")
                }

                If ((Arg0 == "TFN1"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN2"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TFN3"))
                {
                    Return ("INTC1048")
                }

                If ((Arg0 == "TPWR"))
                {
                    Return ("INTC1060")
                }

                If ((Arg0 == "1"))
                {
                    Return ("INTC1061")
                }

                If ((Arg0 == "CHRG"))
                {
                    Return ("INTC1046")
                }

                Return ("XXXX9999")
            }

            Name (_UID, "IETM")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If (CondRefOf (HIWC))
                {
                    If (HIWC (Arg0))
                    {
                        If (CondRefOf (HIDW))
                        {
                            Return (HIDW (Arg0, Arg1, Arg2, Arg3))
                        }
                    }
                }

                Return (Buffer (One)
                {
                     0x00                                             // .
                })
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If (((\DPTF == One) && (\IN34 == One)))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Name (PTRP, Zero)
            Name (PSEM, Zero)
            Name (ATRP, Zero)
            Name (ASEM, Zero)
            Name (YTRP, Zero)
            Name (YSEM, Zero)
            Method (_OSC, 4, Serialized)  // _OSC: Operating System Capabilities
            {
                CreateDWordField (Arg3, Zero, STS1)
                CreateDWordField (Arg3, 0x04, CAP1)
                If ((Arg1 != One))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x0A
                    Return (Arg3)
                }

                If ((Arg2 != 0x02))
                {
                    STS1 &= 0xFFFFFF00
                    STS1 |= 0x02
                    Return (Arg3)
                }

                If (CondRefOf (\_SB.APSV))
                {
                    If ((PSEM == Zero))
                    {
                        PSEM = One
                        PTRP = \_SB.APSV /* External reference */
                    }
                }

                If (CondRefOf (\_SB.AAC0))
                {
                    If ((ASEM == Zero))
                    {
                        ASEM = One
                        ATRP = \_SB.AAC0 /* External reference */
                    }
                }

                If (CondRefOf (\_SB.ACRT))
                {
                    If ((YSEM == Zero))
                    {
                        YSEM = One
                        YTRP = \_SB.ACRT /* External reference */
                    }
                }

                If ((Arg0 == ToUUID ("b23ba85d-c8b7-3542-88de-8de2ffcfd698") /* Unknown UUID */))
                {
                    If (~(STS1 & One))
                    {
                        If ((CAP1 & One))
                        {
                            If ((CAP1 & 0x02))
                            {
                                \_SB.AAC0 = 0x6E
                                \_TZ.ETMD = Zero
                            }
                            Else
                            {
                                \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                                \_TZ.ETMD = One
                            }

                            If ((CAP1 & 0x04))
                            {
                                \_SB.APSV = 0x6E
                            }
                            Else
                            {
                                \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            }

                            If ((CAP1 & 0x08))
                            {
                                \_SB.ACRT = 0xD2
                            }
                            Else
                            {
                                \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            }
                        }
                        Else
                        {
                            \_SB.ACRT = YTRP /* \_SB_.IETM.YTRP */
                            \_SB.APSV = PTRP /* \_SB_.IETM.PTRP */
                            \_SB.AAC0 = ATRP /* \_SB_.IETM.ATRP */
                            \_TZ.ETMD = One
                        }

                        Notify (\_TZ.THRM, 0x81) // Information Change
                    }

                    Return (Arg3)
                }

                Return (Arg3)
            }

            Method (DCFG, 0, NotSerialized)
            {
                Return (\DCFE) /* External reference */
            }

            Name (ODVX, Package (0x06)
            {
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero, 
                Zero
            })
            Method (ODVP, 0, Serialized)
            {
                ODVX [Zero] = \ODV0 /* External reference */
                ODVX [One] = \ODV1 /* External reference */
                ODVX [0x02] = \ODV2 /* External reference */
                ODVX [0x03] = \ODV3 /* External reference */
                ODVX [0x04] = \ODV4 /* External reference */
                ODVX [0x05] = \ODV5 /* External reference */
                Return (ODVX) /* \_SB_.IETM.ODVX */
            }

            Method (GDDV, 0, Serialized)
            {
                Return (Package (0x01)
                {
                    Buffer (0x0597)
                    {
                        /* 0000 */  0xE5, 0x1F, 0x94, 0x00, 0x00, 0x00, 0x00, 0x02,  // ........
                        /* 0008 */  0x00, 0x00, 0x00, 0x40, 0x67, 0x64, 0x64, 0x76,  // ...@gddv
                        /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0020 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0028 */  0x00, 0x00, 0x00, 0x00, 0x4F, 0x45, 0x4D, 0x20,  // ....OEM 
                        /* 0030 */  0x45, 0x78, 0x70, 0x6F, 0x72, 0x74, 0x65, 0x64,  // Exported
                        /* 0038 */  0x20, 0x44, 0x61, 0x74, 0x61, 0x56, 0x61, 0x75,  //  DataVau
                        /* 0040 */  0x6C, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // lt......
                        /* 0048 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0050 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0058 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0060 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                        /* 0068 */  0x00, 0x00, 0x00, 0x00, 0xB2, 0xF9, 0xF3, 0x54,  // .......T
                        /* 0070 */  0x4F, 0x14, 0x1E, 0x0F, 0xF8, 0x80, 0x81, 0x5D,  // O......]
                        /* 0078 */  0x6A, 0x3A, 0x19, 0xC4, 0x80, 0x7E, 0xD8, 0x93,  // j:...~..
                        /* 0080 */  0xF8, 0x65, 0x9D, 0x6B, 0x2A, 0x1D, 0x74, 0x05,  // .e.k*.t.
                        /* 0088 */  0xE7, 0x7E, 0x4C, 0xDF, 0x03, 0x05, 0x00, 0x00,  // .~L.....
                        /* 0090 */  0x52, 0x45, 0x50, 0x4F, 0x5D, 0x00, 0x00, 0x00,  // REPO]...
                        /* 0098 */  0x01, 0x6D, 0x4E, 0x00, 0x00, 0x00, 0x00, 0x00,  // .mN.....
                        /* 00A0 */  0x00, 0x00, 0x72, 0x87, 0xCD, 0xFF, 0x6D, 0x24,  // ..r...m$
                        /* 00A8 */  0x47, 0xDB, 0x3D, 0x24, 0x92, 0xB4, 0x16, 0x6F,  // G.=$...o
                        /* 00B0 */  0x45, 0xD8, 0xC3, 0xF5, 0x66, 0x14, 0x9F, 0x22,  // E...f.."
                        /* 00B8 */  0xD7, 0xF7, 0xDE, 0x67, 0x90, 0x9A, 0xA2, 0x0D,  // ...g....
                        /* 00C0 */  0x39, 0x25, 0xAD, 0xC3, 0x1A, 0xAD, 0x52, 0x0B,  // 9%....R.
                        /* 00C8 */  0x75, 0x38, 0xE1, 0xA4, 0x14, 0x40, 0xBA, 0x88,  // u8...@..
                        /* 00D0 */  0x67, 0x8D, 0xB2, 0x20, 0x79, 0x66, 0xD7, 0xF5,  // g.. yf..
                        /* 00D8 */  0xDB, 0x8D, 0x09, 0xEE, 0x6F, 0xE2, 0xB1, 0x8E,  // ....o...
                        /* 00E0 */  0x54, 0x9F, 0x09, 0x21, 0x29, 0xB7, 0x05, 0xFC,  // T..!)...
                        /* 00E8 */  0x2E, 0xC3, 0x39, 0x23, 0x06, 0xC9, 0x9E, 0x62,  // ..9#...b
                        /* 00F0 */  0x76, 0xA0, 0xB2, 0x31, 0xB7, 0xD7, 0xBC, 0x75,  // v..1...u
                        /* 00F8 */  0x77, 0xAC, 0x9B, 0xBC, 0xF8, 0xA1, 0xAE, 0x7F,  // w.......
                        /* 0100 */  0x19, 0x0A, 0x01, 0xEB, 0x44, 0xB3, 0x4A, 0xE9,  // ....D.J.
                        /* 0108 */  0xE6, 0x85, 0xBF, 0x47, 0xAA, 0x3C, 0xD4, 0x6B,  // ...G.<.k
                        /* 0110 */  0xDD, 0x1C, 0x47, 0xC8, 0x32, 0x4C, 0xA4, 0x71,  // ..G.2L.q
                        /* 0118 */  0x7E, 0xB2, 0x47, 0x5C, 0x54, 0x0D, 0xE6, 0x04,  // ~.G\T...
                        /* 0120 */  0x92, 0x60, 0x91, 0x38, 0xA1, 0xB6, 0x33, 0x3E,  // .`.8..3>
                        /* 0128 */  0x85, 0x74, 0xF3, 0x20, 0xBE, 0xE9, 0x36, 0x2E,  // .t. ..6.
                        /* 0130 */  0xD6, 0xEA, 0x53, 0x19, 0xAA, 0x39, 0xEF, 0xA6,  // ..S..9..
                        /* 0138 */  0xB0, 0x76, 0x90, 0x19, 0x83, 0xDF, 0x98, 0xC7,  // .v......
                        /* 0140 */  0x3F, 0xFD, 0xBF, 0x81, 0x7A, 0x1D, 0x31, 0x5B,  // ?...z.1[
                        /* 0148 */  0x2F, 0x49, 0xB8, 0x1D, 0xC6, 0xEA, 0x1F, 0x3D,  // /I.....=
                        /* 0150 */  0x0A, 0xA5, 0xD6, 0x61, 0x76, 0xD4, 0x5D, 0x4F,  // ...av.]O
                        /* 0158 */  0x0B, 0x6A, 0x69, 0x7C, 0xEC, 0x45, 0x11, 0x3B,  // .ji|.E.;
                        /* 0160 */  0xF7, 0x2C, 0x49, 0xEB, 0x32, 0x86, 0xBC, 0xE1,  // .,I.2...
                        /* 0168 */  0x29, 0x52, 0x9E, 0xE1, 0xE3, 0xC5, 0xB3, 0x86,  // )R......
                        /* 0170 */  0xD1, 0x8A, 0xDC, 0x35, 0x6B, 0xF4, 0x00, 0x5B,  // ...5k..[
                        /* 0178 */  0x16, 0xC4, 0x77, 0x3B, 0xE8, 0xE1, 0xCB, 0x41,  // ..w;...A
                        /* 0180 */  0xBA, 0xFC, 0x1A, 0xF1, 0xF5, 0x5D, 0x3C, 0xF4,  // .....]<.
                        /* 0188 */  0xE4, 0x16, 0x60, 0xEC, 0x80, 0xD3, 0x0A, 0x45,  // ..`....E
                        /* 0190 */  0x5B, 0x49, 0x88, 0x7E, 0x0E, 0x26, 0xD3, 0x48,  // [I.~.&.H
                        /* 0198 */  0xFA, 0x47, 0xED, 0x95, 0xAE, 0x2A, 0xAC, 0x74,  // .G...*.t
                        /* 01A0 */  0x96, 0xB7, 0x9F, 0x71, 0xE5, 0x5E, 0x16, 0xBC,  // ...q.^..
                        /* 01A8 */  0x20, 0x61, 0xFF, 0xED, 0xB5, 0xFD, 0xDF, 0x70,  //  a.....p
                        /* 01B0 */  0x53, 0xC0, 0xD8, 0xF1, 0x64, 0xD3, 0x68, 0x2D,  // S...d.h-
                        /* 01B8 */  0xBB, 0x88, 0xA7, 0x71, 0x47, 0x71, 0xAD, 0x5F,  // ...qGq._
                        /* 01C0 */  0x41, 0x59, 0xD2, 0xC0, 0x24, 0x35, 0x02, 0x2A,  // AY..$5.*
                        /* 01C8 */  0xA7, 0x7B, 0xA8, 0xFB, 0x17, 0x38, 0xFA, 0xC7,  // .{...8..
                        /* 01D0 */  0x96, 0x13, 0x1E, 0xA3, 0x50, 0x23, 0x8E, 0x96,  // ....P#..
                        /* 01D8 */  0x97, 0xA2, 0x9A, 0x6A, 0xCA, 0x16, 0x27, 0x70,  // ...j..'p
                        /* 01E0 */  0x6D, 0x99, 0x19, 0x29, 0xB9, 0xC6, 0x2B, 0x3F,  // m..)..+?
                        /* 01E8 */  0x31, 0xB7, 0x7B, 0x3B, 0xC4, 0xC0, 0xBD, 0x4A,  // 1.{;...J
                        /* 01F0 */  0xB9, 0xF1, 0x6D, 0x56, 0x04, 0xE0, 0x79, 0x59,  // ..mV..yY
                        /* 01F8 */  0x78, 0x2F, 0xE2, 0x3B, 0xA4, 0xEC, 0xF4, 0x3E,  // x/.;...>
                        /* 0200 */  0x89, 0x71, 0xCE, 0x51, 0x3E, 0xA4, 0xD7, 0x06,  // .q.Q>...
                        /* 0208 */  0xFE, 0x43, 0xDC, 0x5A, 0xF6, 0xDD, 0x37, 0xDB,  // .C.Z..7.
                        /* 0210 */  0x27, 0x90, 0x67, 0x2B, 0x68, 0x39, 0x7B, 0x89,  // '.g+h9{.
                        /* 0218 */  0xDB, 0x10, 0x40, 0x8C, 0x4A, 0x16, 0xC2, 0x98,  // ..@.J...
                        /* 0220 */  0xF9, 0xF9, 0x20, 0x29, 0x6B, 0x21, 0xEA, 0x13,  // .. )k!..
                        /* 0228 */  0xB8, 0xB2, 0xE4, 0xCD, 0xE9, 0x47, 0x88, 0x08,  // .....G..
                        /* 0230 */  0x50, 0xED, 0xAE, 0x74, 0xAE, 0x12, 0xF5, 0x39,  // P..t...9
                        /* 0238 */  0xAD, 0xA0, 0xF8, 0xEA, 0xE8, 0xF0, 0xE1, 0x32,  // .......2
                        /* 0240 */  0x3A, 0x35, 0x75, 0x55, 0xE7, 0x14, 0xEC, 0xA2,  // :5uU....
                        /* 0248 */  0x07, 0xD1, 0x63, 0xB4, 0x9C, 0x77, 0x2D, 0xAA,  // ..c..w-.
                        /* 0250 */  0x0D, 0x55, 0x48, 0x94, 0x41, 0x02, 0x91, 0xCB,  // .UH.A...
                        /* 0258 */  0x45, 0xEF, 0x75, 0xBB, 0xE0, 0xDD, 0x26, 0x47,  // E.u...&G
                        /* 0260 */  0xFF, 0x90, 0x6D, 0x31, 0x0A, 0xCA, 0x18, 0x41,  // ..m1...A
                        /* 0268 */  0xC1, 0xDB, 0x69, 0xCC, 0x17, 0xC9, 0x7E, 0x36,  // ..i...~6
                        /* 0270 */  0xC1, 0x03, 0x08, 0x78, 0x4A, 0x6A, 0x5C, 0x53,  // ...xJj\S
                        /* 0278 */  0xD6, 0x37, 0xF3, 0x19, 0xFD, 0x16, 0xC4, 0x8B,  // .7......
                        /* 0280 */  0x70, 0x52, 0xB2, 0x01, 0x97, 0x9E, 0x2C, 0xF8,  // pR....,.
                        /* 0288 */  0x26, 0x5E, 0x96, 0x3B, 0xBB, 0xE2, 0x51, 0x14,  // &^.;..Q.
                        /* 0290 */  0x89, 0xC4, 0x0E, 0x89, 0x7D, 0x14, 0x7E, 0xC5,  // ....}.~.
                        /* 0298 */  0xD1, 0x85, 0x31, 0x84, 0x5B, 0x33, 0x1A, 0xDE,  // ..1.[3..
                        /* 02A0 */  0xB4, 0xCD, 0xB7, 0x06, 0x9F, 0xF3, 0x6C, 0x16,  // ......l.
                        /* 02A8 */  0x7C, 0x99, 0xB1, 0x92, 0x13, 0xD0, 0x60, 0xB9,  // |.....`.
                        /* 02B0 */  0xC2, 0xC2, 0xD6, 0xA8, 0xD0, 0xF6, 0x12, 0x48,  // .......H
                        /* 02B8 */  0x00, 0x84, 0x30, 0x33, 0xFA, 0x31, 0x45, 0x5A,  // ..03.1EZ
                        /* 02C0 */  0xBA, 0x31, 0xE0, 0x72, 0x5F, 0xDC, 0xBB, 0x33,  // .1.r_..3
                        /* 02C8 */  0x4C, 0x17, 0xAD, 0x34, 0x82, 0xCF, 0xE0, 0x99,  // L..4....
                        /* 02D0 */  0x37, 0xA3, 0xCD, 0x69, 0xD7, 0xD6, 0x67, 0x5B,  // 7..i..g[
                        /* 02D8 */  0xE4, 0x38, 0x99, 0x6C, 0x2F, 0xF1, 0xAF, 0x1B,  // .8.l/...
                        /* 02E0 */  0xAB, 0x69, 0xB7, 0x55, 0x94, 0x89, 0xDF, 0x82,  // .i.U....
                        /* 02E8 */  0x41, 0x71, 0x76, 0x01, 0x0B, 0x89, 0xE7, 0x5A,  // Aqv....Z
                        /* 02F0 */  0xDF, 0x22, 0x42, 0x76, 0x5D, 0x65, 0x44, 0xF9,  // ."Bv]eD.
                        /* 02F8 */  0xD1, 0x66, 0xC1, 0x21, 0x15, 0x8A, 0xA3, 0xDD,  // .f.!....
                        /* 0300 */  0x8C, 0x5D, 0xF0, 0xF4, 0x79, 0x0C, 0x43, 0x80,  // .]..y.C.
                        /* 0308 */  0xDA, 0x90, 0xDB, 0xB8, 0x88, 0x15, 0xB7, 0x2F,  // ......./
                        /* 0310 */  0x84, 0x9C, 0x90, 0x77, 0xC9, 0x91, 0xB4, 0x9A,  // ...w....
                        /* 0318 */  0x37, 0xFC, 0xD0, 0x3F, 0xEB, 0x19, 0x64, 0x57,  // 7..?..dW
                        /* 0320 */  0x24, 0x10, 0x7C, 0x91, 0x57, 0x57, 0xF8, 0xBB,  // $.|.WW..
                        /* 0328 */  0x49, 0x61, 0x17, 0xA4, 0xDC, 0x28, 0x15, 0x9C,  // Ia...(..
                        /* 0330 */  0x8F, 0xC2, 0xD2, 0x97, 0x23, 0xD4, 0x89, 0x3C,  // ....#..<
                        /* 0338 */  0x51, 0x4C, 0x6C, 0xD7, 0xCC, 0x36, 0x03, 0x6C,  // QLl..6.l
                        /* 0340 */  0x87, 0x39, 0x34, 0x0F, 0xAD, 0x2D, 0x3D, 0xCC,  // .94..-=.
                        /* 0348 */  0x4E, 0x86, 0x65, 0x2E, 0x0A, 0x82, 0x1D, 0xDD,  // N.e.....
                        /* 0350 */  0x6C, 0xE1, 0xF7, 0x9C, 0x3C, 0x2A, 0xE8, 0x2D,  // l...<*.-
                        /* 0358 */  0x57, 0x42, 0x97, 0xA4, 0x46, 0x89, 0x28, 0x6D,  // WB..F.(m
                        /* 0360 */  0xFF, 0x8A, 0x27, 0x73, 0x47, 0xA1, 0xF7, 0x80,  // ..'sG...
                        /* 0368 */  0x03, 0x78, 0x10, 0xC0, 0x6A, 0xB6, 0xB1, 0x67,  // .x..j..g
                        /* 0370 */  0x5B, 0xF2, 0xBB, 0x84, 0x4E, 0x38, 0x5E, 0x3E,  // [...N8^>
                        /* 0378 */  0x95, 0x61, 0x8C, 0xE9, 0xA7, 0x57, 0xC3, 0xB5,  // .a...W..
                        /* 0380 */  0x1B, 0xC9, 0x5E, 0xF1, 0x7C, 0x9F, 0xED, 0xFC,  // ..^.|...
                        /* 0388 */  0x34, 0x00, 0x5C, 0xFC, 0xE6, 0xA2, 0xDF, 0xE3,  // 4.\.....
                        /* 0390 */  0xF1, 0x5D, 0xC9, 0x5B, 0xD2, 0xD0, 0x9A, 0x58,  // .].[...X
                        /* 0398 */  0xCD, 0x88, 0x0D, 0xE1, 0x42, 0x5A, 0x6C, 0x79,  // ....BZly
                        /* 03A0 */  0x9E, 0x16, 0x23, 0x14, 0x1F, 0x4E, 0x48, 0x76,  // ..#..NHv
                        /* 03A8 */  0x40, 0x30, 0x14, 0xD5, 0xC8, 0xEF, 0xFC, 0x3F,  // @0.....?
                        /* 03B0 */  0x9F, 0x44, 0x1E, 0xC7, 0xE6, 0x37, 0x4E, 0x35,  // .D...7N5
                        /* 03B8 */  0x36, 0xCF, 0x13, 0x67, 0xBB, 0x77, 0x69, 0xEA,  // 6..g.wi.
                        /* 03C0 */  0x3C, 0x95, 0xEB, 0x91, 0xBA, 0x83, 0x7A, 0x9C,  // <.....z.
                        /* 03C8 */  0x97, 0xDE, 0x2D, 0x35, 0x31, 0x61, 0xFD, 0xAE,  // ..-51a..
                        /* 03D0 */  0x25, 0x9D, 0xA1, 0x21, 0xDB, 0x7E, 0xB3, 0x92,  // %..!.~..
                        /* 03D8 */  0x23, 0x5E, 0xC0, 0x69, 0xEF, 0x6B, 0xBB, 0x93,  // #^.i.k..
                        /* 03E0 */  0x38, 0xC7, 0xE5, 0x42, 0x57, 0x79, 0xA6, 0xE1,  // 8..BWy..
                        /* 03E8 */  0x16, 0x15, 0x22, 0xF9, 0xC1, 0x0D, 0xD8, 0x8B,  // ..".....
                        /* 03F0 */  0x49, 0x44, 0xB6, 0x87, 0x69, 0xE2, 0x82, 0x4D,  // ID..i..M
                        /* 03F8 */  0x89, 0x86, 0x3F, 0x58, 0xFE, 0x83, 0x6A, 0x48,  // ..?X..jH
                        /* 0400 */  0x9A, 0x9C, 0xF6, 0x0C, 0x66, 0x5D, 0x2A, 0x2E,  // ....f]*.
                        /* 0408 */  0xF0, 0xEF, 0x49, 0xB9, 0xAA, 0x56, 0x91, 0xE0,  // ..I..V..
                        /* 0410 */  0xA1, 0xCC, 0x5C, 0xCE, 0xE7, 0x3E, 0xBA, 0x90,  // ..\..>..
                        /* 0418 */  0x03, 0x8F, 0xC9, 0xDD, 0x8E, 0x6F, 0x84, 0x04,  // .....o..
                        /* 0420 */  0x0C, 0xC6, 0x1E, 0x77, 0x71, 0x1C, 0x37, 0x54,  // ...wq.7T
                        /* 0428 */  0x82, 0x08, 0x0A, 0x72, 0x3D, 0x4E, 0x50, 0xFC,  // ...r=NP.
                        /* 0430 */  0x82, 0xAC, 0xB2, 0x4F, 0x64, 0x11, 0xF6, 0xB0,  // ...Od...
                        /* 0438 */  0xA4, 0x47, 0x92, 0x7C, 0x59, 0x03, 0x89, 0xA3,  // .G.|Y...
                        /* 0440 */  0xB9, 0xC8, 0xD7, 0x30, 0x36, 0x31, 0x2B, 0xBA,  // ...061+.
                        /* 0448 */  0xB1, 0x8B, 0x87, 0x1B, 0xFE, 0xEA, 0x08, 0xBF,  // ........
                        /* 0450 */  0xE9, 0xD1, 0x45, 0xFC, 0x1B, 0x02, 0x5F, 0x22,  // ..E..._"
                        /* 0458 */  0x02, 0x22, 0xB4, 0x2A, 0x25, 0x33, 0x7A, 0x7B,  // .".*%3z{
                        /* 0460 */  0x58, 0xA5, 0xAD, 0x1E, 0x05, 0xF0, 0x9F, 0x40,  // X......@
                        /* 0468 */  0x83, 0xC6, 0x3D, 0xCD, 0x76, 0xBE, 0x53, 0x70,  // ..=.v.Sp
                        /* 0470 */  0xA6, 0x9A, 0xB6, 0xAD, 0x21, 0x18, 0x19, 0x2F,  // ....!../
                        /* 0478 */  0x9D, 0x2F, 0xF1, 0xB6, 0xE2, 0x6D, 0x46, 0x9A,  // ./...mF.
                        /* 0480 */  0x6E, 0x59, 0xE0, 0x8D, 0x32, 0xF3, 0x2F, 0x18,  // nY..2./.
                        /* 0488 */  0xB9, 0x8B, 0x51, 0x69, 0x7F, 0x8C, 0xF9, 0xB7,  // ..Qi....
                        /* 0490 */  0x37, 0xBD, 0x39, 0x32, 0xC3, 0x2B, 0x69, 0x7A,  // 7.92.+iz
                        /* 0498 */  0x87, 0x29, 0x41, 0x27, 0x25, 0xC5, 0x5B, 0xCE,  // .)A'%.[.
                        /* 04A0 */  0x7A, 0x5C, 0x7B, 0xBE, 0xC6, 0x19, 0xC3, 0x31,  // z\{....1
                        /* 04A8 */  0x67, 0x05, 0x8C, 0x3E, 0xB1, 0x95, 0xCF, 0x0E,  // g..>....
                        /* 04B0 */  0x59, 0x0D, 0x8B, 0xD2, 0xA6, 0x9C, 0x0C, 0x8D,  // Y.......
                        /* 04B8 */  0x5F, 0xAF, 0xD0, 0x7D, 0x1F, 0x62, 0x46, 0xC1,  // _..}.bF.
                        /* 04C0 */  0x67, 0x26, 0x6C, 0x10, 0x35, 0x67, 0x33, 0x60,  // g&l.5g3`
                        /* 04C8 */  0x09, 0x64, 0x82, 0x96, 0x6A, 0x1B, 0x95, 0x71,  // .d..j..q
                        /* 04D0 */  0xCE, 0x46, 0xEB, 0x77, 0xCC, 0x7C, 0x7E, 0x9F,  // .F.w.|~.
                        /* 04D8 */  0xE2, 0x4E, 0x39, 0xAA, 0x58, 0x46, 0x4D, 0xA8,  // .N9.XFM.
                        /* 04E0 */  0x0A, 0xE8, 0x4C, 0xE7, 0xDC, 0xAC, 0xFC, 0x46,  // ..L....F
                        /* 04E8 */  0x77, 0x95, 0x9B, 0x19, 0xAD, 0x91, 0x37, 0x5B,  // w.....7[
                        /* 04F0 */  0xDD, 0x5F, 0x5C, 0x18, 0xE3, 0x93, 0x6C, 0x2E,  // ._\...l.
                        /* 04F8 */  0x7F, 0xF6, 0xF0, 0xDD, 0x99, 0xF5, 0x24, 0xBE,  // ......$.
                        /* 0500 */  0xA8, 0xA1, 0x1C, 0x8F, 0xC2, 0xD0, 0x8A, 0xAF,  // ........
                        /* 0508 */  0x04, 0xB8, 0xE9, 0xDE, 0x4E, 0xB6, 0x64, 0xBE,  // ....N.d.
                        /* 0510 */  0x97, 0xAB, 0x1E, 0x73, 0xCA, 0x04, 0xC6, 0x46,  // ...s...F
                        /* 0518 */  0xF4, 0x32, 0x77, 0x33, 0xF1, 0xE2, 0x22, 0x09,  // .2w3..".
                        /* 0520 */  0xDE, 0x7D, 0xEB, 0x86, 0xD5, 0x57, 0x4E, 0x21,  // .}...WN!
                        /* 0528 */  0x84, 0xD3, 0x59, 0xEA, 0xED, 0x2E, 0xB7, 0x17,  // ..Y.....
                        /* 0530 */  0x3F, 0xEE, 0x1D, 0xE1, 0xD1, 0x1B, 0xEA, 0x24,  // ?......$
                        /* 0538 */  0x1F, 0x9E, 0x3C, 0xB9, 0x77, 0x59, 0x1C, 0xB9,  // ..<.wY..
                        /* 0540 */  0x22, 0xAB, 0x63, 0xEA, 0xE8, 0xF0, 0xF7, 0x31,  // ".c....1
                        /* 0548 */  0x32, 0xF9, 0xEC, 0xF1, 0xB5, 0x19, 0xFC, 0xE3,  // 2.......
                        /* 0550 */  0xAF, 0x1E, 0xFD, 0x64, 0x1E, 0x57, 0xAD, 0xBF,  // ...d.W..
                        /* 0558 */  0x43, 0xF4, 0xC7, 0x54, 0xF4, 0xA5, 0x80, 0x4F,  // C..T...O
                        /* 0560 */  0xFA, 0x85, 0x96, 0xF6, 0x08, 0xA1, 0x7B, 0x75,  // ......{u
                        /* 0568 */  0xAD, 0xB4, 0x49, 0x09, 0x4F, 0x89, 0xBB, 0x02,  // ..I.O...
                        /* 0570 */  0x80, 0xC0, 0x92, 0x73, 0xF0, 0x23, 0xAC, 0xD1,  // ...s.#..
                        /* 0578 */  0x9B, 0x2F, 0x91, 0x9D, 0x4D, 0x9E, 0xA4, 0xD0,  // ./..M...
                        /* 0580 */  0x00, 0xB9, 0x44, 0x7C, 0xCA, 0x19, 0x78, 0x6D,  // ..D|..xm
                        /* 0588 */  0x7A, 0x8E, 0x09, 0xC1, 0x08, 0x9D, 0x4F, 0x94,  // z.....O.
                        /* 0590 */  0xB8, 0xD5, 0x50, 0x84, 0x32, 0xBB, 0x00         // ..P.2..
                    }
                })
            }

            Method (IMOK, 1, NotSerialized)
            {
                Return (Arg0)
            }
        }
    }

    Scope (\_SB.PC00.LPCB.EC0)
    {
        Device (SEN2)
        {
            Name (_UID, "SEN2")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Name (_STR, Unicode ("Thermistor GT VR"))  // _STR: Description String
            Name (PTYP, 0x03)
            Name (CTYP, Zero)
            Name (PFLG, Zero)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((\S2DE == One))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (_TMP, 0, Serialized)  // _TMP: Temperature
            {
                If (\_SB.PC00.LPCB.EC0.ECOK)
                {
                    Acquire (\_SB.PC00.LPCB.EC0.MUT0, 0x1000)
                    Local0 = \_SB.PC00.LPCB.EC0.VLTT /* External reference */
                    Local1 = ((Local0 * 0x0A) + 0x0AAC)
                    Release (\_SB.PC00.LPCB.EC0.MUT0)
                    Return (Local1)
                }

                Return (0x0BB8)
            }

            Name (PATC, 0x02)
            Method (PAT0, 1, Serialized)
            {
            }

            Method (PAT1, 1, Serialized)
            {
            }

            Name (GTSH, 0x14)
            Name (LSTM, Zero)
            Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
            {
                LSTM = Arg0
                Notify (\_SB.PC00.LPCB.EC0.SEN2, 0x91) // Device-Specific
            }

            Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
            {
                Return (0x0ADE)
            }

            Name (S2AC, 0x3C)
            Name (S2A1, 0x32)
            Name (S2A2, 0x28)
            Name (S2PV, 0x41)
            Name (S2CC, 0x50)
            Name (S2C3, 0x46)
            Name (S2HP, 0x4B)
            Name (SSP2, 0x32)
            Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
            {
                Return (SSP2) /* \_SB_.PC00.LPCB.EC0_.SEN2.SSP2 */
            }

            Method (_AC0, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Local1 = \_SB.IETM.CTOK (S2AC)
                If ((LSTM >= Local1))
                {
                    Return ((Local1 - 0x14))
                }
                Else
                {
                    Return (Local1)
                }
            }

            Method (_AC1, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Return (\_SB.IETM.CTOK (S2A1))
            }

            Method (_AC2, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Return (\_SB.IETM.CTOK (S2A2))
            }

            Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
            {
                Return (\_SB.IETM.CTOK (S2PV))
            }

            Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
            {
                Return (\_SB.IETM.CTOK (S2CC))
            }

            Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
            {
                Return (\_SB.IETM.CTOK (S2C3))
            }

            Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
            {
                Return (\_SB.IETM.CTOK (S2HP))
            }
        }
    }

    Scope (\_SB.PC00.LPCB.EC0)
    {
        Device (SEN3)
        {
            Name (_UID, "SEN3")  // _UID: Unique ID
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                Return (\_SB.IETM.GHID (_UID))
            }

            Name (_STR, Unicode ("Remote GT Sensor"))  // _STR: Description String
            Name (PTYP, 0x03)
            Name (CTYP, Zero)
            Name (PFLG, Zero)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((\S3DE == One))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (_TMP, 0, Serialized)  // _TMP: Temperature
            {
                If (\_SB.PC00.LPCB.EC0.ECOK)
                {
                    Acquire (\_SB.PC00.LPCB.EC0.MUT0, 0x1000)
                    Local0 = \_SB.PC00.LPCB.EC0.VRTT /* External reference */
                    Local1 = ((Local0 * 0x0A) + 0x0AAC)
                    Release (\_SB.PC00.LPCB.EC0.MUT0)
                    Return (Local1)
                }

                Return (0x0BB8)
            }

            Name (PATC, 0x02)
            Method (PAT0, 1, Serialized)
            {
            }

            Method (PAT1, 1, Serialized)
            {
            }

            Name (GTSH, 0x14)
            Name (LSTM, Zero)
            Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
            {
                LSTM = Arg0
                Notify (\_SB.PC00.LPCB.EC0.SEN3, 0x91) // Device-Specific
            }

            Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
            {
                Return (0x0ADE)
            }

            Name (S3AC, 0x3C)
            Name (S3A1, 0x32)
            Name (S3A2, 0x28)
            Name (S3PV, 0x41)
            Name (S3CC, 0x50)
            Name (S3C3, 0x46)
            Name (S3HP, 0x4B)
            Name (SSP3, 0x32)
            Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
            {
                Return (SSP3) /* \_SB_.PC00.LPCB.EC0_.SEN3.SSP3 */
            }

            Method (_AC3, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Local1 = \_SB.IETM.CTOK (S3AC)
                If ((LSTM >= Local1))
                {
                    Return ((Local1 - 0x14))
                }
                Else
                {
                    Return (Local1)
                }
            }

            Method (_AC4, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Return (\_SB.IETM.CTOK (S3A1))
            }

            Method (_AC5, 0, Serialized)  // _ACx: Active Cooling, x=0-9
            {
                Return (\_SB.IETM.CTOK (S3A2))
            }

            Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
            {
                Return (\_SB.IETM.CTOK (S3PV))
            }

            Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
            {
                Return (\_SB.IETM.CTOK (S3CC))
            }

            Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
            {
                Return (\_SB.IETM.CTOK (S3C3))
            }

            Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
            {
                Return (\_SB.IETM.CTOK (S3HP))
            }
        }
    }

    Scope (\_SB.PC00.TCPU)
    {
        Name (PFLG, Zero)
        Method (_STA, 0, NotSerialized)  // _STA: Status
        {
            If ((\SADE == One))
            {
                Return (0x0F)
            }
            Else
            {
                Return (Zero)
            }
        }

        OperationRegion (CPWR, SystemMemory, ((\_SB.PC00.MC.MHBR << 0x0F) + 0x5000), 0x1000)
        Field (CPWR, ByteAcc, NoLock, Preserve)
        {
            Offset (0x930), 
            PTDP,   15, 
            Offset (0x932), 
            PMIN,   15, 
            Offset (0x934), 
            PMAX,   15, 
            Offset (0x936), 
            TMAX,   7, 
            Offset (0x938), 
            PWRU,   4, 
            Offset (0x939), 
            EGYU,   5, 
            Offset (0x93A), 
            TIMU,   4, 
            Offset (0x958), 
            Offset (0x95C), 
            LPMS,   1, 
            CTNL,   2, 
            Offset (0x978), 
            PCTP,   8, 
            Offset (0x998), 
            RP0C,   8, 
            RP1C,   8, 
            RPNC,   8, 
            Offset (0xF3C), 
            TRAT,   8, 
            Offset (0xF40), 
            PTD1,   15, 
            Offset (0xF42), 
            TRA1,   8, 
            Offset (0xF44), 
            PMX1,   15, 
            Offset (0xF46), 
            PMN1,   15, 
            Offset (0xF48), 
            PTD2,   15, 
            Offset (0xF4A), 
            TRA2,   8, 
            Offset (0xF4C), 
            PMX2,   15, 
            Offset (0xF4E), 
            PMN2,   15, 
            Offset (0xF50), 
            CTCL,   2, 
                ,   29, 
            CLCK,   1, 
            MNTR,   8
        }

        Name (XPCC, Zero)
        Method (PPCC, 0, Serialized)
        {
            If (((XPCC == Zero) && CondRefOf (\_SB.CBMI)))
            {
                Switch (ToInteger (\_SB.CBMI))
                {
                    Case (Zero)
                    {
                        If (((\_SB.CLVL >= One) && (\_SB.CLVL <= 0x03)))
                        {
                            CPL0 ()
                            XPCC = One
                        }
                    }
                    Case (One)
                    {
                        If (((\_SB.CLVL == 0x02) || (\_SB.CLVL == 0x03)))
                        {
                            CPL1 ()
                            XPCC = One
                        }
                    }
                    Case (0x02)
                    {
                        If ((\_SB.CLVL == 0x03))
                        {
                            CPL2 ()
                            XPCC = One
                        }
                    }

                }
            }

            Return (NPCC) /* \_SB_.PC00.TCPU.NPCC */
        }

        Name (NPCC, Package (0x03)
        {
            0x02, 
            Package (0x06)
            {
                Zero, 
                0x88B8, 
                0xAFC8, 
                0x6D60, 
                0x7D00, 
                0x03E8
            }, 

            Package (0x06)
            {
                One, 
                0xDBBA, 
                0xDBBA, 
                Zero, 
                Zero, 
                0x03E8
            }
        })
        Method (CPNU, 2, Serialized)
        {
            Name (CNVT, Zero)
            Name (PPUU, Zero)
            Name (RMDR, Zero)
            If ((PWRU == Zero))
            {
                PPUU = One
            }
            Else
            {
                PPUU = (PWRU-- << 0x02)
            }

            Divide (Arg0, PPUU, RMDR, CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
            If ((Arg1 == Zero))
            {
                Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
            }
            Else
            {
                CNVT *= 0x03E8
                RMDR *= 0x03E8
                RMDR /= PPUU
                CNVT += RMDR /* \_SB_.PC00.TCPU.CPNU.RMDR */
                Return (CNVT) /* \_SB_.PC00.TCPU.CPNU.CNVT */
            }
        }

        Method (CPL0, 0, NotSerialized)
        {
            \_SB.PC00.TCPU.NPCC [Zero] = 0x02
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL10, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW0 * 0x03E8)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW0 * 0x03E8
                ) + 0x0FA0)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL20, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL20, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
        }

        Method (CPL1, 0, NotSerialized)
        {
            \_SB.PC00.TCPU.NPCC [Zero] = 0x02
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL11, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW1 * 0x03E8)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW1 * 0x03E8
                ) + 0x0FA0)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL21, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL21, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
        }

        Method (CPL2, 0, NotSerialized)
        {
            \_SB.PC00.TCPU.NPCC [Zero] = 0x02
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [Zero] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [One] = 0x7D
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x02] = CPNU (\_SB.PL12, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x03] = (\_SB.PLW2 * 0x03E8)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x04] = ((\_SB.PLW2 * 0x03E8
                ) + 0x0FA0)
            DerefOf (\_SB.PC00.TCPU.NPCC [One]) [0x05] = PPSZ /* External reference */
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [Zero] = One
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [One] = CPNU (\_SB.PL22, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x02] = CPNU (\_SB.PL22, One)
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x03] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x04] = Zero
            DerefOf (\_SB.PC00.TCPU.NPCC [0x02]) [0x05] = PPSZ /* External reference */
        }

        Name (LSTM, Zero)
        Name (_PPC, Zero)  // _PPC: Performance Present Capabilities
        Method (SPPC, 1, Serialized)
        {
            If (CondRefOf (\_SB.CPPC))
            {
                \_SB.CPPC = Arg0
            }

            If ((ToInteger (\TCNT) > Zero))
            {
                Notify (\_SB.PR00, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > One))
            {
                Notify (\_SB.PR01, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x02))
            {
                Notify (\_SB.PR02, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x03))
            {
                Notify (\_SB.PR03, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x04))
            {
                Notify (\_SB.PR04, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x05))
            {
                Notify (\_SB.PR05, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x06))
            {
                Notify (\_SB.PR06, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x07))
            {
                Notify (\_SB.PR07, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x08))
            {
                Notify (\_SB.PR08, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x09))
            {
                Notify (\_SB.PR09, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0A))
            {
                Notify (\_SB.PR10, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0B))
            {
                Notify (\_SB.PR11, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0C))
            {
                Notify (\_SB.PR12, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0D))
            {
                Notify (\_SB.PR13, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0E))
            {
                Notify (\_SB.PR14, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x0F))
            {
                Notify (\_SB.PR15, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x10))
            {
                Notify (\_SB.PR16, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x11))
            {
                Notify (\_SB.PR17, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x12))
            {
                Notify (\_SB.PR18, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x13))
            {
                Notify (\_SB.PR19, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x14))
            {
                Notify (\_SB.PR20, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x15))
            {
                Notify (\_SB.PR21, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x16))
            {
                Notify (\_SB.PR22, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x17))
            {
                Notify (\_SB.PR23, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x18))
            {
                Notify (\_SB.PR24, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x19))
            {
                Notify (\_SB.PR25, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1A))
            {
                Notify (\_SB.PR26, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1B))
            {
                Notify (\_SB.PR27, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1C))
            {
                Notify (\_SB.PR28, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1D))
            {
                Notify (\_SB.PR29, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1E))
            {
                Notify (\_SB.PR30, 0x80) // Status Change
            }

            If ((ToInteger (\TCNT) > 0x1F))
            {
                Notify (\_SB.PR31, 0x80) // Status Change
            }
        }

        Method (SPUR, 1, NotSerialized)
        {
            If ((Arg0 <= \TCNT))
            {
                If ((\_SB.PAGD._STA () == 0x0F))
                {
                    \_SB.PAGD._PUR [One] = Arg0
                    Notify (\_SB.PAGD, 0x80) // Status Change
                }
            }
        }

        Method (PCCC, 0, Serialized)
        {
            PCCX [Zero] = One
            Switch (ToInteger (CPNU (PTDP, Zero)))
            {
                Case (0x39)
                {
                    DerefOf (PCCX [One]) [Zero] = 0xA7F8
                    DerefOf (PCCX [One]) [One] = 0x00017318
                }
                Case (0x2F)
                {
                    DerefOf (PCCX [One]) [Zero] = 0x9858
                    DerefOf (PCCX [One]) [One] = 0x00014C08
                }
                Case (0x25)
                {
                    DerefOf (PCCX [One]) [Zero] = 0x7148
                    DerefOf (PCCX [One]) [One] = 0xD6D8
                }
                Case (0x19)
                {
                    DerefOf (PCCX [One]) [Zero] = 0x3E80
                    DerefOf (PCCX [One]) [One] = 0x7D00
                }
                Case (0x0F)
                {
                    DerefOf (PCCX [One]) [Zero] = 0x36B0
                    DerefOf (PCCX [One]) [One] = 0x7D00
                }
                Case (0x0B)
                {
                    DerefOf (PCCX [One]) [Zero] = 0x36B0
                    DerefOf (PCCX [One]) [One] = 0x61A8
                }
                Default
                {
                    DerefOf (PCCX [One]) [Zero] = 0xFF
                    DerefOf (PCCX [One]) [One] = 0xFF
                }

            }

            Return (PCCX) /* \_SB_.PC00.TCPU.PCCX */
        }

        Name (PCCX, Package (0x02)
        {
            0x80000000, 
            Package (0x02)
            {
                0x80000000, 
                0x80000000
            }
        })
        Name (KEFF, Package (0x1E)
        {
            Package (0x02)
            {
                0x01BC, 
                Zero
            }, 

            Package (0x02)
            {
                0x01CF, 
                0x27
            }, 

            Package (0x02)
            {
                0x01E1, 
                0x4B
            }, 

            Package (0x02)
            {
                0x01F3, 
                0x6C
            }, 

            Package (0x02)
            {
                0x0206, 
                0x8B
            }, 

            Package (0x02)
            {
                0x0218, 
                0xA8
            }, 

            Package (0x02)
            {
                0x022A, 
                0xC3
            }, 

            Package (0x02)
            {
                0x023D, 
                0xDD
            }, 

            Package (0x02)
            {
                0x024F, 
                0xF4
            }, 

            Package (0x02)
            {
                0x0261, 
                0x010B
            }, 

            Package (0x02)
            {
                0x0274, 
                0x011F
            }, 

            Package (0x02)
            {
                0x032C, 
                0x01BD
            }, 

            Package (0x02)
            {
                0x03D7, 
                0x0227
            }, 

            Package (0x02)
            {
                0x048B, 
                0x026D
            }, 

            Package (0x02)
            {
                0x053E, 
                0x02A1
            }, 

            Package (0x02)
            {
                0x05F7, 
                0x02C6
            }, 

            Package (0x02)
            {
                0x06A8, 
                0x02E6
            }, 

            Package (0x02)
            {
                0x075D, 
                0x02FF
            }, 

            Package (0x02)
            {
                0x0818, 
                0x0311
            }, 

            Package (0x02)
            {
                0x08CF, 
                0x0322
            }, 

            Package (0x02)
            {
                0x179C, 
                0x0381
            }, 

            Package (0x02)
            {
                0x2DDC, 
                0x039C
            }, 

            Package (0x02)
            {
                0x44A8, 
                0x039E
            }, 

            Package (0x02)
            {
                0x5C35, 
                0x0397
            }, 

            Package (0x02)
            {
                0x747D, 
                0x038D
            }, 

            Package (0x02)
            {
                0x8D7F, 
                0x0382
            }, 

            Package (0x02)
            {
                0xA768, 
                0x0376
            }, 

            Package (0x02)
            {
                0xC23B, 
                0x0369
            }, 

            Package (0x02)
            {
                0xDE26, 
                0x035A
            }, 

            Package (0x02)
            {
                0xFB7C, 
                0x034A
            }
        })
        Name (CEUP, Package (0x06)
        {
            0x80000000, 
            0x80000000, 
            0x80000000, 
            0x80000000, 
            0x80000000, 
            0x80000000
        })
        Method (TMPX, 0, Serialized)
        {
            Return (\_SB.IETM.CTOK (PCTP))
        }

        Method (_DTI, 1, NotSerialized)  // _DTI: Device Temperature Indication
        {
            LSTM = Arg0
            Notify (\_SB.PC00.TCPU, 0x91) // Device-Specific
        }

        Method (_NTT, 0, NotSerialized)  // _NTT: Notification Temperature Threshold
        {
            Return (0x0ADE)
        }

        Name (PTYP, Zero)
        Method (_PSS, 0, NotSerialized)  // _PSS: Performance Supported States
        {
            If (CondRefOf (\_SB.PR00._PSS))
            {
                Return (\_SB.PR00._PSS ())
            }
            Else
            {
                Return (Package (0x02)
                {
                    Package (0x06)
                    {
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero
                    }, 

                    Package (0x06)
                    {
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero
                    }
                })
            }
        }

        Method (_TSS, 0, NotSerialized)  // _TSS: Throttling Supported States
        {
            If (CondRefOf (\_SB.PR00._TSS))
            {
                Return (\_SB.PR00._TSS ())
            }
            Else
            {
                Return (Package (0x01)
                {
                    Package (0x05)
                    {
                        One, 
                        Zero, 
                        Zero, 
                        Zero, 
                        Zero
                    }
                })
            }
        }

        Method (_TPC, 0, NotSerialized)  // _TPC: Throttling Present Capabilities
        {
            If (CondRefOf (\_SB.PR00._TPC))
            {
                Return (\_SB.PR00._TPC) /* External reference */
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (_PTC, 0, NotSerialized)  // _PTC: Processor Throttling Control
        {
            If ((CondRefOf (\PF00) && (\PF00 != 0x80000000)))
            {
                If ((\PF00 & 0x04))
                {
                    Return (Package (0x02)
                    {
                        ResourceTemplate ()
                        {
                            Register (FFixedHW, 
                                0x00,               // Bit Width
                                0x00,               // Bit Offset
                                0x0000000000000000, // Address
                                ,)
                        }, 

                        ResourceTemplate ()
                        {
                            Register (FFixedHW, 
                                0x00,               // Bit Width
                                0x00,               // Bit Offset
                                0x0000000000000000, // Address
                                ,)
                        }
                    })
                }
                Else
                {
                    Return (Package (0x02)
                    {
                        ResourceTemplate ()
                        {
                            Register (SystemIO, 
                                0x05,               // Bit Width
                                0x00,               // Bit Offset
                                0x0000000000001810, // Address
                                ,)
                        }, 

                        ResourceTemplate ()
                        {
                            Register (SystemIO, 
                                0x05,               // Bit Width
                                0x00,               // Bit Offset
                                0x0000000000001810, // Address
                                ,)
                        }
                    })
                }
            }
            Else
            {
                Return (Package (0x02)
                {
                    ResourceTemplate ()
                    {
                        Register (FFixedHW, 
                            0x00,               // Bit Width
                            0x00,               // Bit Offset
                            0x0000000000000000, // Address
                            ,)
                    }, 

                    ResourceTemplate ()
                    {
                        Register (FFixedHW, 
                            0x00,               // Bit Width
                            0x00,               // Bit Offset
                            0x0000000000000000, // Address
                            ,)
                    }
                })
            }
        }

        Method (_TSD, 0, NotSerialized)  // _TSD: Throttling State Dependencies
        {
            If (CondRefOf (\_SB.PR00._TSD))
            {
                Return (\_SB.PR00._TSD ())
            }
            Else
            {
                Return (Package (0x01)
                {
                    Package (0x05)
                    {
                        0x05, 
                        Zero, 
                        Zero, 
                        0xFC, 
                        Zero
                    }
                })
            }
        }

        Method (_TDL, 0, NotSerialized)  // _TDL: T-State Depth Limit
        {
            If ((CondRefOf (\_SB.PR00._TSS) && CondRefOf (\_SB.CFGD)))
            {
                If ((\_SB.CFGD & 0x2000))
                {
                    Return ((SizeOf (\_SB.PR00.TSMF) - One))
                }
                Else
                {
                    Return ((SizeOf (\_SB.PR00.TSMC) - One))
                }
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (_PDL, 0, NotSerialized)  // _PDL: P-state Depth Limit
        {
            If (CondRefOf (\_SB.PR00._PSS))
            {
                If ((\_SB.OSCP & 0x0400))
                {
                    Return ((SizeOf (\_SB.PR00.TPSS) - One))
                }
                Else
                {
                    Return ((SizeOf (\_SB.PR00.LPSS) - One))
                }
            }
            Else
            {
                Return (Zero)
            }
        }

        Name (TJMX, 0x6E)
        Method (_TSP, 0, Serialized)  // _TSP: Thermal Sampling Period
        {
            Return (Zero)
        }

        Method (_AC0, 0, Serialized)  // _ACx: Active Cooling, x=0-9
        {
            Local1 = \_SB.IETM.CTOK (TJMX)
            Local1 -= 0x0A
            If ((LSTM >= Local1))
            {
                Return ((Local1 - 0x14))
            }
            Else
            {
                Return (Local1)
            }
        }

        Method (_AC1, 0, Serialized)  // _ACx: Active Cooling, x=0-9
        {
            Local1 = \_SB.IETM.CTOK (TJMX)
            Local1 -= 0x1E
            If ((LSTM >= Local1))
            {
                Return ((Local1 - 0x14))
            }
            Else
            {
                Return (Local1)
            }
        }

        Method (_AC2, 0, Serialized)  // _ACx: Active Cooling, x=0-9
        {
            Local1 = \_SB.IETM.CTOK (TJMX)
            Local1 -= 0x28
            If ((LSTM >= Local1))
            {
                Return ((Local1 - 0x14))
            }
            Else
            {
                Return (Local1)
            }
        }

        Method (_AC3, 0, Serialized)  // _ACx: Active Cooling, x=0-9
        {
            Local1 = \_SB.IETM.CTOK (TJMX)
            Local1 -= 0x37
            If ((LSTM >= Local1))
            {
                Return ((Local1 - 0x14))
            }
            Else
            {
                Return (Local1)
            }
        }

        Method (_AC4, 0, Serialized)  // _ACx: Active Cooling, x=0-9
        {
            Local1 = \_SB.IETM.CTOK (TJMX)
            Local1 -= 0x46
            If ((LSTM >= Local1))
            {
                Return ((Local1 - 0x14))
            }
            Else
            {
                Return (Local1)
            }
        }

        Method (_PSV, 0, Serialized)  // _PSV: Passive Temperature
        {
            Return (\_SB.IETM.CTOK (TJMX))
        }

        Method (_CRT, 0, Serialized)  // _CRT: Critical Temperature
        {
            Return (\_SB.IETM.CTOK (TJMX))
        }

        Method (_CR3, 0, Serialized)  // _CR3: Warm/Standby Temperature
        {
            Return (\_SB.IETM.CTOK (TJMX))
        }

        Method (_HOT, 0, Serialized)  // _HOT: Hot Temperature
        {
            Return (\_SB.IETM.CTOK (TJMX))
        }

        Method (UVTH, 1, Serialized)
        {
        }
    }

    Scope (\_SB.IETM)
    {
        Method (KTOC, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return (((Arg0 - 0x0AAC) / 0x0A))
            }
            Else
            {
                Return (Zero)
            }
        }

        Method (CTOK, 1, Serialized)
        {
            Return (((Arg0 * 0x0A) + 0x0AAC))
        }

        Method (C10K, 1, Serialized)
        {
            Name (TMP1, Buffer (0x10)
            {
                 0x00                                             // .
            })
            CreateByteField (TMP1, Zero, TMPL)
            CreateByteField (TMP1, One, TMPH)
            Local0 = (Arg0 + 0x0AAC)
            TMPL = (Local0 & 0xFF)
            TMPH = ((Local0 & 0xFF00) >> 0x08)
            ToInteger (TMP1, Local1)
            Return (Local1)
        }

        Method (K10C, 1, Serialized)
        {
            If ((Arg0 > 0x0AAC))
            {
                Return ((Arg0 - 0x0AAC))
            }
            Else
            {
                Return (Zero)
            }
        }
    }
}

