/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20230628 (64-bit version)
 * Copyright (c) 2000 - 2023 Intel Corporation
 * 
 * Disassembly of phat.dat
 *
 * ACPI Data Table [PHAT]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000 004h]                   Signature : "PHAT"    [Platform Health Assessment Table]
[004h 0004 004h]                Table Length : 0000062D
[008h 0008 001h]                    Revision : 01
[009h 0009 001h]                    Checksum : DC
[00Ah 0010 006h]                      Oem ID : "_ASUS_"
[010h 0016 008h]                Oem Table ID : "Notebook"
[018h 0024 004h]                Oem Revision : 00000005
[01Ch 0028 004h]             Asl Compiler ID : "MSFT"
[020h 0032 004h]       Asl Compiler Revision : 0100000D


[024h 0036 002h]               Subtable Type : 0000 [Firmware Version Data]
[026h 0038 002h]                      Length : 03C4
[028h 0040 001h]                    Revision : 01
[035h 0053 003h]                    Reserved : 000000
[038h 0056 004h]               Element Count : 00000022

/* Version Element #1h Offset 30h */

[030h 0048 010h]                        GUID : DC4F0355-5297-4EDB-B4D7-9B83615136E4
[040h 0064 008h]               Version Value : 000000020000015C
[048h 0072 004h]                 Producer ID : 43544E49

/* Version Element #2h Offset 4Ch */

[04Ch 0076 010h]                        GUID : 27097CFD-46E5-4E39-B8E4-33E439A13EAF
[05Ch 0092 008h]               Version Value : 0000000C008B0010
[064h 0100 004h]                 Producer ID : 43544E49

/* Version Element #3h Offset 68h */

[068h 0104 010h]                        GUID : 64C1A63E-BA2D-41DE-9655-2F70FE618F00
[078h 0120 008h]               Version Value : 0000000800260020
[080h 0128 004h]                 Producer ID : 43544E49

/* Version Element #4h Offset 84h */

[084h 0132 010h]                        GUID : 2199CBED-4D3E-45EF-85D1-E1BAD5A370A9
[094h 0148 008h]               Version Value : 000000FFFFFFFFFF
[09Ch 0156 004h]                 Producer ID : 43544E49

/* Version Element #5h Offset A0h */

[0A0h 0160 010h]                        GUID : A83F7361-FEFA-42D6-8B32-95F2989BF6D4
[0B0h 0176 008h]               Version Value : 000000FFFFFFFFFF
[0B8h 0184 004h]                 Producer ID : 43544E49

/* Version Element #6h Offset BCh */

[0BCh 0188 010h]                        GUID : D84CA716-7ED3-4C4B-B1D5-2B8C274A19F1
[0CCh 0204 008h]               Version Value : 000000FFFFFF0011
[0D4h 0212 004h]                 Producer ID : 43544E49

/* Version Element #7h Offset D8h */

[0D8h 0216 010h]                        GUID : E9B095BF-DEE2-4AC9-8778-9E41FB86C436
[0E8h 0232 008h]               Version Value : 0000000246000000
[0F0h 0240 004h]                 Producer ID : 43544E49

/* Version Element #8h Offset F4h */

[0F4h 0244 010h]                        GUID : E385DD67-8F03-4872-AB47-3CA114BE1E52
[104h 0260 008h]               Version Value : 0000000400000000
[10Ch 0268 004h]                 Producer ID : 43544E49

/* Version Element #9h Offset 110h */

[110h 0272 010h]                        GUID : 38D8AF9D-0F3D-4E48-A36B-FAFD686148A0
[120h 0288 008h]               Version Value : 0000000800260020
[128h 0296 004h]                 Producer ID : 43544E49

/* Version Element #Ah Offset 12Ch */

[12Ch 0300 010h]                        GUID : 73256EE6-A990-4B13-8ABA-76E22C3E993C
[13Ch 0316 008h]               Version Value : 0000000000040093
[144h 0324 004h]                 Producer ID : 43544E49

/* Version Element #Bh Offset 148h */

[148h 0328 010h]                        GUID : FA2B5B12-3124-43DE-84E6-C4114E881F43
[158h 0344 008h]               Version Value : 0000000800260020
[160h 0352 004h]                 Producer ID : 43544E49

/* Version Element #Ch Offset 164h */

[164h 0356 010h]                        GUID : 47A463E7-196D-4577-B536-3B9A85B7384B
[174h 0372 008h]               Version Value : 000000FFFFFFFFFF
[17Ch 0380 004h]                 Producer ID : 43544E49

/* Version Element #Dh Offset 180h */

[180h 0384 010h]                        GUID : 3D3CE021-CE65-A2F5-A2F5-008B2BBB0CA2
[190h 0400 008h]               Version Value : 0000000000000001
[198h 0408 004h]                 Producer ID : 43544E49

/* Version Element #Eh Offset 19Ch */

[19Ch 0412 010h]                        GUID : 84A3FF3C-CE65-448D-81C4-1A62A8A9C36E
[1ACh 0428 008h]               Version Value : 0000000000000001
[1B4h 0436 004h]                 Producer ID : 43544E49

/* Version Element #Fh Offset 1B8h */

[1B8h 0440 010h]                        GUID : FA50153E-6627-4714-9220-B8C71DAB429A
[1C8h 0456 008h]               Version Value : 000000FFFFFFFFFF
[1D0h 0464 004h]                 Producer ID : 43544E49

/* Version Element #10h Offset 1D4h */

[1D4h 0468 010h]                        GUID : 0169518D-0480-5676-A9C1-5A8903C4992B
[1E4h 0484 008h]               Version Value : 000000FFFFFFFFFF
[1ECh 0492 004h]                 Producer ID : 43544E49

/* Version Element #11h Offset 1F0h */

[1F0h 0496 010h]                        GUID : 0094DFCF-D97B-51A2-8F85-EC2482BD6296
[200h 0512 008h]               Version Value : 000000FFFFFFFFFF
[208h 0520 004h]                 Producer ID : 43544E49

/* Version Element #12h Offset 20Ch */

[20Ch 0524 010h]                        GUID : D1ADD6FD-B056-5486-AD96-5163D1B9CCDC
[21Ch 0540 008h]               Version Value : 000000FFFFFFFFFF
[224h 0548 004h]                 Producer ID : 43544E49

/* Version Element #13h Offset 228h */

[228h 0552 010h]                        GUID : 8F90AA30-B959-57DB-98ED-AEDA14BB4F7F
[238h 0568 008h]               Version Value : 000000FFFFFFFFFF
[240h 0576 004h]                 Producer ID : 43544E49

/* Version Element #14h Offset 244h */

[244h 0580 010h]                        GUID : 877778B9-CF22-476A-97A1-27530D9AFE42
[254h 0596 008h]               Version Value : 0000000800260020
[25Ch 0604 004h]                 Producer ID : 43544E49

/* Version Element #15h Offset 260h */

[260h 0608 010h]                        GUID : DA72FEF3-782B-4C38-8540-2C90217C1673
[270h 0624 008h]               Version Value : 0000000000010015
[278h 0632 004h]                 Producer ID : 43544E49

/* Version Element #16h Offset 27Ch */

[27Ch 0636 010h]                        GUID : EC643DAC-ABB9-465F-83A6-A857E1D03BA2
[28Ch 0652 008h]               Version Value : 00000001120D0000
[294h 0660 004h]                 Producer ID : 43544E49

/* Version Element #17h Offset 298h */

[298h 0664 010h]                        GUID : 6858C460-15BA-4EAB-B67C-0053FFCDED54
[2A8h 0680 008h]               Version Value : 0000000800260020
[2B0h 0688 004h]                 Producer ID : 43544E49

/* Version Element #18h Offset 2B4h */

[2B4h 0692 010h]                        GUID : A62BA25D-FFFC-4AC6-A90E-2457AC0E477E
[2C4h 0708 008h]               Version Value : 00000010011B0880
[2CCh 0716 004h]                 Producer ID : 43544E49

/* Version Element #19h Offset 2D0h */

[2D0h 0720 010h]                        GUID : C519A3EB-6D2A-47D0-AAD3-5EB006B63121
[2E0h 0736 008h]               Version Value : 000000FFFFFFFFFF
[2E8h 0744 004h]                 Producer ID : 43544E49

/* Version Element #1Ah Offset 2ECh */

[2ECh 0748 010h]                        GUID : E1EC257D-43D4-415B-9503-9EC04AC56158
[2FCh 0764 008h]               Version Value : 000000FFFFFFFFFF
[304h 0772 004h]                 Producer ID : 43544E49

/* Version Element #1Bh Offset 308h */

[308h 0776 010h]                        GUID : E323121F-E60C-43D8-8E0F-69D9D7DAB3A0
[318h 0792 008h]               Version Value : 000000FFFFFFFFFF
[320h 0800 004h]                 Producer ID : 43544E49

/* Version Element #1Ch Offset 324h */

[324h 0804 010h]                        GUID : AB0CDEEF-0B27-4C2B-B6B7-9B734043E3DE
[334h 0820 008h]               Version Value : 000000FFFFFFFFFF
[33Ch 0828 004h]                 Producer ID : 43544E49

/* Version Element #1Dh Offset 340h */

[340h 0832 010h]                        GUID : BB11C4EA-6928-4F6C-B348-72C0CFC9D04D
[350h 0848 008h]               Version Value : 000000FFFFFFFFFF
[358h 0856 004h]                 Producer ID : 43544E49

/* Version Element #1Eh Offset 35Ch */

[35Ch 0860 010h]                        GUID : 3381C8E3-B92C-4BAC-B6C6-4390911E934D
[36Ch 0876 008h]               Version Value : 0000000105020000
[374h 0884 004h]                 Producer ID : 43544E49

/* Version Element #1Fh Offset 378h */

[378h 0888 010h]                        GUID : 427DCDB4-1C33-4F2B-B736-F8DABE9E9ACD
[388h 0904 008h]               Version Value : 0000000007000000
[390h 0912 004h]                 Producer ID : 43544E49

/* Version Element #20h Offset 394h */

[394h 0916 010h]                        GUID : CCB0BC86-1BCD-476F-AABE-E19159244BFD
[3A4h 0932 008h]               Version Value : 0000000C008B0010
[3ACh 0940 004h]                 Producer ID : 43544E49

/* Version Element #21h Offset 3B0h */

[3B0h 0944 010h]                        GUID : 988C3FD6-92B2-41A0-B5C3-78A5CD4712F7
[3C0h 0960 008h]               Version Value : 0000000000000000
[3C8h 0968 004h]                 Producer ID : 43544E49

/* Version Element #22h Offset 3CCh */

[3CCh 0972 010h]                        GUID : 03285589-1E37-4B72-A5F9-70D1ADAE5D34
[3DCh 0988 008h]               Version Value : 0000000043000000
[3E4h 0996 004h]                 Producer ID : 43544E49

[3E8h 1000 002h]               Subtable Type : 0001 [Firmware Health Data]
[3EAh 1002 002h]                      Length : 0127
[3ECh 1004 001h]                    Revision : 01
[3F1h 1009 002h]                    Reserved : 0000
[3F3h 1011 001h]                      Health : 01
[3F4h 1012 010h]                 Device GUID : 93A41C2F-A09F-E7C2-AC1F-F2488F03EEC3
[404h 1028 004h]      Device-Specific Offset : 00000074
[3ECh 1004 038h]                 Device Path : "VenHw(93A41C2F-A09F-E7C2-AC"
[424h 1060 0D3h]        Device-Specific Data : 46 00 2D 00 46 00 32 00 34 00 38 00 38 00 46 00 /* F.-.F.2.4.8.8.F. */\
/* 434h 1076  16 */                            30 00 33 00 45 00 45 00 43 00 33 00 29 00 00 00 /* 0.3.E.E.C.3.)... */\
/* 444h 1092  16 */                            01 D6 5E E7 2A 03 00 00 00 20 CE 4A 81 02 00 00 /* ..^.*.... .J.... */\
/* 454h 1108  16 */                            00 18 01 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 464h 1124  16 */                            00 00 00 92 5D 58 69 0A B5 D7 4A B2 65 2E B1 AE /* ....]Xi...J.e... */\
/* 474h 1140  16 */                            06 65 74 00 00 00 00 00 00 00 00 00 00 00 00 00 /* .et............. */\
/* 484h 1156  16 */                            00 00 00 FC CB E2 2F AA B9 93 4A AB 5B 40 17 3B /* ....../...J.[@.; */\
/* 494h 1172  16 */                            58 1C 42 00 00 00 00 00 00 00 00 00 00 00 00 00 /* X.B............. */\
/* 4A4h 1188  16 */                            00 00 00 2D 04 13 F7 EE B1 39 55 B0 4D 25 59 F5 /* ...-.....9U.M%Y. */\
/* 4B4h 1204  16 */                            AD 1C 73 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ..s............. */\
/* 4C4h 1220  16 */                            00 00 00 8B 06 88 4E B2 41 05 4E 89 3C DB 0B 43 /* ......N.A.N.<..C */\
/* 4D4h 1236  16 */                            F7 D3 48 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ..H............. */\
/* 4E4h 1252  16 */                            00 00 00 39 97 A6 7A 78 8F CB 41 BF 44 85 4E 2C /* ...9..zx..A.D.N, */\
/* 4F4h 1268   3 */                            B5 16 BD                                        /* ... */\

[4F7h 1271 002h]               Subtable Type : 0000 [Firmware Version Data]
[4F9h 1273 002h]                      Length : 0000
[4FBh 1275 001h]                    Revision : 00
[508h 1288 003h]                    Reserved : 000000
[50Bh 1291 004h]               Element Count : 7AA69739

[503h 1283 002h]               Subtable Type : 8F78 [Unknown Subtable Type]
[505h 1285 002h]                      Length : 41CB
[507h 1287 001h]                    Revision : BF

Raw Table Data: Length 1581 (0x62D)

    0000: 50 48 41 54 2D 06 00 00 01 DC 5F 41 53 55 53 5F  // PHAT-....._ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 05 00 00 00 4D 53 46 54  // Notebook....MSFT
    0020: 0D 00 00 01 00 00 C4 03 01 00 00 00 22 00 00 00  // ............"...
    0030: 55 03 4F DC 97 52 DB 4E B4 D7 9B 83 61 51 36 E4  // U.O..R.N....aQ6.
    0040: 5C 01 00 00 02 00 00 00 49 4E 54 43 FD 7C 09 27  // \.......INTC.|.'
    0050: E5 46 39 4E B8 E4 33 E4 39 A1 3E AF 10 00 8B 00  // .F9N..3.9.>.....
    0060: 0C 00 00 00 49 4E 54 43 3E A6 C1 64 2D BA DE 41  // ....INTC>..d-..A
    0070: 96 55 2F 70 FE 61 8F 00 20 00 26 00 08 00 00 00  // .U/p.a.. .&.....
    0080: 49 4E 54 43 ED CB 99 21 3E 4D EF 45 85 D1 E1 BA  // INTC...!>M.E....
    0090: D5 A3 70 A9 FF FF FF FF FF 00 00 00 49 4E 54 43  // ..p.........INTC
    00A0: 61 73 3F A8 FA FE D6 42 8B 32 95 F2 98 9B F6 D4  // as?....B.2......
    00B0: FF FF FF FF FF 00 00 00 49 4E 54 43 16 A7 4C D8  // ........INTC..L.
    00C0: D3 7E 4B 4C B1 D5 2B 8C 27 4A 19 F1 11 00 FF FF  // .~KL..+.'J......
    00D0: FF 00 00 00 49 4E 54 43 BF 95 B0 E9 E2 DE C9 4A  // ....INTC.......J
    00E0: 87 78 9E 41 FB 86 C4 36 00 00 00 46 02 00 00 00  // .x.A...6...F....
    00F0: 49 4E 54 43 67 DD 85 E3 03 8F 72 48 AB 47 3C A1  // INTCg.....rH.G<.
    0100: 14 BE 1E 52 00 00 00 00 04 00 00 00 49 4E 54 43  // ...R........INTC
    0110: 9D AF D8 38 3D 0F 48 4E A3 6B FA FD 68 61 48 A0  // ...8=.HN.k..haH.
    0120: 20 00 26 00 08 00 00 00 49 4E 54 43 E6 6E 25 73  //  .&.....INTC.n%s
    0130: 90 A9 13 4B 8A BA 76 E2 2C 3E 99 3C 93 00 04 00  // ...K..v.,>.<....
    0140: 00 00 00 00 49 4E 54 43 12 5B 2B FA 24 31 DE 43  // ....INTC.[+.$1.C
    0150: 84 E6 C4 11 4E 88 1F 43 20 00 26 00 08 00 00 00  // ....N..C .&.....
    0160: 49 4E 54 43 E7 63 A4 47 6D 19 77 45 B5 36 3B 9A  // INTC.c.Gm.wE.6;.
    0170: 85 B7 38 4B FF FF FF FF FF 00 00 00 49 4E 54 43  // ..8K........INTC
    0180: 21 E0 3C 3D 65 CE F5 A2 A2 F5 00 8B 2B BB 0C A2  // !.<=e.......+...
    0190: 01 00 00 00 00 00 00 00 49 4E 54 43 3C FF A3 84  // ........INTC<...
    01A0: 65 CE 8D 44 81 C4 1A 62 A8 A9 C3 6E 01 00 00 00  // e..D...b...n....
    01B0: 00 00 00 00 49 4E 54 43 3E 15 50 FA 27 66 14 47  // ....INTC>.P.'f.G
    01C0: 92 20 B8 C7 1D AB 42 9A FF FF FF FF FF 00 00 00  // . ....B.........
    01D0: 49 4E 54 43 8D 51 69 01 80 04 76 56 A9 C1 5A 89  // INTC.Qi...vV..Z.
    01E0: 03 C4 99 2B FF FF FF FF FF 00 00 00 49 4E 54 43  // ...+........INTC
    01F0: CF DF 94 00 7B D9 A2 51 8F 85 EC 24 82 BD 62 96  // ....{..Q...$..b.
    0200: FF FF FF FF FF 00 00 00 49 4E 54 43 FD D6 AD D1  // ........INTC....
    0210: 56 B0 86 54 AD 96 51 63 D1 B9 CC DC FF FF FF FF  // V..T..Qc........
    0220: FF 00 00 00 49 4E 54 43 30 AA 90 8F 59 B9 DB 57  // ....INTC0...Y..W
    0230: 98 ED AE DA 14 BB 4F 7F FF FF FF FF FF 00 00 00  // ......O.........
    0240: 49 4E 54 43 B9 78 77 87 22 CF 6A 47 97 A1 27 53  // INTC.xw.".jG..'S
    0250: 0D 9A FE 42 20 00 26 00 08 00 00 00 49 4E 54 43  // ...B .&.....INTC
    0260: F3 FE 72 DA 2B 78 38 4C 85 40 2C 90 21 7C 16 73  // ..r.+x8L.@,.!|.s
    0270: 15 00 01 00 00 00 00 00 49 4E 54 43 AC 3D 64 EC  // ........INTC.=d.
    0280: B9 AB 5F 46 83 A6 A8 57 E1 D0 3B A2 00 00 0D 12  // .._F...W..;.....
    0290: 01 00 00 00 49 4E 54 43 60 C4 58 68 BA 15 AB 4E  // ....INTC`.Xh...N
    02A0: B6 7C 00 53 FF CD ED 54 20 00 26 00 08 00 00 00  // .|.S...T .&.....
    02B0: 49 4E 54 43 5D A2 2B A6 FC FF C6 4A A9 0E 24 57  // INTC].+....J..$W
    02C0: AC 0E 47 7E 80 08 1B 01 10 00 00 00 49 4E 54 43  // ..G~........INTC
    02D0: EB A3 19 C5 2A 6D D0 47 AA D3 5E B0 06 B6 31 21  // ....*m.G..^...1!
    02E0: FF FF FF FF FF 00 00 00 49 4E 54 43 7D 25 EC E1  // ........INTC}%..
    02F0: D4 43 5B 41 95 03 9E C0 4A C5 61 58 FF FF FF FF  // .C[A....J.aX....
    0300: FF 00 00 00 49 4E 54 43 1F 12 23 E3 0C E6 D8 43  // ....INTC..#....C
    0310: 8E 0F 69 D9 D7 DA B3 A0 FF FF FF FF FF 00 00 00  // ..i.............
    0320: 49 4E 54 43 EF DE 0C AB 27 0B 2B 4C B6 B7 9B 73  // INTC....'.+L...s
    0330: 40 43 E3 DE FF FF FF FF FF 00 00 00 49 4E 54 43  // @C..........INTC
    0340: EA C4 11 BB 28 69 6C 4F B3 48 72 C0 CF C9 D0 4D  // ....(ilO.Hr....M
    0350: FF FF FF FF FF 00 00 00 49 4E 54 43 E3 C8 81 33  // ........INTC...3
    0360: 2C B9 AC 4B B6 C6 43 90 91 1E 93 4D 00 00 02 05  // ,..K..C....M....
    0370: 01 00 00 00 49 4E 54 43 B4 CD 7D 42 33 1C 2B 4F  // ....INTC..}B3.+O
    0380: B7 36 F8 DA BE 9E 9A CD 00 00 00 07 00 00 00 00  // .6..............
    0390: 49 4E 54 43 86 BC B0 CC CD 1B 6F 47 AA BE E1 91  // INTC......oG....
    03A0: 59 24 4B FD 10 00 8B 00 0C 00 00 00 49 4E 54 43  // Y$K.........INTC
    03B0: D6 3F 8C 98 B2 92 A0 41 B5 C3 78 A5 CD 47 12 F7  // .?.....A..x..G..
    03C0: 00 00 00 00 00 00 00 00 49 4E 54 43 89 55 28 03  // ........INTC.U(.
    03D0: 37 1E 72 4B A5 F9 70 D1 AD AE 5D 34 00 00 00 43  // 7.rK..p...]4...C
    03E0: 00 00 00 00 49 4E 54 43 01 00 27 01 01 00 00 01  // ....INTC..'.....
    03F0: 2F 1C A4 93 9F A0 C2 E7 AC 1F F2 48 8F 03 EE C3  // /..........H....
    0400: 74 00 00 00 56 00 65 00 6E 00 48 00 77 00 28 00  // t...V.e.n.H.w.(.
    0410: 39 00 33 00 41 00 34 00 31 00 43 00 32 00 46 00  // 9.3.A.4.1.C.2.F.
    0420: 2D 00 41 00 30 00 39 00 46 00 2D 00 45 00 37 00  // -.A.0.9.F.-.E.7.
    0430: 43 00 32 00 2D 00 41 00 43 00 31 00 46 00 2D 00  // C.2.-.A.C.1.F.-.
    0440: 46 00 32 00 34 00 38 00 38 00 46 00 30 00 33 00  // F.2.4.8.8.F.0.3.
    0450: 45 00 45 00 43 00 33 00 29 00 00 00 01 D6 5E E7  // E.E.C.3.).....^.
    0460: 2A 03 00 00 00 20 CE 4A 81 02 00 00 00 18 01 00  // *.... .J........
    0470: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 92  // ................
    0480: 5D 58 69 0A B5 D7 4A B2 65 2E B1 AE 06 65 74 00  // ]Xi...J.e....et.
    0490: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FC  // ................
    04A0: CB E2 2F AA B9 93 4A AB 5B 40 17 3B 58 1C 42 00  // ../...J.[@.;X.B.
    04B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 2D  // ...............-
    04C0: 04 13 F7 EE B1 39 55 B0 4D 25 59 F5 AD 1C 73 00  // .....9U.M%Y...s.
    04D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 8B  // ................
    04E0: 06 88 4E B2 41 05 4E 89 3C DB 0B 43 F7 D3 48 00  // ..N.A.N.<..C..H.
    04F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 39  // ...............9
    0500: 97 A6 7A 78 8F CB 41 BF 44 85 4E 2C B5 16 BD 01  // ..zx..A.D.N,....
    0510: 00 7D 00 01 00 00 01 09 FF 0D A3 BF 56 22 46 A9  // .}..........V"F.
    0520: E7 39 9B 0A 79 E7 C7 74 00 00 00 56 00 65 00 6E  // .9..y..t...V.e.n
    0530: 00 48 00 77 00 28 00 41 00 33 00 30 00 44 00 46  // .H.w.(.A.3.0.D.F
    0540: 00 46 00 30 00 39 00 2D 00 35 00 36 00 42 00 46  // .F.0.9.-.5.6.B.F
    0550: 00 2D 00 34 00 36 00 32 00 32 00 2D 00 41 00 39  // .-.4.6.2.2.-.A.9
    0560: 00 45 00 37 00 2D 00 33 00 39 00 39 00 42 00 30  // .E.7.-.3.9.9.B.0
    0570: 00 41 00 37 00 39 00 45 00 37 00 43 00 37 00 29  // .A.7.9.E.7.C.7.)
    0580: 00 00 00 01 01 05 00 00 00 00 01 00 01 00 A1 00  // ................
    0590: 01 00 00 01 08 98 B2 46 F0 06 A0 4C 9F 92 5C 49  // .......F...L..\I
    05A0: 67 82 92 78 74 00 00 00 56 00 65 00 6E 00 48 00  // g..xt...V.e.n.H.
    05B0: 77 00 28 00 34 00 36 00 42 00 32 00 39 00 38 00  // w.(.4.6.B.2.9.8.
    05C0: 30 00 38 00 2D 00 30 00 36 00 46 00 30 00 2D 00  // 0.8.-.0.6.F.0.-.
    05D0: 34 00 43 00 41 00 30 00 2D 00 39 00 46 00 39 00  // 4.C.A.0.-.9.F.9.
    05E0: 32 00 2D 00 35 00 43 00 34 00 39 00 36 00 37 00  // 2.-.5.C.4.9.6.7.
    05F0: 38 00 32 00 39 00 32 00 37 00 38 00 29 00 00 00  // 8.2.9.2.7.8.)...
    0600: 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  // ................
    0610: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  // ................
    0620: 00 00 00 00 00 00 00 00 00 00 00 00 00           // .............
