/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20230628 (64-bit version)
 * Copyright (c) 2000 - 2023 Intel Corporation
 * 
 * Disassembly of msdm.dat
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000 004h]                   Signature : "MSDM"    [Microsoft Data Management Table]
[004h 0004 004h]                Table Length : 00000055
[008h 0008 001h]                    Revision : 03
[009h 0009 001h]                    Checksum : 13
[00Ah 0010 006h]                      Oem ID : "_ASUS_"
[010h 0016 008h]                Oem Table ID : "Notebook"
[018h 0024 004h]                Oem Revision : 01072009
[01Ch 0028 004h]             Asl Compiler ID : "ASUS"
[020h 0032 004h]       Asl Compiler Revision : 00000001

[024h 0036 031h] Software Licensing Structure : 4D 53 44 4D 55 00 00 00 03 13 5F 41 53 55 53 5F /* MSDMU....._ASUS_ */\
/* 034h 0052  16 */                            4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 53 55 53 /* Notebook. ..ASUS */\
/* 044h 0068  16 */                            01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00 /* ................ */\
/* 054h 0084   1 */                            00                                              /* . */\

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 13 5F 41 53 55 53 5F  // MSDMU....._ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 53 55 53  // Notebook. ..ASUS
    0020: 01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 39 58 34 4E 58 2D 57 34  // ........9X4NX-W4
    0040: 34 47 4A 2D 34 57 4B 58 32 2D 51 58 51 34 4A 2D  // 4GJ-4WKX2-QXQ4J-
    0050: 37 46 54 50 34                                   // 7FTP4
