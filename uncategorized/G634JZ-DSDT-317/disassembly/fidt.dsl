/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20230628 (64-bit version)
 * Copyright (c) 2000 - 2023 Intel Corporation
 * 
 * Disassembly of fidt.dat
 *
 * ACPI Data Table [FIDT]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000 004h]                   Signature : "FIDT"    
[004h 0004 004h]                Table Length : 0000009C
[008h 0008 001h]                    Revision : 01
[009h 0009 001h]                    Checksum : 3C
[00Ah 0010 006h]                      Oem ID : "_ASUS_"
[010h 0016 008h]                Oem Table ID : "Notebook"
[018h 0024 004h]                Oem Revision : 01072009
[01Ch 0028 004h]             Asl Compiler ID : "AMI "
[020h 0032 004h]       Asl Compiler Revision : 00010013


**** Unknown ACPI table signature [FIDT]


Raw Table Data: Length 156 (0x9C)

    0000: 46 49 44 54 9C 00 00 00 01 3C 5F 41 53 55 53 5F  // FIDT.....<_ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 4D 49 20  // Notebook. ..AMI 
    0020: 13 00 01 00 24 46 49 44 04 78 00 31 41 59 52 54  // ....$FID.x.1AYRT
    0030: 30 32 36 00 07 91 2F 8B 1A E5 0B 19 19 48 51 62  // 026.../......HQb
    0040: FB AC EA 05 30 35 00 32 37 00 30 30 00 32 36 00  // ....05.27.00.26.
    0050: E7 07 07 0C 07 08 0F FF FF 5F 41 53 55 53 5F 4E  // ........._ASUS_N
    0060: 6F 74 65 62 6F 6F 6B 31 00 00 00 FF FF FF FF FF  // otebook1........
    0070: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0080: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0090: FF FF FF FF FF FF FF FF FF FF FF FF              // ............
