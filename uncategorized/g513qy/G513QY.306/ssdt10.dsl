/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20210331 (64-bit version)
 * Copyright (c) 2000 - 2021 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt10.dat, Sun Jul 11 06:28:47 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x000005DE (1502)
 *     Revision         0x01
 *     Checksum         0x1D
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20190509 (538510601)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.PCI0.GPP0.M407, IntObj)
    External (_SB_.PCI0.GPP0.SWUS.SWDS.VGA_, DeviceObj)
    External (_SB_.PCI0.GPP0.VGA_, DeviceObj)
    External (AFN8, MethodObj)    // 0 Arguments
    External (M000, MethodObj)    // 1 Arguments
    External (M029, MethodObj)    // 1 Arguments
    External (M031, MethodObj)    // 1 Arguments
    External (M032, MethodObj)    // 2 Arguments
    External (M037, DeviceObj)
    External (M046, DeviceObj)
    External (M047, DeviceObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M404, DeviceObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)

    Scope (\_GPE)
    {
        Method (_L00, 0, NotSerialized)  // _Lxx: Level-Triggered GPE, xx=0x00-0xFF
        {
            Local0 = M049 (M133, 0x14)
            If (M031 (Local0))
            {
                If (M029 (Local0))
                {
                    If ((\_SB.PCI0.GPP0.M407 == One))
                    {
                        M000 (0xB2)
                        AFN8 ()
                    }

                    M032 (Local0, (M029 (Local0) ^ One))
                    If ((\_SB.PCI0.GPP0.M407 == One))
                    {
                        If (CondRefOf (\_SB.PCI0.GPP0.VGA))
                        {
                            Notify (\_SB.PCI0.GPP0.VGA, 0x81) // Information Change
                        }

                        If (CondRefOf (\_SB.PCI0.GPP0.SWUS.SWDS.VGA))
                        {
                            Notify (\_SB.PCI0.GPP0.SWUS.SWDS.VGA, 0x81) // Information Change
                        }
                    }
                }
            }
            ElseIf (!M029 (Local0))
            {
                If ((\_SB.PCI0.GPP0.M407 == One))
                {
                    M000 (0xB3)
                    AFN8 ()
                }

                M032 (Local0, (M029 (Local0) ^ One))
                If ((\_SB.PCI0.GPP0.M407 == One))
                {
                    If (CondRefOf (\_SB.PCI0.GPP0.VGA))
                    {
                        Notify (\_SB.PCI0.GPP0.VGA, 0x81) // Information Change
                    }

                    If (CondRefOf (\_SB.PCI0.GPP0.SWUS.SWDS.VGA))
                    {
                        Notify (\_SB.PCI0.GPP0.SWUS.SWDS.VGA, 0x81) // Information Change
                    }
                }
            }
        }
    }
}

