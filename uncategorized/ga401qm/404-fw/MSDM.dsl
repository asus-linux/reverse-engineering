/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20200925 (64-bit version)
 * Copyright (c) 2000 - 2020 Intel Corporation
 * 
 * Disassembly of MSDM, Tue Apr  6 19:52:02 2021
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "MSDM"    [Microsoft Data Management table]
[004h 0004   4]                 Table Length : 00000055
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : 3A
[00Ah 0010   6]                       Oem ID : "ALASKA"
[010h 0016   8]                 Oem Table ID : "A M I "
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "ASUS"
[020h 0032   4]        Asl Compiler Revision : 00000001

[024h 0036  49] Software Licensing Structure : \
    01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 \
    1D 00 00 00 33 58 59 48 4D 2D 54 4E 50 59 4B 2D \
    43 58 38 56 32 2D 48 44 43 52 38 2D 51 44 42 4B \
    33 

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 3A 41 4C 41 53 4B 41  // MSDMU....:ALASKA
    0010: 41 20 4D 20 49 20 00 00 09 20 07 01 41 53 55 53  // A M I ... ..ASUS
    0020: 01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 33 58 59 48 4D 2D 54 4E  // ........3XYHM-TN
    0040: 50 59 4B 2D 43 58 38 56 32 2D 48 44 43 52 38 2D  // PYK-CX8V2-HDCR8-
    0050: 51 44 42 4B 33                                   // QDBK3
