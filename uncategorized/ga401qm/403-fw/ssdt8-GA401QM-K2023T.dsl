/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20200925 (64-bit version)
 * Copyright (c) 2000 - 2020 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt8-GA401QM-K2023T.dat, Sat Mar 27 20:37:34 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00003AA3 (15011)
 *     Revision         0x01
 *     Checksum         0xF5
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20190509 (538510601)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.ALIB, MethodObj)    // 2 Arguments
    External (_SB_.CFML, IntObj)
    External (_SB_.GGOV, MethodObj)    // 2 Arguments
    External (_SB_.GPCE, IntObj)
    External (_SB_.MACO, BuffFieldObj)
    External (_SB_.PCI0.GPP0, DeviceObj)
    External (_SB_.PCI0.GPP0.SWUS, DeviceObj)
    External (_SB_.PCI0.GPP0.SWUS.SWDS, DeviceObj)
    External (_SB_.PCI0.SBRG.EC0_.AGPL, MethodObj)    // 1 Arguments
    External (_SB_.PCI0.SBRG.EC0_.BRAH, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.CMUT, MutexObj)
    External (_SB_.PCI0.SBRG.EC0_.ECPU, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.EGPT, UnknownObj)
    External (_SB_.PCI0.SBRG.EC0_.STCC, MethodObj)    // 2 Arguments
    External (_SB_.PCI0.SBRG.EC0_.WEBC, MethodObj)    // 3 Arguments
    External (_SB_.PLTF.P000, DeviceObj)
    External (_SB_.PLTF.P001, DeviceObj)
    External (_SB_.PLTF.P002, DeviceObj)
    External (_SB_.PLTF.P003, DeviceObj)
    External (_SB_.PLTF.P004, DeviceObj)
    External (_SB_.PLTF.P005, DeviceObj)
    External (_SB_.PLTF.P006, DeviceObj)
    External (_SB_.PLTF.P007, DeviceObj)
    External (_SB_.PLTF.P008, DeviceObj)
    External (_SB_.PLTF.P009, DeviceObj)
    External (_SB_.PLTF.P00A, DeviceObj)
    External (_SB_.PLTF.P00B, DeviceObj)
    External (_SB_.PLTF.P00C, DeviceObj)
    External (_SB_.PLTF.P00D, DeviceObj)
    External (_SB_.PLTF.P00E, DeviceObj)
    External (_SB_.PLTF.P00F, DeviceObj)
    External (_SB_.SGOV, MethodObj)    // 3 Arguments
    External (_SB_.UMAF, IntObj)
    External (_SB_.WOSR, IntObj)
    External (IOBS, UnknownObj)
    External (M000, MethodObj)    // 1 Arguments
    External (M010, MethodObj)    // 2 Arguments
    External (M013, MethodObj)    // 4 Arguments
    External (M014, MethodObj)    // 5 Arguments
    External (M017, MethodObj)    // 6 Arguments
    External (M018, MethodObj)    // 7 Arguments
    External (M019, MethodObj)    // 4 Arguments
    External (M020, MethodObj)    // 5 Arguments
    External (M021, MethodObj)    // 4 Arguments
    External (M023, MethodObj)    // 3 Arguments
    External (M024, MethodObj)    // 3 Arguments
    External (M025, MethodObj)    // 4 Arguments
    External (M026, MethodObj)    // 3 Arguments
    External (M027, MethodObj)    // 3 Arguments
    External (M028, MethodObj)    // 4 Arguments
    External (M037, DeviceObj)
    External (M046, DeviceObj)
    External (M047, DeviceObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M04A, MethodObj)    // 2 Arguments
    External (M04B, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M111, MethodObj)    // 2 Arguments
    External (M112, MethodObj)    // 2 Arguments
    External (M113, MethodObj)    // 1 Arguments
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M232, MethodObj)    // 3 Arguments
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M275, MethodObj)    // 2 Arguments
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M402, MethodObj)    // 2 Arguments
    External (M403, MethodObj)    // 3 Arguments
    External (M404, DeviceObj)
    External (M414, FieldUnitObj)
    External (M429, MethodObj)    // 7 Arguments
    External (M430, MethodObj)    // 3 Arguments
    External (M444, FieldUnitObj)

    Scope (\)
    {
        Name (GPUF, One)
    }

    OperationRegion (AECM, SystemIO, 0x72, 0x02)
    Field (AECM, ByteAcc, NoLock, Preserve)
    {
        ACM1,   8, 
        ACM2,   8
    }

    IndexField (ACM1, ACM2, ByteAcc, NoLock, Preserve)
    {
        Offset (0x7F), 
        EGPS,   8
    }

    Scope (\_SB.PCI0.GPP0)
    {
        Name (M236, Buffer (0x0C)
        {
            /* 0000 */  0x04, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00                           // ....
        })
        Name (M266, Zero)
        Name (M267, Zero)
        Name (M268, Zero)
        Name (M269, Zero)
        Name (M270, Zero)
        Name (M271, Zero)
        Name (M407, One)
        Name (M442, 0x00010000)
        Name (M443, Zero)
        Name (M434, 0x2711)
        Name (M350, Buffer (0x18)
        {
            /* 0000 */  0x07, 0x00, 0x10, 0x00, 0x00, 0x01, 0x01, 0x00,  // ........
            /* 0008 */  0x00, 0xFB, 0x00, 0xFC, 0x01, 0x00, 0xF1, 0x01,  // ........
            /* 0010 */  0xFC, 0x00, 0x00, 0x00, 0xFE, 0x00, 0x00, 0x00   // ........
        })
        Name (M351, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M352, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M353, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x10, 0x00, 0xFC, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x0C, 0x00, 0x00, 0x00, 0x01, 0xE0, 0x00, 0x00,  // ........
            /* 0010 */  0xDE, 0x10, 0x20, 0x25, 0x00, 0x00, 0x00, 0x00   // .. %....
        })
        Name (M354, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M355, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M356, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M357, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Name (M358, Buffer (0x18)
        {
            /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // ........
        })
        Method (M371, 5, NotSerialized)
        {
            CreateDWordField (Arg3, Zero, M365)
            CreateDWordField (Arg3, 0x04, M366)
            CreateDWordField (Arg3, 0x08, M367)
            CreateDWordField (Arg3, 0x0C, M368)
            CreateDWordField (Arg3, 0x10, M369)
            CreateDWordField (Arg3, 0x14, M370)
            If ((Arg4 < 0x02))
            {
                If ((Arg4 == Zero))
                {
                    M365 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M366 = M019 (Arg0, Arg1, Arg2, 0x18)
                    M367 = M019 (Arg0, Arg1, Arg2, 0x20)
                    M368 = M019 (Arg0, Arg1, Arg2, 0x24)
                    M369 = M019 (Arg0, Arg1, Arg2, 0x28)
                    M370 = M019 (Arg0, Arg1, Arg2, 0x2C)
                }
                Else
                {
                    M365 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M366 = M019 (Arg0, Arg1, Arg2, 0x18)
                    M367 = M019 (Arg0, Arg1, Arg2, 0x1C)
                    M368 = M019 (Arg0, Arg1, Arg2, 0x24)
                    M369 = M019 (Arg0, Arg1, Arg2, Zero)
                    M370 = Arg0
                }
            }
            Else
            {
                If (((Arg4 & One) == Zero))
                {
                    M020 (Arg0, Arg1, Arg2, 0x18, M366)
                    M020 (Arg0, Arg1, Arg2, 0x20, M367)
                    M020 (Arg0, Arg1, Arg2, 0x24, M368)
                    M020 (Arg0, Arg1, Arg2, 0x28, M369)
                    M020 (Arg0, Arg1, Arg2, 0x2C, M370)
                }
                Else
                {
                    M020 (Arg0, Arg1, Arg2, 0x18, M366)
                    M020 (Arg0, Arg1, Arg2, 0x1C, M367)
                    M020 (Arg0, Arg1, Arg2, 0x24, M368)
                }

                If (((Arg4 & 0x04) == 0x04))
                {
                    M020 (Arg0, Arg1, Arg2, 0x04, (M365 & 0x06FFFFFF))
                }
                Else
                {
                    Local0 = M019 (Arg0, Arg1, Arg2, 0x04)
                    M020 (Arg0, Arg1, Arg2, 0x04, ((Local0 & 0x06FFFFF8) | 0x02))
                }
            }
        }

        Method (M372, 0, NotSerialized)
        {
            CreateDWordField (M353, 0x10, M362)
            Local0 = Zero
            If (((M362 & 0xFFDF) == 0x1002))
            {
                Local1 = (M362 >> 0x10)
                Local1 &= 0xFFFF
                If (((Local1 >= 0x67C0) && (Local1 <= 0x67DF)))
                {
                    Local0 = 0x10
                }
                ElseIf (((Local1 >= 0x67E0) && (Local1 <= 0x67FF)))
                {
                    Local0 = 0x11
                }
                ElseIf (((Local1 >= 0x6900) && (Local1 <= 0x695F)))
                {
                    Local0 = 0x12
                }
                ElseIf (((Local1 >= 0x6980) && (Local1 <= 0x699F)))
                {
                    Local0 = 0x13
                }
                ElseIf (((Local1 >= 0x6860) && (Local1 <= 0x687F)))
                {
                    Local0 = 0x20
                }
                ElseIf (((Local1 >= 0x69A0) && (Local1 <= 0x69BF)))
                {
                    Local0 = 0x21
                }
                ElseIf (((Local1 >= 0x7310) && (Local1 <= 0x731F)))
                {
                    Local0 = 0x22
                }
                ElseIf ((Local1 == 0x7330))
                {
                    Local0 = 0x22
                }
                ElseIf (((Local1 >= 0x7340) && (Local1 <= 0x734F)))
                {
                    Local0 = 0x23
                }
                ElseIf (((Local1 >= 0x73A0) && (Local1 <= 0x73BF)))
                {
                    Local0 = 0x24
                }
                ElseIf (((Local1 >= 0x73C0) && (Local1 <= 0x73DF)))
                {
                    Local0 = 0x26
                }
                ElseIf (((Local1 >= 0x73E0) && (Local1 <= 0x73FF)))
                {
                    Local0 = 0x27
                }
                ElseIf (((Local1 >= 0x7420) && (Local1 <= 0x743F)))
                {
                    Local0 = 0x28
                }
                Else
                {
                    Local0 = Zero
                }
            }
            ElseIf (((M362 & 0xFFFF) == 0x10DE))
            {
                Local0 = 0xC0
            }

            Return (Local0)
        }

        Method (M373, 3, NotSerialized)
        {
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M354 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M355 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M356 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M357 = M358 /* \_SB_.PCI0.GPP0.M358 */
            M371 (Arg0, Arg1, Arg2, M354, Zero)
            M020 (Arg0, Arg1, Arg2, 0x18, M359)
            Local0 = (M359 >> 0x08)
            Local0 &= 0xFF
            If ((M360 != Zero))
            {
                M371 (Local0, Zero, Zero, M355, Zero)
                M020 (Local0, Zero, Zero, 0x18, M360)
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
            }

            If (((M360 != Zero) && (M361 != Zero)))
            {
                M371 (Local0, Zero, Zero, M356, Zero)
                M020 (Local0, Zero, Zero, 0x18, M361)
                Local0 = (M361 >> 0x08)
                Local0 &= 0xFF
            }

            M371 (Local0, Zero, Zero, M357, One)
        }

        Method (M374, 3, NotSerialized)
        {
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M371 (Arg0, Arg1, Arg2, M350, 0x02)
            Local0 = (M359 >> 0x08)
            Local0 &= 0xFF
            If ((M360 != Zero))
            {
                M371 (Local0, Zero, Zero, M351, 0x02)
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
            }

            If (((M360 != Zero) && (M361 != Zero)))
            {
                M371 (Local0, Zero, Zero, M352, 0x02)
                Local0 = (M361 >> 0x08)
                Local0 &= 0xFF
            }

            M371 (Local0, Zero, Zero, M353, 0x03)
        }

        Method (M375, 0, Serialized)
        {
            CreateQWordField (M353, 0x04, M363)
            CreateDWordField (M353, 0x0C, M364)
            Local0 = M372 ()
            If (((Local0 >= Zero) && (Local0 <= 0x0F)))
            {
                M232 (M097, Zero, 0x10)
            }
            ElseIf (((Local0 >= 0x10) && (Local0 <= 0x1F)))
            {
                Local2 = M013 ((M364 & 0xFFFFFFF0), 0x5418, Zero, 0x20)
                M014 ((M364 & 0xFFFFFFF0), 0x5418, Zero, 0x20, (Local2 & 0xFFFFFFFD))
            }
            ElseIf (((Local0 >= 0x20) && (Local0 <= 0xBF)))
            {
                Local2 = M013 ((M364 & 0xFFFFFFF0), 0x34E0, Zero, 0x20)
                M014 ((M364 & 0xFFFFFFF0), 0x34E0, Zero, 0x20, (Local2 & 0xEFFFFFFF))
            }
        }

        Method (M424, 0, Serialized)
        {
            CreateDWordField (M353, 0x0C, M364)
            Local0 = M372 ()
            If (((Local0 >= 0x22) && (Local0 <= 0xBF)))
            {
                Local0 = (M364 & 0xFFFFFFF0)
                Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                Local2 = 0x4EEA
                While (((Local2 > Zero) && ((Local1 & 0x80000000) != 0x80000000)))
                {
                    Local2 = (Local2 - One)
                    Stall (0x63)
                    Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                }

                M014 (Local0, 0x0005818C, Zero, 0x20, 0x01000000)
                Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                Local2 = 0x4EEA
                While (((Local2 > Zero) && ((Local1 & 0x80000000) != 0x80000000)))
                {
                    Local2 = (Local2 - One)
                    Stall (0x63)
                    Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                }

                If (((Local2 > Zero) && ((Local1 & 0xFFFF) == Zero)))
                {
                    If ((M013 (Local0, 0x00058190, Zero, 0x20) == One))
                    {
                        M014 (Local0, 0x0005818C, Zero, 0x20, 0x00400000)
                        Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                        Local2 = 0x4EEA
                        While (((Local2 > Zero) && ((Local1 & 0x80000000) != 0x80000000)))
                        {
                            Local2 = (Local2 - One)
                            Stall (0x63)
                            Local1 = M013 (Local0, 0x0005818C, Zero, 0x20)
                        }
                    }
                }
            }
        }

        Method (M376, 0, Serialized)
        {
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M236, 0x04, M246)
            CreateDWordField (M236, 0x08, M247)
            Local0 = M372 ()
            If (((Local0 >= Zero) && (Local0 <= 0xBF)))
            {
                Local2 = 0x7FFFFFFF
                Local2 |= 0x80000000
                M020 (M245, Zero, Zero, 0x4C, M246)
                Local1 = M019 (M245, Zero, One, Zero)
                If (((Local1 != Local2) && (M247 != Local2)))
                {
                    M020 (M245, Zero, One, 0x4C, M247)
                }
            }
        }

        Method (M377, 3, NotSerialized)
        {
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M350, 0x04, M359)
            CreateDWordField (M351, 0x04, M360)
            CreateDWordField (M352, 0x04, M361)
            M371 (M245, Zero, Zero, M357, 0x05)
            If (((M360 != Zero) && (M361 != Zero)))
            {
                Local0 = (M360 >> 0x08)
                Local0 &= 0xFF
                M371 (Local0, Zero, Zero, M356, 0x04)
            }

            If ((M360 != Zero))
            {
                Local0 = (M359 >> 0x08)
                Local0 &= 0xFF
                M371 (Local0, Zero, Zero, M355, 0x04)
            }

            M371 (Arg0, Arg1, Arg2, M354, 0x04)
        }

        Method (M439, 3, NotSerialized)
        {
            Local6 = 0x7FFFFFFF
            Local6 |= 0x80000000
            Local1 = M019 (Arg0, Arg1, Arg2, 0x54)
            M020 (Arg0, Arg1, Arg2, 0x54, (Local1 & 0xFFFF7FFC))
            Local2 = M017 (Arg0, Arg1, Arg2, 0x19, Zero, 0x08)
            Local4 = One
            Local5 = 0x28
            While ((Local4 && Local5))
            {
                Local0 = M019 (Local2, Zero, Zero, Zero)
                If ((Local0 != Local6))
                {
                    Local0 = M372 ()
                    If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                    {
                        M373 (Arg0, Arg1, Arg2)
                        M374 (Arg0, Arg1, Arg2)
                        If ((M097 != Zero))
                        {
                            M375 ()
                        }

                        If ((M443 != Zero))
                        {
                            M376 ()
                        }

                        If ((((M049 (M128, 0x66) >> One) & One) == One))
                        {
                            M424 ()
                        }

                        M377 (Arg0, Arg1, Arg2)
                    }

                    Local4 = Zero
                }
                Else
                {
                    Sleep (0x19)
                    Local5--
                }
            }

            M020 (Arg0, Arg1, Arg2, 0x54, (Local1 & 0xFFFF7FFF))
        }

        Mutex (EEBC, 0x00)
        Method (M241, 1, NotSerialized)
        {
            Acquire (EEBC, 0xFFFF)
            CreateByteField (M236, Zero, M242)
            CreateByteField (M236, One, M243)
            CreateByteField (M236, 0x02, M244)
            CreateByteField (M236, 0x03, M245)
            CreateDWordField (M236, 0x04, M246)
            CreateDWordField (M236, 0x08, M247)
            Name (M272, Zero)
            Name (M273, Zero)
            Name (M274, Zero)
            Name (M400, Zero)
            Name (M427, Zero)
            Name (M431, Zero)
            If ((M085 >= 0x08))
            {
                M400 = ((M049 (M128, 0x66) >> Zero) & One)
            }

            M273 = ((M049 (M128, 0x65) >> 0x05) & One)
            M274 = ((M049 (M128, 0x65) >> 0x06) & One)
            M427 = ((M049 (M128, 0x66) >> 0x02) & One)
            M431 = ((M049 (M128, 0x66) >> 0x03) & One)
            If ((M273 != One))
            {
                Local7 = Buffer (0x05) {}
                CreateWordField (Local7, Zero, M197)
                CreateField (Local7, 0x10, 0x03, M200)
                CreateField (Local7, 0x13, 0x05, M199)
                CreateByteField (Local7, 0x03, M198)
                CreateByteField (Local7, 0x04, M201)
                M197 = 0x05
                M198 = Zero
                Local0 = M243 /* \_SB_.PCI0.GPP0.M241.M243 */
                M199 = Local0
                Local0 = M244 /* \_SB_.PCI0.GPP0.M241.M244 */
                M200 = Local0
            }

            Name (M447, Zero)
            If (((M085 == 0x09) || (M085 == 0x0A)))
            {
                M447 = One
            }
            ElseIf (((M085 == 0x0D) || (M085 == 0x0E)))
            {
                M447 = One
            }
            ElseIf ((M085 == 0x0F))
            {
                M447 = One
            }

            Local3 = Buffer (0x08) {}
            CreateWordField (Local3, Zero, M254)
            CreateByteField (Local3, 0x02, M255)
            CreateDWordField (Local3, 0x03, M256)
            M254 = 0x07
            M255 = 0x10
            M443 = M049 (M133, 0x01E0)
            M442 = M04B (M133, 0x01DC)
            If ((M442 == Zero))
            {
                If ((\_SB.PCI0.GPP0.M434 <= 0x2710))
                {
                    M442 = \_SB.PCI0.GPP0.M434
                    M256 = M442 /* \_SB_.PCI0.GPP0.M442 */
                    Local0 = M255 /* \_SB_.PCI0.GPP0.M241.M255 */
                    M255 = 0x12
                    \_SB.ALIB (0x0C, Local3)
                    M255 = Local0
                }
            }

            If ((M431 == One))
            {
                M439 (Zero, M243, M244)
                Release (EEBC)
                Return (Zero)
            }

            M266 = M04A (M133, 0x01D4)
            M267 = M04A (M133, 0x01D6)
            M268 = M049 (M133, 0x01D8)
            M269 = M049 (M133, 0x01D9)
            M270 = M049 (M133, 0x01DA)
            M271 = M049 (M133, 0x01DB)
            Local0 = ((M084 + 0x1502) + ((M266 & 0xFF) * 0x04
                ))
            OperationRegion (VAMM, SystemMemory, Local0, One)
            Field (VAMM, ByteAcc, NoLock, Preserve)
            {
                P011,   8
            }

            Local0 = ((M084 + 0x1502) + ((M267 & 0xFF) * 0x04
                ))
            OperationRegion (VANN, SystemMemory, Local0, One)
            Field (VANN, ByteAcc, NoLock, Preserve)
            {
                P141,   8
            }

            If ((M274 == One))
            {
                Local6 = One
            }
            Else
            {
                Local6 = M113 (M242)
            }

            M023 (Zero, M243, M244)
            If ((M275 (M242, Arg0) == Zero))
            {
                If ((Arg0 && Local6))
                {
                    M000 (0x9D)
                    If (CondRefOf (\_SB.MACO))
                    {
                        If ((\_SB.MACO == One))
                        {
                            If ((M267 < 0x0100))
                            {
                                P141 = 0xC4
                            }
                            Else
                            {
                                M010 (M267, One)
                            }

                            Sleep (M270)
                            M112 (M242, One)
                            Sleep (M271)
                            If ((M266 < 0x0100))
                            {
                                P011 = 0x84
                            }
                            Else
                            {
                                M010 (M266, Zero)
                            }

                            \_SB.MACO = Zero
                        }
                        Else
                        {
                            M112 (M242, Zero)
                            M111 (M242, One)
                            Sleep (0x20)
                            M112 (M242, 0x02)
                            M112 (M242, One)
                            Sleep (0x64)
                        }
                    }
                    Else
                    {
                        M112 (M242, Zero)
                        M111 (M242, One)
                        Sleep (0x20)
                        M112 (M242, 0x02)
                        M112 (M242, One)
                        Sleep (0x64)
                    }

                    If ((M400 == One))
                    {
                        M403 (M243, M244, One)
                    }

                    M000 (0x9E)
                    M272 = One
                    If ((M273 == One))
                    {
                        If ((M400 == Zero))
                        {
                            Local1 = M017 (Zero, M243, M244, 0x68, Zero, 0x08)
                            M018 (Zero, M243, M244, 0x68, Zero, 0x08, (Local1 & 0xEF))
                            Sleep (0x18)
                            Local1 = M025 (Zero, M243, M244, Zero)
                        }

                        Local1 = Zero
                        Local2 = 0x13BB
                        While ((((Local1 & 0x28) != 0x20) && (Local2 > Zero)))
                        {
                            M000 (0xC0)
                            Local1 = M017 (Zero, M243, M244, 0x6B, Zero, 0x08)
                            Local2 = (Local2 - One)
                            Stall (0x63)
                        }
                    }
                    Else
                    {
                        Sleep (0x14)
                        M201 = One
                        Local6 = \_SB.ALIB (0x06, Local7)
                        If ((M085 < 0x08))
                        {
                            M272 = Zero
                            Local2 = Zero
                            While ((Local2 < 0x0F))
                            {
                                M023 (Zero, M243, M244)
                                Local4 = One
                                Local5 = 0xC8
                                While ((Local4 && Local5))
                                {
                                    Local0 = M021 (Zero, M243, M244, 0xA5)
                                    Local0 &= 0x7F
                                    If (((Local0 >= 0x10) && (Local0 != 0x7F)))
                                    {
                                        Local4 = Zero
                                    }
                                    Else
                                    {
                                        Sleep (0x05)
                                        Local5--
                                    }
                                }

                                If (!Local4)
                                {
                                    Local5 = M024 (Zero, M243, M244)
                                    If (Local5)
                                    {
                                        M026 (Zero, M243, M244)
                                        Sleep (0x05)
                                        Local2++
                                    }
                                    Else
                                    {
                                        Local0 = Zero
                                        If ((M025 (Zero, M243, M244, Zero) == Ones))
                                        {
                                            Local0 = One
                                        }

                                        If (Local0)
                                        {
                                            M272 = One
                                            Local2 = 0x10
                                        }
                                        Else
                                        {
                                            M272 = Zero
                                            Local2 = 0x10
                                        }
                                    }
                                }
                                Else
                                {
                                    Local2 = 0x10
                                }
                            }

                            If (!M272)
                            {
                                M000 (0x9F)
                                Local1 = M019 (M245, Zero, Zero, Zero)
                                Sleep (0x0A)
                                Local4 = One
                                Local5 = 0x05
                                While ((Local4 && Local5))
                                {
                                    Local0 = M021 (Zero, M243, M244, 0xA5)
                                    Local0 &= 0x7F
                                    If (((Local0 <= 0x04) || (Local0 == 0x1F)))
                                    {
                                        Local4 = Zero
                                    }
                                    Else
                                    {
                                        Local0 = M019 (M245, Zero, Zero, Zero)
                                        Sleep (0x05)
                                        Local5--
                                    }
                                }

                                M201 = Zero
                                \_SB.ALIB (0x06, Local7)
                            }
                        }
                    }

                    If ((M400 == One))
                    {
                        M403 (M243, M244, Zero)
                    }

                    M000 (0xC1)
                    If ((M272 == One))
                    {
                        M439 (Zero, M243, M244)
                    }

                    If ((M447 == One))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = One
                            \_SB.ALIB (0x0C, Local3)
                        }
                    }

                    \_SB.PCI0.GPP0.M407 = One
                    M000 (0xA0)
                }
                Else
                {
                    M000 (0xA1)
                    \_SB.PCI0.GPP0.M407 = ((M049 (M128, 0x65) >> 0x07) & One)
                    If ((M447 == One))
                    {
                        Local0 = M372 ()
                        If (((Local0 >= Zero) && (Local0 <= 0xBF)))
                        {
                            M256 = Zero
                            \_SB.ALIB (0x0C, Local3)
                        }
                    }

                    If ((M273 == One))
                    {
                        Local1 = M019 (Zero, M243, M244, 0x54)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFC))
                        Sleep (One)
                        Local2 = M017 (Zero, M243, M244, 0x19, Zero, 0x08)
                        M028 (Local2, Zero, Zero, Zero)
                        Local3 = M027 (Local2, Zero, Zero)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFF))
                        If ((M400 == Zero))
                        {
                            Local1 = M017 (Zero, M243, M244, 0x68, Zero, 0x08)
                            M018 (Zero, M243, M244, 0x68, Zero, 0x08, (Local1 | 0x10))
                            Sleep (0x18)
                        }
                    }
                    Else
                    {
                        Local1 = M019 (Zero, M243, M244, 0x54)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFC))
                        M201 = Zero
                        \_SB.ALIB (0x06, Local7)
                        M020 (Zero, M243, M244, 0x54, (Local1 & 0xFFFF7FFF))
                    }

                    If ((M427 == One))
                    {
                        Local1 = M430 (Zero, M243, M244)
                        Local1 &= 0x0F
                        If ((Local1 > One))
                        {
                            Local4 = M025 (Zero, M243, M244, One)
                        }
                    }

                    If ((M400 == One))
                    {
                        M402 (M243, M244)
                    }

                    If ((M442 > Zero))
                    {
                        Divide (M442, 0x03E8, Local5, Local4)
                        If ((Local4 >= One))
                        {
                            Sleep (Local4)
                        }

                        Divide (Local5, 0x63, Local5, Local4)
                        While ((Local4 >= One))
                        {
                            Local4--
                            Stall (0x63)
                        }

                        If ((Local5 >= One))
                        {
                            Stall (Local5)
                        }
                    }

                    M000 (0xA2)
                    If (CondRefOf (\_SB.MACO))
                    {
                        If ((\_SB.MACO == One))
                        {
                            If ((M266 < 0x0100))
                            {
                                P011 = 0xC4
                            }
                            Else
                            {
                                M010 (M266, One)
                            }

                            Stall (M268)
                            If ((M267 < 0x0100))
                            {
                                P141 = 0x84
                            }
                            Else
                            {
                                M010 (M267, Zero)
                            }

                            Sleep (M269)
                            M112 (M242, Zero)
                        }
                        Else
                        {
                            M112 (M242, Zero)
                            Sleep (0x0A)
                            M111 (M242, Zero)
                        }
                    }
                    Else
                    {
                        M112 (M242, Zero)
                        Sleep (0x0A)
                        M111 (M242, Zero)
                    }

                    If ((M427 == One))
                    {
                        If ((Local1 > One))
                        {
                            M429 (Zero, M243, M244, 0x88, Zero, 0x04, Local1)
                        }
                    }

                    M023 (Zero, M243, M244)
                    If ((M400 == Zero))
                    {
                        Local1 = M019 (M245, Zero, Zero, Zero)
                        Sleep (0x0A)
                    }

                    If ((M085 < 0x08))
                    {
                        Local4 = One
                        Local5 = 0x05
                        While ((Local4 && Local5))
                        {
                            Local0 = M021 (Zero, M243, M244, 0xA5)
                            Local0 &= 0x7F
                            If (((Local0 <= 0x04) || (Local0 == 0x1F)))
                            {
                                Local4 = Zero
                            }
                            Else
                            {
                                Local1 = M019 (M245, Zero, Zero, Zero)
                                Sleep (0x05)
                                Local5--
                            }
                        }
                    }

                    M000 (0xA3)
                    M272 = 0x02
                }
            }

            Release (EEBC)
        }

        PowerResource (PG00, 0x00, 0x0000)
        {
            Name (M239, One)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                Return (M239) /* \_SB_.PCI0.GPP0.PG00.M239 */
            }

            Method (_ON, 0, NotSerialized)  // _ON_: Power On
            {
                Local6 = (EGPS & 0x04)
                Local6 >>= 0x02
                If ((Local6 == Zero))
                {
                    \_SB.PCI0.SBRG.EC0.WEBC (0x07, Zero, Zero)
                    If ((M239 == Zero))
                    {
                        If ((\_SB.PCI0.GPP0.PEGP.TDGC == One))
                        {
                            If ((\_SB.PCI0.GPP0.PEGP.DGCX == 0x03))
                            {
                                \_SB.PCI0.GPP0.PEGP.GC6O ()
                            }
                            ElseIf ((\_SB.PCI0.GPP0.PEGP.DGCX == 0x04))
                            {
                                \_SB.PCI0.GPP0.PEGP.GC6O ()
                            }

                            \_SB.PCI0.GPP0.PEGP.TDGC = Zero
                            \_SB.PCI0.GPP0.PEGP.DGCX = Zero
                        }
                        Else
                        {
                            SGPC (One)
                            \_SB.PCI0.GPP0.CMDR = 0x06
                            \_SB.PCI0.GPP0.D0ST = Zero
                            \GPUF = One
                        }
                    }

                    M239 = One
                }
            }

            Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
            {
                If ((M239 == One))
                {
                    If ((\_SB.PCI0.GPP0.PEGP.TDGC == One))
                    {
                        CreateField (\_SB.PCI0.GPP0.PEGP.TGPC, Zero, 0x03, GUPC)
                        If ((ToInteger (GUPC) == One))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6I ()
                        }
                        ElseIf ((ToInteger (GUPC) == 0x02))
                        {
                            \_SB.PCI0.GPP0.PEGP.GC6I ()
                        }
                    }
                    Else
                    {
                        SGPC (Zero)
                        \GPUF = Zero
                    }
                }

                \_SB.PCI0.SBRG.EC0.WEBC (0x08, Zero, Zero)
                M239 = Zero
                WOSR = Zero
            }
        }

        Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
        {
            PG00
        })
        Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
        {
            PG00
        })
        Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
        {
            PG00
        })
        Name (_S0W, 0x04)  // _S0W: S0 Device Wake State
        OperationRegion (RPCX, SystemMemory, 0xF0009000, 0x1000)
        Field (RPCX, DWordAcc, NoLock, Preserve)
        {
            Offset (0x04), 
            CMDR,   8, 
            Offset (0x19), 
            PRBN,   8, 
            Offset (0x52), 
                ,   13, 
            LASX,   1, 
            Offset (0x54), 
            D0ST,   2, 
            Offset (0x62), 
            CEDR,   1, 
            Offset (0x68), 
            ASPM,   2, 
                ,   2, 
            LNKD,   1, 
            Offset (0x80), 
                ,   10, 
            LREN,   1, 
            Offset (0xE2), 
                ,   2, 
            L23E,   1, 
            L23R,   1
        }

        Device (PEGP)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Name (LTRE, Zero)
            Method (_STA, 0, Serialized)  // _STA: Status
            {
                Local2 = M017 (Zero, One, One, 0x68, 0x04, One)
                If ((Local2 == Zero))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (_EJ0, 1, NotSerialized)  // _EJx: Eject Device, x=0-9
            {
                If ((GPCE == One))
                {
                    SGPC (Zero)
                    \_SB.PCI0.GPP0.PG00.M239 = Zero
                }
            }

            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
            }

            OperationRegion (PCIM, SystemMemory, (0xF0000000 + (\_SB.PCI0.GPP0.PRBN << 0x14)), 0x0600)
            Field (PCIM, DWordAcc, NoLock, Preserve)
            {
                NVID,   16, 
                NDID,   16, 
                CMDR,   8, 
                VGAR,   2000, 
                Offset (0x48B), 
                    ,   1, 
                NHDA,   1
            }

            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }
        }

        Device (HDAU)
        {
            Name (_ADR, One)  // _ADR: Address
            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }
        }

        Name (VGAB, Buffer (0xFA)
        {
             0x00                                             // .
        })
        Method (SGPC, 1, Serialized)
        {
            CreateByteField (M236, One, M243)
            CreateByteField (M236, 0x02, M244)
            If ((Arg0 == One))
            {
                If ((\_SB.GGOV (Zero, 0x54) == One))
                {
                    Return (Zero)
                }

                \_SB.SGOV (Zero, 0x45, Zero)
                Sleep (One)
                \_SB.SGOV (Zero, 0x0C, Zero)
                Local0 = 0x64
                While (Local0)
                {
                    If ((\_SB.GGOV (Zero, 0x54) == One))
                    {
                        Break
                    }

                    Sleep (One)
                    Local0--
                }

                Sleep (0x03)
                \_SB.SGOV (Zero, 0x45, One)
                Sleep (0x64)
                If ((GPCE == 0x02))
                {
                    LNKD = Zero
                    Sleep (One)
                    Local6 = 0x7FFFFFFF
                    Local6 |= 0x80000000
                    Local2 = M017 (Zero, One, One, 0x19, Zero, 0x08)
                    Local1 = M019 (Zero, One, One, 0x54)
                    M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFC))
                    Local4 = One
                    Local5 = 0x28
                    While ((Local4 && Local5))
                    {
                        Local0 = M019 (Local2, Zero, Zero, Zero)
                        If ((Local0 != Local6))
                        {
                            Local4 = Zero
                        }
                        Else
                        {
                            Sleep (0x05)
                            Local5--
                        }
                    }
                }
                Else
                {
                    M403 (M243, M244, One)
                    Local1 = Zero
                    Local2 = 0x13BB
                    While ((((Local1 & 0x28) != 0x20) && (Local2 > Zero)))
                    {
                        M000 (0xC0)
                        Local1 = M017 (Zero, M243, M244, 0x6B, Zero, 0x08)
                        Local2 = (Local2 - One)
                        Stall (0x63)
                    }

                    M403 (M243, M244, Zero)
                }

                \_SB.PCI0.GPP0.LREN = \_SB.PCI0.GPP0.PEGP.LTRE
                \_SB.PCI0.GPP0.CEDR = One
                M020 (Zero, One, One, 0x54, (Local1 & 0xFFFF7FFF))
            }
            Else
            {
                \_SB.PCI0.GPP0.PEGP.LTRE = \_SB.PCI0.GPP0.LREN
                If ((GPCE == One))
                {
                    LNKD = One
                }
                Else
                {
                    M402 (M243, M244)
                    Debug = "SGPC(Zero).ConfigGPIO"
                }

                \_SB.SGOV (Zero, 0x45, Zero)
                Sleep (One)
                \_SB.SGOV (Zero, 0x0C, One)
                Sleep (One)
                M023 (Zero, One, One)
            }
        }
    }

    Scope (\_SB.PCI0.GPP0.PEGP)
    {
        OperationRegion (PCIS, PCI_Config, Zero, 0x0100)
        Field (PCIS, AnyAcc, NoLock, Preserve)
        {
            PVID,   16, 
            PDID,   16
        }

        CreateByteField (M236, One, M243)
        CreateByteField (M236, 0x02, M244)
        Name (OPCE, 0x02)
        Name (DGPS, Zero)
        Name (_PSC, Zero)  // _PSC: Power State Current
        Name (GPRF, Zero)
        Name (INIA, Zero)
        Name (DSTA, Zero)
        Name (NLIM, Zero)
        Name (PSLS, Zero)
        Name (VPSC, One)
        Name (GPSP, Buffer (0x28) {})
        CreateDWordField (GPSP, Zero, RETN)
        CreateDWordField (GPSP, 0x04, VRV1)
        CreateDWordField (GPSP, 0x08, TGPU)
        CreateDWordField (GPSP, 0x0C, PDTS)
        CreateDWordField (GPSP, 0x10, SFAN)
        CreateDWordField (GPSP, 0x14, SKNT)
        CreateDWordField (GPSP, 0x18, CPUE)
        CreateDWordField (GPSP, 0x1C, TMP1)
        CreateDWordField (GPSP, 0x20, TMP2)
        Name (TGPC, Buffer (0x04)
        {
             0x00                                             // .
        })
        Name (GC6S, Zero)
        Name (TDGC, Zero)
        Name (DGCX, Zero)
        Method (_PS0, 0, NotSerialized)  // _PS0: Power State 0
        {
            _PSC = Zero
            If ((DGPS != Zero))
            {
                \_SB.PCI0.GPP0.PG00._ON ()
                DGPS = Zero
            }
        }

        Method (_PS3, 0, NotSerialized)  // _PS3: Power State 3
        {
            If ((OPCE == 0x03))
            {
                If ((DGPS == Zero))
                {
                    \_SB.PCI0.GPP0.PG00._OFF ()
                    DGPS = One
                }

                OPCE = 0x02
            }

            _PSC = 0x03
        }

        Method (SGST, 0, Serialized)
        {
            If ((PVID != 0xFFFF))
            {
                Return (0x0F)
            }

            Return (Zero)
        }

        Method (CMPB, 2, NotSerialized)
        {
            Local1 = SizeOf (Arg0)
            If ((Local1 != SizeOf (Arg1)))
            {
                Return (Zero)
            }

            Local0 = Zero
            While ((Local0 < Local1))
            {
                If ((DerefOf (Arg0 [Local0]) != DerefOf (Arg1 [Local0]
                    )))
                {
                    Return (Zero)
                }

                Local0++
            }

            Return (One)
        }

        Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
        {
            CreateByteField (Arg0, 0x03, GUID)
            If (CMPB (Arg0, ToUUID ("d4a50b75-65c7-46f7-bfb7-41514cea0244") /* Unknown UUID */))
            {
                Return (NBCI (Arg0, Arg1, Arg2, Arg3))
            }
            ElseIf (CMPB (Arg0, ToUUID ("a486d8f8-0bda-471b-a72b-6042a6b5bee0") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NVOP (Arg0, Arg1, Arg2, Arg3))
            }
            ElseIf (CMPB (Arg0, ToUUID ("a3132d01-8cda-49ba-a52e-bc9d46df6b81") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.GPS (Arg0, Arg1, Arg2, Arg3))
            }
            ElseIf (CMPB (Arg0, ToUUID ("cbeca351-067b-4924-9cbd-b46b00b86f34") /* Unknown UUID */))
            {
                Return (\_SB.PCI0.GPP0.PEGP.NVJT (Arg0, Arg1, Arg2, Arg3))
            }

            Return (0x80000002)
        }

        Method (GC6I, 0, Serialized)
        {
            Debug = "JT GC6I"
            GC6S = One
            \_SB.PCI0.GPP0.PEGP.LTRE = \_SB.PCI0.GPP0.LREN
            M402 (M243, M244)
            Debug = "SGPC(Zero).ConfigGPIO"
            Sleep (0x0A)
            \_SB.SGOV (Zero, 0x45, Zero)
        }

        Method (GC6O, 0, Serialized)
        {
            Debug = "JT GC6O"
            GC6S = Zero
            \_SB.SGOV (Zero, 0x45, One)
            Sleep (0x0A)
            M403 (M243, M244, One)
            Local1 = Zero
            Local2 = 0x13BB
            While ((((Local1 & 0x28) != 0x20) && (Local2 > Zero)))
            {
                M000 (0xC0)
                Local1 = M017 (Zero, M243, M244, 0x6B, Zero, 0x08)
                Local2 = (Local2 - One)
                Stall (0x63)
            }

            M403 (M243, M244, Zero)
            \_SB.PCI0.GPP0.CMDR |= 0x04
            \_SB.PCI0.GPP0.LREN = \_SB.PCI0.GPP0.PEGP.LTRE
            \_SB.PCI0.GPP0.CEDR = One
        }

        Method (NVJT, 4, Serialized)
        {
            Debug = "------- NV JT DSM --------"
            If ((Arg1 < 0x0100))
            {
                Return (0x80000001)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Debug = "JT fun0 JT_FUNC_SUPPORT"
                    Return (Buffer (0x04)
                    {
                         0x1B, 0x00, 0x00, 0x00                           // ....
                    })
                }
                Case (One)
                {
                    Debug = "JT fun1 JT_FUNC_CAPS"
                    Name (JTCA, Buffer (0x04)
                    {
                         0x00                                             // .
                    })
                    CreateField (JTCA, Zero, One, JTEN)
                    CreateField (JTCA, One, 0x02, SREN)
                    CreateField (JTCA, 0x03, 0x02, PLPR)
                    CreateField (JTCA, 0x05, One, SRPR)
                    CreateField (JTCA, 0x06, 0x02, FBPR)
                    CreateField (JTCA, 0x08, 0x02, GUPR)
                    CreateField (JTCA, 0x0A, One, GC6R)
                    CreateField (JTCA, 0x0B, One, PTRH)
                    CreateField (JTCA, 0x0D, One, MHYB)
                    CreateField (JTCA, 0x0E, One, RPCL)
                    CreateField (JTCA, 0x0F, 0x02, GC6V)
                    CreateField (JTCA, 0x11, One, GEIS)
                    CreateField (JTCA, 0x12, One, GSWS)
                    CreateField (JTCA, 0x14, 0x0C, JTRV)
                    JTEN = One
                    GC6R = Zero
                    MHYB = One
                    RPCL = One
                    SREN = One
                    FBPR = Zero
                    MHYB = One
                    PLPR = 0x02
                    SRPR = Zero
                    GC6V = 0x02
                    JTRV = 0x0200
                    Return (JTCA) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTCA */
                }
                Case (0x02)
                {
                    Debug = "JT fun2 JT_FUNC_POLICYSELECT"
                    Return (0x80000002)
                }
                Case (0x03)
                {
                    Debug = "JT fun3 JT_FUNC_POWERCONTROL"
                    CreateField (Arg3, Zero, 0x03, GUPC)
                    CreateField (Arg3, 0x04, One, PLPC)
                    CreateField (Arg3, 0x07, One, ECOC)
                    CreateField (Arg3, 0x0E, 0x02, DFGC)
                    CreateField (Arg3, 0x10, 0x03, GPCX)
                    \_SB.PCI0.GPP0.PEGP.TGPC = Arg3
                    If (((ToInteger (GUPC) != Zero) || (ToInteger (DFGC
                        ) != Zero)))
                    {
                        TDGC = ToInteger (DFGC)
                        DGCX = ToInteger (GPCX)
                    }

                    Name (JTPC, Buffer (0x04)
                    {
                         0x00                                             // .
                    })
                    CreateField (JTPC, Zero, 0x03, GUPS)
                    CreateField (JTPC, 0x03, One, GPWO)
                    CreateField (JTPC, 0x07, One, PLST)
                    If ((ToInteger (DFGC) != Zero))
                    {
                        GPWO = One
                        GUPS = One
                        Return (JTPC) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTPC */
                    }

                    Debug = "   JT fun3 GUPC="
                    Debug = ToInteger (GUPC)
                    If ((ToInteger (GUPC) == One))
                    {
                        GC6I ()
                    }
                    ElseIf ((ToInteger (GUPC) == 0x02))
                    {
                        GC6I ()
                        If ((ToInteger (PLPC) == Zero))
                        {
                            PLST = Zero
                        }
                    }
                    ElseIf ((ToInteger (GUPC) == 0x03))
                    {
                        GC6O ()
                        If ((ToInteger (PLPC) == Zero))
                        {
                            PLST = Zero
                        }

                        GPWO = One
                        GUPS = One
                    }
                    ElseIf ((ToInteger (GUPC) == 0x04))
                    {
                        GC6O ()
                        If ((ToInteger (PLPC) != Zero))
                        {
                            PLST = Zero
                        }

                        GPWO = One
                        GUPS = One
                    }
                    Else
                    {
                        Debug = "<<< GUPC 5/6 >>>"
                        If ((GC6S == Zero))
                        {
                            Debug = "   JT GETS() return 0x1"
                            GPWO = One
                            GUPS = One
                        }
                        Else
                        {
                            Debug = "   JT GETS() return 0x3"
                            GPWO = Zero
                            GUPS = 0x03
                        }
                    }

                    Return (JTPC) /* \_SB_.PCI0.GPP0.PEGP.NVJT.JTPC */
                }
                Case (0x04)
                {
                    Debug = "   JT fun4 JT_FUNC_PLATPOLICY"
                    CreateField (Arg3, 0x02, One, PAUD)
                    CreateField (Arg3, 0x03, One, PADM)
                    CreateField (Arg3, 0x04, 0x04, PDGS)
                    Local0 = Zero
                    Local0 = (\_SB.PCI0.GPP0.PEGP.NHDA << 0x02)
                    Return (Local0)
                }

            }

            Return (0x80000002)
        }

        Method (NVOP, 4, Serialized)
        {
            Debug = "------- NVOP --------"
            If ((Arg2 == Zero))
            {
                Return (Buffer (0x04)
                {
                     0x01, 0x00, 0x00, 0x04                           // ....
                })
            }
            ElseIf ((Arg2 == 0x1A))
            {
                Debug = "------- NVOP 0x1A --------"
                CreateField (Arg3, 0x18, 0x02, OMPR)
                CreateField (Arg3, Zero, One, FLCH)
                CreateField (Arg3, One, One, DVSR)
                CreateField (Arg3, 0x02, One, DVSC)
                If (ToInteger (FLCH))
                {
                    \_SB.PCI0.GPP0.PEGP.OPCE = OMPR /* \_SB_.PCI0.GPP0.PEGP.NVOP.OMPR */
                }

                Local0 = Buffer (0x04)
                    {
                         0x00, 0x00, 0x00, 0x00                           // ....
                    }
                CreateField (Local0, Zero, One, OPEN)
                CreateField (Local0, 0x03, 0x02, CGCS)
                CreateField (Local0, 0x06, One, SHPC)
                CreateField (Local0, 0x08, One, SNSR)
                CreateField (Local0, 0x18, 0x03, DGPC)
                CreateField (Local0, 0x1B, 0x02, HDAC)
                OPEN = One
                SHPC = One
                HDAC = 0x03
                DGPC = One
                If (ToInteger (DVSC))
                {
                    If (ToInteger (DVSR))
                    {
                        \_SB.PCI0.GPP0.PEGP.GPRF = One
                    }
                    Else
                    {
                        \_SB.PCI0.GPP0.PEGP.GPRF = Zero
                    }
                }

                SNSR = \_SB.PCI0.GPP0.PEGP.GPRF
                If ((SGST () != Zero))
                {
                    Debug = "GPU power is on  --------"
                    CGCS = 0x03
                }

                Return (Local0)
            }

            Return (0x80000002)
        }

        Name (GDRG, Buffer (0xA1)
        {
            /* 0000 */  0x57, 0x74, 0xDC, 0x86, 0x75, 0x84, 0xEC, 0xE7,  // Wt..u...
            /* 0008 */  0x52, 0x44, 0xA1, 0x00, 0x00, 0x00, 0x00, 0x01,  // RD......
            /* 0010 */  0x00, 0x00, 0x00, 0x00, 0xDE, 0x10, 0x00, 0x00,  // ........
            /* 0018 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0020 */  0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x34, 0x00,  // ......4.
            /* 0028 */  0x00, 0x00, 0x01, 0x00, 0x47, 0x00, 0x00, 0x00,  // ....G...
            /* 0030 */  0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00,  // ..E.....
            /* 0038 */  0x51, 0x00, 0x00, 0x00, 0x04, 0x00, 0x4F, 0x00,  // Q.....O.
            /* 0040 */  0x00, 0x00, 0x05, 0x00, 0x4D, 0x00, 0x00, 0x00,  // ....M...
            /* 0048 */  0x06, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x07, 0x00,  // ..K.....
            /* 0050 */  0x49, 0x00, 0x00, 0x00, 0x08, 0x00, 0x47, 0x00,  // I.....G.
            /* 0058 */  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD9, 0x1C,  // ........
            /* 0060 */  0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  // ........
            /* 0068 */  0x41, 0x5D, 0xC9, 0x00, 0x01, 0x24, 0x2E, 0x00,  // A]...$..
            /* 0070 */  0x02, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x01,  // ........
            /* 0078 */  0x00, 0x00, 0x00, 0xD9, 0x1C, 0x04, 0x00, 0x00,  // ........
            /* 0080 */  0x00, 0x01, 0x00, 0x00, 0x00, 0x60, 0x68, 0x9E,  // .....`h.
            /* 0088 */  0x35, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // 5.......
            /* 0090 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 0098 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
            /* 00A0 */  0x00                                             // .
        })
        Method (NBCI, 4, Serialized)
        {
            Debug = "------- NBCI --------"
            If ((Arg1 != 0x0102))
            {
                Return (0x80000002)
            }

            If ((Arg2 == Zero))
            {
                Return (Buffer (0x04)
                {
                     0x01, 0x00, 0x01, 0x00                           // ....
                })
            }

            If ((Arg2 == One))
            {
                Name (TEMP, Buffer (0x04)
                {
                     0x00, 0x00, 0x00, 0x00                           // ....
                })
                CreateDWordField (TEMP, Zero, STS0)
                STS0 |= Zero
                Return (TEMP) /* \_SB_.PCI0.GPP0.PEGP.NBCI.TEMP */
            }

            If ((Arg2 == 0x10))
            {
                CreateWordField (Arg3, 0x02, BFF0)
                If ((BFF0 == 0x4452))
                {
                    Debug = "Get DR key"
                    Return (GDRG) /* \_SB_.PCI0.GPP0.PEGP.GDRG */
                }
            }
        }

        Method (ICNV, 0, NotSerialized)
        {
            If (INIA)
            {
                Return (Zero)
            }
            Else
            {
                INIA = One
                If ((DSTA == Zero))
                {
                    Return (Zero)
                }
                Else
                {
                    Notify (PEGP, DSTA)
                }
            }
        }

        Method (GPS, 4, Serialized)
        {
            Debug = "<<< GPS >>>"
            If ((\_SB.PCI0.GPP0.PEGP.INIA == Zero))
            {
                \_SB.PCI0.GPP0.PEGP.ICNV ()
                \_SB.PCI0.SBRG.EC0.AGPL (0xD1)
            }

            If ((Arg1 != 0x0200))
            {
                Return (0x80000002)
            }

            Switch (ToInteger (Arg2))
            {
                Case (Zero)
                {
                    Debug = "GPS fun 0"
                    Return (Buffer (0x08)
                    {
                         0x01, 0x00, 0x08, 0x00, 0x01, 0x04, 0x00, 0x00   // ........
                    })
                }
                Case (0x13)
                {
                    Debug = "GPS fun 19"
                    CreateDWordField (Arg3, Zero, TEMP)
                    If ((TEMP == Zero))
                    {
                        Return (0x04)
                    }

                    TEMP &= 0x0F
                    If ((TEMP == 0x04))
                    {
                        Return (Arg3)
                    }
                }
                Case (0x1C)
                {
                    Debug = "   GPS fun 28"
                    CreateField (Arg3, Zero, 0x04, RTFS)
                    CreateField (Arg3, 0x08, 0x08, VPS0)
                    CreateField (Arg3, 0x24, 0x08, VPS1)
                    If ((ToInteger (RTFS) == Zero))
                    {
                        Local0 = 0x02
                        If ((VPSC == Zero))
                        {
                            Local0 |= 0x0600
                        }
                        Else
                        {
                            Local0 |= Zero
                        }

                        Return (Local0)
                    }
                    ElseIf ((ToInteger (RTFS) == 0x02))
                    {
                        Return (Zero)
                    }
                }
                Case (0x20)
                {
                    Debug = "GPS fun 32"
                    Name (RET1, Zero)
                    CreateBitField (Arg3, 0x02, SPBI)
                    If (NLIM)
                    {
                        RET1 |= One
                    }

                    If (PSLS)
                    {
                        RET1 |= 0x02
                    }

                    Return (RET1) /* \_SB_.PCI0.GPP0.PEGP.GPS_.RET1 */
                }
                Case (0x2A)
                {
                    Debug = "GPS fun 42"
                    CreateField (Arg3, Zero, 0x04, PSH0)
                    CreateBitField (Arg3, 0x08, GPUT)
                    VRV1 = 0x00010000
                    Switch (ToInteger (PSH0))
                    {
                        Case (Zero)
                        {
                            Acquire (\_SB.PCI0.SBRG.EC0.CMUT, 0xFFFF)
                            \_SB.PCI0.SBRG.EC0.BRAH = 0xC9
                            PDTS = \_SB.PCI0.SBRG.EC0.ECPU /* External reference */
                            Release (\_SB.PCI0.SBRG.EC0.CMUT)
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (One)
                        {
                            RETN = 0x0100
                            RETN |= ToInteger (PSH0)
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }
                        Case (0x02)
                        {
                            RETN = 0x0102
                            If ((TGPU == Zero))
                            {
                                TGPU = \_SB.PCI0.SBRG.EC0.STCC (Zero, 0x27)
                            }
                            Else
                            {
                            }

                            NLIM = Zero
                            Return (GPSP) /* \_SB_.PCI0.GPP0.PEGP.GPSP */
                        }

                    }

                    Return (0x80000002)
                }

            }

            Return (0x80000002)
        }
    }

    Scope (\_SB)
    {
        Device (NPCF)
        {
            Name (PABS, One)
            Name (CTGP, One)
            Name (UOCT, Zero)
            Name (DTGP, One)
            Name (AMAT, 0xA0)
            Name (AMIT, Zero)
            Name (DMAT, 0x50)
            Name (DMIT, Zero)
            Name (ATPP, 0x0118)
            Name (DTPP, 0xC8)
            Name (HPCT, 0x02)
            Name (CMPL, 0xFF)
            Name (CNPL, 0xAC)
            Name (CDIS, Zero)
            Name (CUSL, Zero)
            Name (WM2M, One)
            Name (CTDI, Zero)
            Name (GTDI, Zero)
            Name (AVGF, Zero)
            Name (AVGI, Zero)
            Name (AVG0, Zero)
            Name (AVG1, Zero)
            Name (AVG2, Zero)
            Name (AVG3, Zero)
            Name (AVG4, Zero)
            Method (_HID, 0, NotSerialized)  // _HID: Hardware ID
            {
                CDIS = Zero
                Return ("NVDA0820")
            }

            Name (_UID, "NPCF")  // _UID: Unique ID
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((CDIS == One))
                {
                    Return (0x0D)
                }

                Return (0x0F)
            }

            Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
            {
                CDIS = One
            }

            Method (CMPC, 2, NotSerialized)
            {
                Local1 = SizeOf (Arg0)
                If ((Local1 != SizeOf (Arg1)))
                {
                    Return (Zero)
                }

                Local0 = Zero
                While ((Local0 < Local1))
                {
                    If ((DerefOf (Arg0 [Local0]) != DerefOf (Arg1 [Local0]
                        )))
                    {
                        Return (Zero)
                    }

                    Local0++
                }

                Return (One)
            }

            Name (SFTN, Zero)
            Name (SCFI, Buffer (0x0C)
            {
                /* 0000 */  0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C, 0x3D, 0x41,  // ..-27<=A
                /* 0008 */  0x42, 0x46, 0x47, 0x4B                           // BFGK
            })
            Name (SGFI, Buffer (0x0C)
            {
                /* 0000 */  0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C, 0x3D, 0x41,  // ..-27<=A
                /* 0008 */  0x42, 0x46, 0x47, 0x4B                           // BFGK
            })
            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
                SFTN = 0x06
            }

            Method (MAVT, 1, Serialized)
            {
                Switch (ToInteger (AVGI))
                {
                    Case (Zero)
                    {
                        AVG0 = Arg0
                    }
                    Case (One)
                    {
                        AVG1 = Arg0
                    }
                    Case (0x02)
                    {
                        AVG2 = Arg0
                    }
                    Case (0x03)
                    {
                        AVG3 = Arg0
                    }
                    Case (0x04)
                    {
                        AVG4 = Arg0
                    }

                }

                If ((AVGI >= 0x04))
                {
                    AVGI = Zero
                    AVGF = One
                }
                Else
                {
                    AVGI += One
                }

                If ((AVGF >= One))
                {
                    Divide ((AVG0 + (AVG1 + (AVG2 + (AVG3 + AVG4))
                        )), 0x05, Local1, Local0)
                }
                Else
                {
                    Divide ((AVG0 + (AVG1 + (AVG2 + (AVG3 + AVG4))
                        )), AVGI, Local1, Local0)
                }

                Return (Local0)
            }

            Method (FCGI, 2, Serialized)
            {
                Local0 = CTDI /* \_SB_.NPCF.CTDI */
                While ((Local0 < SFTN))
                {
                    Local1 = ((Local0 * 0x02) + One)
                    If ((Arg0 >= DerefOf (SCFI [Local1])))
                    {
                        CTDI = Local0
                        Local0++
                    }
                    Else
                    {
                        Break
                    }
                }

                If ((CTDI == Local0))
                {
                    While ((Local0 > Zero))
                    {
                        Local1 = (Local0 * 0x02)
                        If ((Arg0 <= DerefOf (SCFI [Local1])))
                        {
                            Local0--
                            CTDI = Local0
                        }
                        Else
                        {
                            Break
                        }
                    }
                }

                Local0 = GTDI /* \_SB_.NPCF.GTDI */
                While ((Local0 < SFTN))
                {
                    Local1 = ((Local0 * 0x02) + One)
                    If ((Arg1 >= DerefOf (SGFI [Local1])))
                    {
                        GTDI = Local0
                        Local0++
                    }
                    Else
                    {
                        Break
                    }
                }

                If ((GTDI == Local0))
                {
                    While ((Local0 > Zero))
                    {
                        Local1 = (Local0 * 0x02)
                        If ((Arg1 <= DerefOf (SGFI [Local1])))
                        {
                            Local0--
                            GTDI = Local0
                        }
                        Else
                        {
                            Break
                        }
                    }
                }

                Local0 = (CTDI | (GTDI << 0x08))
                Return (Local0)
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                Return (NPCF (Arg0, Arg1, Arg2, Arg3))
            }

            Method (RCHV, 0, NotSerialized)
            {
                If ((IOBS != Zero))
                {
                    OperationRegion (NVIO, SystemIO, IOBS, 0x10)
                    Field (NVIO, ByteAcc, NoLock, Preserve)
                    {
                        CPUC,   8
                    }
                }
            }

            Method (NPCF, 4, Serialized)
            {
                Debug = "------- NVPCF DSM --------"
                If ((ToInteger (Arg1) != 0x0200))
                {
                    Return (0x80000001)
                }

                Switch (ToInteger (Arg2))
                {
                    Case (Zero)
                    {
                        Debug = "   NVPCF sub-func#0"
                        Return (Buffer (0x04)
                        {
                             0x7F, 0x00, 0x00, 0x00                           // ....
                        })
                    }
                    Case (One)
                    {
                        Debug = "   NVPCF sub-func#1"
                        Return (Buffer (0x1B)
                        {
                            /* 0000 */  0x20, 0x03, 0x01, 0x01, 0x20, 0x05, 0x02, 0x0F,  //  ... ...
                            /* 0008 */  0x01, 0x64, 0x00, 0x01, 0x01, 0x00, 0x00, 0xE8,  // .d......
                            /* 0010 */  0x00, 0x00, 0x66, 0x0E, 0x00, 0x10, 0x00, 0x00,  // ..f.....
                            /* 0018 */  0x00, 0x00, 0xD2                                 // ...
                        })
                    }
                    Case (0x02)
                    {
                        Debug = "   NVPCF sub-func#2"
                        Name (PBD2, Buffer (0x31)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBD2, Zero, PTV2)
                        CreateByteField (PBD2, One, PHB2)
                        CreateByteField (PBD2, 0x02, GSB2)
                        CreateByteField (PBD2, 0x03, CTB2)
                        CreateByteField (PBD2, 0x04, NCE2)
                        PTV2 = 0x20
                        PHB2 = 0x05
                        GSB2 = 0x10
                        CTB2 = 0x1C
                        NCE2 = One
                        CreateWordField (PBD2, 0x05, PGSO)
                        CreateByteField (PBD2, 0x15, PC0O)
                        CreateWordField (PBD2, 0x19, TPPA)
                        CreateWordField (PBD2, 0x1B, TPPD)
                        CreateWordField (PBD2, 0x1D, MAGA)
                        CreateWordField (PBD2, 0x1F, MAGD)
                        CreateWordField (PBD2, 0x21, MIGA)
                        CreateWordField (PBD2, 0x23, MIGD)
                        CreateDWordField (PBD2, 0x25, PA4O)
                        CreateDWordField (PBD2, 0x29, PA5O)
                        CreateDWordField (PBD2, 0x2D, PA6O)
                        CreateField (Arg3, 0x28, 0x02, NIGS)
                        CreateByteField (Arg3, 0x15, IORC)
                        CreateField (Arg3, 0xB0, One, CSSC)
                        If ((ToInteger (NIGS) == Zero))
                        {
                            If ((CTGP == One))
                            {
                                PGSO = UOCT /* \_SB_.NPCF.UOCT */
                            }
                            Else
                            {
                                PGSO = Zero
                            }

                            PC0O = Zero
                            TPPA = ATPP /* \_SB_.NPCF.ATPP */
                            TPPD = DTPP /* \_SB_.NPCF.DTPP */
                            If ((DTGP == One))
                            {
                                MAGA = AMAT /* \_SB_.NPCF.AMAT */
                                MAGD = DMAT /* \_SB_.NPCF.DMAT */
                                MIGA = AMIT /* \_SB_.NPCF.AMIT */
                                MIGD = DMIT /* \_SB_.NPCF.DMIT */
                            }
                            Else
                            {
                                MAGA = Zero
                                MAGD = Zero
                                MIGA = Zero
                                MIGD = Zero
                            }
                        }

                        If ((ToInteger (NIGS) == One))
                        {
                            If ((ToInteger (CSSC) == One))
                            {
                                DTGP = One
                            }
                            Else
                            {
                                DTGP = Zero
                            }

                            PGSO = Zero
                            PC0O = Zero
                            TPPA = Zero
                            TPPD = Zero
                            MAGA = Zero
                            MIGA = Zero
                            MAGD = Zero
                            MIGD = Zero
                        }

                        Return (PBD2) /* \_SB_.NPCF.NPCF.PBD2 */
                    }
                    Case (0x03)
                    {
                        Debug = "   NVPCF sub-func#3"
                        Return (Buffer (0x1E)
                        {
                            /* 0000 */  0x11, 0x04, 0x0D, 0x02, 0x00, 0xFF, 0x00, 0x2D,  // .......-
                            /* 0008 */  0x32, 0x37, 0x3C, 0x3D, 0x41, 0x42, 0x46, 0x47,  // 27<=ABFG
                            /* 0010 */  0x4B, 0x05, 0xFF, 0x00, 0x2D, 0x32, 0x37, 0x3C,  // K...-27<
                            /* 0018 */  0x3D, 0x41, 0x42, 0x46, 0x47, 0x4B               // =ABFGK
                        })
                    }
                    Case (0x04)
                    {
                        Debug = "   NVPCF sub-func#4"
                        Return (Buffer (0x32)
                        {
                            /* 0000 */  0x11, 0x04, 0x2E, 0x01, 0x05, 0x00, 0x01, 0x02,  // ........
                            /* 0008 */  0x03, 0x04, 0x03, 0x00, 0x01, 0x02, 0x00, 0x01,  // ........
                            /* 0010 */  0x02, 0x03, 0x03, 0x03, 0x01, 0x01, 0x02, 0x03,  // ........
                            /* 0018 */  0x03, 0x03, 0x02, 0x02, 0x02, 0x03, 0x03, 0x03,  // ........
                            /* 0020 */  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,  // ........
                            /* 0028 */  0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,  // ........
                            /* 0030 */  0x03, 0x03                                       // ..
                        })
                    }
                    Case (0x05)
                    {
                        Debug = "   NVPCF sub-func#5"
                        Name (PBD5, Buffer (0x28)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBD5, Zero, PTV5)
                        CreateByteField (PBD5, One, PHB5)
                        CreateByteField (PBD5, 0x02, TEB5)
                        CreateByteField (PBD5, 0x03, NTE5)
                        PTV5 = 0x11
                        PHB5 = 0x04
                        TEB5 = 0x24
                        NTE5 = One
                        CreateDWordField (PBD5, 0x04, F5O0)
                        CreateDWordField (PBD5, 0x08, F5O1)
                        CreateDWordField (PBD5, 0x0C, F5O2)
                        CreateDWordField (PBD5, 0x10, F5O3)
                        CreateDWordField (PBD5, 0x14, F5O4)
                        CreateDWordField (PBD5, 0x18, F5O5)
                        CreateDWordField (PBD5, 0x1C, F5O6)
                        CreateDWordField (PBD5, 0x20, F5O7)
                        CreateDWordField (PBD5, 0x24, F5O8)
                        CreateField (Arg3, 0x20, 0x02, INC5)
                        CreateByteField (Arg3, 0x08, F5P1)
                        Switch (ToInteger (INC5))
                        {
                            Case (Zero)
                            {
                                F5O0 = WM2M /* \_SB_.NPCF.WM2M */
                                F5O1 = Zero
                                F5O2 = Zero
                                F5O3 = Zero
                            }
                            Case (One)
                            {
                                F5O0 = 0x0C
                                F5O1 = Zero
                                F5O2 = Zero
                                F5O3 = Zero
                            }
                            Case (0x02)
                            {
                                F5O0 = Zero
                                Acquire (\_SB.PCI0.SBRG.EC0.CMUT, 0xFFFF)
                                \_SB.PCI0.SBRG.EC0.BRAH = 0xC9
                                Local0 = \_SB.PCI0.SBRG.EC0.ECPU /* External reference */
                                Local1 = \_SB.PCI0.SBRG.EC0.EGPT /* External reference */
                                Release (\_SB.PCI0.SBRG.EC0.CMUT)
                                Local0 = MAVT (Local0)
                                Local2 = FCGI (Local0, Local1)
                                F5O1 = ((Local0 << 0x10) | (Local2 & 0xFF))
                                F5O2 = ((Local1 << 0x10) | ((Local2 >> 0x08) & 0xFF
                                    ))
                            }
                            Case (0x03)
                            {
                                CUSL = F5P1 /* \_SB_.NPCF.NPCF.F5P1 */
                            }
                            Default
                            {
                                Return (0x80000002)
                            }

                        }

                        Return (PBD5) /* \_SB_.NPCF.NPCF.PBD5 */
                    }
                    Case (0x06)
                    {
                        Debug = "   NVPCF sub-func#6"
                        Name (PBD6, Buffer (0x11)
                        {
                             0x00                                             // .
                        })
                        CreateByteField (PBD6, Zero, CCHV)
                        CreateByteField (PBD6, One, CCHB)
                        CreateByteField (PBD6, 0x02, CCTB)
                        CreateByteField (PBD6, 0x03, RES0)
                        CreateByteField (PBD6, 0x04, RES1)
                        CCHV = 0x10
                        CCHB = 0x05
                        CCTB = 0x0C
                        CreateByteField (PBD6, 0x05, F6O0)
                        CreateByteField (PBD6, 0x09, F6MP)
                        CreateByteField (PBD6, 0x0A, F6NP)
                        CreateDWordField (PBD6, 0x0D, F6O2)
                        CreateDWordField (Arg3, 0x05, INC6)
                        CreateByteField (Arg3, 0x09, NCHP)
                        OperationRegion (NVIO, SystemIO, IOBS, 0x10)
                        Field (NVIO, ByteAcc, NoLock, Preserve)
                        {
                            CPUC,   8, 
                            CPUN,   8
                        }

                        CMPL = CPUC /* \_SB_.NPCF.NPCF.CPUC */
                        CNPL = CPUN /* \_SB_.NPCF.NPCF.CPUN */
                        Switch (ToInteger (INC6))
                        {
                            Case (Zero)
                            {
                                If ((IOBS != Zero))
                                {
                                    F6O0 = HPCT /* \_SB_.NPCF.HPCT */
                                    F6MP = CMPL /* \_SB_.NPCF.CMPL */
                                    F6NP = CNPL /* \_SB_.NPCF.CNPL */
                                    F6O2 = IOBS /* External reference */
                                }
                            }
                            Case (One)
                            {
                                If ((IOBS != Zero))
                                {
                                    CPUC = NCHP /* \_SB_.NPCF.NPCF.NCHP */
                                    Sleep (0x05)
                                    Notify (\_SB.PLTF.P000, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P001, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P002, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P003, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P004, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P005, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P006, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P007, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P008, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P009, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00A, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00B, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00C, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00D, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00E, 0x85) // Device-Specific
                                    Notify (\_SB.PLTF.P00F, 0x85) // Device-Specific
                                }
                            }
                            Default
                            {
                                Return (0x80000002)
                            }

                        }

                        Return (PBD6) /* \_SB_.NPCF.NPCF.PBD6 */
                    }

                }

                Return (0x80000002)
            }
        }
    }
}

