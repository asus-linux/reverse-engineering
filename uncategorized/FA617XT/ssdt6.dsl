/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt6.dat, Wed Nov 22 15:56:19 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x000009BB (2491)
 *     Revision         0x02
 *     Checksum         0x8B
 *     OEM ID           "AMD"
 *     OEM Table ID     "CPMDFDG2"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "AMD", "CPMDFDG2", 0x00000001)
{
    External (_SB_.GPCE, IntObj)
    External (_SB_.PCI0.GPP0, DeviceObj)
    External (_SB_.PCI0.GPP0.M241, MethodObj)    // 1 Arguments
    External (_SB_.PCI0.GPP0.M434, IntObj)
    External (_SB_.PCI0.SBRG.IVGA, FieldUnitObj)
    External (_SB_.PCI0.SBRG.NEDP, FieldUnitObj)
    External (_SB_.WOSR, IntObj)
    External (M037, DeviceObj)
    External (M046, IntObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M23A, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M29A, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M31C, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M404, BuffObj)
    External (M408, MutexObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)
    External (M453, FieldUnitObj)
    External (M454, FieldUnitObj)
    External (M455, FieldUnitObj)
    External (M456, FieldUnitObj)
    External (M457, FieldUnitObj)
    External (M460, MethodObj)    // 7 Arguments
    External (M4C0, FieldUnitObj)
    External (M4F0, FieldUnitObj)
    External (M610, FieldUnitObj)
    External (M620, FieldUnitObj)
    External (M631, FieldUnitObj)
    External (M652, FieldUnitObj)

    Scope (\_SB.PCI0.GPP0)
    {
        PowerResource (M237, 0x00, 0x0000)
        {
            Name (M239, One)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((\_SB.PCI0.SBRG.IVGA == One))
                {
                    M239 = Zero
                }

                Return (M239) /* \_SB_.PCI0.GPP0.M237.M239 */
            }

            Method (_ON, 0, NotSerialized)  // _ON_: Power On
            {
                If ((M239 == Zero))
                {
                    If (CondRefOf (\_SB.PCI0.GPP0.M241))
                    {
                        If ((\_SB.PCI0.SBRG.IVGA == Zero))
                        {
                            \_SB.PCI0.GPP0.M241 (One)
                            M239 = One
                        }
                    }
                }
            }

            Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
            {
                If ((M239 == One))
                {
                    If (CondRefOf (\_SB.PCI0.GPP0.M241))
                    {
                        \_SB.PCI0.GPP0.M241 (Zero)
                        M239 = Zero
                    }
                }
            }
        }

        Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
        {
            M237
        })
        Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
        {
            M237
        })
        Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
        {
            M237
        })
        Name (_S0W, 0x04)  // _S0W: S0 Device Wake State
        Device (SWUS)
        {
            PowerResource (M237, 0x00, 0x0000)
            {
                Name (M239, One)
                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    Return (M239) /* \_SB_.PCI0.GPP0.SWUS.M237.M239 */
                }

                Method (_ON, 0, NotSerialized)  // _ON_: Power On
                {
                    M239 = One
                }

                Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
                {
                    M239 = Zero
                }
            }

            Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
            {
                M237
            })
            Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
            {
                M237
            })
            Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
            {
                M237
            })
            Name (_S0W, 0x04)  // _S0W: S0 Device Wake State
            Name (_ADR, Zero)  // _ADR: Address
            Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
            {
                Return (Zero)
            }

            Name (DDPM, Package (0x02)
            {
                0x08, 
                0x04
            })
            Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
            {
                Return (DDPM) /* \_SB_.PCI0.GPP0.SWUS.DDPM */
            }

            Device (SWDS)
            {
                PowerResource (M237, 0x00, 0x0000)
                {
                    Name (M239, One)
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (M239) /* \_SB_.PCI0.GPP0.SWUS.SWDS.M237.M239 */
                    }

                    Method (_ON, 0, NotSerialized)  // _ON_: Power On
                    {
                        M239 = One
                    }

                    Method (_OFF, 0, NotSerialized)  // _OFF: Power Off
                    {
                        M239 = Zero
                    }
                }

                Name (_PR0, Package (0x01)  // _PR0: Power Resources for D0
                {
                    M237
                })
                Name (_PR2, Package (0x01)  // _PR2: Power Resources for D2
                {
                    M237
                })
                Name (_PR3, Package (0x01)  // _PR3: Power Resources for D3hot
                {
                    M237
                })
                Name (_S0W, 0x04)  // _S0W: S0 Device Wake State
                Name (_ADR, Zero)  // _ADR: Address
                Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
                {
                    Return (Zero)
                }

                Name (DDPM, Package (0x02)
                {
                    0x08, 
                    0x04
                })
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Return (DDPM) /* \_SB_.PCI0.GPP0.SWUS.SWDS.DDPM */
                }

                Device (VGA)
                {
                    Name (_ADR, Zero)  // _ADR: Address
                    Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
                    {
                        Return (Zero)
                    }

                    Device (LCD1)
                    {
                        Name (_ADR, 0x0110)  // _ADR: Address
                    }

                    Method (_EJ0, 1, NotSerialized)  // _EJx: Eject Device, x=0-9
                    {
                        M460 (" VGA _EJ0 Arg0 = 0x%X GPCE = 0x%X \n", ToInteger (Arg0), ToInteger (\_SB.GPCE), Zero, Zero, Zero, Zero)
                        \_SB.PCI0.GPP0.M241 (Zero)
                        \_SB.PCI0.GPP0.M237.M239 = Zero
                        \_SB.WOSR = Zero
                    }
                }

                Device (HDAU)
                {
                    Name (_ADR, One)  // _ADR: Address
                    Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
                    {
                        Return (Zero)
                    }

                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        If ((M097 != Zero))
                        {
                            Return (Zero)
                        }
                        Else
                        {
                            Return (0x0F)
                        }
                    }
                }

                Method (_DSD, 0, Serialized)  // _DSD: Device-Specific Data
                {
                    Return (Package (0x02)
                    {
                        ToUUID ("6b4ad420-8fd3-4364-acf8-eb94876fd9eb") /* Unknown UUID */, 
                        Package (0x00) {}
                    })
                }

                Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
                {
                    Name (M432, Zero)
                    Name (M433, Zero)
                    If ((Arg0 == ToUUID ("e5c937d0-3553-4d7a-9117-ea4d19c3434d") /* Device Labeling Interface */))
                    {
                        Switch (ToInteger (Arg2))
                        {
                            Case (Zero)
                            {
                                Name (M435, Buffer (0x02)
                                {
                                     0x00, 0x00                                       // ..
                                })
                                CreateBitField (M435, Zero, M436)
                                CreateBitField (M435, 0x05, M445)
                                CreateBitField (M435, 0x0A, M437)
                                CreateBitField (M435, 0x0B, M438)
                                If ((Arg1 >= 0x04))
                                {
                                    M436 = One
                                    M445 = One
                                    M432 = ((M049 (M128, 0x66) >> 0x04) & One)
                                    M433 = ((M049 (M128, 0x66) >> 0x05) & One)
                                    If ((M432 == One))
                                    {
                                        M437 = One
                                    }

                                    If ((M433 == One))
                                    {
                                        M438 = One
                                    }
                                }
                                Else
                                {
                                    M436 = One
                                    M445 = One
                                }

                                Return (M435) /* \_SB_.PCI0.GPP0.SWUS.SWDS._DSM.M435 */
                            }
                            Case (0x05)
                            {
                                Return (Zero)
                            }
                            Case (0x0A)
                            {
                                Return (One)
                            }
                            Case (0x0B)
                            {
                                Local0 = ToInteger (Arg3)
                                If ((Local0 <= 0x2710))
                                {
                                    \_SB.PCI0.GPP0.M434 = 0x2710
                                }
                                Else
                                {
                                    Local0 = 0x2710
                                }

                                Return (Local0)
                            }
                            Default
                            {
                                Return (Zero)
                            }

                        }
                    }
                    Else
                    {
                        Return (Buffer (One)
                        {
                             0x00                                             // .
                        })
                    }
                }
            }
        }

        Method (_DSD, 0, Serialized)  // _DSD: Device-Specific Data
        {
            Return (Package (0x04)
            {
                ToUUID ("6211e2c0-58a3-4af3-90e1-927a4e0c55a4") /* Unknown UUID */, 
                Package (0x01)
                {
                    Package (0x02)
                    {
                        "HotPlugSupportInD3", 
                        One
                    }
                }, 

                ToUUID ("fdf06fad-f744-4451-bb64-ecd792215b10") /* Unknown UUID */, 
                Package (0x01)
                {
                    Package (0x02)
                    {
                        "FundamentalDeviceResetTriggeredOnD3ToD0", 
                        One
                    }
                }
            })
        }
    }
}

