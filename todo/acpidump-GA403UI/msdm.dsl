/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembly of msdm.dat, Fri Mar  1 10:12:45 2024
 *
 * ACPI Data Table [MSDM]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue (in hex)
 */

[000h 0000   4]                    Signature : "MSDM"    [Microsoft Data Management Table]
[004h 0004   4]                 Table Length : 00000055
[008h 0008   1]                     Revision : 03
[009h 0009   1]                     Checksum : 10
[00Ah 0010   6]                       Oem ID : "_ASUS_"
[010h 0016   8]                 Oem Table ID : "Notebook"
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "ASUS"
[020h 0032   4]        Asl Compiler Revision : 00000001

[024h 0036  49] Software Licensing Structure : 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 034h 0052  16 */                            00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 044h 0068  16 */                            00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 /* ................ */\
/* 054h 0084   1 */                            00                                              /* . */\

Raw Table Data: Length 85 (0x55)

    0000: 4D 53 44 4D 55 00 00 00 03 10 5F 41 53 55 53 5F  // MSDMU....._ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 53 55 53  // Notebook. ..ASUS
    0020: 01 00 00 00 01 00 00 00 00 00 00 00 01 00 00 00  // ................
    0030: 00 00 00 00 1D 00 00 00 4E 42 57 33 51 2D 57 46  // ........NBW3Q-WF
    0040: 37 33 46 2D 59 4D 50 42 38 2D 50 38 36 4D 59 2D  // 73F-YMPB8-P86MY-
    0050: 43 57 47 46 38                                   // CWGF8
