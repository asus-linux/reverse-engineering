/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt26.dat, Wed Nov 22 15:56:19 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x00000E0E (3598)
 *     Revision         0x02
 *     Checksum         0x29
 *     OEM ID           "AMD"
 *     OEM Table ID     "CPMUCSI"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "AMD", "CPMUCSI", 0x00000001)
{
    External (_SB_.PCI0.SBRG.SEC1, MethodObj)    // 1 Arguments
    External (M000, MethodObj)    // 1 Arguments
    External (M013, MethodObj)    // 4 Arguments
    External (M037, DeviceObj)
    External (M046, IntObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M23A, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M29A, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M31C, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M404, BuffObj)
    External (M408, MutexObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)
    External (M453, FieldUnitObj)
    External (M454, FieldUnitObj)
    External (M455, FieldUnitObj)
    External (M456, FieldUnitObj)
    External (M457, FieldUnitObj)
    External (M4C0, FieldUnitObj)
    External (M4F0, FieldUnitObj)
    External (M610, FieldUnitObj)
    External (M620, FieldUnitObj)
    External (M631, FieldUnitObj)
    External (M652, FieldUnitObj)

    Scope (\_SB)
    {
        OperationRegion (PM0A, SystemMemory, M322, 0x02)
        Field (PM0A, ByteAcc, Lock, Preserve)
        {
            VER0,   8, 
            VER1,   8
        }

        OperationRegion (PM04, SystemMemory, M323, 0x04)
        Field (PM04, ByteAcc, Lock, Preserve)
        {
            CCI0,   8, 
            CCI1,   8, 
            CCI2,   8, 
            CCI3,   8
        }

        OperationRegion (PM05, SystemMemory, M324, 0x08)
        Field (PM05, ByteAcc, Lock, Preserve)
        {
            CTL0,   8, 
            CTL1,   8, 
            CTL2,   8, 
            CTL3,   8, 
            CTL4,   8, 
            CTL5,   8, 
            CTL6,   8, 
            CTL7,   8
        }

        OperationRegion (PM06, SystemMemory, M325, 0x10)
        Field (PM06, ByteAcc, Lock, Preserve)
        {
            MGI0,   8, 
            MGI1,   8, 
            MGI2,   8, 
            MGI3,   8, 
            MGI4,   8, 
            MGI5,   8, 
            MGI6,   8, 
            MGI7,   8, 
            MGI8,   8, 
            MGI9,   8, 
            MGIA,   8, 
            MGIB,   8, 
            MGIC,   8, 
            MGID,   8, 
            MGIE,   8, 
            MGIF,   8
        }

        OperationRegion (PM07, SystemMemory, M326, 0x10)
        Field (PM07, ByteAcc, Lock, Preserve)
        {
            MGO0,   8, 
            MGO1,   8, 
            MGO2,   8, 
            MGO3,   8, 
            MGO4,   8, 
            MGO5,   8, 
            MGO6,   8, 
            MGO7,   8, 
            MGO8,   8, 
            MGO9,   8, 
            MGOA,   8, 
            MGOB,   8, 
            MGOC,   8, 
            MGOD,   8, 
            MGOE,   8, 
            MGOF,   8
        }

        Device (UBTC)
        {
            Name (_HID, EisaId ("USBC000"))  // _HID: Hardware ID
            Name (_CID, EisaId ("PNP0CA0"))  // _CID: Compatible ID
            Name (_UID, Zero)  // _UID: Unique ID
            Name (_DDN, "USB Type C")  // _DDN: DOS Device Name
            Name (_ADR, Zero)  // _ADR: Address
            Name (M311, Buffer (0x14)
            {
                /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                /* 0010 */  0x00, 0x00, 0x00, 0x00                           // ....
            })
            Name (CRS, ResourceTemplate ()
            {
                Memory32Fixed (ReadWrite,
                    0x00000000,         // Address Base
                    0x00001000,         // Address Length
                    _Y2C)
            })
            Device (CR01)
            {
                Name (_ADR, Zero)  // _ADR: Address
                Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
                {
                    CreateDWordField (M311, Zero, M312)
                    CreateDWordField (M311, 0x04, M313)
                    CreateDWordField (M311, 0x08, M314)
                    CreateDWordField (M311, 0x0C, M315)
                    CreateDWordField (M311, 0x10, M316)
                    Local0 = M310 /* External reference */
                    If (Local0)
                    {
                        Local0 += 0x4E
                        M312 = M013 ((Local0 + Zero), Zero, Zero, 0x20)
                        M313 = M013 ((Local0 + 0x04), Zero, Zero, 0x20)
                        M314 = M013 ((Local0 + 0x08), Zero, Zero, 0x20)
                        M315 = M013 ((Local0 + 0x0C), Zero, Zero, 0x20)
                        M316 = M013 ((Local0 + 0x10), Zero, Zero, 0x20)
                    }

                    Return (M311) /* \_SB_.UBTC.M311 */
                }
            }

            Device (CR02)
            {
                Name (_ADR, One)  // _ADR: Address
                Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
                {
                    CreateDWordField (M311, Zero, M312)
                    CreateDWordField (M311, 0x04, M313)
                    CreateDWordField (M311, 0x08, M314)
                    CreateDWordField (M311, 0x0C, M315)
                    CreateDWordField (M311, 0x10, M316)
                    Local0 = M310 /* External reference */
                    If (Local0)
                    {
                        Local0 += 0x62
                        M312 = M013 ((Local0 + Zero), Zero, Zero, 0x20)
                        M313 = M013 ((Local0 + 0x04), Zero, Zero, 0x20)
                        M314 = M013 ((Local0 + 0x08), Zero, Zero, 0x20)
                        M315 = M013 ((Local0 + 0x0C), Zero, Zero, 0x20)
                        M316 = M013 ((Local0 + 0x10), Zero, Zero, 0x20)
                    }

                    Return (M311) /* \_SB_.UBTC.M311 */
                }
            }

            Method (_CRS, 0, Serialized)  // _CRS: Current Resource Settings
            {
                CreateDWordField (CRS, \_SB.UBTC._Y2C._BAS, M317)  // _BAS: Base Address
                Local0 = M310 /* External reference */
                M317 = (Local0 + 0x1D)
                Return (CRS) /* \_SB_.UBTC.CRS_ */
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((M049 (M128, 0x78) == One))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            OperationRegion (PM08, SystemMemory, M320, 0x30)
            Field (PM08, ByteAcc, Lock, Preserve)
            {
                VER0,   8, 
                VER1,   8, 
                RSV0,   8, 
                RSV1,   8, 
                CCI0,   8, 
                CCI1,   8, 
                CCI2,   8, 
                CCI3,   8, 
                CTL0,   8, 
                CTL1,   8, 
                CTL2,   8, 
                CTL3,   8, 
                CTL4,   8, 
                CTL5,   8, 
                CTL6,   8, 
                CTL7,   8, 
                MGI0,   8, 
                MGI1,   8, 
                MGI2,   8, 
                MGI3,   8, 
                MGI4,   8, 
                MGI5,   8, 
                MGI6,   8, 
                MGI7,   8, 
                MGI8,   8, 
                MGI9,   8, 
                MGIA,   8, 
                MGIB,   8, 
                MGIC,   8, 
                MGID,   8, 
                MGIE,   8, 
                MGIF,   8, 
                MGO0,   8, 
                MGO1,   8, 
                MGO2,   8, 
                MGO3,   8, 
                MGO4,   8, 
                MGO5,   8, 
                MGO6,   8, 
                MGO7,   8, 
                MGO8,   8, 
                MGO9,   8, 
                MGOA,   8, 
                MGOB,   8, 
                MGOC,   8, 
                MGOD,   8, 
                MGOE,   8, 
                MGOF,   8
            }

            Method (M318, 0, Serialized)
            {
                \_SB.UBTC.MGI0 = \_SB.MGI0
                \_SB.UBTC.MGI1 = \_SB.MGI1
                \_SB.UBTC.MGI2 = \_SB.MGI2
                \_SB.UBTC.MGI3 = \_SB.MGI3
                \_SB.UBTC.MGI4 = \_SB.MGI4
                \_SB.UBTC.MGI5 = \_SB.MGI5
                \_SB.UBTC.MGI6 = \_SB.MGI6
                \_SB.UBTC.MGI7 = \_SB.MGI7
                \_SB.UBTC.MGI8 = \_SB.MGI8
                \_SB.UBTC.MGI9 = \_SB.MGI9
                \_SB.UBTC.MGIA = \_SB.MGIA
                \_SB.UBTC.MGIB = \_SB.MGIB
                \_SB.UBTC.MGIC = \_SB.MGIC
                \_SB.UBTC.MGID = \_SB.MGID
                \_SB.UBTC.MGIE = \_SB.MGIE
                \_SB.UBTC.MGIF = \_SB.MGIF
                \_SB.UBTC.CCI0 = \_SB.CCI0
                \_SB.UBTC.CCI1 = \_SB.CCI1
                \_SB.UBTC.CCI2 = \_SB.CCI2
                \_SB.UBTC.CCI3 = \_SB.CCI3
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If ((Arg0 == ToUUID ("6f8398c2-7ca4-11e4-ad36-631042b5008f") /* Unknown UUID */))
                {
                    If ((ToInteger (Arg2) == Zero))
                    {
                        Return (Buffer (One)
                        {
                             0x0F                                             // .
                        })
                    }
                    ElseIf ((ToInteger (Arg2) == One))
                    {
                        M000 (0x0DA8)
                        \_SB.MGO0 = \_SB.UBTC.MGO0
                        \_SB.MGO1 = \_SB.UBTC.MGO1
                        \_SB.MGO2 = \_SB.UBTC.MGO2
                        \_SB.MGO3 = \_SB.UBTC.MGO3
                        \_SB.MGO4 = \_SB.UBTC.MGO4
                        \_SB.MGO5 = \_SB.UBTC.MGO5
                        \_SB.MGO6 = \_SB.UBTC.MGO6
                        \_SB.MGO7 = \_SB.UBTC.MGO7
                        \_SB.MGO8 = \_SB.UBTC.MGO8
                        \_SB.MGO9 = \_SB.UBTC.MGO9
                        \_SB.MGOA = \_SB.UBTC.MGOA
                        \_SB.MGOB = \_SB.UBTC.MGOB
                        \_SB.MGOC = \_SB.UBTC.MGOC
                        \_SB.MGOD = \_SB.UBTC.MGOD
                        \_SB.MGOE = \_SB.UBTC.MGOE
                        \_SB.MGOF = \_SB.UBTC.MGOF
                        \_SB.CTL0 = \_SB.UBTC.CTL0
                        \_SB.CTL1 = \_SB.UBTC.CTL1
                        \_SB.CTL2 = \_SB.UBTC.CTL2
                        \_SB.CTL3 = \_SB.UBTC.CTL3
                        \_SB.CTL4 = \_SB.UBTC.CTL4
                        \_SB.CTL5 = \_SB.UBTC.CTL5
                        \_SB.CTL6 = \_SB.UBTC.CTL6
                        \_SB.CTL7 = \_SB.UBTC.CTL7
                        \_SB.CCI0 = Zero
                        \_SB.CCI1 = Zero
                        \_SB.CCI2 = Zero
                        \_SB.CCI3 = Zero
                        \_SB.PCI0.SBRG.SEC1 (0x18)
                        M000 (0x0DA9)
                    }
                    ElseIf ((ToInteger (Arg2) == 0x02))
                    {
                        M000 (0x0DAA)
                        M318 ()
                        M000 (0x0DAB)
                    }
                }

                Return (Zero)
            }
        }
    }
}

