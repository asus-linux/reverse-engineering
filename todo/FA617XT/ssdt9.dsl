/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20220331 (64-bit version)
 * Copyright (c) 2000 - 2022 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt9.dat, Wed Nov 22 15:56:19 2023
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000106A (4202)
 *     Revision         0x02
 *     Checksum         0x6F
 *     OEM ID           "AMD"
 *     OEM Table ID     "CPMSMMUX"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20200717 (538969879)
 */
DefinitionBlock ("", "SSDT", 2, "AMD", "CPMSMMUX", 0x00000001)
{
    External (_SB_.PCI0, UnknownObj)
    External (_SB_.PCI0.GP17.VGA_, DeviceObj)
    External (_SB_.PCI0.GP17.VGA_.M460, UnknownObj)
    External (_SB_.PCI0.GPP0.SWUS.SWDS.VGA_, DeviceObj)
    External (M037, DeviceObj)
    External (M045, MethodObj)    // 0 Arguments
    External (M046, IntObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M111, MethodObj)    // 2 Arguments
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M23A, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M29A, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M31C, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M404, BuffObj)
    External (M408, MutexObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)
    External (M453, FieldUnitObj)
    External (M454, FieldUnitObj)
    External (M455, FieldUnitObj)
    External (M456, FieldUnitObj)
    External (M457, FieldUnitObj)
    External (M460, MethodObj)    // 7 Arguments
    External (M470, MethodObj)    // 2 Arguments
    External (M4C0, FieldUnitObj)
    External (M4F0, FieldUnitObj)
    External (M610, FieldUnitObj)
    External (M620, FieldUnitObj)
    External (M631, FieldUnitObj)
    External (M652, FieldUnitObj)

    Scope (\_SB)
    {
        Device (MUX1)
        {
            Name (_HID, "MSFT0005")  // _HID: Hardware ID
            Name (_UID, Zero)  // _UID: Unique ID
            Name (M30C, 0x12)
            Name (M30D, 0x01)
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((((M049 (M133, 0x16) >> 0x03) & One) == One))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            Method (AMQU, 1, Serialized)
            {
                M460 ("  FEA-ASL-\\_SB.MUX1.DMQU (0x%X)\n", ToInteger (Arg0), Zero, Zero, Zero, Zero, Zero)
                Switch (ToInteger (Arg0))
                {
                    Case (One)
                    {
                        Local0 = M470 (M30C, M30D)
                        If ((Local0 == Zero))
                        {
                            M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GP17.VGA.EDP1")
                        }
                        Else
                        {
                            M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                        }
                    }
                    Case (0x02)
                    {
                        Local0 = M049 (M133, 0x0207)
                        M460 ("    Return (%d)\n", Local0, Zero, Zero, Zero, Zero, Zero)
                        Return (Local0)
                    }
                    Case (0x03)
                    {
                        Local0 = M049 (M133, 0x0208)
                        If ((Local0 == Zero))
                        {
                            M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GP17.VGA.EDP1")
                        }
                        Else
                        {
                            M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                        }
                    }
                    Case (0x04)
                    {
                        Local0 = M049 (M133, 0x0208)
                        If ((Local0 == Zero))
                        {
                            M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                        }
                        Else
                        {
                            M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                            Return ("_SB.PCI0.GP17.VGA.EDP1")
                        }
                    }
                    Default
                    {
                        M460 ("    Return ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("")
                    }

                }
            }

            Method (AMCF, 1, Serialized)
            {
                Local0 = SizeOf (Arg0)
                M460 ("  FEA-ASL-\\_SB.MUX1.DMCF (%S) String Length = %d\n", Arg0, Local0, Zero, Zero, Zero, Zero)
                If ((Local0 > 0x1C))
                {
                    M460 ("    Mux switch to dGPU\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M111 (M30C, M30D)
                    M460 ("    Return (0)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }
                Else
                {
                    M460 ("    Mux switch to iGPU\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    M111 (M30C, ((M30D ^ One) & One))
                    M460 ("    Return (0)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
                    Return (Zero)
                }

                M460 ("    Return (2)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
                Return (0x02)
            }
        }
    }

    Scope (\_SB.PCI0.GP17.VGA)
    {
        Method (_DEP, 0, Serialized)  // _DEP: Dependencies
        {
            If ((M046 == 0xAA))
            {
                Local0 = M045 ()
            }

            If ((((M049 (M133, 0x16) & 0x20) == 0x20) && (CondRefOf (
                \_SB.MUX1) && (M046 >= 0x0B))))
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GP17.VGA._DEP () Return (Package (1) {\\_SB.MUX1})\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return (Package (0x01)
                {
                    \_SB.MUX1
                })
            }
            Else
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GP17.VGA._DEP () Return (Package (1) {\\_SB.PCI0})\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return (Package (0x01)
                {
                    \_SB.PCI0
                })
            }
        }

        Device (EDP1)
        {
            Name (_ADR, 0x0400)  // _ADR: Address
            Method (DMID, 0, Serialized)
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GP17.VGA.EDP1.DMID () Return (\\_SB.MUX1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return ("_SB.MUX1")
            }
        }
    }

    Scope (\_SB.PCI0.GPP0.SWUS.SWDS.VGA)
    {
        Method (_DEP, 0, Serialized)  // _DEP: Dependencies
        {
            If ((M046 == 0xAA))
            {
                Local0 = M045 ()
            }

            If ((((M049 (M133, 0x16) & 0x20) == 0x20) && (CondRefOf (
                \_SB.MUX1) && (M046 >= 0x0B))))
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GPP0.SWUS.SWDS.VGA._DEP () Return (Package (1) {\\_SB.MUX1})\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return (Package (0x01)
                {
                    \_SB.MUX1
                })
            }
            Else
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GPP0.SWUS.SWDS.VGA._DEP () Return (Package (1) {\\_SB.PCI0})\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return (Package (0x01)
                {
                    \_SB.PCI0
                })
            }
        }

        Name (M30E, 0x12)
        Name (M30F, 0x01)
        Method (DMQU, 1, Serialized)
        {
            M460 ("  FEA-ASL-\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.DMQU (0x%X)\n", ToInteger (Arg0), Zero, Zero, Zero, Zero, Zero)
            Switch (ToInteger (Arg0))
            {
                Case (One)
                {
                    Local0 = M470 (M30E, M30F)
                    If ((Local0 == Zero))
                    {
                        M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GP17.VGA.EDP1")
                    }
                    Else
                    {
                        M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                    }
                }
                Case (0x02)
                {
                    Local0 = M049 (M133, 0x0207)
                    M460 ("    Return (%d)\n", Local0, Zero, Zero, Zero, Zero, Zero)
                    Return (Local0)
                }
                Case (0x03)
                {
                    Local0 = M049 (M133, 0x0208)
                    If ((Local0 == Zero))
                    {
                        M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GP17.VGA.EDP1")
                    }
                    Else
                    {
                        M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                    }
                }
                Case (0x04)
                {
                    Local0 = M049 (M133, 0x0208)
                    If ((Local0 == Zero))
                    {
                        M460 ("    Return (\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2")
                    }
                    Else
                    {
                        M460 ("    Return (\\_SB.PCI0.GP17.VGA.EDP1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                        Return ("_SB.PCI0.GP17.VGA.EDP1")
                    }
                }
                Default
                {
                    M460 ("    Return ()\n", Zero, Zero, Zero, Zero, Zero, Zero)
                    Return ("")
                }

            }
        }

        Method (DMCF, 1, Serialized)
        {
            Local0 = SizeOf (Arg0)
            M460 ("  FEA-ASL-\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.DMCF (%S) String Length = %d\n", Arg0, Local0, Zero, Zero, Zero, Zero)
            If ((Local0 > 0x1C))
            {
                M460 ("    Mux switch to dGPU\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M111 (M30E, M30F)
                M460 ("    Return (0)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
                Return (Zero)
            }
            Else
            {
                M460 ("    Mux switch to iGPU\n", Zero, Zero, Zero, Zero, Zero, Zero)
                M111 (M30E, ((M30F ^ One) & One))
                M460 ("    Return (0)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
                Return (Zero)
            }

            M460 ("    Return (2)\n", Arg0, Zero, Zero, Zero, Zero, Zero)
            Return (0x02)
        }

        Device (EDP2)
        {
            Name (_ADR, 0x0400)  // _ADR: Address
            Method (DMID, 0, Serialized)
            {
                M460 ("  FEA-ASL-\\_SB.PCI0.GPP0.SWUS.SWDS.VGA.EDP2.DMID () Return (\\_SB.MUX1)\n", Zero, Zero, Zero, Zero, Zero, Zero)
                Return ("_SB.MUX1")
            }
        }
    }
}

