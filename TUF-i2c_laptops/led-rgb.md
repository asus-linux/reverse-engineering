Multicolour!

Function to register on INIT = led_classdev_multicolor_register_ext
hid-playstation.c is a great example
leds-turris-omnia.c

```c
struct led_classdev_mc
struct mc_subled

struct rgb_led {
	struct led_classdev_mc mc_cdev;
	struct mc_subled subled_info[3];
	int reg;
};
```
Need to set index like:
```c
mc_led_info[0].color_index = LED_COLOR_ID_RED;
mc_led_info[1].color_index = LED_COLOR_ID_GREEN;
mc_led_info[2].color_index = LED_COLOR_ID_BLUE;
```
The way it gets set is with:
```
led_cdev->brightness_set_blocking = brightness_set;
```
for example:
```
ret = ps_lightbar_register(ps_dev, &ds->lightbar, dualsense_lightbar_set_brightness);
```
where `dualsense_lightbar_set_brightness` is a function pointer passed
to method `ps_lightbar_register` which sets up all the deets.

The asus bit is going to need to store states now..
If the user plugs RGB in to multicolour they would expect the
LEDS to change, and the current mode/speed/direction to stay.

Multizone support should become easier?

The definitions:
```c
struct mc_subled {
	unsigned int color_index;
	unsigned int brightness;
	unsigned int intensity;
	unsigned int channel;
};

struct led_classdev_mc {
	/* led class device */
	struct led_classdev led_cdev;
	unsigned int num_colors;

	struct mc_subled *subled_info;
};
```

https://docs.kernel.org/leds/leds-class.html
What is https://lkml.org/lkml/2022/2/11/992 ?
Some discussions https://lkml.org/lkml/2019/1/5/191

----------------------------------------------------------------------

LED control for TUF

The first method in ACPI is 0x00100056 for cmd, mode, R,G,B, speed
Two 32bit Args are used:
```
U32 1:
      0xFF = Command
    0xFF00 = Mode
  0xFF0000 = Red
0xFF000000 = Green
U32 2:
      0xFF = Blue
    0xFF00 = Speed
```

Set/apply, Mode, 0xRRGGBB, Speed
---------------------------------
As a buffer? 1 1 0xffffff 2
Named: tuf_keyboard_rgb

The second method is 0x00100057 for power control:
One U32 is used:
```
    0xFF = Command
  0xFF00 = Power flags
0x00FF0000 = Save
```

Power flags are:
```
0000, 0010 = 0x02 Boot
0000, 1000 = 0x08 Awake
0010, 0000 = 0x20 Sleep
1000, 0000 = 0x80 Keyboard? 
```
As not all bytes are used, is there anything like a lightbar?

Both of these will return a 1 if the interface is supported.

The entire range of TUF seems not to have changed the feature-set on these keyboards

```
If ((IIA0 == 0x00100056))
{
/** FIRST ARG **********************************************************/
    Local0 = (IIA1 & 0xFF)   // COMMAND: 0xB3 or 0xB4
    Local1 = (IIA1 & 0xFF00) // MODE
    Local1 >>= 0x08           
    Local2 = (IIA1 & 0x00FF0000) // RED
    Local2 >>= 0x10
    Local3 = (IIA1 & 0xFF000000) // GREEN
    Local3 >>= 0x18
/** SECOND ARG ********************************************************/
    Local4 = (IIA2 & 0xFF)      // BLUE
    Local5 = (IIA2 & 0xFF00)    // SPEED
    Local5 >>= 0x08
    If ((Local0 == 0xB3)) // SET (different to USB which is B5 to set
    {
        // SNIP
    }
    ElseIf ((Local0 == 0xB4)) // APPLY (save)
    {
       // SNIP
/** SAVE THE SETTINGS **********************************************/
        KBBE = Local1  // MODE
        KBBR = Local2  // RED
        KBBG = Local3  // GREEN
        KBBB = Local4  // BLUE
        KBBS = Local5  // SPEED
        KBE1 = One
    }
    Else
    {
        Return (Zero) // If the command byte is incorrect
    }

    Return (One)
}

// This one has three bytes involved
// command, flags, save
If ((IIA0 == 0x00100057))
{
    Local0 = (IIA1 & 0xFF) // Command, should be 0xBD, same as for USB keyboards power setting
    Local1 = (IIA1 & 0xFF00)  // FLAGS ?? 0x02 = boot, 0x08 = awake, 0x20 = sleep 0x80 = keyboard?
    Local1 >>= 0x08
    Local2 = (IIA1 & 0x00FF0000)  // SAVE ??
    Local2 >>= 0x10
    If ((Local0 == 0xBD))
    {
        // SNIP
        If ((Local1 == One))
        {
            KBST = Local2
            KBE2 = One
        }
    }
    Else
    {
        Return (Zero) // If the command byte is incorrect
    }

    Return (One)
}
```
