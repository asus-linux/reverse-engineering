#!/bin/bash
#####################################################################
# Author: Evan Sklarski https://github.com/esklarski
#
# Version 0.1
#
# This script will pull the acpi table from your machine and decode
# it for sharing and reading. It will create a folder containing all
# the data.
#
# You will be asked for your sudo password, it is necessary for
# access to copy the tables, finding the Bios device name, and to
# transfer ownership of copied files to the current user.
#
#    Created Structure:
#
#       acpi-dump
#         -> {deviceBiosVersion}
#           -> all .dsl files generated
#           -> raw
#             -> {deviceBiosVersion}.tar.gz
#
#
# I have tried to avoid keeping sensitive information:
#   -- MSDM containing any Windows license key is deleted
#      so it cannot be shared publicly accidentaly. 
#
# I have commented out the lines that also decode the files as this
# is error prone and different machines need different commands.
# The provided code should give some hints.
#####################################################################

#Get device bios version for naming files.
deviceName=$(sudo dmidecode -s bios-version)

echo "Found: $deviceName"
echo "Executing pull..."

#Make directory for output.
mkdir acpi-dump && cd acpi-dump
#mkdir $deviceName && cd $deviceName

#Copy acpi table information from system
sudo cp -R /sys/firmware/acpi/tables/ ./

#Transfer ownership to current user and rename
whoami | xargs -I{} sudo chown -R {} tables
chmod -R 750 tables
mv tables $deviceName

cd $deviceName

#Remove MSDM
rm MSDM

#Delete folders, we don't need them.
find . -mindepth 1 -maxdepth 1 -type d | xargs -I{} rm -r {}

#Decoding
echo "Decoding"

## main DSDT and external entries
iasl -e SSDT* -d DSDT
mv DSDT.dsl ../DSDT-$deviceName.dsl

##Decode the rest.
#for fileName in $(ls)
#do
#	if [[ "$fileName" == "DSDT" ]]
#	then
#		echo "skipping DSDT"
#	else
#		iasl -e *.dsl -d $fileName
#		mv $fileName.dsl ../$fileName.dsl
#	fi
#done

#tar raw contents
#tar -cf $deviceName.tar.gz --gzip --remove-files *

echo "Done!"

