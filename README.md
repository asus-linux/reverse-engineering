# ACPI Dump
This precedure is useful to dump a machine ACPI interface.

## Required software
On archlinux install these (for other distros names might vary):

```sh
sudo pacman -S acpica iasl
```

## Dump the ACPI

First of all identify the board name and the bios version:

```sh
cat /sys/devices/virtual/dmi/id/board_name
cat /sys/devices/virtual/dmi/id/bios_version
```

```sh
mkdir board_name_bios_version
cd board_name_bios_version
sudo acpidump > acpi_tables.txt 
acpixtract -a acpi_tables.txt
iasl -d *.dat
mkdir disassembly
mv *.dsl disassembly/
mkdir hex
mv *.dat hex
```

## Compare dumps
If you have gathered more than one dump from the same device (after a bios update for example) do the following to compare them:

```sh
diff -q directory-1/ directory-2/
```
