- A = The getter only returns whether the method is supported
- G = Get value + is-supported
- S = Set value
- I = Implemented in kernel
- A = Ally
- R = ROG laptops
- T = TUF laptops
- X = Tablets 2/1
- (N) = Appended to ARX, not supported

| Function name                   |G|S| WMI Hex    | K | Device   | Notes                          |
|---------------------------------|-|-|------------|---|----------|--------------------------------|
| TUF RGB keyboard version 1      |G|S| 0x00100056 | I | T        |                                |
| TUF RGB keyboard version 2      |G|S| 0x0010005A | I | T        | kernel patch submitted upstream 2024-03-11 |
| TUF RGB keyboard power states   |G|S| 0x00100057 | I | T        |                                |
| Auto/Manual fan control         |G| | 0x00110011 | I | R        |                                |
| Auto/Manual fan control         | | | 0x00110012 | I |          | deprecated                     |
| CPU auto/Manual fan control     |G|S| 0x00110013 | I | ARX      |                                |
| GPU auto/Manual fan control     |G|S| 0x00110014 | I | ARX      |                                |
| Mid auto/Manual fan control     |G|S| 0x00110031 | I | ARX      |                                |
| CPU fan curve settings          |G|S| 0x00110024 | I | ARX      |                                |
| GPU fan curve settings          |G|S| 0x00110025 | I | ARX      |                                |
| Mid fan curve settings          |G|S| 0x00110032 | I | ARX      |                                |
| GetFanCoverStatus               |G| | 0x0011001D |   | X        |                                |
| GetBBYThermalPolicy             |G|S| 0x0012005B |   | A        |                                |
| GetPDStatus                     |G| | 0x0012006C | I | ARX      |                                |
| GetThermalPolicy                |G|S| 0x00120075 | I | ARX      |                                |
| GetVCoreOffset                  | | | 0x00120076 |   | R(N)X(N) | How it works is unknown        |
| GetVCoreLimit                   | | | 0x00120077 |   | R(N)X(N) | How it works is unknown        |
| GetBIOSWhisperMode              |A|S| 0x0012007A |   | RX       | Some ACPI return "always on"   |
| Set/GetBIOSPerCore              |G|S| 0x0012007B |   | R        | G634, always returns 0xFFFFFFFE |
| Set/GetOCRuleController         |G|S| 0x0012007D |   | R        | G634, always returns 0xFFFFFFFE |
| GetBootStatus                   |G|S| 0x00120095 |   | RX       | Only on G14, G713P              |
| GetAITweakerStatus              |G| | 0x00120096 |   | R(N)X(N) |                                |
| PPT_PL2_SPPT                    |A|S| 0x001200A0 | I | ARX      |                                |
| EDCV                            |A|S| 0x001200A1 |   | ARX      | Maybe implement? Risky         |
| TDCV                            |A|S| 0x001200A2 |   | ARX      | Maybe implement? Risky         |
| PPT_PL1_SPL                     |A|S| 0x001200A3 | I | ARX      |                                |
| APU_SPPT                        |A|S| 0x001200B0 | I | ARX      |                                |
| PPT_PLAT_SPPT                   |A|S| 0x001200B1 | I | ARX      |                                |
| NV_DYN_BOOST                    |A|S| 0x001200C0 | I | ARX      | Ally shouldn't support (bug)   |
| PPT_FPPT                        |A|S| 0x001200C1 | I | ARX      |                                |
| NV_THERM_TARGET                 |A|S| 0x001200C2 | I | ARX      |                                |
| GetPowerMaxCore                 | |S| 0x001200C3 |   | X        | GZ301Z setter. Incomplete?     |
| GetBIOSPerCoreMax               | | | 0x001200D1 | / | R        | G634, always returns 0xFFFFFFFE |
| GetBIOSEnabledCores             |G|S| 0x001200D2 |   | R        |  |
| GetBIOSPerCoreMax               |G|S| 0x001200D3 |   | R        |  |
| GetExtremePowerSaving           | | | 0x001200E1 |   |          | Don't know what it does        |
| GetMCUPowerSaving               |G|S| 0x001200E2 | I | A        |                                |
| SupportBoostMode                |G| | 0x001200E4 | I | A        |                                |
| SupportBoostMode                |G|S| 0x001200EB |   | -        |                                |
| Screenpad Off                   |G|S| 0x00050031 | I | R        |                                |
| Screenpad brightness (turn on)  |G|S| 0x00050032 | I | R        |                                |
| Mini LED                        |G|S| 0x0005001E | I | R        |                                |
| TUF RGB keyboard brightness     |G|S| 0x00050021 | I | T        |                                |
| Mini LED ??                     |G|S| 0x0005002E | I | R        | kernel patch submitted upstream 2024-03-11 |
| Tablet State                    |G|?| 0x00060077 | I | AX       | Ally has same methods and function as X13 |
| GetGPUModeStatus (MUX)          |G|S| 0x00090016 | I | RX       | Get/Set the display MUX        |
| DockingStatus    (EGPU)         |G| | 0x00090017 |   | ARX      | only AR support (EGPU)         |
| SwitchLockStatus (EGPU)         |G| | 0x00090018 | I | ARX      | only AR support (EGPU)         |
| GPUModeStatus    (EGPU)         |G|S| 0x00090019 | I | ARX      | only AR support (EGPU control) |
| DGPUMode                        |G|S| 0x00090020 | I | ARX      | enable/disable                 |
| SupportEGPUFlag                 |A| | 0x0009001C |   | ARX      | only supported by AX           |
| SetBIOSDynamicWhisperMode       |A|S| 0x00090022 |   | R        | GU603                          |
| IsBiosSupportDynamicWhisperMode |G|S| 0x00090023 |   | R        | GU603                          |
| Boot Sound                      |G|S| 0x00130022 | I | ARX      | kernel patch submitted upstream 2024-03-11 |
| SetCPUSettingsInThermal         |G|S| args?      |   |          |                                |
| GetFanSpeed                     |G|S| args?      |   |          |                                |
			
Tablet modes:
0. = notebook
1. = tablet
2. = tent
3. = rotated

- 0x0011 = fan controls?
- 0x0012 = power controls?
- 0x0009 = gpu controls?

Both Notebook/Machine is Gen4, notify DEVS ID 0x0006005B value 0x3 then reboot \r\n ??

Ally issue:
```
8642:                    If ((IIA0 == 0x001200C0))
8643-                    {
8644-                        Return (0x00010000)
8645-                        Return (Zero)
??????????????????????????????????????????????????
```

Need kernel patches for:
- 0x00090017
- 0x0009001C
- 0x001200D2
- 0x001200D3

