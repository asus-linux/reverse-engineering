HID Report
    Usage Page (Lighting and Illumination)
        Header
            .... ..10 = bSize: 2 bytes (2)
            .... 01.. = bType: Global (1)
            0000 .... = bTag: Usage Page (0x0)
        Usage Page: Lighting and Illumination (0x59)
    Usage (LampArray)
        Header
            .... ..01 = bSize: 1 byte (1)
            .... 10.. = bType: Local (2)
            0000 .... = bTag: Usage (0x0)
        Usage: LampArray (0x01)
    Collection (Application)
        Header
            .... ..01 = bSize: 1 byte (1)
            .... 00.. = bType: Main (0)
            1010 .... = bTag: Collection (0xa)
        Collection type: Application (0x01)
        Report ID (0x01)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x01
        Usage (LampArrayAttributesReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampArrayAttributesReport (0x02)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (LampCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampCount (0x03)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (65535)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 65535
            Report Size (16)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 16
            Report Count (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 1
            Feature (Const,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 1 = Data/constant: Constant
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (BoundingBoxWidthInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BoundingBoxWidthInMicrometers (0x04)
            Usage (BoundingBoxHeightInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BoundingBoxHeightInMicrometers (0x05)
            Usage (BoundingBoxDepthInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BoundingBoxDepthInMicrometers (0x06)
            Usage (LampArrayKind)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampArrayKind (0x07)
            Usage (MinUpdateIntervalInMicroseconds)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: MinUpdateIntervalInMicroseconds (0x08)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (2147483647)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 2147483647
            Report Size (32)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 32
            Report Count (5)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 5
            Feature (Const,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 1 = Data/constant: Constant
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        Report ID (0x02)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x02
        Usage (LampAttributesRequestReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampAttributesRequestReport (0x20)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (LampId)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampId (0x21)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (65535)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 65535
            Report Size (16)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 16
            Report Count (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 1
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        Report ID (0x03)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x03
        Usage (LampAttributesResponseReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampAttributesResponseReport (0x22)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (LampId)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampId (0x21)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (65535)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 65535
            Report Size (16)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 16
            Report Count (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 1
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (PositionXInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: PositionXInMicrometers (0x23)
            Usage (PositionYInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: PositionYInMicrometers (0x24)
            Usage (PositionZInMicrometers)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: PositionZInMicrometers (0x25)
            Usage (UpdateLatencyInMicroseconds)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: UpdateLatencyInMicroseconds (0x27)
            Usage (LampPurposes)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampPurposes (0x26)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (2147483647)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 2147483647
            Report Size (32)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 32
            Report Count (5)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 5
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (RedLevelCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedLevelCount (0x28)
            Usage (GreenLevelCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenLevelCount (0x29)
            Usage (BlueLevelCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueLevelCount (0x2a)
            Usage (IntensityLevelCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityLevelCount (0x2b)
            Usage (IsProgrammable)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IsProgrammable (0x2c)
            Usage (InputBinding)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: InputBinding (0x2d)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (255)
                Header
                    .... ..10 = bSize: 2 bytes (2)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 255
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (6)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 6
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        Report ID (0x04)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x04
        Usage (LampMultiUpdateReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampMultiUpdateReport (0x50)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (LampCount)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampCount (0x03)
            Usage (LampUpdateFlags)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampUpdateFlags (0x55)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 8
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (2)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 2
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (LampId)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampId (0x21)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (65535)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 65535
            Report Size (16)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 16
            Report Count (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 8
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (255)
                Header
                    .... ..10 = bSize: 2 bytes (2)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 255
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (32)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 32
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        Report ID (0x05)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x05
        Usage (LampRangeUpdateReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampRangeUpdateReport (0x60)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (LampUpdateFlags)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampUpdateFlags (0x55)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 8
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 1
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (LampIdStart)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampIdStart (0x61)
            Usage (LampIdEnd)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: LampIdEnd (0x62)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (65535)
                Header
                    .... ..11 = bSize: 4 bytes (3)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 65535
            Report Size (16)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 16
            Report Count (2)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 2
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            Usage (RedUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: RedUpdateChannel (0x51)
            Usage (GreenUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: GreenUpdateChannel (0x52)
            Usage (BlueUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: BlueUpdateChannel (0x53)
            Usage (IntensityUpdateChannel)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: IntensityUpdateChannel (0x54)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (255)
                Header
                    .... ..10 = bSize: 2 bytes (2)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 255
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (4)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 4
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        Report ID (0x06)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 01.. = bType: Global (1)
                1000 .... = bTag: Report ID (0x8)
            Report ID: 0x06
        Usage (LampArrayControlReport)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 10.. = bType: Local (2)
                0000 .... = bTag: Usage (0x0)
            Usage: LampArrayControlReport (0x70)
        Collection (Logical)
            Header
                .... ..01 = bSize: 1 byte (1)
                .... 00.. = bType: Main (0)
                1010 .... = bTag: Collection (0xa)
            Collection type: Logical (0x02)
            Usage (AutonomousMode)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 10.. = bType: Local (2)
                    0000 .... = bTag: Usage (0x0)
                Usage: AutonomousMode (0x71)
            Logical Minimum (0)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0001 .... = bTag: Logical Minimum (0x1)
                Logical minimum: 0
            Logical Maximum (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0010 .... = bTag: Logical Maximum (0x2)
                Logical maximum: 1
            Report Size (8)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    0111 .... = bTag: Report Size (0x7)
                Report size: 8
            Report Count (1)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 01.. = bType: Global (1)
                    1001 .... = bTag: Report Count (0x9)
                Report count: 1
            Feature (Data,Var,Abs)
                Header
                    .... ..01 = bSize: 1 byte (1)
                    .... 00.. = bType: Main (0)
                    1011 .... = bTag: Feature (0xb)
                .... .... 0 = Data/constant: Data
                .... ...1 . = Data type: Variable
                .... ..0. . = Coordinates: Absolute
                .... .0.. . = Min/max wraparound: No Wrap
                .... 0... . = Physical relationship to data: Linear
                ...0 .... . = Preferred state: Preferred State
                ..0. .... . = Has null position: No Null position
                .0.. .... . = (Non)-volatile: Non Volatile
                0... .... . = Bits or bytes: Buffered bytes (default, no second byte present)
            End Collection
                Header
                    .... ..00 = bSize: 0 bytes (0)
                    .... 00.. = bType: Main (0)
                    1100 .... = bTag: End Collection (0xc)
        End Collection
            Header
                .... ..00 = bSize: 0 bytes (0)
                .... 00.. = bType: Main (0)
                1100 .... = bTag: End Collection (0xc)

