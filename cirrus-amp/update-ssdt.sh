#!/bin/sh
iasl -tc g634j.dsl
mkdir -p kernel/firmware/acpi
cp g634j.aml kernel/firmware/acpi
find kernel | cpio -H newc --create > patched_cirrus_acpi.cpio
sudo cp patched_cirrus_acpi.cpio /boot/patched_cirrus_acpi.cpio
